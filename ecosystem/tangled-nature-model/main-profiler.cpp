#include <iostream>
#include <fstream>
#include <ctime>
#include "classTangledNature.cpp"
#include "classTNInvasionSystem.cpp"
#include "mtrand64.cpp"

#define BENCH
//#define STEADY
#define PERIODIC
//#define OUTPOP

int main (int argc, char* argv[]) {
  if( argc != 2) { std::cerr << "Input the filename." << std::endl; exit(1);}
  //check the validity of given parameters
//   if( argc != 17) {
//     std::cerr << "Input arguments" << std::endl
// 	      << "program <fecundity> <resource> <b_max> <eta_max> <eta_prob> <mij_max> <mij_prob> <mii_max> "
// 	      << "<x_initial> <threshold> <dt> <percentage> <t_end> <max_species> <seed> <ID>" << std::endl;
//     throw 1;
//   }
  double fecundity = 2.0;
  double resource = 2000.0;
  double b_max = 1.0;
  double eta_max = 1.0;
  double eta_prob = 0.05;
  double mij_max = 1.0;
  double mij_prob = 0.1;
  double mii_max = 1.0;
  double x_initial = 1.0;
  double threshold = 0.5;
  double dt = 0.01;
  double percentage = 0.01;
  long t_end = 4000;
  int max_species = 100;
  unsigned long long seed = 892341;
  char* id = argv[1];

  if( fecundity <= 1.0 || resource <= 0.0 || b_max <= 0.0 || eta_max <= 0.0 ||
      eta_prob <= 0.0 || eta_prob >= 1.0 || mij_max <= 0.0 || mij_prob <= 0.0 ||
      mij_prob >= 1.0 || mii_max <= 0.0 || x_initial <= 0.0 || threshold <= 0.0 ||
      dt <= 0.0 || percentage <= 0.0 || t_end <= 0 || max_species <= 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    std::cerr << "Lists of given parameters are as follows:" << std::endl
	      << "fecundity:\t" << fecundity << std::endl
	      << "resource:\t" << resource << std::endl
	      << "b_max:\t" << b_max << std::endl
	      << "eta_max:\t" << eta_max << std::endl
	      << "eta_prob:\t" << eta_prob << std::endl
	      << "mij_max:\t" << mij_max << std::endl
	      << "mij_prob:\t" << mij_prob << std::endl
	      << "mii_max:\t" << mii_max << std::endl
	      << "x_initial:\t" << x_initial << std::endl
	      << "threshold:\t" << threshold << std::endl
	      << "dt:\t" << dt << std::endl
	      << "percentage:\t" << percentage << std::endl
	      << "t_end:\t" << t_end << std::endl
	      << "max_species:\t" << max_species << std::endl
	      << "seed:\t" << seed << std::endl
	      << "id:\t" << id << std::endl;
    throw 1;
  }

  //ofstreams
  char suvfilename[20];
  sprintf(suvfilename,"%s.suv",id);
  std::ofstream suvout(suvfilename);

  char divfilename[20];
  sprintf(divfilename,"%s.diversity",id);
  std::ofstream divout(divfilename);

  char jumyofilename[20];
  sprintf( jumyofilename, "%s.jumyo",id);

  char bmassfilename[20];
  sprintf(bmassfilename,"%s.biomass",id);
  std::ofstream bmassout(bmassfilename);

  
  classTNInvasionSystem eco( max_species, percentage, dt,
			     fecundity, resource, threshold,
			     jumyofilename,
			     b_max, eta_max, eta_prob,
			     mij_max, mij_prob, mii_max,
			     x_initial, seed);

  int output_species = 125; //100種になったらa,yを出力

#ifdef OUTPOP
  std::ofstream popout("population.dat");
#endif

#ifdef BENCH
  clock_t start = clock();
#endif

  //============== main loop ========================
  for( long times_invasion = 0; times_invasion <t_end; times_invasion++) {
    //======== eco dynamics =========
    try{
      eco.AddInvador();
    } catch( int i) {
      std::cerr << "Number of species exceeds max_species" << std::endl;
      break;
    }

#ifdef STEADY
    eco.UpdateDynamicsStable();
#endif
#ifdef PERIODIC
    long loop = (long) (1.0 / dt);
    eco.UpdateDynamicsTime(loop);
#endif
#ifdef OUTPOP
    //    if( times_invasion < 400 ) eco.UpdateDynamicsStable();
    //    else {
    for( int t=0; t < 10; t++) {
      eco.Xoutput( popout);
	//eco.UpdateDynamicsTime(100);
      std::cout << eco.UpdateDynamicsTime( 1000 ) << std::endl;
    }
      //    }
#endif
    //======== output ===============
    int survivor = eco.numSurvivor();
    suvout << times_invasion << ' ' << survivor;
    for( int i=1; i<6; i++) suvout << ' ' << eco.numTrophSpecies(i);
    suvout << std::endl;

    divout << times_invasion << ' ' << eco.expSWindexAll() << ' '
	   << eco.expSWindexProducer() << ' ' << eco.expSWindexConsumer() << std::endl;

    bmassout << times_invasion << ' ' << eco.TotalBiomass();
    for( int i=1; i<6; i++) bmassout << ' ' << eco.TrophicBiomass(i);
    bmassout << std::endl;

    if( survivor == output_species ) {
	char afilename[20];
	char xfilename[20];
	sprintf( afilename, "%s.a%04d",id,survivor);
	sprintf( xfilename, "%s.x%04d",id,survivor);
	eco.X_b_eta_output(xfilename);
	eco.Moutput(afilename);
	output_species *= 2;
    }
  }
  //======== loop end ===============    
  char afilename[20];
  char xfilename[20];
  sprintf( afilename, "%s.afinal",id);
  sprintf( xfilename, "%s.xfinal",id);
  eco.X_b_eta_output(xfilename);
  eco.Moutput(afilename);


#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  
  return 0;
}
