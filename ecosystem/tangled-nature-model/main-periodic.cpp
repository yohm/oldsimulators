#include <iostream>
#include <fstream>
#include <ctime>
#include "classTangledNature.cpp"
#include "classTNInvasionSystem.cpp"
#include "mtrand64.cpp"

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 17 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <fecundity> <resource> <b_max> <eta_max> <eta_prob> <mij_max> <mij_prob> <mii_max> "
	      << "<x_initial> <threshold> <dt> <inv_interval> <t_end> <max_species> <seed> <ID>" << std::endl
	      << "\tor" << std::endl
	      << "program <binary-filename> <inv_interval> <dt> <t_restart> <t_end> <ID>" << std::endl;
    throw 1;
  }
  
  double fecundity = atof( argv[1] );
  double resource = atof( argv[2] );
  double b_max = atof( argv[3] );
  double eta_max = atof( argv[4] );
  double eta_prob = atof( argv[5] );
  double mij_max = atof( argv[6] );
  double mij_prob = atof( argv[7] );
  double mii_max = atof( argv[8] );
  double x_initial = atof( argv[9] );
  double threshold = atof( argv[10] );
  double dt = atof( argv[11] );
  double percentage = 0.01;
  double invasion_interval = atof( argv[12] );
  long t_end = atol( argv[13] );
  int max_species = atoi( argv[14] );
  unsigned long long seed = atol( argv[15] );
  char* id = argv[16];

  if( fecundity <= 1.0 || resource <= 0.0 || b_max <= 0.0 || eta_max <= 0.0 ||
      eta_prob <= 0.0 || eta_prob >= 1.0 || mij_max <= 0.0 || mij_prob <= 0.0 ||
      mij_prob >= 1.0 || mii_max <= 0.0 || x_initial <= 0.0 || threshold <= 0.0 ||
      dt <= 0.0 || percentage <= 0.0 || t_end <= 0 || max_species <= 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    std::cerr << "Lists of given parameters are as follows:" << std::endl
	      << "fecundity:\t" << fecundity << std::endl
	      << "resource:\t" << resource << std::endl
	      << "b_max:\t" << b_max << std::endl
	      << "eta_max:\t" << eta_max << std::endl
	      << "eta_prob:\t" << eta_prob << std::endl
	      << "mij_max:\t" << mij_max << std::endl
	      << "mij_prob:\t" << mij_prob << std::endl
	      << "mii_max:\t" << mii_max << std::endl
	      << "x_initial:\t" << x_initial << std::endl
	      << "threshold:\t" << threshold << std::endl
	      << "dt:\t" << dt << std::endl
	      << "percentage:\t" << percentage << std::endl
	      << "invasion interval:\t" << invasion_interval << std::endl
	      << "t_end:\t" << t_end << std::endl
	      << "max_species:\t" << max_species << std::endl
	      << "seed:\t" << seed << std::endl
	      << "id:\t" << id << std::endl;
    throw 1;
  }

  //ofstreams
  char suvfilename[20];
  sprintf(suvfilename,"%s.suv",id);
  std::ofstream suvout(suvfilename);

  char divfilename[20];
  sprintf(divfilename,"%s.diversity",id);
  std::ofstream divout(divfilename);

  char jumyofilename[20];
  sprintf( jumyofilename, "%s.jumyo",id);

  char bmassfilename[20];
  sprintf(bmassfilename,"%s.biomass",id);
  std::ofstream bmassout(bmassfilename);

  
  classTNInvasionSystem eco( max_species, percentage, dt,
			   fecundity, resource, threshold,
			   jumyofilename,
			   b_max, eta_max, eta_prob,
			   mij_max, mij_prob, mii_max,
			   x_initial, seed);


#ifdef BENCH
  clock_t start = clock();
#endif

  long loop_between_one_invasion = (long) (invasion_interval / dt);
  //============== main loop ========================
  for( long times_invasion = 0; times_invasion <t_end; times_invasion++) {
    //======== eco dynamics =========
    try{
      eco.AddInvador();
    } catch( int i) {
      std::cerr << "Number of species exceeds max_species" << std::endl;
      break;
    }

    eco.UpdateDynamicsTime(loop_between_one_invasion);

    //======== output ===============
    int survivor = eco.numSurvivor();
    suvout << times_invasion << ' ' << survivor;
    for( int i=1; i<5; i++) suvout << ' ' << eco.numTrophSpecies(i);
    suvout << std::endl;

    divout << times_invasion << ' ' << eco.expSWindexAll() << ' '
	   << eco.expSWindexProducer() << ' ' << eco.expSWindexConsumer() << std::endl;

    bmassout << times_invasion << ' ' << eco.TotalBiomass();
    for( int i=1; i<5; i++) bmassout << ' ' << eco.TrophicBiomass(i);
    bmassout << std::endl;

    if( (times_invasion%10000) == 0 ) {
      if( times_invasion == 0 ) continue;
	char afilename[20];
	char xfilename[20];
	sprintf( afilename, "%s.a%04d",id, (int)(times_invasion/10000) );
	sprintf( xfilename, "%s.x%04d",id, (int)(times_invasion/10000) );
	eco.X_b_eta_output(xfilename);
	eco.Moutput(afilename);
    }
  }
  //======== loop end ===============    
  char afilename[20];
  char xfilename[20];
  sprintf( afilename, "%s.afinal",id);
  sprintf( xfilename, "%s.xfinal",id);
  eco.X_b_eta_output(xfilename);
  eco.Moutput(afilename);
  char binfilename[20]; sprintf( binfilename, "%s.binary",id);

#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  
  return 0;
}

//----------------------end of the program-------------------------------
