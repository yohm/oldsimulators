#include "classTangledNature2.hpp"
#include "classTNInvasionSystem2.hpp"

//------------------------------------
classTNInvasionSystem::classTNInvasionSystem(  int t_max_species, double t_percentage, double t_dt,
					   double t_fecundity, double t_resource, double t_threshold,
					   const char* t_jumyofilename,
					   double t_b_max, double t_eta_max, double t_eta_prob,
					   double t_mij_max, double t_mij_prob, double t_mii_max,
					   double t_x_initial, unsigned long long t_seed) 
  : classTangledNature( t_max_species, t_percentage, t_dt, t_fecundity, t_resource, t_threshold, t_jumyofilename) {
  b_max = t_b_max;
  eta_max = t_eta_max;
  eta_prob = t_eta_prob;
  mij_max = t_mij_max;
  mij_prob = t_mij_prob;
  mii_max = t_mii_max;
  x_initial = t_x_initial;
  seed = t_seed;
  rnd.init_genrand64( seed );
  //-------------------
  b_max_65535_inv = b_max / 65535.0;
  eta_prob_65535 = (int) (eta_prob * 65536.0);
  eta_max_65535_inv = eta_max / 65535.0;
  mii_max_65535_inv = mii_max / 65535.0;
  mij_prob_65535 = (int) (mij_prob * 65536.0);
  mij_max_65535_inv = mij_max / 65535.0;
}

//------------------------------------
bool classTNInvasionSystem::AddInvador() {
  int invador_id = ZeroPoint();
  SetRandomMIJ_b_eta( invador_id );
  SetInitialPop( invador_id );
  //  IncrementAge();
  return true;
}

//------------------------------------
int classTNInvasionSystem::ZeroPoint() {
  int temp = species;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) temp = i;
  }
  if( temp == max_species ) throw 10;

  if(temp == species) species++;

  if( species > current_max_species ) ReallocateMemory();
  
  return temp;
}

//------------------------------------
void classTNInvasionSystem::SetRandomMIJ_b_eta(int id) {
  //必要な乱数の数 4 + 2*species = b,eta(2),mii,mij(2*species)
  b[id] = rnd.load_16bit_integer() * b_max_65535_inv;
  if( rnd.load_16bit_integer() < eta_prob_65535 ) {
    producer[id] = true;
    eta[id] = rnd.load_16bit_integer() * eta_max_65535_inv;
  }
  else {
    producer[id] = false;
    eta[id] = 0;
  }

  m[id][id] = - rnd.load_16bit_integer() * mii_max_65535_inv;
  
  for( int j=0; j<species; j++) {
    if(!alive[j] ) continue;
    if( rnd.load_16bit_integer() < mij_prob_65535 ) {
      double temp = ( rnd.load_16bit_integer() + rnd.load_16bit_integer() - 65535) * mij_max_65535_inv;
      if( producer[j]==true && producer[id]==false ) m[id][j] = fabs(temp);
      else if( producer[j]==false && producer[id]==true ) m[id][j] = - fabs(temp);
      else m[id][j] = temp;
      m[j][id] = -m[id][j];
    }
  }

}

//------------------------------------
void classTNInvasionSystem::SetInitialPop(int id) {
  x[id] = x_initial;
  alive[id] = true;
  age[id] = 0;
  CalcXtot();
}

//------------------------------------
// void classTNInvasionSystem::IncrementAge() {
//   for( int i=0; i<species; i++) {
//     if(!alive[i]) continue;
//     age[i]++;
//   }
// }
