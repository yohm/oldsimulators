#include <iostream>
#include <fstream>
#include <ctime>
#include "classTangledNature2.cpp"
#include "classTNInvasionSystem2.cpp"
#include "mtrand64.cpp"

void output( classTNInvasionSystem& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, long time, char* id);
void output_final( classTNInvasionSystem & eco, char* id);

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 18 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <fecundity> <resource> <b_max> <eta_max> <eta_prob> <mij_max> <mij_prob> <mii_max> "
	      << "<x_initial> <threshold> <dt> <inv_interval> <sampling_interval> <t_end(sec)> <max_species> <seed> <ID>" << std::endl
	      << std::endl;
    throw 1;
  }
  
  double fecundity = atof( argv[1] );
  double resource = atof( argv[2] );
  double b_max = atof( argv[3] );
  double eta_max = atof( argv[4] );
  double eta_prob = atof( argv[5] );
  double mij_max = atof( argv[6] );
  double mij_prob = atof( argv[7] );
  double mii_max = atof( argv[8] );
  double x_initial = atof( argv[9] );
  double threshold = atof( argv[10] );
  double dt = atof( argv[11] );
  double percentage = 0.001;
  double invasion_interval = atof( argv[12] );
  double sampling_interval = atof( argv[13] );
  long t_end = atol( argv[14] );
  int max_species = atoi( argv[15] );
  unsigned long long seed = atol( argv[16] );
  char* id = argv[17];

  if( fecundity <= 1.0 || resource <= 0.0 || b_max <= 0.0 || eta_max <= 0.0 ||
      eta_prob <= 0.0 || eta_prob >= 1.0 || mij_max <= 0.0 || mij_prob <= 0.0 ||
      mij_prob >= 1.0 || mii_max <= 0.0 || x_initial <= 0.0 || threshold <= 0.0 ||
      dt <= 0.0 || percentage <= 0.0 || t_end <= 0 || max_species <= 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    std::cerr << "Lists of given parameters are as follows:" << std::endl
	      << "fecundity:\t" << fecundity << std::endl
	      << "resource:\t" << resource << std::endl
	      << "b_max:\t" << b_max << std::endl
	      << "eta_max:\t" << eta_max << std::endl
	      << "eta_prob:\t" << eta_prob << std::endl
	      << "mij_max:\t" << mij_max << std::endl
	      << "mij_prob:\t" << mij_prob << std::endl
	      << "mii_max:\t" << mii_max << std::endl
	      << "x_initial:\t" << x_initial << std::endl
	      << "threshold:\t" << threshold << std::endl
	      << "dt:\t" << dt << std::endl
	      << "percentage:\t" << percentage << std::endl
	      << "invasion interval:\t" << invasion_interval << std::endl
	      << "t_end:\t" << t_end << std::endl
	      << "max_species:\t" << max_species << std::endl
	      << "seed:\t" << seed << std::endl
	      << "id:\t" << id << std::endl;
    throw 1;
  }

  //ofstreams
  char suvfilename[20];
  sprintf(suvfilename,"%s.suv",id);
  std::ofstream suvout(suvfilename);

  char divfilename[20];
  sprintf(divfilename,"%s.diversity",id);
  std::ofstream divout(divfilename);

  char jumyofilename[20];
  sprintf( jumyofilename, "%s.jumyo",id);

  char bmassfilename[20];
  sprintf(bmassfilename,"%s.biomass",id);
  std::ofstream bmassout(bmassfilename);

  char invtimefilename[20];
  sprintf(invtimefilename,"%s.invtime",id);
  std::ofstream itimeout(invtimefilename);
  
  classTNInvasionSystem eco( max_species, percentage, dt,
			   fecundity, resource, threshold,
			   jumyofilename,
			   b_max, eta_max, eta_prob,
			   mij_max, mij_prob, mii_max,
			   x_initial, seed);


#ifdef BENCH
  clock_t start = clock();
#endif

  mtrand64 rnd2(seed + 100ULL); //used for the exponential distribution
  long loop_sampling_interval = static_cast<long>(sampling_interval / dt);
  long loop_invasion_interval = static_cast<long>(invasion_interval / dt);
  long loop_total = static_cast<long>(t_end/dt);
  eco.AddInvador();
  double inv65536 = 1.0/65536.0;
  long next_invasion = static_cast<long>(loop_invasion_interval * -log( (rnd2.load_16bit_integer()+0.5)*inv65536 ) );
  long next_output = loop_sampling_interval;
  //============== main loop ========================
  for( long time_steps = 0; time_steps < loop_total; ) {
    if( next_output <= next_invasion ) {
      eco.UpdateDynamicsTime(next_output);
      time_steps += next_output;
      next_invasion -= next_output;
      next_output = loop_sampling_interval;
      output(eco,suvout,divout,bmassout,time_steps*dt,id);
    }
    else {
      itimeout << time_steps*dt << std::endl;
      eco.UpdateDynamicsTime(next_invasion);
      time_steps += next_invasion;
      next_output -= next_invasion;
      next_invasion = static_cast<long>(loop_invasion_interval * -log( (rnd2.load_16bit_integer()+0.5)*inv65536 ) );
      try{
	eco.AddInvador();
      } catch( int i) {
	std::cerr << "Number of species exceeds max_species" << std::endl;
      }
    }
  }
  output_final(eco,id);
#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  return 0;
}



//--------------------    
void output( classTNInvasionSystem& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, long time, char* id) {
  static long times_output = 0;
  //======== output ===============
  int survivor = eco.numSurvivor();
  suvout << time << ' ' << survivor;
  for( int i=1; i<5; i++) suvout << ' ' << eco.numTrophSpecies(i);
  suvout << ' ' << eco.NonSteadiness() << std::endl;
  
  divout << time << ' ' << eco.expSWindexAll() << ' '
	 << eco.expSWindexProducer() << ' ' << eco.expSWindexConsumer() << std::endl;
  
  bmassout << time << ' ' << eco.TotalBiomass();
  for( int i=1; i<5; i++) bmassout << ' ' << eco.TrophicBiomass(i);
  bmassout << std::endl;

  if( (times_output%10000) == 0 ) {
    char afilename[20];
    char xfilename[20];
    sprintf( afilename, "%s.a%04d",id, (int)(times_output/10000) );
    sprintf( xfilename, "%s.x%04d",id, (int)(times_output/10000) );
    eco.X_b_eta_output(xfilename);
    eco.Moutput(afilename);
  }
  times_output++;
}

//-----------------------
void output_final( classTNInvasionSystem & eco, char* id){
  char afilename[20];
  char xfilename[20];
  sprintf( afilename, "%s.afinal",id);
  sprintf( xfilename, "%s.xfinal",id);
  eco.X_b_eta_output(xfilename);
  eco.Moutput(afilename);
}  

//----------------------end of the program-------------------------------
