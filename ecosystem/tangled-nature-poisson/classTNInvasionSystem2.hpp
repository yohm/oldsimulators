#include "classTangledNature2.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_INVASION_H_
#define _C_TN_INVASION_H_

class classTNInvasionSystem : public classTangledNature {
public:
  classTNInvasionSystem( int t_max_species, double t_percentage, double t_dt,
			 double t_fecundity, double t_resource, double t_threshold,
			 const char* jumyofilename,
			 double t_b_max, double t_eta_max, double t_eta_prob,
			 double t_mij_max, double t_mij_prob, double t_mii_max,
			 double t_x_initial, unsigned long long t_seed);
  ~classTNInvasionSystem() {};
  bool AddInvador(); //add one invador. Throw exception(10) when species exceeds max_species.
protected:
  double b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max, x_initial;
  unsigned long long seed;
  int ZeroPoint();
  void SetRandomMIJ_b_eta(int zero_point); //set random interactions
  void SetInitialPop( int zero_point );
  //  void IncrementAge();
  mtrand64 rnd;
  //----- variables used in SetRandomMIJ_b_eta(int) -----------
  double b_max_65535_inv, eta_max_65535_inv, mii_max_65535_inv, mij_max_65535_inv;
  int eta_prob_65535, mij_prob_65535;
};


#endif
