#include <iostream>
#include <fstream>
#include <cmath>
#include <string>

#ifndef _C_TANGLED_NATURE_H_
#define _C_TANGLED_NATURE_H_

class classTangledNature {
public:
  classTangledNature(int t_max_species, double t_percentage, double t_dt,
		     double t_fecundity, double t_resource, double t_threshold,
		     const char* jumyofilename);
  classTangledNature();
  ~classTangledNature();
  int UpdateDynamicsStable(); //update population dynamics until all x become stable. return number of update steps
  int UpdateDynamicsTime( int i); //update population dynamics for i steps. return inv_ng.
  
  int numSurvivor() const; //return number of all species
  int numProducer() const; //return number of producers
  int numConsumer() const; //return number of consumers
  
  double expSWindexAll() const; // return exponential Shannon-Wiener index for all species.
  double expSWindexProducer() const; //return exponential Shannon-Wiener index for producer species.
  double expSWindexConsumer() const; //return exponential Shannon-Wiener index for consumer species.
  
  int numTrophSpecies( int t_level); //return number of species belonging to trophic level t_level. Producers are counted as species having level 1.
  double expSWindexLevels(int t_level); // return exponential Shannon-Wiener index for species in trophic level t_level.
  
  void Xoutput( std::ofstream & t_fout); //output x[i] to the ofstream. "x[0] x[1] x[2] .... \n"
  void X_b_eta_output( const char* filename); //output x[i] to file. "x[0]\nx[1]\nx[2]\n...."
  void Moutput( const char* filename); //output a[i][j] to the file 'filename'.
  
  double TotalBiomass() {return x_tot;} //return total biomass of all species.
  double ProducerBiomass() const; //return total biomass for producer species
  double ConsumerBiomass() const; //return total biomass for consumer species
  double TrophicBiomass(int t_level); //return summation of x of species in trophic level t_level.
  bool Alive(int i) {return alive[i];}
  int NonSteadiness() {return inv_ng;}
protected:
  double *x_0,*x_dash_0,*x_dash_1,*x_dash_2,*x_dash_3,*x,**m, *b, *eta;
  //b denotes metablic rate. always positive. eta denotes ability to utilize resources
  int *trophic_level,*age;
  bool *alive, *producer; //if producer[i] is true, i'th species is producer. (eta[i]>0)
  int species;
  double fecundity, resource, threshold, dt;
  void AllocateMemory(int size); //called in constructor.
  void ReleaseMemory(); //called in destructor and ReallocateMemory.
  int inv_ng;
  int max_species,current_max_species;
  void ReallocateMemory();
  
  int Solve(); //return invasion_ng. invasion_ng == 0 means system becomes stable.
  void CalcXtot(); //update x_tot and x_tot_inv. This function is called whenever x changes.
  double ReproductionProb( int i) const; //Return reproduction probability of species i.
  double x_tot, x_tot_inv; // x_tot = \sum_j x[j]. x_tot_inv = 1/x_tot.  these must be calculated immediately after x changes.
  double percentage; //used for judging if the system is stable or not.
  void Extinct(int i); //Extinction of species i.
  std::ofstream jumyo_out;

  int TrophicCount(); //Count trophic levels. This routine was called from numTrophSpecies.
  bool trophic_level_counted_flag; //When trophic level and sum_trophic have been counted, true. Else false.
  int sum_trophic[10]; //sum_trophic[i] means the number of species belonging to trophic_level i
  bool Overmaxspecies();

  std::string jumyofilename;
};

#endif
