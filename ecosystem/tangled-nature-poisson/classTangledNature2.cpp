#include "classTangledNature2.hpp"

//---------------------------------------------
classTangledNature::classTangledNature( int t_max_species, double t_percentage, double t_dt,
					double t_fecundity, double t_resource, double t_threshold,
					const char* t_jumyofilename) {
  max_species = t_max_species;
  percentage = t_percentage;
  dt = t_dt;
  fecundity = t_fecundity;
  resource = t_resource;
  threshold = t_threshold;
  
  if( max_species <= 500 ) current_max_species = max_species;
  else current_max_species = 500;

  species = 0;
  
  AllocateMemory(current_max_species);

  // AddInvador?
  jumyofilename = t_jumyofilename;
  jumyo_out.open( jumyofilename.c_str() );
}
//---------------------------------------------
classTangledNature::classTangledNature() {
  x_0 = x_dash_0 = x_dash_1 = x_dash_2 = x_dash_3 = x = b = eta = NULL;
  m = NULL;
  trophic_level = age = NULL;
  alive = producer = NULL;
  species = 0;
  fecundity = resource = threshold = dt = 0.0;
  inv_ng = 0;
  max_species = current_max_species = 0;
  x_tot = x_tot_inv = 0;
  percentage = 0.0;
  trophic_level_counted_flag = false;
  for( int i=0; i<10; i++) { sum_trophic[i] = 0;}
}  
//---------------------------------------------
void classTangledNature::AllocateMemory( int size) {
  x = new double[size];
  x_0 = new double[size];
  x_dash_0 = new double[size];
  x_dash_1 = new double[size];
  x_dash_2 = new double[size];
  x_dash_3 = new double[size];
  m = new double* [size];
  for( int j=0; j<size; j++) m[j] = new double[size];
  b = new double [size];
  eta = new double [size];
  trophic_level = new int [size];
  age = new int [size];
  alive = new bool [size];
  producer = new bool [size];
  
  for( int i=0; i<size; i++) {
    x[i] = x_0[i] = x_dash_0[i] = x_dash_1[i] = x_dash_2[i] = x_dash_3[i] = 0.0;
    CalcXtot();
    for( int j=0; j<size; j++) m[i][j] = 0.0;
    b[i] = eta[i] = 0.0;
    trophic_level[i] = age[i] = 0;
    alive[i] = producer[i] = false;
  }
}

//---------------------------------------------
void classTangledNature::ReallocateMemory() {
  int size = current_max_species + 500;

  if( size > max_species ) size = max_species;
  double * t_x = new double[size];
  double * t_x_0 = new double[size];
  double * t_x_dash_0 = new double[size];
  double * t_x_dash_1 = new double[size];
  double * t_x_dash_2 = new double[size];
  double * t_x_dash_3 = new double[size];
  double ** t_m = new double* [size];
  for( int j=0; j<size; j++) t_m[j] = new double[size];
  double * t_b = new double [size];
  double * t_eta = new double [size];
  int * t_trophic_level = new int [size];
  int * t_age = new int [size];
  bool * t_alive = new bool [size];
  bool * t_producer = new bool [size];
  
  for( int i=0; i<size; i++) {
    t_x[i] = t_x_0[i] = t_x_dash_0[i] = t_x_dash_1[i] = t_x_dash_2[i] = t_x_dash_3[i] = 0.0;
    for( int j=0; j<size; j++) t_m[i][j] = 0.0;
    t_b[i] = t_eta[i] = 0.0;
    t_trophic_level[i] = t_age[i] = 0;
    t_alive[i] = t_producer[i] = false;
  }
  
  for( int i=0; i<current_max_species; i++) {
    t_x[i] = x[i];
    t_x_0[i] = x_0[i];
    t_x_dash_0[i] = x_dash_0[i];
    t_x_dash_1[i] = x_dash_1[i];
    t_x_dash_2[i] = x_dash_2[i];
    t_x_dash_3[i] = x_dash_3[i];
    for( int j=0; j<current_max_species; j++) { t_m[i][j] = m[i][j];}
    t_b[i] = b[i];
    t_eta[i] = eta[i];
    t_trophic_level[i] = trophic_level[i];
    t_age[i] = age[i];
    t_alive[i] = alive[i];
    t_producer[i] = producer[i];
  }

  ReleaseMemory();

  x = t_x; x_0 = t_x_0;
  x_dash_0 = t_x_dash_0;
  x_dash_1 = t_x_dash_1;
  x_dash_2 = t_x_dash_2;
  x_dash_3 = t_x_dash_3;
  m = t_m;
  b = t_b;
  eta = t_eta;
  trophic_level = t_trophic_level;
  age = t_age;
  alive = t_alive;
  producer = t_producer;
}
//---------------------------------------------
classTangledNature::~classTangledNature() {
  ReleaseMemory();
}

//----------------------------------------------
void classTangledNature::ReleaseMemory() {
  delete [] x; delete [] x_0; delete [] x_dash_0;
  delete [] x_dash_1; delete [] x_dash_2; delete [] x_dash_3;
  for( int i=0; i<current_max_species; i++) {
    delete [] m[i];
  }
  delete [] m;
  delete [] b;  delete [] eta;
  delete [] trophic_level;  delete [] age;
  delete [] alive;  delete [] producer;
}

//---------------------------------------------
int classTangledNature::UpdateDynamicsStable() {
  int ng = 0;
  int count = 0;
  do {
    ng = Solve();
    count++;
  } while ( ng > 0);
  return count;
}
//---------------------------------------------
int classTangledNature::UpdateDynamicsTime(int i) {
  int ng = 0;
  for(int j=0; j<i; j++) {
    ng = Solve();
  }

  return ng;
}
//---------------------------------------------
int classTangledNature::Solve() {
  //precoreでループを一つ進める
  //species 番目の要素まで使う
  //extinct したら inv_ng++としてもう一度ループを回す
  //inv_ngを返す
  inv_ng = 0;
  //x_dashのコピー
  for(int i=0; i<species; i++) {
    x_dash_3[i] = x_dash_2[i];
    x_dash_2[i] = x_dash_1[i];
    x_dash_1[i] = x_dash_0[i];
  }

  //calculation of x_dash_0  ------------------------------------------------
  for( int i=0; i<species; i++) {
    if( !alive[i]) continue;
    x_dash_0[i] = (fecundity*ReproductionProb(i)-1.0) * x[i];
  }


  //calculation of predictor -----------------------------------------------

  for(int i=0; i<species; i++) {
    if(!(alive[i])) continue;
    x_0[i] = x[i];
    x[i] += (dt/24.0) * (55*x_dash_0[i] -59*x_dash_1[i] + 37*x_dash_2[i] - 9*x_dash_3[i]);
  }

  CalcXtot();


  //calculation of y'_1    ---------------------------------------------
  //x_dash_3にy'_1をいれる。
  for( int i=0; i<species; i++) {
    if( !alive[i]) continue;
    x_dash_3[i] = (fecundity*ReproductionProb(i)-1.0) * x[i];
  }

  //calculation of corrector ---------------------------------------------
  for(int i=0; i<species; i++) {
    if(!(alive[i])) continue;
    double d = (dt/24) * (9*x_dash_3[i] + 19*x_dash_0[i] - 5*x_dash_1[i] + x_dash_2[i]);
    x[i] = x_0[i] + d;

    if(x_0[i] * percentage * dt < fabs(d) ) inv_ng++;
    if(x[i] <= threshold) {
      Extinct(i);
      inv_ng++;
    }
  }
  CalcXtot();

  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    age[i]++;
  }

  trophic_level_counted_flag = false;

  return inv_ng;
}
//---------------------------------------------
void classTangledNature::CalcXtot() {
  x_tot = 0.0;
  for( int j=0; j<species; j++) { x_tot += x[j]; }
  x_tot_inv = 1.0 / x_tot;
}
//---------------------------------------------
double classTangledNature::ReproductionProb(int i) const {
  double delta = -b[i];
  double inter = eta[i] * resource;
  for( int j=0; j<species; j++) {
    if(!alive[j]) continue;
    inter += m[i][j]*x[j];
  }
  delta = delta + inter * x_tot_inv;
  return 1.0 / ( 1.0 + exp(-delta) );
}

//---------------------------------------------
void classTangledNature::Extinct( int i) {
  jumyo_out << (double)age[i]*dt << ' ' << producer[i] << std::endl; //絶滅するごとにjumyoの書き出し
  alive[i] = false; producer[i] = false;
  x[i] = 0.0;
  x_0[i] = 0.0;
  x_dash_0[i] = 0.0;
  x_dash_1[i] = 0.0;
  x_dash_2[i] = 0.0;
  x_dash_3[i] = 0.0;
  for(int j=0; j<species; j++) {
    m[i][j] = 0.0;
    m[j][i] = 0.0;
  }
  b[i] = 0.0; eta[i] = 0.0;
  trophic_level[i] = 0;
  age[i] = 0;
}

//---------------------------------------------
int classTangledNature::numSurvivor() const{
  int survivor = 0;
  for( int i=0; i< species; i++) {
    if( alive[i] ) survivor++;
  }
  return survivor;
}

//---------------------------------------------
int classTangledNature::numProducer() const{
  int num_producer = 0;
  for( int i=0; i<species; i++) {
    if( alive[i] && producer[i] ) num_producer++;
  }
  return num_producer;
}

//---------------------------------------------
int classTangledNature::numConsumer() const{
  int num_consumer = 0;
  for( int i=0; i<species; i++) {
    if( alive[i] && (!producer[i]) ) num_consumer++;
  }
  return num_consumer;
}

//---------------------------------------------
double classTangledNature::expSWindexAll() const{
  double diversity = x_tot;
  if( x_tot == 0 ) return 0.0;
  for( int i=0; i<species; i++) {
    if( !alive[i] ) continue;
    diversity *= pow( x[i], -x[i]/x_tot);
  }
  return diversity;
}

//---------------------------------------------
double classTangledNature::expSWindexProducer() const{
  double total = ProducerBiomass();
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=0; i<species; i++) {
    if( alive[i] && producer[i] ) {
      diversity *= pow( x[i], -x[i]/total);
    }
  }
  return diversity;
}

//---------------------------------------------
double classTangledNature::expSWindexConsumer() const{
  double total = ConsumerBiomass();
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=0; i<species; i++) {
    if( alive[i] && (!producer[i]) ) {
      diversity *= pow( x[i], -x[i]/total);
    }
  }
  return diversity;
}

//---------------------------------------------
int classTangledNature::numTrophSpecies(int t_level) {
  if( !trophic_level_counted_flag ) { TrophicCount();}
  return sum_trophic[t_level];
}
//---------------------------------------------
double classTangledNature::expSWindexLevels(int t_level) {
  if( !trophic_level_counted_flag ) {TrophicCount();}
  double total = TrophicBiomass( t_level);
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=0; i<species; i++) {
    if( alive[i] && (trophic_level[i] == t_level) ) {
      diversity *= pow( x[i], -x[i]/total);
    }
  }
  return diversity;
}
//---------------------------------------------
int classTangledNature::TrophicCount() {
  bool finished = true;  //after compeletion of judgement : true

  for(int i=0; i<species; i++) {
    trophic_level[i] = 0;
  } //initialize

  //level 1 (producer) -----------------
  for(int i=0; i<species; i++) {
    if( alive[i] && producer[i] ) trophic_level[i] = 1;
    if( alive[i] && (!producer[i]) ) finished=false;
  }

  int max_level = 1;

  //level2~ --------------------------
  //level(k+1)の判定 -----------------
  for(int k=1; !finished && k<9; k++) {

    for(int i=0; i<species; i++) {
      if(trophic_level[i] == k) {
	for(int j=0; j<species; j++) {
	  if(m[i][j] <0 && alive[j] && trophic_level[j]==0) {
	    trophic_level[j] = k+1;
	  }
	}
      }
    }

    //全種完了したか確かめる(完了していなかったらfinished=false)
    finished=true;
    for(int i=0; i<species; i++) {
      if(trophic_level[i]==0 && alive[i]) {
	finished=false;
	break;
      }
    }
    max_level = k+1;
  }

  //trophic levelの種数のカウント
  if( max_level > 9 ) { std::cerr << "Trophic level exceeds 9."<<std::endl; throw(1);}
  for( int i=0; i<10; i++) sum_trophic[i] = 0;

  for( int i=0; i<species; i++) {
    if( alive[i] ) sum_trophic[ trophic_level[i] ]++;
  }

  trophic_level_counted_flag = true;
  return max_level;
}

//---------------------------------------
void classTangledNature::Xoutput( std::ofstream & t_fout ) {
  for( int i=0; i<species; i++) {
    //    if( !alive[i]) continue;
    t_fout << x[i] << ' ';
  }
  t_fout << std::endl;
}

//---------------------------------------
void classTangledNature::X_b_eta_output( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    fout << x[i] << ' ' << b[i] << ' ' << eta[i] << ' ' << age[i] << std::endl;
  }
  fout.close();
}

//---------------------------------------
void classTangledNature::Moutput( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=0; j<species; j++) {
      if(!alive[j]) continue;
      fout << m[i][j] << ' ';
    }
    fout << std::endl;
  }

  fout.close();
}

//---------------------------------------
double classTangledNature::ProducerBiomass() const{
  double temp = 0.0;
  for( int i=0; i<species; i++) {
    if(alive[i] && producer[i]) temp += x[i];
  }
  return temp;
}
//---------------------------------------
double classTangledNature::ConsumerBiomass() const {
  double temp = 0.0;
  for( int i=0; i<species; i++) {
    if(alive[i] && (!producer[i]) ) temp += x[i];
  }
  return temp;
}

//---------------------------------------
double classTangledNature::TrophicBiomass(int t_level) {
  double temp = 0.0;
  if(!trophic_level_counted_flag) { TrophicCount();}
  
  for( int i=0; i<species; i++) {
    if( alive[i] && (trophic_level[i]==t_level) ) temp += x[i];
  }
  return temp;
}
