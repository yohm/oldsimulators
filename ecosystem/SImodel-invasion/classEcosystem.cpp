#include "classEcosystem.hpp"

//---------------------------------------------
classEcosystem::classEcosystem( int t_max_species, double t_percentage, double t_dt,
				int t_num_plant, double t_growth_rate,
				double t_lambda, const char* jumyofile) {
  max_species = t_max_species;
  percentage = t_percentage;
  dt = t_dt;
  num_plant = t_num_plant;
  lambda = t_lambda;
  
  if( max_species <= 500 ) current_max_species = max_species;
  else current_max_species = 500;
  
  AllocateMemory(current_max_species);

  for( int i=0; i< num_plant; i++) {
    x[i] = 1.0; age[i] = 1; alive[i] = true;
    b[i] = t_growth_rate;
    species = num_plant;
  }

  jumyo_out.open( jumyofile);
}
//---------------------------------------------
void classEcosystem::AllocateMemory( int size) {
  x = new double[size];
  x_0 = new double[size];
  x_dash_0 = new double[size];
  x_dash_1 = new double[size];
  x_dash_2 = new double[size];
  x_dash_3 = new double[size];
  a = new double* [size];
  for( int j=0; j<size; j++) a[j] = new double[size];
  b = new double [size];
  trophic_level = new int [size];
  age = new int [size];
  alive = new bool [size];
  
  for( int i=0; i<size; i++) {
    x[i] = x_0[i] = x_dash_0[i] = x_dash_1[i] = x_dash_2[i] = x_dash_3[i] = 0.0;
    for( int j=0; j<size; j++) a[i][j] = 0.0;
    b[i] = 0.0;
    trophic_level[i] = age[i] = 0;
    alive[i] = false;
  }
}

//---------------------------------------------
void classEcosystem::ReallocateMemory() {
  int size = current_max_species + 500;

  if( size > max_species ) size = max_species;
  double * t_x = new double[size];
  double * t_x_0 = new double[size];
  double * t_x_dash_0 = new double[size];
  double * t_x_dash_1 = new double[size];
  double * t_x_dash_2 = new double[size];
  double * t_x_dash_3 = new double[size];
  double ** t_a = new double* [size];
  for( int j=0; j<size; j++) t_a[j] = new double[size];
  double * t_b = new double [size];
  int * t_trophic_level = new int [size];
  int * t_age = new int [size];
  bool * t_alive = new bool [size];
  
  for( int i=0; i<size; i++) {
    t_x[i] = t_x_0[i] = t_x_dash_0[i] = t_x_dash_1[i] = t_x_dash_2[i] = t_x_dash_3[i] = 0.0;
    for( int j=0; j<size; j++) t_a[i][j] = 0.0;
    t_b[i] = 0.0;
    t_trophic_level[i] = t_age[i] = 0;
    t_alive[i] = false;
  }
  
  for( int i=0; i<current_max_species; i++) {
    t_x[i] = x[i];
    t_x_0[i] = x_0[i];
    t_x_dash_0[i] = x_dash_0[i];
    t_x_dash_1[i] = x_dash_1[i];
    t_x_dash_2[i] = x_dash_2[i];
    t_x_dash_3[i] = x_dash_3[i];
    for( int j=0; j<current_max_species; j++) { t_a[i][j] = a[i][j];}
    t_b[i] = b[i];
    t_trophic_level[i] = trophic_level[i];
    t_alive[i] = alive[i];
  }

  ReleaseMemory();

  x = t_x; x_0 = t_x_0;
  x_dash_0 = t_x_dash_0;
  x_dash_1 = t_x_dash_1;
  x_dash_2 = t_x_dash_2;
  x_dash_3 = t_x_dash_3;
  a = t_a;
  b = t_b;
  trophic_level = t_trophic_level;
  alive = t_alive;
  
}
//---------------------------------------------
classEcosystem::~classEcosystem() {
  ReleaseMemory();
}

//----------------------------------------------
void classEcosystem::ReleaseMemory() {
  delete [] x; delete [] x_0; delete [] x_dash_0;
  delete [] x_dash_1; delete [] x_dash_2; delete [] x_dash_3;
  for( int i=0; i<current_max_species; i++) {
    delete [] a[i];
  }
  delete [] a;
  delete [] b;
  delete [] trophic_level; delete [] age;
  delete [] alive;
}

//---------------------------------------------
void classEcosystem::UpdateDynamicsStable() {
  int ng = 0;
  do {
    ng = Solve();
  } while ( ng > 0);
}
//---------------------------------------------
void classEcosystem::UpdateDynamicsTime(int i) {
  for(int j=0; j<i; j++) {
    Solve();
  }
}
//---------------------------------------------
int classEcosystem::Solve() {
  //precoreでループを一つ進める
  //species 番目の要素まで使う
  //extinct したら inv_ng++としてもう一度ループを回す
  //inv_ngを返す
  inv_ng = 0;
  //x_dashのコピー
  for(int i=0; i<species; i++) {
    x_dash_3[i] = x_dash_2[i];
    x_dash_2[i] = x_dash_1[i];
    x_dash_1[i] = x_dash_0[i];
  }


  //calculation of x_dash_0  ------------------------------------------------
  for( int i=0; i<num_plant; i++) {
    if( !alive[i]) continue;
    x_dash_0[i] = b[i]*x[i]*(1.0-x[i]);
  }
  for(int i=num_plant; i<species; i++) {
    if(!(alive[i])) continue;
    x_dash_0[i] = -b[i]*x[i];
  }

  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=i+1; j<species; j++) {
      if(!alive[j]) continue;
      double h = Interaction(i,j);
      x_dash_0[i] += h;
      x_dash_0[j] -= h;
    }
  }

  //calculation of predictor -----------------------------------------------

  for(int i=0; i<species; i++) {
    if(!(alive[i])) continue;
    x_0[i] = x[i];
    x[i] += (dt/24.0) * (55*x_dash_0[i] -59*x_dash_1[i] + 37*x_dash_2[i] - 9*x_dash_3[i]);
    if(x[i] < 0.0) {
      Extinct(i);
      inv_ng++;
    }
  }

  //calculation of y'_1    ---------------------------------------------
  //x_dash_3にy'_1をいれる。
  for( int i=0; i<num_plant; i++) {
    if( !alive[i]) continue;
    x_dash_3[i] = b[i]*x[i]*(1.0-x[i]);
  }
  for(int i=num_plant; i<species; i++) {
    if(!(alive[i])) continue;
    x_dash_3[i] = -b[i]*x[i];
  }

  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=i+1; j<species; j++) {
      if(!alive[j]) continue;
      double h = Interaction(i,j);
      x_dash_3[i] += h;
      x_dash_3[j] -= h;
    }
  }

  //calculation of corrector ---------------------------------------------

  for(int i=0; i<species; i++) {
    if(!(alive[i])) continue;
    double d = (dt/24) * (9*x_dash_3[i] + 19*x_dash_0[i] - 5*x_dash_1[i] + x_dash_2[i]);
    x[i] = x_0[i] + d;

    if(x_0[i] * percentage < fabs(d) ) inv_ng++;

    if(x[i] <= 0.0) {
      Extinct(i);
      inv_ng++;
    }
  }

  //孤立種の絶滅---------------------------------------------------------
  for(int i=num_plant; i<species; i++) {  //植物は判定しないのでnum_plantから
    if(!(alive[i])) continue;
    int preying = 0;
    for(int j=0; j<species; j++) {
      if(alive[j] && a[i][j] > 0) preying++;
    }
    if(preying == 0) {
      Extinct(i);
      inv_ng++;
    }
  }

  trophic_level_counted_flag = false;

  return inv_ng;
}

//---------------------------------------------
double classEcosystem::Interaction(int i, int j) const {
  double temp;
  if( a[i][j] < 0.0 ) temp = a[i][j] * pow(x[i],lambda) * pow(x[j],1.0-lambda);
  else if( a[i][j] > 0.0 ) temp = a[i][j] * pow(x[i],1.0-lambda) * pow(x[j],lambda);
  else temp = 0.0;
  return temp;
}

//---------------------------------------------
void classEcosystem::Extinct( int i) {
  alive[i] = 0;
  x[i] = 0;
  x_0[i] = 0;
  x_dash_0[i] = 0;
  x_dash_1[i] = 0;
  x_dash_2[i] = 0;
  x_dash_3[i] = 0;
  for(int j=0; j<species; j++) {
    a[i][j] = 0;
    a[j][i] = 0;
  }
  b[i] = 0;
  trophic_level[i] = 0;
  if( age[i] > 1) jumyo_out << age[i] -1 << std::endl; //絶滅するごとにjumyoの書き出し
  age[i] = 0;
}

//---------------------------------------------
int classEcosystem::numSurvivor() const{
  int survivor = 0;
  for( int i=0; i< species; i++) {
    if( alive[i] ) survivor++;
  }
  return survivor;
}

//---------------------------------------------
double classEcosystem::expSWindexAnimals() const{
  double total = 0.0;
  for( int i=num_plant; i<species; i++) total += x[i];
  double diversity = total;
  for( int i=num_plant; i<species; i++) {
    if( !alive[i] ) continue;
    diversity *= pow( x[i], -x[i]/total);
  }
  return diversity;
}
//---------------------------------------------
int classEcosystem::numTrophSpecies(int t_level) {
  if( !trophic_level_counted_flag ) { TrophicCount();}
  return sum_trophic[t_level];
}
//---------------------------------------------
double classEcosystem::expSWindexAnimals(int t_level) {
  if( !trophic_level_counted_flag ) {TrophicCount();}
  double total = 0.0;
  for( int i=num_plant; i<species; i++) {
    if( trophic_level[i] == t_level ) total += x[i];
  }
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=num_plant; i<species; i++) {
    if(!alive[i]) continue;
    if( trophic_level[i] == t_level ) {
      diversity *= pow( x[i], -x[i]/total);
    }
  }
  return diversity;
}
//---------------------------------------------
int classEcosystem::TrophicCount() {
  int c=1;  //判定完了後は1

  for(int i=0; i<species; i++) {
    trophic_level[i] = 0;
  } //initialize

  //level 1 -----------------
  for(int i=num_plant; i<species; i++) {
    for( int j=0; j<num_plant; j++) {
      if(a[j][i] < 0) {trophic_level[i] = 1; break;}
    }
  }
  for(int i=num_plant; i<species; i++) {
    if(!(trophic_level[i]) && alive[i]) {
      c=0;
      break;
    }
  }
  int max_level = 1;


  //level2~ --------------------------
  //level(k+1)の判定 -----------------
  for(int k=1; !c; k++) {

    for(int i=num_plant; i<species; i++) {
      if(trophic_level[i] == k) {
	for(int j=num_plant; j<species; j++) {
	  if(a[i][j] <0 && trophic_level[j]==0 && alive[j]) {
	    trophic_level[j] = k+1;
	  }
	}
      }
    }

    //全種完了したか確かめる(完了していなかったらc=0)
    c=1;
    for(int i=num_plant; i<species; i++) {
      if(trophic_level[i]==0 && alive[i]) {
	c=0;
	break;
      }
    }
    max_level = k+1;
  }

  //trophic levelの種数のカウント
  if( max_level > 9 ) { std::cerr << "Trophic level exceeds 9."<<std::endl; exit(1);}
  for( int i=0; i<10; i++) sum_trophic[i] = 0;
  for( int i=0; i<species; i++) {
    if( !alive[i] ) continue;
    sum_trophic[ trophic_level[i] ]++;
  }

  trophic_level_counted_flag = true;
  return max_level;
}


//---------------------------------------
void classEcosystem::Xoutput( std::ofstream & t_fout ) {
  for( int i=0; i<species; i++) {
    if( !alive[i]) continue;
    t_fout << x[i] << ' ';
  }
  t_fout << std::endl;
}

//---------------------------------------
void classEcosystem::Xoutput( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    fout << x[i] << std::endl;
  }
  fout.close();
}

//---------------------------------------
void classEcosystem::Aoutput( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=0; j<species; j++) {
      if(!alive[j]) continue;
      fout << a[i][j] << ' ';
    }
    fout << std::endl;
  }

  fout.close();
}

//---------------------------------------
double classEcosystem::TotalAnimalBiomass() {
  double temp = 0.0;
  for( int i=num_plant; i<species; i++) {
    if(!alive[i]) continue;
    temp += x[i];
  }
  return temp;
}
//---------------------------------------
double classEcosystem::TotalAnimalBiomass(int t_level) {
  double temp = 0.0;
  if(!trophic_level_counted_flag) { TrophicCount();}
  
  for( int i=num_plant; i<species; i++) {
    if(!alive[i]) continue;
    if( trophic_level[i] == t_level) temp += x[i];
  }
  return temp;
}
