#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>
#include <map>
#include "mtrand64.hpp"

#ifndef _C_TN_A_NOISED_MAP_H_
#define _C_TN_A_NOISED_MAP_H_

class classTNANoisedMap {
public:
  classTNANoisedMap( double t_fecundity, double t_N_0, double t_noise_strength, unsigned long long seed);
  classTNANoisedMap();
  ~classTNANoisedMap();
  void Update();
  
  int numSurvivor() const; //return number of all species
  
  double expSWindexAll() const; // return exponential Shannon-Wiener index for all species.
  
  void Xoutput( std::ofstream & t_fout); //output x[i] to the ofstream. "x[0] x[1] x[2] .... \n"
  void Xoutput( const char* filename); //output x[i] to file. "x[0] age[0]\nx[1] age[1]\nx[2] age[2]\n...."
  void Moutput( const char* filename); //output a[i][j] to the file 'filename'.
  void GenomeOutput( std::ofstream & genout, long time); //output "time genome[0]\ntime genome[1]\n..."
  
  double TotalBiomass() {return x_tot;} //return total biomass of all species.
  bool Alive(int i) {return alive[i];}
  void LspanOutput( const char* lspanfilename); //output life span to "lspanfilename". and reset the histogram.
  
protected:
  double *x;
  long *age, *id;
  double **m, *reproduction_prob;
  //b denotes metablic rate. always positive. eta denotes ability to utilize resources
  bool *alive; //if producer[i] is true, i'th species is producer. (eta[i]>0)

  int species;
  double fecundity;
  double N_0;
  void AllocateMemory(int size); //called in constructor.
  void ReleaseMemory(); //called in destructor and ReallocateMemory.
  int inv_ng;
  int max_species,current_max_species;
  void ReallocateMemory();
  
  double noise_strength;
  unsigned long long seed;
  mtrand64 rnd;
  
  void CalcPi(); // calculation of P_I.
  void CalcXtot(); //update x_tot and x_tot_inv. This function is called whenever x changes.
  double ReproductionProb( int i) const; //Return reproduction probability of species i.
  double x_tot;
  double x_tot_inv; // x_tot = \sum_j x[j]. x_tot_inv = 1/x_tot.  these must be calculated immediately after x changes.
  double threshold; //threshold for extinction, which is 0.5 by default.
  void Extinct(int i);//make species i go extinct.

  int lspan_vec_size; //size of lspan_prod and lspan_cons.
  std::vector<long> lspan_all;
  std::map<long,long> lspan_all_map;

  int SpeciesNumber( long genome_id); //return the memory position of species having 'genome_id'. If there is no species, return -1.

  int TrophicCount(); //Count trophic levels. This routine was called from numTrophSpecies.
  bool trophic_level_counted_flag; //When trophic level and sum_trophic have been counted, true. Else false.
  int sum_trophic[10]; //sum_trophic[i] means the number of species belonging to trophic_level i
  bool Overmaxspecies();

  std::string jumyofilename;
};

#endif
