#include "classTNANoisedMap.hpp"
#include "classTNANMMutation.hpp"

//#define DEBUG

//------------------------------------
classTNANMMutation::classTNANMMutation( double t_fecundity, double t_N_0, double t_noise_strength,
					double t_mij_max, bool t_uniform_or_triangle,
					int t_l_genome, int t_log2_allel, double t_mutation_rate, unsigned long long t_seed)
  : classTNANoisedMap( t_fecundity, t_N_0, t_noise_strength, t_seed) {
  uniform_or_triangle = t_uniform_or_triangle;
  mij_max = t_mij_max;
  l_genome = t_l_genome;
  l_allel = t_log2_allel;
  mutation_rate = t_mutation_rate;
  total_species = 1;
  for( int i=0; i<(l_genome*l_allel); i++) { total_species *= 2; }
  //--------------------
  AllocateArrays();
  SetCoefficients();
  SetInitialState();
}

//------------------------------------
void classTNANMMutation::AllocateArrays() {
  if( uniform_or_triangle ) {
    mij_matrix = new double*[total_species];
    for( long i=0; i<total_species; i++) mij_matrix[i] = new double[total_species];
    mij_x = NULL; mij_y = NULL;
  }
  else {
    mij_x = new double [3*total_species];
    mij_y = new double [3*total_species];
    mij_matrix = NULL;
  }
}

//------------------------------------
void classTNANMMutation::SetCoefficients() {
  if( uniform_or_triangle ) {
    for( long i=0; i<total_species; i++) {
      for( long j=0; j<total_species; j++) {
	if( i==j ) mij_matrix[i][j] = 0.0;
	else mij_matrix[i][j] = (2.0*rnd.genrand64_real3()-1.0) * mij_max;
      }
    }
  }
  else {
    for( long i=0; i< 3*total_species; i++) {
      mij_x[i] = rnd.genrand64_real3() * mij_max;
      mij_y[i] = rnd.genrand64_real3() * mij_max;
    }
  }
}


//------------------------------------
void classTNANMMutation::SetInitialState() {
  long initial = static_cast<long>( rnd.genrand64_bit( l_genome*l_allel) );
  int invader_number = ZeroPoint();
  AddNewSpecies( invader_number, initial);
  x[invader_number] = 100.0;
  CalcXtot();
  
}

//------------------------------
classTNANMMutation::~classTNANMMutation() {
  delete [] mij_matrix;
  delete [] mij_x;
  delete [] mij_y;
}

//------------------------------------
bool classTNANMMutation::UpdateMutate() {
  Update();
  return Mutate();
}

//------------------------------------
bool classTNANMMutation::Mutate() {
  if( x_tot <= 0 ) {
#ifdef DEBUG
    std::cerr << std::endl << std::endl << "Invasion started here." << std::endl << std::endl;
#endif
    Invade(); return false;} //there is no species
#ifdef DEBUG
  std::cerr << std::endl <<std::endl << "Mutation started here." << std::endl<< "Current configurations are:"<<std::endl;
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    std::cerr << id[i] << "\t" << x[i] << std::endl;
  }
#endif


#ifdef BASIC
  static const double prob_mutation_gene = mutation_rate / l_genome;
  for( int i=0; i<species; i++) { //loop for species
    if(!alive[i]) continue;
    for( int j=0; j<x[i]+0.5; j++) { //loop for individual who belongs to species i.
      long mutant_id = id[i];
      for( int k=0; k<l_genome; k++) { // loop for genome string
	if( rnd.genrand64_real3() < prob_mutation_gene) mutant_id = MutantID( mutant_id, k);
      }
      if(mutant_id != id[i]) { //j'th individual mutates to the species whose id is mutant_id.
	int mutant_number = SpeciesNumber(mutant_id); // if there is no species(mutants_id), mutant_number = -1.
	if( mutant_number < 0 ) {
	  mutant_number = ZeroPoint();
	  AddNewSpecies( mutant_number, mutant_id );
	}
	mutQueue.push(i);
	mutQueue.push(mutant_number);
      }
    }
  }
#endif

#ifndef BASIC
  static const double dev = 1.0 / log(1.0 - mutation_rate/l_genome);
  long gen = static_cast<long>( log(rnd.genrand64_real3())*dev );
  long mut_ind = gen / l_genome;
  long count = 0;
  for( int i=0; i<species; i++) {
    if( !alive[i]) continue;
    count += static_cast<long>(x[i]+0.5);
    while( mut_ind < count) { //species i is going to mutate
      long mutant_id = MutantID( id[i], (gen%l_genome) );
      gen +=  static_cast<long>( log(rnd.genrand64_real3())*dev ) + 1;
      while( mut_ind == (gen/l_genome) ) { //checking for multi-bit flip
	mutant_id = MutantID( mutant_id, (gen%l_genome) );
	gen += static_cast<long>( log(rnd.genrand64_real3())*dev ) + 1;
      }
      int mutant_number = SpeciesNumber( mutant_id); //searching for a mutant number
      if( mutant_number < 0 ) {
	mutant_number = ZeroPoint();
	AddNewSpecies( mutant_number, mutant_id );
      }
      mutQueue.push(i); mutQueue.push(mutant_number);
      mut_ind = gen / l_genome;
    }
  }  
#endif

  while( !mutQueue.empty() ) { //Set populations
    int p_species = mutQueue.front();
    mutQueue.pop();
    int m_species = mutQueue.front();
    mutQueue.pop();
#ifdef DEBUG
    std::cerr << "parent_id & mutant_id:\t" << id[p_species] << ' ' << id[m_species] << std::endl;
#endif
    SetInitialPop( p_species, m_species);
  }
  
  for( int i=0; i<species; i++) {
    if( x[i] <= threshold ) Extinct(i);
  }
  CalcXtot();

  return true;
}

//------------------------------------
long classTNANMMutation::MutantID( long id, int flipping_genome) {
  if( l_allel == 1) return id^(1<<flipping_genome);
  unsigned long mask = 0;
  while( mask == 0) { mask = rnd.genrand64_bit(l_allel);}
  mask = mask<<(flipping_genome*l_allel);
  return id^mask;
}

//------------------------------------
int classTNANMMutation::ZeroPoint() {
  int temp = species;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) temp = i;
  }
  if( temp == max_species ) throw 10;

  if(temp == species) species++;

  if( species > current_max_species ) ReallocateMemory();
  
  return temp;
}

//------------------------------------
void classTNANMMutation::AddNewSpecies(int mutant_number, long mutant_id) {
  x[mutant_number] = 0.0;
  id[mutant_number] = mutant_id;
  alive[mutant_number] = true;
  age[mutant_number] = 0;

  if( uniform_or_triangle ) {
    for( int i=0; i<species; i++) {
      if(!alive[i] || i==mutant_number) continue;
      m[i][mutant_number] = mij_matrix[id[i]][mutant_id];
      m[mutant_number][i] = mij_matrix[mutant_id][id[i]];
    }
  }
  else {
    for( int i=0; i<species; i++) {
      if(!alive[i] || i==mutant_number) continue;
      long id1 = id[i];
      long id2 = mutant_id;
      long k = id1 ^ id2;
      m[i][mutant_number] = (mij_x[k+2*(id1+1)] + mij_y[k+2*(id2+1)] - 1.0);
      m[mutant_number][i] = (mij_x[k+2*(id2+1)] + mij_y[k+2*(id1+1)] - 1.0);
    }
  }
  m[mutant_number][mutant_number] = 0.0;

#ifdef DEBUG
  std::cerr << std::endl << "mutant_id :\t" << mutant_id << std::endl
	    << "mutant_number:\t" << mutant_number << std::endl
  for( int i=0; i<species; i++) {
    if(!alive[i] ) continue;
    std::cerr << "id:\t"<<id[i]<<std::endl;
    for( int j=0; j<species; j++) {
      if(!alive[j]) continue;
      std::cerr << m[i][j] << ' ';
    }
    std::cerr << std::endl;
  }
#endif  
}

//------------------------------------
void classTNANMMutation::SetInitialPop(int parent_number, int mutant_number) {
  x[mutant_number] += 1.0;
  x[parent_number] -= 1.0;
}

//-------------------------------------
void classTNANMMutation::Invade() {
  long invader_id = static_cast<long>(rnd.genrand64_bit(l_genome*l_allel));
  int invader_number = SpeciesNumber(invader_id); // if there is no species(mutants_id), mutant_number = -1.
  if( invader_number < 0 ) {
    invader_number = ZeroPoint();
    AddNewSpecies( invader_number, invader_id );
  }
  x[invader_number] += 1;
  CalcXtot();
}
