#include "classTNANoisedMap.hpp"

//---------------------------------------------
classTNANoisedMap::classTNANoisedMap( double t_fecundity, double t_N_0,
				      double t_noise_strength, unsigned long long t_seed) {
  max_species = 1000;
  fecundity = t_fecundity;
  N_0 = t_N_0;
  noise_strength = t_noise_strength;
  seed = t_seed;
  rnd.init_genrand64(seed);
  
  if( max_species <= 1000 ) current_max_species = max_species;
  else current_max_species = 1000;

  species = 0;
  threshold = 0.5;
  
  AllocateMemory(current_max_species);

  lspan_vec_size = 1000;
  lspan_all.assign(lspan_vec_size,0);
}
//---------------------------------------------
classTNANoisedMap::classTNANoisedMap() {
  x = NULL;
  age = id = NULL;
  reproduction_prob = NULL;
  m = NULL;
  alive = NULL;
  
  species = 0;
  fecundity = 0;
  N_0 = 0.0;
  noise_strength = 0.0;
  seed = 0;
  max_species = current_max_species = 0;
  x_tot = x_tot_inv = 0.0;
}  
//---------------------------------------------
void classTNANoisedMap::AllocateMemory( int size) {
  x = new double[size];
  m = new double* [size];
  for( int j=0; j<size; j++) m[j] = new double[size];
  reproduction_prob = new double [size];
  age = new long [size];
  alive = new bool [size];
  id = new long[size];
  
  for( int i=0; i<size; i++) {
    x[i] = 0.0;
    CalcXtot();
    for( int j=0; j<size; j++) m[i][j] = 0.0;
    age[i] = 0;
    alive[i] = false;
    id[i] = -1;
  }
}

//---------------------------------------------
void classTNANoisedMap::ReallocateMemory() {

  int size = current_max_species + 500;

  if( size > max_species ) size = max_species;
  double * t_x = new double[size];
  double ** t_m = new double* [size];
  for( int j=0; j<size; j++) t_m[j] = new double[size];
  double * t_reproduction_prob = new double [size];
  long * t_age = new long [size];
  bool * t_alive = new bool [size];
  long * t_id = new long[size];
  
  for( int i=0; i<size; i++) {
    t_x[i] = 0.0;
    for( int j=0; j<size; j++) t_m[i][j] = 0.0;
    t_reproduction_prob[i] = 0.0;
    t_age[i] = 0;
    t_alive[i] = false;
    t_id[i] = -1;
  }
  
  for( int i=0; i<current_max_species; i++) {
    t_x[i] = x[i];
    for( int j=0; j<current_max_species; j++) { t_m[i][j] = m[i][j];}
    t_reproduction_prob[i] = reproduction_prob[i];
    t_age[i] = age[i];
    t_alive[i] = alive[i];
    t_id[i] = id[i];
  }

  ReleaseMemory();

  x = t_x; 
  m = t_m;
  reproduction_prob = t_reproduction_prob;
  age = t_age;
  alive = t_alive;
  id = t_id;
  
  current_max_species += 500;
}
//---------------------------------------------
classTNANoisedMap::~classTNANoisedMap() {
  ReleaseMemory();
}

//----------------------------------------------
void classTNANoisedMap::ReleaseMemory() {
  delete [] x; 
  for( int i=0; i<current_max_species; i++) delete [] m[i];
  delete [] m;
  delete [] reproduction_prob;
  delete [] age;
  delete [] alive;
  delete [] id;
}

//---------------------------------------------
void classTNANoisedMap::Update() {
  CalcXtot();
  CalcPi();
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    double p = reproduction_prob[i];
    x[i] = fecundity * ( p * x[i] + noise_strength*sqrt(x[i]*p*(1.0-p))*rnd.genrand64_nd() );
    if( x[i] <= threshold ) Extinct(i);
    else age[i]++;
  }
  CalcXtot();
  trophic_level_counted_flag = false;
}

//--------------------------------------------
void classTNANoisedMap::CalcPi() {
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    reproduction_prob[i] = ReproductionProb(i);
  }
}

//---------------------------------------------
void classTNANoisedMap::CalcXtot() {
  x_tot = 0;
  for( int j=0; j<species; j++) { x_tot += x[j]; }
  x_tot_inv = 1.0/static_cast<double>(x_tot);
}
//---------------------------------------------
double classTNANoisedMap::ReproductionProb(int i) const {
  double inter = 0.0;
  for( int j=0; j<species; j++) {
    if(!alive[j]) continue;
    inter += m[i][j]*x[j];
  }
  double delta = inter * x_tot_inv - x_tot/N_0;
  return 1.0 / ( 1.0 + exp(-delta) );
}

//---------------------------------------------
void classTNANoisedMap::Extinct(int i) {
  if( age[i] < 1 ) {} //skip if life span is less than 1 generation.
  else if(age[i]<lspan_vec_size) { //count of life span
    lspan_all[age[i]]++;
  }
  else {
    lspan_all_map[age[i]]++;
  }
  
  alive[i] = false;
  x[i] = 0.0;
  for(int j=0; j<species; j++) {
    m[i][j] = 0.0;
    m[j][i] = 0.0;
  }
  reproduction_prob[i] = 0.0;

  age[i] = 0;
  id[i] = -1;
}

//---------------------------------------------
int classTNANoisedMap::numSurvivor() const{
  int survivor = 0;
  for( int i=0; i< species; i++) {
    if( alive[i] ) survivor++;
  }
  return survivor;
}

//---------------------------------------------
double classTNANoisedMap::expSWindexAll() const{
  double diversity = x_tot;
  if( x_tot == 0 ) return 0.0;
  for( int i=0; i<species; i++) {
    if( !alive[i] ) continue;
    diversity *= pow( (double)x[i], -(double)x[i]*x_tot_inv);
  }
  return diversity;
}

//---------------------------------------
void classTNANoisedMap::Xoutput( std::ofstream & t_fout ) {
  for( int i=0; i<species; i++) {
    //    if( !alive[i]) continue;
    t_fout << x[i] << ' ';
  }
  t_fout << std::endl;
}

//---------------------------------------
void classTNANoisedMap::Xoutput( const char* filename ) {
  std::ofstream fout( filename );
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    fout << x[i] << ' ' << age[i] << ' ' << id[i] << std::endl;
  }
  fout.close();
}

//---------------------------------------
void classTNANoisedMap::Moutput( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=0; j<species; j++) {
      if(!alive[j]) continue;
      fout << m[i][j] << ' ';
    }
    fout << std::endl;
  }

  fout.close();
}

//---------------------------------------
int classTNANoisedMap::SpeciesNumber( long genome_id ) {
  int num = -1;
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    if( id[i] == genome_id ) {
      num = i;
      break;
    }
  }
  return num;
}

//--------------------------------------
void classTNANoisedMap::GenomeOutput( std::ofstream & genout, long time) {
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    if( x[i] >= 10) genout << time << ' ' << id[i] << std::endl;
  }
}

//-------------------------------------
void classTNANoisedMap::LspanOutput( const char* filename) {
  std::ofstream lout( filename);
  for( long i=1; i<lspan_vec_size; i++) {
    lout << i << ' ' << lspan_all[i] << std::endl;
  }
  std::map<long,long>::iterator iter;
  for( iter = lspan_all_map.begin(); iter!=lspan_all_map.end(); iter++) {
    lout << iter->first << ' ' << iter->second << std::endl;
  }

  lspan_all.assign(lspan_vec_size,0);
  lspan_all_map.clear();
}
