//#define DEBUG
//#define BASIC
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include "classTNANoisedMap.cpp"
#include "classTNANMMutation.cpp"
#include "mtrand64.cpp"


void output( classTNANMMutation& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, std::ofstream& genout, long time, std::string id);
void output_final( classTNANMMutation & eco, std::string id);

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 12 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <1:noise_strength> <2:fecundity> <3:N_0> <4:uniform?> "
	      <<"<5:genome_length> <6:log2_allel> <7:mut_rate> <8:samp_interval> <9:t_end(sec)> <10:seed> <11:ID>" << std::endl
	      << std::endl;
    throw 1;
  }
  
  double noise_strength = boost::lexical_cast<double>( argv[1] );
  double fecundity = boost::lexical_cast<double>( argv[2] );
  double N_0 = boost::lexical_cast<double>( argv[3] );
  bool uniform_or_triangle = boost::lexical_cast<bool>( argv[4] ); //true:uniform, false:triangle (When uniform, matrix is stored in memory.)
  double mij_max = 1.0;
  int l_genome = boost::lexical_cast<int>( argv[5] ); // each genome has l_genome genes.
  int log2_allel = boost::lexical_cast<int>( argv[6] ); //each gene has a l_allel. Number of potential species is 2**(log2_allel*l_genome).
  double mutation_rate = boost::lexical_cast<double>( argv[7] );
  long sampling_interval = boost::lexical_cast<long>( argv[8] );
  long t_end = boost::lexical_cast<long>( argv[9] );
  unsigned long long seed = boost::lexical_cast<unsigned long long>( argv[10] );
  std::string id = argv[11];

  std::cerr << "Lists of given parameters are as follows:" << std::endl
	    << "noise_strength:\t" << noise_strength << std::endl
	    << "fecundity:\t" << fecundity << std::endl
	    << "N_0:\t" << N_0 << std::endl
	    << "uniform_or_triangle:\t";
  if( uniform_or_triangle ) std::cerr << "uniform" << std::endl;
  else std::cerr << "triangle" << std::endl;
  std::cerr << "mij_max:\t" << mij_max << std::endl
	    << "l_genome:\t" << l_genome << std::endl
	    << "log2_allel:\t" << log2_allel << std::endl
	    << "mutation rate:\t" << mutation_rate << std::endl
	    << "sampling_interval:\t" << sampling_interval << std::endl
	    << "t_end:\t" << t_end << std::endl
	    << "seed:\t" << seed << std::endl
	    << "id:\t" << id << std::endl;

  if( noise_strength < -0.01 || fecundity <= 1.0 || N_0 <= 0.0 ||
      mij_max <= 0.0 || l_genome <= 0 || t_end < 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    throw 1;
  }

  //ofstreams
  std::string sname = id + ".suv";
  std::ofstream suvout(sname.c_str());

  std::string divname = id + ".diversity";
  std::ofstream divout(divname.c_str());

  std::string bname = id + ".biomass";
  std::ofstream bmassout(bname.c_str());

  std::string gname = id +".genome";
  std::ofstream genout(gname.c_str());
  
  classTNANMMutation eco( fecundity, N_0, noise_strength, mij_max, uniform_or_triangle,
			  l_genome, log2_allel, mutation_rate, seed);

#ifdef BENCH
  clock_t start = clock();
#endif

  //============== main loop ========================
  for( long t = 0; t < t_end; t++) {
    eco.UpdateMutate();
    if( (t%sampling_interval)==0 ) output( eco, suvout, divout, bmassout, genout, t, id);
  }
  //============== end of main loop =================
  output_final(eco,id);
#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  return 0;
}



//--------------------    
void output( classTNANMMutation& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, std::ofstream& genout, long time, std::string id) {
  //======== output ===============
  int survivor = eco.numSurvivor();
  suvout << time << ' ' << survivor << std::endl;

  divout << time << ' ' << eco.expSWindexAll() << std::endl;
  
  bmassout << time << ' ' << eco.TotalBiomass() << std::endl;

  if( (time%8192) == 0 ) {
    eco.GenomeOutput(genout,time);
  }

  if( (time%1048576) == 0 ) {
    std::string temp = boost::io::str( boost::format("%04d") % static_cast<int>(time/1048576) );
    std::string afilename = id + ".a" + temp;
    std::string xfilename = id + ".x" + temp;
    eco.Moutput(afilename.c_str());
    eco.Xoutput(xfilename.c_str());
  }
}

//-----------------------
void output_final( classTNANMMutation & eco, std::string id){
  std::string afilename = id + ".afinal";
  std::string xfilename = id + ".xfinal";
  std::string lspanfilename = id + ".lspan";
  eco.Xoutput(xfilename.c_str());
  eco.Moutput(afilename.c_str());
  eco.LspanOutput(lspanfilename.c_str());
}
//----------------------end of the program-------------------------------
