#include <cmath>
#include <queue>
#include "classTNANoisedMap.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_A_NOISEDMAP_MUTATION_H_
#define _C_TN_A_NOISEDMAP_MUTATION_H_

class classTNANMMutation : public classTNANoisedMap {
public:
  classTNANMMutation( double t_fecundity, double t_N_0, double t_noise_strength,
		      double t_mij_max, bool t_uniform_or_triangle,
		      int t_l_genome, int t_log2_allel, double t_mutation_rate, unsigned long long t_seed);
  ~classTNANMMutation();
  bool UpdateMutate(); //update dynamics 1 generation and mutate species. If there is no species, invasion is applied insteady of mutation and returns false.
protected:
  void SetInitialState(); //choose single species and 100 individuals of the species are injected
  bool Mutate();
  void Invade(); //randomly chosen species are added to the system.
  int l_genome, l_allel;
  long total_species; // number of potential species ( = 2^(l_genome*l_allel) )
  bool uniform_or_triangle; //true:uniform, false: triangle
  double mij_max, mutation_rate;
  double **mij_matrix;
  double *mij_x, *mij_y;
  void AllocateArrays();
  void SetCoefficients();
  long MutantID( long id, int flipping_genome);
  long SelectMutant( int parent_number);
  int ZeroPoint();
  void AddNewSpecies( int mutant_number, long mutant_id);
  void SetInitialPop( int parent_number, int mutant_number );
  std::queue<int> mutQueue; //used in Mutate method
};


#endif
