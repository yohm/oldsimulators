//#define DEBUG
//#define BASIC
#include <iostream>
#include <fstream>
#include <ctime>
#include <boost/lexical_cast.hpp>
#include "classTNNoisedMap.cpp"
#include "classTNNMMutation.cpp"
#include "mtrand64.cpp"


void output( classTNNMMutation& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, std::ofstream& genout, long time, char* id);
void output_final( classTNNMMutation & eco, char* id);

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 12 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <resource> <noise_strength> <eta_prob> <mij_prob> <genome_length> <log2_allel> <mut_rate> <samp_interval> <t_end(sec)> <seed> <ID>" << std::endl
	      << std::endl;
    throw 1;
  }
  
  double fecundity = 2.0;
  double resource = boost::lexical_cast<double>( argv[1] );
  double noise_strength = boost::lexical_cast<double>( argv[2]);
  double b_max = 1.0;
  double eta_max = 1.0;
  double eta_prob = boost::lexical_cast<double>( argv[3] );
  double mij_max = 1.0;
  double mij_prob = boost::lexical_cast<double>( argv[4] );
  double mii_max = 1.0;
  int l_genome = boost::lexical_cast<int>( argv[5] ); // each genome has l_genome genes.
  int log2_allel = boost::lexical_cast<int>( argv[6] ); //each gene has a l_allel. Number of potential species is 2**(log2_allel*l_genome).
  double mutation_rate = boost::lexical_cast<double>( argv[7] );
  long sampling_interval = boost::lexical_cast<long>( argv[8] );
  long t_end = boost::lexical_cast<long>( argv[9] );
  unsigned long long seed = boost::lexical_cast<unsigned long long>( argv[10] );
  char* id = argv[11];

  std::cerr << "Lists of given parameters are as follows:" << std::endl
	    << "fecundity:\t" << fecundity << std::endl
	    << "resource:\t" << resource << std::endl
	    << "noise_strength:\t" << noise_strength << std::endl
	    << "b_max:\t" << b_max << std::endl
	    << "eta_max:\t" << eta_max << std::endl
	    << "eta_prob:\t" << eta_prob << std::endl
	    << "mij_max:\t" << mij_max << std::endl
	    << "mij_prob:\t" << mij_prob << std::endl
	    << "mii_max:\t" << mii_max << std::endl
	    << "l_genome:\t" << l_genome << std::endl
	    << "log2_allel:\t" << log2_allel << std::endl
	    << "mutation rate:\t" << mutation_rate << std::endl
	    << "t_end:\t" << t_end << std::endl
	    << "seed:\t" << seed << std::endl
	    << "id:\t" << id << std::endl;

  if( fecundity <= 1.0 || resource <= 0.0 || b_max <= 0.0 || eta_max <= 0.0 ||
      eta_prob <= 0.0 || eta_prob >= 1.0 || mij_max <= 0.0 || mij_prob <= 0.0 ||
      mij_prob >= 1.0 || mii_max <= 0.0 || l_genome <= 0 || t_end <= 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    throw 1;
  }

  //ofstreams
  char suvfilename[20];
  sprintf(suvfilename,"%s.suv",id);
  std::ofstream suvout(suvfilename);

  char divfilename[20];
  sprintf(divfilename,"%s.diversity",id);
  std::ofstream divout(divfilename);


  char bmassfilename[20];
  sprintf(bmassfilename,"%s.biomass",id);
  std::ofstream bmassout(bmassfilename);

  char genomefilename[20];
  sprintf(genomefilename,"%s.genome",id);
  std::ofstream genout(genomefilename);
  
  classTNNMMutation eco( fecundity, resource, noise_strength,
			b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max,
			l_genome, log2_allel, mutation_rate, seed);

#ifdef BENCH
  clock_t start = clock();
#endif

  eco.Invade();

  //============== main loop ========================
  for( long t = 0; t < t_end; t++) {
    eco.UpdateMutate();
    if( (t%sampling_interval)==0 ) output( eco, suvout, divout, bmassout, genout, t, id);
  }
  //============== end of main loop =================
  output_final(eco,id);
#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  return 0;
}



//--------------------    
void output( classTNNMMutation& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, std::ofstream& genout, long time, char* id) {
  static long times_output = 0;
  //======== output ===============
  int survivor = eco.numSurvivor();
  suvout << time << ' ' << survivor;
  for( int i=1; i<5; i++) suvout << ' ' << eco.numTrophSpecies(i);
  suvout << std::endl;
  
  divout << time << ' ' << eco.expSWindexAll() << ' '
	 << eco.expSWindexProducer() << ' ' << eco.expSWindexConsumer() << std::endl;
  
  bmassout << time << ' ' << eco.TotalBiomass();
  for( int i=1; i<5; i++) bmassout << ' ' << eco.TrophicBiomass(i);
  bmassout << std::endl;

  if( (times_output%64) == 0 ) {
    eco.GenomeOutput(genout,time);
  }

  if( (time%1048576) == 0 ) {
    char afilename[20];
    char xfilename[20];
    sprintf( afilename, "%s.a%04d",id, (int)(time/1048576) );
    sprintf( xfilename, "%s.x%04d",id, (int)(time/1048576) );
    eco.X_b_eta_output(xfilename);
    eco.Moutput(afilename);
  }
  times_output++;
}

//-----------------------
void output_final( classTNNMMutation & eco, char* id){
  char afilename[20];
  char xfilename[20];
  char lspanfilename[20];
  sprintf( afilename, "%s.afinal",id);
  sprintf( xfilename, "%s.xfinal",id);
  sprintf( lspanfilename, "%s.lspan",id);
  eco.X_b_eta_output(xfilename);
  eco.Moutput(afilename);
  eco.LspanOutput(lspanfilename);
}  

//----------------------end of the program-------------------------------
