#include <cmath>
#include <queue>
#include "classTNNoisedMap.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_NOISEDMAP_MUTATION_H_
#define _C_TN_NOISEDMAP_MUTATION_H_

class classTNNMMutation : public classTNNoisedMap {
public:
  classTNNMMutation( double t_fecundity, double t_resource, double t_noise_strength,
		     double t_b_max, double t_eta_max, double t_eta_prob,
		     double t_mij_max, double t_mij_prob, double t_mii_max,
		     int t_l_genome, int t_log2_allel, double t_mutation_rate, unsigned long long t_seed);
  ~classTNNMMutation();
  bool UpdateMutate(); //update dynamics 1 generation and mutate species. If there is no species, invasion is applied insteady of mutation and returns false.
  void Invade(); //randomly chosen species are added to the system.
protected:
  bool Mutate();
  int l_genome, l_allel;
  long total_species; // number of potential species ( = 2^(l_genome*l_allel) )
  double b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max, mutation_rate;
  double *b_array, *eta_array, *mij_x, *mij_y, *mii_array;
  bool *prod_array, *connect_x, *connect_y;
  void AllocateArrays();
  void SetCoefficients();
  long MutantID( long id, int flipping_genome);
  long SelectMutant( int parent_number);
  int ZeroPoint();
  void AddNewSpecies( int mutant_number, long mutant_id);
  void SetInitialPop( int parent_number, int mutant_number );
  std::queue<int> mutQueue; //used in Mutate method
};


#endif
