#include "classTNNoisedMap.hpp"

//---------------------------------------------
classTNNoisedMap::classTNNoisedMap( double t_fecundity, double t_resource,
				    double t_noise_strength, unsigned long long t_seed) {
  max_species = 2000;
  fecundity = t_fecundity;
  resource = t_resource;
  noise_strength = t_noise_strength;
  seed = t_seed;
  rnd.init_genrand64(seed);
  
  if( max_species <= 1000 ) current_max_species = max_species;
  else current_max_species = 1000;

  species = 0;
  threshold = 0.5;
  
  AllocateMemory(current_max_species);

  lspan_vec_size = 1000;
  lspan_all.assign(lspan_vec_size,0);
  lspan_prod.assign(lspan_vec_size,0);
}
//---------------------------------------------
classTNNoisedMap::classTNNoisedMap() {
  x = NULL;
  trophic_level = age = id = NULL;
  b = eta = reproduction_prob = NULL;
  m = NULL;
  alive = producer = NULL;
  
  species = 0;
  fecundity = 0;
  resource = 0.0;
  noise_strength = 0.0;
  seed = 0;
  max_species = current_max_species = 0;
  x_tot = x_tot_inv = 0.0;
  trophic_level_counted_flag = false;
  for( int i=0; i<10; i++) { sum_trophic[i] = 0;}
}  
//---------------------------------------------
void classTNNoisedMap::AllocateMemory( int size) {
  x = new double[size];
  m = new double* [size];
  for( int j=0; j<size; j++) m[j] = new double[size];
  b = new double [size];
  eta = new double [size];
  reproduction_prob = new double [size];
  trophic_level = new long [size];
  age = new long [size];
  alive = new bool [size];
  producer = new bool [size];
  id = new long[size];
  
  for( int i=0; i<size; i++) {
    x[i] = 0.0;
    CalcXtot();
    for( int j=0; j<size; j++) m[i][j] = 0.0;
    b[i] = eta[i] = 0.0;
    trophic_level[i] = age[i] = 0;
    alive[i] = producer[i] = false;
    id[i] = -1;
  }
}

//---------------------------------------------
void classTNNoisedMap::ReallocateMemory() {
  int size = current_max_species + 500;

  if( size > max_species ) size = max_species;
  double * t_x = new double[size];
  double ** t_m = new double* [size];
  for( int j=0; j<size; j++) t_m[j] = new double[size];
  double * t_b = new double [size];
  double * t_eta = new double [size];
  double * t_reproduction_prob = new double [size];
  long * t_trophic_level = new long [size];
  long * t_age = new long [size];
  bool * t_alive = new bool [size];
  bool * t_producer = new bool [size];
  long * t_id = new long[size];
  
  for( int i=0; i<size; i++) {
    t_x[i] = 0.0;
    for( int j=0; j<size; j++) t_m[i][j] = 0.0;
    t_b[i] = t_eta[i] = t_reproduction_prob[i] = 0.0;
    t_trophic_level[i] = t_age[i] = 0;
    t_alive[i] = t_producer[i] = false;
    t_id[i] = -1;
  }
  
  for( int i=0; i<current_max_species; i++) {
    t_x[i] = x[i];
    for( int j=0; j<current_max_species; j++) { t_m[i][j] = m[i][j];}
    t_b[i] = b[i];
    t_eta[i] = eta[i];
    t_reproduction_prob[i] = reproduction_prob[i];
    t_trophic_level[i] = trophic_level[i];
    t_age[i] = age[i];
    t_alive[i] = alive[i];
    t_producer[i] = producer[i];
    t_id[i] = id[i];
  }

  ReleaseMemory();

  x = t_x; 
  m = t_m;
  b = t_b;
  eta = t_eta;
  reproduction_prob = t_reproduction_prob;
  trophic_level = t_trophic_level;
  age = t_age;
  alive = t_alive;
  producer = t_producer;
  id = t_id;

  current_max_species += 500;
}
//---------------------------------------------
classTNNoisedMap::~classTNNoisedMap() {
  ReleaseMemory();
}

//----------------------------------------------
void classTNNoisedMap::ReleaseMemory() {
  delete [] x; 
  for( int i=0; i<current_max_species; i++) delete [] m[i];
  delete [] m;
  delete [] b;  delete [] eta; delete [] reproduction_prob;
  delete [] trophic_level;  delete [] age;
  delete [] alive;  delete [] producer;
  delete [] id;
}

//---------------------------------------------
void classTNNoisedMap::Update() {
  CalcXtot();
  CalcPi();
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    double p = reproduction_prob[i];
    x[i] = fecundity * ( p * x[i] + noise_strength*sqrt(x[i]*p*(1.0-p))*rnd.genrand64_nd() );
    if( x[i] <= threshold ) Extinct(i);
    else age[i]++;
  }
  CalcXtot();
  trophic_level_counted_flag = false;
}

//--------------------------------------------
void classTNNoisedMap::CalcPi() const{
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    reproduction_prob[i] = ReproductionProb(i);
  }
}

//---------------------------------------------
void classTNNoisedMap::CalcXtot() {
  x_tot = 0;
  for( int j=0; j<species; j++) { x_tot += x[j]; }
  x_tot_inv = 1.0/static_cast<double>(x_tot);
}
//---------------------------------------------
double classTNNoisedMap::ReproductionProb(int i) const {
  double delta = -b[i];
  double inter = eta[i] * resource;
  for( int j=0; j<species; j++) {
    if(!alive[j]) continue;
    inter += m[i][j]*x[j];
  }
  delta = delta + inter * x_tot_inv;
  return 1.0 / ( 1.0 + exp(-delta) );
}

//---------------------------------------------
void classTNNoisedMap::Extinct(int i) {
  if( age[i] < 1 ) {} //skip if life span is less than 1 generation.
  else if(age[i]<lspan_vec_size) { //count of life span
    lspan_all[age[i]]++;
    if(producer[i]) lspan_prod[age[i]]++;
  }
  else {
    lspan_all_map[age[i]]++;
    if(producer[i]) lspan_prod_map[age[i]]++;
  }
  
  alive[i] = false; producer[i] = false;
  x[i] = 0.0;
  for(int j=0; j<species; j++) {
    m[i][j] = 0.0;
    m[j][i] = 0.0;
  }
  b[i] = 0.0; eta[i] = 0.0;  reproduction_prob[i] = 0.0;
  trophic_level[i] = 0;
  age[i] = 0;
  id[i] = -1;
}

//---------------------------------------------
int classTNNoisedMap::numSurvivor() const{
  int survivor = 0;
  for( int i=0; i< species; i++) {
    if( alive[i] ) survivor++;
  }
  return survivor;
}

//---------------------------------------------
int classTNNoisedMap::numProducer() const{
  int num_producer = 0;
  for( int i=0; i<species; i++) {
    if( alive[i] && producer[i] ) num_producer++;
  }
  return num_producer;
}

//---------------------------------------------
int classTNNoisedMap::numConsumer() const{
  int num_consumer = 0;
  for( int i=0; i<species; i++) {
    if( alive[i] && (!producer[i]) ) num_consumer++;
  }
  return num_consumer;
}

//---------------------------------------------
double classTNNoisedMap::expSWindexAll() const{
  double diversity = x_tot;
  if( x_tot == 0 ) return 0.0;
  for( int i=0; i<species; i++) {
    if( !alive[i] ) continue;
    diversity *= pow( (double)x[i], -(double)x[i]*x_tot_inv);
  }
  return diversity;
}

//---------------------------------------------
double classTNNoisedMap::expSWindexProducer() const{
  double total = ProducerBiomass();
  double total_inv = 1.0 / total;
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=0; i<species; i++) {
    if( alive[i] && producer[i] ) {
      diversity *= pow( (double)x[i], -(double)x[i]*total_inv);
    }
  }
  return diversity;
}

//---------------------------------------------
double classTNNoisedMap::expSWindexConsumer() const{
  double total = ConsumerBiomass();
  double total_inv = 1.0 / total;
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=0; i<species; i++) {
    if( alive[i] && (!producer[i]) ) {
      diversity *= pow( (double)x[i], -(double)x[i]*total_inv);
    }
  }
  return diversity;
}

//---------------------------------------------
int classTNNoisedMap::numTrophSpecies(int t_level) {
  if( !trophic_level_counted_flag ) { TrophicCount();}
  return sum_trophic[t_level];
}
//---------------------------------------------
double classTNNoisedMap::expSWindexLevels(int t_level) {
  if( !trophic_level_counted_flag ) {TrophicCount();}
  double total = TrophicBiomass( t_level);
  double total_inv = 1.0 / total;
  if( total == 0.0 ) return 0.0;
  double diversity = total;
  for( int i=0; i<species; i++) {
    if( alive[i] && (trophic_level[i] == t_level) ) {
      diversity *= pow( (double)x[i], -(double)x[i]*total_inv);
    }
  }
  return diversity;
}
//---------------------------------------------
int classTNNoisedMap::TrophicCount() {
  bool finished = true;  //after compeletion of judgement : true

  for(int i=0; i<species; i++) {
    trophic_level[i] = 0;
  } //initialize

  //level 1 (producer) -----------------
  for(int i=0; i<species; i++) {
    if( alive[i] && producer[i] ) trophic_level[i] = 1;
    if( alive[i] && (!producer[i]) ) finished=false;
  }

  int max_level = 1;

  //level2~ --------------------------
  //level(k+1)の判定 -----------------
  for(int k=1; !finished && k<9; k++) {

    for(int i=0; i<species; i++) {
      if(trophic_level[i] == k) {
	for(int j=0; j<species; j++) {
	  if(m[i][j] <0 && alive[j] && trophic_level[j]==0) {
	    trophic_level[j] = k+1;
	  }
	}
      }
    }

    //全種完了したか確かめる(完了していなかったらfinished=false)
    finished=true;
    for(int i=0; i<species; i++) {
      if(trophic_level[i]==0 && alive[i]) {
	finished=false;
	break;
      }
    }
    max_level = k+1;
  }

  //trophic levelの種数のカウント
  if( max_level > 9 ) { std::cerr << "Trophic level exceeds 9."<<std::endl; throw(1);}
  for( int i=0; i<10; i++) sum_trophic[i] = 0;

  for( int i=0; i<species; i++) {
    if( alive[i] ) sum_trophic[ trophic_level[i] ]++;
  }

  trophic_level_counted_flag = true;
  return max_level;
}

//---------------------------------------
void classTNNoisedMap::Xoutput( std::ofstream & t_fout ) {
  for( int i=0; i<species; i++) {
    //    if( !alive[i]) continue;
    t_fout << x[i] << ' ';
  }
  t_fout << std::endl;
}

//---------------------------------------
void classTNNoisedMap::X_b_eta_output( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    fout << x[i] << ' ' << b[i] << ' ' << eta[i] << ' ' << age[i] << ' ' << id[i] << std::endl;
  }
  fout.close();
}

//---------------------------------------
void classTNNoisedMap::Moutput( const char* filename ) {
  std::ofstream fout( filename);
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=0; j<species; j++) {
      if(!alive[j]) continue;
      fout << m[i][j] << ' ';
    }
    fout << std::endl;
  }

  fout.close();
}

//---------------------------------------
double classTNNoisedMap::ProducerBiomass() const{
  double temp = 0;
  for( int i=0; i<species; i++) {
    if(alive[i] && producer[i]) temp += x[i];
  }
  return temp;
}
//---------------------------------------
double classTNNoisedMap::ConsumerBiomass() const {
  double temp = 0;
  for( int i=0; i<species; i++) {
    if(alive[i] && (!producer[i]) ) temp += x[i];
  }
  return temp;
}

//---------------------------------------
double classTNNoisedMap::TrophicBiomass(int t_level) {
  double temp = 0;
  if(!trophic_level_counted_flag) { TrophicCount();}
  
  for( int i=0; i<species; i++) {
    if( alive[i] && (trophic_level[i]==t_level) ) temp += x[i];
  }
  return temp;
}


//---------------------------------------
int classTNNoisedMap::SpeciesNumber( long genome_id ) {
  int num = -1;
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    if( id[i] == genome_id ) {
      num = i;
      break;
    }
  }
  return num;
}

//--------------------------------------
void classTNNoisedMap::GenomeOutput( std::ofstream & genout, long time) {
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    if( x[i] >= 10) genout << time << ' ' << id[i] << std::endl;
  }
}

//-------------------------------------
void classTNNoisedMap::LspanOutput( const char* filename) {
  std::ofstream lout( filename);
  for( long i=1; i<lspan_vec_size; i++) {
    lout << i << ' ' << lspan_all[i] << ' ' << lspan_prod[i] << ' ' << lspan_all[i] - lspan_prod[i] <<std::endl;
  }
  std::map<long,long>::iterator iter;
  for( iter = lspan_all_map.begin(); iter!=lspan_all_map.end(); iter++) {
    long key = iter->first;
    long value = iter->second;
    lout << key << ' ' << value << ' ' << lspan_prod_map[key] << ' ' << value - lspan_prod_map[key] << std::endl;
  }

  lspan_all.assign(lspan_vec_size,0);
  lspan_prod.assign(lspan_vec_size,0);
  lspan_all_map.clear();
  lspan_prod_map.clear();
}
