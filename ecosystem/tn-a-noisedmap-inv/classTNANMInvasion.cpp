#include "classTNANoisedMap.hpp"
#include "classTNANMInvasion.hpp"

//#define DEBUG

//------------------------------------
classTNANMInvasion::classTNANMInvasion( double t_fecundity, double t_N_0, double t_noise_strength,
					double t_mij_max, unsigned long long t_seed)
  : classTNANoisedMap( t_fecundity, t_N_0, t_noise_strength, t_seed) {
  mij_max = t_mij_max;
  //--------------------
}

//------------------------------------
classTNANMInvasion::~classTNANMInvasion() {
}

//------------------------------------
void classTNANMInvasion::AddInvader() {
  int invader_number = ZeroPoint();
  SetRandomMIJ( invader_number);
  SetInitialPop( invader_number);
}

//------------------------------------
int classTNANMInvasion::ZeroPoint() {
  int temp = species;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) temp = i;
  }
  if( temp == max_species ) throw 10;

  if(temp == species) species++;

  if( species > current_max_species ) ReallocateMemory();
  
  return temp;
}

//------------------------------------
void classTNANMInvasion::SetRandomMIJ(long id) {
  static const double mij_max_32768_inv = mij_max / 32768.0;
  m[id][id] = 0.0;
  for( int j=0; j<species; j++) {
    if(!alive[j] ) continue;
    m[id][j] = (rnd.load_16bit_integer() - 32768) * mij_max_32768_inv;
    m[j][id] = (rnd.load_16bit_integer() - 32768) * mij_max_32768_inv;
  }

}

//------------------------------------
void classTNANMInvasion::SetInitialPop( long invader_number) {
  x[invader_number] = 1.0;
  alive[invader_number] = true;
  age[invader_number] = 0;
  CalcXtot();
}
