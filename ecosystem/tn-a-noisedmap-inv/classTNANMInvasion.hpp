#include <cmath>
#include <queue>
#include "classTNANoisedMap.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_A_NOISEDMAP_INVASION_H_
#define _C_TN_A_NOISEDMAP_INVASION_H_

class classTNANMInvasion : public classTNANoisedMap {
public:
  classTNANMInvasion( double t_fecundity, double t_N_0, double t_noise_strength,
		      double t_mij_max, unsigned long long t_seed);
  ~classTNANMInvasion();
  void AddInvader();
  void UpdateDynamics(long times) { for( long t=0; t<times; t++) Update();}
protected:
  double mij_max;
  int ZeroPoint();
  void SetRandomMIJ( long invader_number);
  void SetInitialPop( long invader_number );
};


#endif
