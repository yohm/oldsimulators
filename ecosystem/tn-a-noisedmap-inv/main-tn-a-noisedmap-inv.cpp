//#define DEBUG
//#define BASIC
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include "classTNANoisedMap.cpp"
#include "classTNANMInvasion.cpp"
#include "mtrand64.cpp"


void output( classTNANMInvasion& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, long time, std::string id);
void output_final( classTNANMInvasion & eco, std::string id);

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 11 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <1:noise_strength> <2:fecundity> <3:N_0> " << std::endl 
	      << "<4:invasion_interval> <5:num_invaders> <6:regular_or_poisson?[1,0]> " << std::endl
	      << "<7:samp_interval> <8:t_end> <9:seed> <10:ID>" << std::endl
	      << std::endl;
    throw 1;
  }
  
  double noise_strength = boost::lexical_cast<double>( argv[1] );
  double fecundity = boost::lexical_cast<double>( argv[2] );
  double N_0 = boost::lexical_cast<double>( argv[3] );
  double mij_max = 1.0;
  long invasion_interval = boost::lexical_cast<long>( argv[4] );
  long num_invaders = boost::lexical_cast<long>( argv[5] );
  bool regular_or_poisson = boost::lexical_cast<bool>( argv[6] ); //true:regular, false:poisson invasion.
  long sampling_interval = boost::lexical_cast<long>( argv[7] );
  unsigned long long t_end = boost::lexical_cast<unsigned long long>( argv[8] );
  unsigned long long seed = boost::lexical_cast<unsigned long long>( argv[9] );
  std::string id = argv[10];

  std::cerr << "Lists of given parameters are as follows:" << std::endl
	    << "noise_strength:\t" << noise_strength << std::endl
	    << "fecundity:\t" << fecundity << std::endl
	    << "N_0:\t" << N_0 << std::endl
	    << "mij_max:\t" << mij_max << std::endl
	    << "invasion_interval:\t" << invasion_interval << std::endl
	    << "num_invaders:\t" << num_invaders << std::endl
	    << "regular_or_poisson:\t";
  if( regular_or_poisson ) std::cerr << "regular invasion" << std::endl;
  else std::cerr << "poisson invasion" << std::endl;
  std::cerr << "sampling_interval:\t" << sampling_interval << std::endl
	    << "t_end:\t" << t_end << std::endl
	    << "id:\t" << id << std::endl;

  if( noise_strength < -0.001 || fecundity <= 1.0 || N_0 <= 0.0 ||
      mij_max <= 0.0 || invasion_interval <= 0 || num_invaders <= 0 ||
      t_end < 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    throw 1;
  }

  //ofstreams
  std::string sname = id + ".suv";
  std::ofstream suvout(sname.c_str());

  std::string divname = id + ".diversity";
  std::ofstream divout(divname.c_str());

  std::string bname = id + ".biomass";
  std::ofstream bmassout(bname.c_str());

  classTNANMInvasion eco( fecundity, N_0, noise_strength, mij_max, seed);
  eco.AddInvader();

  long next_invasion;
  mtrand64 rnd2(seed + 10712394ULL); //used for the exponential distribution
  if(regular_or_poisson) next_invasion = invasion_interval; //const time invasions
  else next_invasion = static_cast<long>(0.5 + invasion_interval * (-log(rnd2.genrand64_real3())) ); //poisson
  
  long next_output = sampling_interval;


#ifdef BENCH
  clock_t start = clock();
#endif
  //============== main loop ========================
  for( unsigned long long t = 0; t < t_end; ) {
    if( next_output <= next_invasion ) {//output
      eco.UpdateDynamics(next_output);
      t += next_output;
      next_invasion -= next_output;
      next_output = sampling_interval;
      output(eco,suvout,divout,bmassout,t,id);
    }
    else {//invasion
      eco.UpdateDynamics(next_invasion);
      t += next_invasion;
      next_output -= next_invasion;
      if(regular_or_poisson) next_invasion = invasion_interval;
      else next_invasion = static_cast<long>(invasion_interval * -log(rnd2.genrand64_real3()) + 0.5);
      try{
	for( int i=0; i<num_invaders; i++) eco.AddInvader();
      } catch( int i) {
        std::cerr << "Number of species exceeds max_species" << std::endl;
      }
    }
  }

  //============== end of main loop =================
  output_final(eco,id);
#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  return 0;
}



//--------------------    
void output( classTNANMInvasion& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, long time, std::string id) {
  //======== output ===============
  int survivor = eco.numSurvivor();
  suvout << time << ' ' << survivor << std::endl;

  divout << time << ' ' << eco.expSWindexAll() << std::endl;
  
  bmassout << time << ' ' << eco.TotalBiomass() << std::endl;

  if( (time%1048576) == 0 ) {
    std::string temp = boost::io::str( boost::format("%04d") % static_cast<int>(time/1048576) );
    std::string afilename = id + ".a" + temp;
    std::string xfilename = id + ".x" + temp;
    eco.Moutput(afilename.c_str());
    eco.Xoutput(xfilename.c_str());
  }
}

//-----------------------
void output_final( classTNANMInvasion & eco, std::string id){
  std::string afilename = id + ".afinal";
  std::string xfilename = id + ".xfinal";
  std::string lspanfilename = id + ".lspan";
  eco.Xoutput(xfilename.c_str());
  eco.Moutput(afilename.c_str());
  eco.LspanOutput(lspanfilename.c_str());
  std::string command = "tar cvf " + id + ".ax.tar " + id + ".a* " + id + ".x*";
  int status = system( command.c_str() );
  if( status ) { throw 52;}
  command = "rm " + id + ".a[0-9]* " + id + ".x[0-9]* " + id + ".*final ";
  status = system ( command.c_str() );
  //  if( status ) { throw 53;}
}
//----------------------end of the program-------------------------------
