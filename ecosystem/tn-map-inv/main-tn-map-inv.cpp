//tangled nature model. model B. individual based dynamics. invasion.

//#define DEBUG
//#define BASIC
#include <iostream>
#include <fstream>
#include <ctime>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <string>
#include "classTNMap.cpp"
#include "classTNMInvasion.cpp"
#include "mtrand64.cpp"


void output( classTNMInvasion& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, long time, std::string id);
void output_final( classTNMInvasion & eco, std::string id);

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 11 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <1:resource> <2:eta_prob> <3:mij_prob> <4:invaison_interval> <5:num_invador> <6:const_time_invasion?[1,0]> "
	      << "<7:samp_interval> <8:t_end(sec)> <9:seed> <10:ID>" << std::endl
	      << std::endl;
    throw 1;
  }

  int fecundity = 2;
  double resource = boost::lexical_cast<double>( argv[1] );
  double b_max = 1.0;
  double eta_max = 1.0;
  double eta_prob = boost::lexical_cast<double>( argv[2] );
  double mij_max = 1.0;
  double mij_prob = boost::lexical_cast<double>( argv[3] );
  double mii_max = 1.0;
  long invasion_interval = boost::lexical_cast<long>( argv[4] );
  long num_invador = boost::lexical_cast<long>( argv[5] );
  bool const_or_poisson = boost::lexical_cast<bool>( argv[6] ); //true:const, false:poisson
  long sampling_interval = boost::lexical_cast<long>( argv[7] );
  unsigned long long t_end = boost::lexical_cast<unsigned long long>( argv[8] );
  unsigned long long seed = boost::lexical_cast<unsigned long long>( argv[9] );
  std::string id = argv[10];

  std::cerr << "Lists of given parameters are as follows:" << std::endl
	    << "fecundity:\t" << fecundity << std::endl
	    << "resource:\t" << resource << std::endl
	    << "b_max:\t" << b_max << std::endl
	    << "eta_max:\t" << eta_max << std::endl
	    << "eta_prob:\t" << eta_prob << std::endl
	    << "mij_max:\t" << mij_max << std::endl
	    << "mij_prob:\t" << mij_prob << std::endl
	    << "mii_max:\t" << mii_max << std::endl
	    << "invasion_interval:\t" << invasion_interval << std::endl
	    << "num_invador:\t" << num_invador << std::endl
	    << "const_or_poisson:\t" << const_or_poisson << std::endl
	    << "sampling_interval:\t" << sampling_interval << std::endl
	    << "t_end:\t" << t_end << std::endl
	    << "seed:\t" << seed << std::endl
	    << "id:\t" << id << std::endl;
  if( fecundity <= 1 || resource <= 0 || b_max <= 0.0 || eta_max <= 0.0 ||
      eta_prob <= 0.0 || eta_prob >= 1.0 || mij_max <= 0.0 || mij_prob <= 0.0 ||
      mij_prob >= 1.0 || mii_max <= 0.0 || t_end <= 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    throw 1;
  }

  //ofstreams
  std::string sname = id + ".suv";
  std::ofstream suvout( sname.c_str() );

  std::string dname = id + ".diversity";
  std::ofstream divout( dname.c_str() );

  std::string bname = id + ".biomass";
  std::ofstream bmassout( bname.c_str() );

  classTNMInvasion eco( fecundity, resource,
			 b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max, seed);
  eco.AddInvador();

  long next_invasion;
  mtrand64 rnd2(seed + 10712394ULL); //used for the exponential distribution
  if(const_or_poisson) next_invasion = invasion_interval; //const time invasions
  else next_invasion = static_cast<long>(0.5 + invasion_interval * (-log(rnd2.genrand64_real3())) ); //poisson
  
  long next_output = sampling_interval;

#ifdef BENCH
  clock_t start = clock();
#endif

  //============== main loop ========================
  for( unsigned long long t = 0; t < t_end; ) {
    if( next_output <= next_invasion ) {//output
      eco.UpdateDynamics(next_output);
      t += next_output;
      next_invasion -= next_output;
      next_output = sampling_interval;
      output(eco,suvout,divout,bmassout,t,id);
    }
    else {//invasion
      eco.UpdateDynamics(next_invasion);
      t += next_invasion;
      next_output -= next_invasion;
      if(const_or_poisson) next_invasion = invasion_interval;
      else next_invasion = static_cast<long>(invasion_interval * -log(rnd2.genrand64_real3()) + 0.5);
      try{
	for( int i=0; i<num_invador; i++) eco.AddInvador();
      } catch( int i) {
        std::cerr << "Number of species exceeds max_species" << std::endl;
      }
    }
  }
  //============== end of main loop =================
  output_final(eco,id);
#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  return 0;
}



//--------------------    
void output( classTNMInvasion& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, long time, std::string id) {
  //static long times_output = 0;
  //times_output++;
  //======== output ===============
  int survivor = eco.numSurvivor();
  suvout << time << ' ' << survivor;
  for( int i=1; i<5; i++) suvout << ' ' << eco.numTrophSpecies(i);
  suvout << std::endl;
  
  divout << time << ' ' << eco.expSWindexAll() << ' '
	 << eco.expSWindexProducer() << ' ' << eco.expSWindexConsumer() << std::endl;
  
  bmassout << time << ' ' << eco.TotalBiomass();
  for( int i=1; i<5; i++) bmassout << ' ' << eco.TrophicBiomass(i);
  bmassout << std::endl;

  if( (time%1048576) == 0 ) {
    std::string temp = boost::io::str( boost::format("%04d") % static_cast<int>(time/1048576) );
    std::string afilename = id + ".a" + temp;
    std::string xfilename = id + ".x" + temp;
    eco.X_b_eta_output(xfilename.c_str());
    eco.Moutput(afilename.c_str());
  }
}

//-----------------------
void output_final( classTNMInvasion & eco, std::string id){
  std::string afilename = id + ".afinal";
  std::string xfilename = id + ".xfinal";
  std::string lspanfilename = id + ".lspan";
  eco.X_b_eta_output(xfilename.c_str());
  eco.Moutput(afilename.c_str());
  eco.LspanOutput(lspanfilename.c_str());
}  

//----------------------end of the program-------------------------------
