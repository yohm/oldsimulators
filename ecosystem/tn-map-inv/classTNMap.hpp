#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>
#include <map>
#include "mtrand64.hpp"

#ifndef _C_TN_MAP_H_
#define _C_TN_MAP_H_

class classTNMap {
public:
  classTNMap( double t_fecundity, double t_resource);
  classTNMap();
  ~classTNMap();
  void Update();
  
  int numSurvivor() const; //return number of all species
  int numProducer() const; //return number of producers
  int numConsumer() const; //return number of consumers
  
  double expSWindexAll() const; // return exponential Shannon-Wiener index for all species.
  double expSWindexProducer() const; //return exponential Shannon-Wiener index for producer species.
  double expSWindexConsumer() const; //return exponential Shannon-Wiener index for consumer species.
  
  int numTrophSpecies( int t_level); //return number of species belonging to trophic level t_level. Producers are counted as species having level 1.
  double expSWindexLevels(int t_level); // return exponential Shannon-Wiener index for species in trophic level t_level.
  
  void Xoutput( std::ofstream & t_fout); //output x[i] to the ofstream. "x[0] x[1] x[2] .... \n"
  void X_b_eta_output( const char* filename); //output x[i] to file. "x[0]\nx[1]\nx[2]\n...."
  void Moutput( const char* filename); //output a[i][j] to the file 'filename'.
  void GenomeOutput( std::ofstream & genout, long time); //output "time genome[0]\ntime genome[1]\n..."
  
  double TotalBiomass() {return x_tot;} //return total biomass of all species.
  double ProducerBiomass() const; //return total biomass for producer species
  double ConsumerBiomass() const; //return total biomass for consumer species
  double TrophicBiomass(int t_level); //return summation of x of species in trophic level t_level.
  bool Alive(int i) {return alive[i];}
  
  void LspanOutput( const char* lspanfilename); //output life span to "lspanfilename". and reset the histogram.
  
protected:
  double *x;
  long *trophic_level,*age, *id;
  double **m, *b, *eta, *reproduction_prob;
  //b denotes metablic rate. always positive. eta denotes ability to utilize resources
  bool *alive, *producer; //if producer[i] is true, i'th species is producer. (eta[i]>0)
  int species;
  double fecundity;
  double resource;
  void AllocateMemory(int size); //called in constructor.
  void ReleaseMemory(); //called in destructor and ReallocateMemory.
  int inv_ng;
  int max_species,current_max_species;
  void ReallocateMemory();
  
  void CalcPi() const; // calculation of P_I.
  void CalcXtot(); //update x_tot and x_tot_inv. This function is called whenever x changes.
  double ReproductionProb( int i) const; //Return reproduction probability of species i.
  double x_tot;
  double x_tot_inv; // x_tot = \sum_j x[j]. x_tot_inv = 1/x_tot.  these must be calculated immediately after x changes.
  double threshold; //threshold for extinction, which is 0.5 by default.
  void Extinct(int i);//make species i go extinct.
  
  int lspan_vec_size; //size of lspan_prod and lspan_cons.
  std::vector<long> lspan_all;
  std::vector<long> lspan_prod;
  std::map<long,long> lspan_all_map;
  std::map<long,long> lspan_prod_map;

  int SpeciesNumber( long genome_id); //return the memory position of species having 'genome_id'. If there is no species, return -1.

  int TrophicCount(); //Count trophic levels. This routine was called from numTrophSpecies.
  bool trophic_level_counted_flag; //When trophic level and sum_trophic have been counted, true. Else false.
  int sum_trophic[10]; //sum_trophic[i] means the number of species belonging to trophic_level i
  bool Overmaxspecies();

  std::string jumyofilename;
};

#endif
