#include <cmath>
#include <queue>
#include "classTNMap.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_MAP_INVASION_H_
#define _C_TN_MAP_INVASION_H_

class classTNMInvasion : public classTNMap {
public:
  classTNMInvasion( double t_fecundity, double t_resource, 
		    double t_b_max, double t_eta_max, double t_eta_prob,
		    double t_mij_max, double t_mij_prob, double t_mii_max,
		    unsigned long long t_seed);
  ~classTNMInvasion();
  void UpdateDynamics( int times); //update dynamics 1 generation and mutate species. If there is no species, invasion is applied insteady of mutation and returns false.
  void AddInvador(); //randomly chosen species are added to the system.
protected:
  double b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max;
  int ZeroPoint();
  void SetRandomMIJ_b_eta( int num);
  void SetInitialPop(int num);

  unsigned long long seed;
  mtrand64 rnd;
};


#endif
