#include <cmath>
#include <queue>
#include "classTNIndBased.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_INVASION_HPP_
#define _C_TN_INVASION_HPP_

class classTNIBInvasion : public classTNIndBased {
public:
  classTNIBInvasion( int t_fecundity, double t_resource,
		     double t_b_max, double t_eta_max, double t_eta_prob,
		     double t_mij_max, double t_mij_prob, double t_mii_max,
		     unsigned long long t_seed);
  ~classTNIBInvasion();
  void AddInvador();
  void UpdateDynamics(int i); //update dynamics by i generations.
protected:
  double b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max;
  int ZeroPoint();
  void SetRandomMIJ_b_eta( int num);
  void SetInitialPop(int num);
};


#endif
