#include "classTNIndBased.hpp"
#include "classTNIBInvasion.hpp"

//------------------------------------
classTNIBInvasion::classTNIBInvasion( int t_fecundity, double t_resource,
				      double t_b_max, double t_eta_max, double t_eta_prob,
				      double t_mij_max, double t_mij_prob, double t_mii_max,
				      unsigned long long t_seed)
  : classTNIndBased( t_fecundity, t_resource, t_seed) {
  b_max = t_b_max;
  eta_max = t_eta_max;
  eta_prob = t_eta_prob;
  mij_max = t_mij_max;
  mij_prob = t_mij_prob;
  mii_max = t_mii_max;


}

//------------------------------
classTNIBInvasion::~classTNIBInvasion() {
}

//------------------------------------
void classTNIBInvasion::UpdateDynamics( int times) {
  for( int i=0; i<times; i++) Update();
}

//------------------------------------
void classTNIBInvasion::AddInvador() {
  int invador_num = ZeroPoint();
  SetRandomMIJ_b_eta( invador_num );
  SetInitialPop( invador_num );
}

//------------------------------------
int classTNIBInvasion::ZeroPoint() {
  int temp = species;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) temp = i;
  }
  if( temp == max_species ) throw 10;

  if(temp == species) species++;

  if( species > current_max_species ) ReallocateMemory();
  
  return temp;
}

//------------------------------------
void classTNIBInvasion::SetRandomMIJ_b_eta(int id) {
  //-------------------
  static const double b_max_65535_inv = b_max / 65535.0;
  static const double eta_prob_65535 = (int) (eta_prob * 65536.0);
  static const double  eta_max_65535_inv = eta_max / 65535.0;
  static const double  mii_max_65535_inv = mii_max / 65535.0;
  static const double  mij_prob_65535 = (int) (mij_prob * 65536.0);
  static const double  mij_max_65535_inv = mij_max / 65535.0;
  //-------------------

  //必要な乱数の数 4 + 2*species = b,eta(2),mii,mij(2*species)
  b[id] = rnd.load_16bit_integer() * b_max_65535_inv;
  if( rnd.load_16bit_integer() < eta_prob_65535 ) {
    producer[id] = true;
    eta[id] = rnd.load_16bit_integer() * eta_max_65535_inv;
  }
  else {
    producer[id] = false;
    eta[id] = 0;
  }

  m[id][id] = - rnd.load_16bit_integer() * mii_max_65535_inv;
  
  for( int j=0; j<species; j++) {
    if(!alive[j] ) continue;
    if( rnd.load_16bit_integer() < mij_prob_65535 ) {
      double temp = ( rnd.load_16bit_integer() + rnd.load_16bit_integer() - 65535) * mij_max_65535_inv;
      if( producer[j]==true && producer[id]==false ) m[id][j] = fabs(temp);
      else if( producer[j]==false && producer[id]==true ) m[id][j] = - fabs(temp);
      else m[id][j] = temp;
      m[j][id] = -m[id][j];
    }
  }

}

//------------------------------------
void classTNIBInvasion::SetInitialPop(int num) {
  x[num] = 1;
  alive[num] = true;
  age[num] = 0;
  CalcXtot();
}
