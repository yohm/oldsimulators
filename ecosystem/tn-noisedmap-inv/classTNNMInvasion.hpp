#include <cmath>
#include <queue>
#include "classTNNoisedMap.hpp"
#include "mtrand64.hpp"

#ifndef _C_TN_NOISEDMAP_INVASION_H_
#define _C_TN_NOISEDMAP_INVASION_H_

class classTNNMInvasion : public classTNNoisedMap {
public:
  classTNNMInvasion( double noise_strength,
		     double t_fecundity, double t_resource,
		     double t_b_max, double t_eta_max, double t_eta_prob,
		     double t_mij_max, double t_mij_prob, double t_mii_max,
		     unsigned long long t_seed);
  ~classTNNMInvasion();
  void AddInvader(); //add one invader to the system.
  void UpdateDynamics( long time) { for( long t = 0; t < time; t++) Update();}
protected:
  double b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max, mutation_rate;
  int ZeroPoint();
  void SetRandomMIJ_b_eta( long invader_number);
  void SetInitialPop( long invader_number);
};


#endif
