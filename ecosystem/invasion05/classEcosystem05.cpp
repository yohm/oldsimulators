//Class Ecosystem
//lambda = 0.5の場合の線形化された場合の計算

#include "classEcosystem05.hpp"

//------------------------------
Ecosystem05::Ecosystem05(int num_plant,double growth,double h,double percentage){
  NUM_PLANT = num_plant;
  GROWTH = growth;
  H = h;
  PERCENTAGE = percentage;
  EPSIRON = 0.001;
  if( NUM_PLANT<1 || GROWTH<=0.0 || H <=0.0 || PERCENTAGE <= 0.0 || EPSIRON <= 0.0) {
    cerr << "not appropriate number" << endl;
    cerr << "NUM_PLANT GROWTH H PERCENTAGE EPSIRON" << endl <<
      NUM_PLANT<<' '<<GROWTH<<' '<<H<<' '<<PERCENTAGE<<' '<<EPSIRON<<endl;
    exit(1);
  }
  
  current_max_species = MAX_SPECIES;
  y = new double[MAX_SPECIES];
  y_0 = new double[MAX_SPECIES];
  y_dash_0 = new double[MAX_SPECIES];
  y_dash_1 = new double[MAX_SPECIES];
  y_dash_2 = new double[MAX_SPECIES];
  y_dash_3 = new double[MAX_SPECIES];
  a = new double*[MAX_SPECIES];
  for(int i=0; i<MAX_SPECIES; i++) {a[i] = new double[MAX_SPECIES];}
#ifdef COMPET
  b = new double*[MAX_SPECIES];
  for(int i=0; i<MAX_SPECIES; i++) {b[i] = new double[MAX_SPECIES];}
#endif
  alive = new bool[MAX_SPECIES];
  connect = new bool * [MAX_SPECIES];
  for( int i=0; i<MAX_SPECIES; i++) {connect[i] = new bool [MAX_SPECIES];}
  trophic_level = new int [MAX_SPECIES];
  age = new int [MAX_SPECIES];
#ifdef COD
  cod1 = new Coordinate(NUM_PLANT);
#endif
  for(int i=0; i<MAX_SPECIES; i++) {
    y[i] = 0;
    y_0[i] = 0;
    y_dash_0[i] = 0;
    y_dash_1[i] = 0;
    y_dash_2[i] = 0;
    y_dash_3[i] = 0;
    alive[i] = 0;
    for(int j=0; j<MAX_SPECIES; j++) {a[i][j] = 0; connect[i][j] = false;}
#ifdef COMPET
    for( int j=0; j<MAX_SPECIES; j++) {b[i][j] = 0;}
#endif
  }
  Set_initial_state();
}

//------------------------------
Ecosystem05::~Ecosystem05(){
  delete [] y;
  delete [] y_0;
  delete [] y_dash_0;
  delete [] y_dash_1;
  delete [] y_dash_2;
  delete [] y_dash_3;
  delete [] a;
#ifdef COMPET
  delete [] b;
#endif
  delete [] alive;
  delete [] trophic_level;
  delete [] age;
}

//------------------------------
void Ecosystem05::Set_initial_state(){
  for(int i=0; i<NUM_PLANT; i++) {
    alive[i] = 1;
    y[i] = EPSIRON;
  }
  species = NUM_PLANT;
}

//------------------------------
void Ecosystem05::Copy_y_dash() {
  for(int i=0; i<species; i++) {
    y_dash_3[i] = y_dash_2[i];
    y_dash_2[i] = y_dash_1[i];
    y_dash_1[i] = y_dash_0[i];
  }
}

//------------------------------
double Ecosystem05::Y_dash(int i){
  //微分方程式の右辺を計算する
  double d;
  if(i>=NUM_PLANT) {
    d = -y[i];
    for(int j=0; j<species; j++) {
      if(connect[i][j]) {
	d += a[i][j] * y[j];
      }
#ifdef COMPET
      d += b[i][j] * y[j];
#endif
    }
  }
  else {
    d = GROWTH * y[i] * (1- y[i]*y[i] ) ;
    for(int j=0; j<species; j++) {
      if(connect[i][j]) {
	d += a[i][j] * y[j];
      }
#ifdef COMPET
      d += b[i][j] * y[j];
#endif
    }
  }
  return d*0.5;
}
//-----------------------------
void Ecosystem05::Calc_y_dash_0() {
  for(int i=0; i<species; i++) {
    if(alive[i]) y_dash_0[i] = Y_dash(i);
  }
}

//------------------------------
int Ecosystem05::Calc_predictor() {
  int inv_ng=0;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) continue;
    y_0[i] = y[i];
    y[i] += (H/24.0) * (55*y_dash_0[i] -59*y_dash_1[i] + 37*y_dash_2[i] - 9*y_dash_3[i]);
    if(y[i] < 0) {
      Extinct(i);
      inv_ng++;
    }
  }
  return inv_ng;
}

//------------------------------
void Ecosystem05::Extinct(int i) {
  if(i < NUM_PLANT) Extinct_plant( i);
  else {
    alive[i] = 0;
    y[i] = 0;
    y_0[i] = 0;
    y_dash_0[i] = 0;
    y_dash_1[i] = 0;
    y_dash_2[i] = 0;
    y_dash_3[i] = 0;
    for(int j=0; j<species; j++) {
      a[i][j] = 0;
      a[j][i] = 0;
      connect[i][j] = false;
      connect[j][i] = false;
    }
    trophic_level[i] = 0;
#ifdef COD
    cod1->Delete_coordinate(i);
#endif
  }
}

//------------------------------
void Ecosystem05::Extinct_plant(int i) {
  if(NUM_PLANT==1) {
    cerr << "plant is extinct" << endl;
    exit(1);
  }
  else {
    cout << "#plant :" << i << " is extinct."<<endl;
    y[i] = EPSIRON;
    y_0[i] = EPSIRON;
    y_dash_0[i] = 0;
    y_dash_1[i] = 0;
    y_dash_2[i] = 0;
    y_dash_3[i] = 0;
    for(int j=0; j<species; j++) {
      a[i][j] = 0;
      a[j][i] = 0;
      connect[i][j] = false;
      connect[j][i] = false;
    }
  }
}
//------------------------------
void Ecosystem05::Calc_y_dash_1() {
  for(int i=0; i<species; i++) {
    if(alive[i]) y_dash_3[i] = Y_dash(i);
  }
}

//------------------------------
int Ecosystem05::Calc_corrector() {
  int inv_ng=0;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) continue;
    double d = (H/24) * (9*y_dash_3[i] + 19*y_dash_0[i] - 5*y_dash_1[i] + y_dash_2[i]);
    y[i] = y_0[i] + d;

    if(y_0[i]* H * PERCENTAGE < fabs(d) ) inv_ng++;

    if(y[i] <= 0.0) {
      Extinct(i);
      inv_ng++;
    }
  }
  return inv_ng;
}


//------------------------------
int Ecosystem05::Check_isolated_species() {
  int inv_ng=0;
  for(int i=NUM_PLANT; i<species; i++) {
    if(alive[i]) {   //以下は生きている動物iに対して行われる
      int preying=0;
      for(int j=0; j<species; j++) {
	if(a[i][j]>0) preying++;
      }
      if(!preying) {Extinct(i); inv_ng++;}
    }
  }
  return inv_ng;
}

//------------------------------
void Ecosystem05::Show_y(char *str){
  ofstream yout(str);
  for(int i=0; i< species; i++) {
    if(alive[i]) yout << y[i] << endl;
  }
  yout.close();
}

//------------------------------
void Ecosystem05::Show_a(char *str) {
  ofstream aout( str);
  for(int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for(int j=0; j<species ; j++) {
      if(!alive[j]) continue;
      aout << a[i][j] << ' ';
    }
    aout << endl;
  }
  aout.close();
}

//------------------------------
void Ecosystem05::Show_ay(char *str) {
  ofstream yaout(str);
  for(int i=0; i<species; i++) {
    if(!alive[i]) continue;
    for(int j=0; j<species ; j++) {
      if(!alive[j]) continue;
      yaout << a[i][j]*y[j] << ' ';
    }
    yaout << endl;
  }
  yaout.close();
}

//------------------------------
int Ecosystem05::Solve(){
  int inv_ng=0;

  Copy_y_dash();
  Calc_y_dash_0();
  inv_ng +=  Calc_predictor();
  Calc_y_dash_1();
  inv_ng +=  Calc_corrector();
  inv_ng +=  Check_isolated_species();

  return inv_ng;
}

//------------------------------
int Ecosystem05::Survivor() {
  int count = 0;
  for(int i=0; i<species; i++) {
    if(alive[i]) count++;
  }
  return count;
}

//------------------------------
double Ecosystem05::Plant_pop() {
  double total = 0;
  for(int i=0; i<NUM_PLANT; i++) {
    if(alive[i]) total += y[i];
  }
  return total;
}

//------------------------------
double Ecosystem05::Total_pop() {
  double total = 0;
  for(int i=NUM_PLANT; i<species; i++) {
    if(alive[i]) total += y[i];
  }
  return total;
}

//-------------------------------
void Ecosystem05::Show_coordinate(string str) {
  ofstream coout( str.c_str() );
  for(int i=0; i<species; i++) {
    if(!alive[i]) continue;
#ifdef COD
    coout << y[i] << ' '<<cod1->Coordinate_x( i) << ' '<<cod1->Coordinate_y( i) << endl;
#else
    coout << y[i] << endl;
#endif
  }
  coout.close();
}

//-------------------------------
void Ecosystem05::Show_niche(string str) {
  ofstream nicheout( str.c_str() );
  for(int i=NUM_PLANT; i<species; i++) {
    if(!alive[i]) continue;
    nicheout << y[i] << ' ';
    for(int j=0; j<NUM_PLANT; j++) nicheout << a[i][j]*y[j] << ' ';
    double total = 0;
    double total2 = 0;
    for(int j=NUM_PLANT; j<species; j++) {
      if(!alive[j]) continue;
      total += a[i][j] * y[j];
      total2 += fabs( a[i][j] * y[j]);
    }
    nicheout << total << ' '<< total2 << endl;
  }
  nicheout.close();
}

//-------------------------------
void Ecosystem05::Output_movie(int i) {
  char filename[10];
  sprintf(filename,"y%03d.dat",i);
  Show_coordinate( filename );
  char filename2[10];
  sprintf(filename2,"a%03d.dat",i);
  Show_a( filename2 );
  char filename3[10];
  sprintf(filename3,"niche%03d.dat",i);
  Show_niche( filename3 );
}

//------------------------------
void Ecosystem05::Reallocate_memory() {
  //allocate
  int new_max_species = current_max_species + MAX_SPECIES;
  double * tmpy = new double[new_max_species];
  double *tmpy_0 = new double[new_max_species];
  double *tmpy_dash_0 = new double[new_max_species];
  double *tmpy_dash_1 = new double[new_max_species];
  double *tmpy_dash_2 = new double[new_max_species];
  double *tmpy_dash_3 = new double[new_max_species];
  double **tmpa = new double*[new_max_species];
  for( int i=0; i<new_max_species; i++) tmpa[i] = new double[new_max_species];
#ifdef COMPET
  double **tmpb = new double*[new_max_species];
  for( int i=0; i<new_max_species; i++) tmpb[i] = new double[new_max_species];
#endif
  bool *tmpalive = new bool[new_max_species];
  bool ** tmpconnect = new bool*[new_max_species];
  for( int i=0; i<new_max_species; i++) tmpconnect[i] = new bool[new_max_species];
  int * tmptrophic_level = new int [new_max_species];
  int * tmpage = new int [new_max_species];
  //initialize
  for( int i=0; i<new_max_species; i++) {
    tmpy[i] = tmpy_0[i] = tmpy_dash_0[i] = tmpy_dash_1[i] = tmpy_dash_2[i] = tmpy_dash_3[i] = 0.0;
    for( int j=0; j<new_max_species; j++) { tmpa[i][j] = 0.0;}
#ifdef COMPET
    for( int j=0; j<new_max_species; j++) { tmpb[i][j] = 0.0;}
#endif
    tmpalive[i] = false;
    for( int j=0; j<new_max_species; j++) { tmpconnect[i][j] = false;}
    tmptrophic_level[i] = 0;
    tmpage[i] = 0;
  }
  //copy
  for( int i=0; i<current_max_species; i++) {
    tmpy[i] = y[i];
    tmpy_0[i] = y_0[i];
    tmpy_dash_0[i] = y_dash_0[i];
    tmpy_dash_1[i] = y_dash_1[i];
    tmpy_dash_2[i] = y_dash_2[i];
    tmpy_dash_3[i] = y_dash_3[i];
    for( int j=0; j<current_max_species; j++) tmpa[i][j] = a[i][j];
#ifdef COMPET
    for( int j=0; j<current_max_species; j++) tmpb[i][j] = b[i][j];
#endif
    tmpalive[i] = alive[i];
    for( int j=0; j<current_max_species;j++) tmpconnect[i][j] = connect[i][j];
    tmptrophic_level[i] = trophic_level[i];
    tmpage[i] = age[i];
  }
  //delete
  delete [] y;  delete [] y_0;  delete [] y_dash_0;  delete [] y_dash_1;  delete [] y_dash_2;  delete [] y_dash_3;
  for( int i=0; i<current_max_species; i++) {
    delete [] a[i];    delete [] connect[i];
  }
  delete [] a; delete [] connect;  delete [] alive;  delete [] trophic_level; delete [] age;
#ifdef COMPET
  for( int i=0; i<current_max_species; i++) {delete [] b[i];}
  delete [] b;
#endif
  //--------
  y = tmpy;
  y_0 = tmpy_0;
  y_dash_0 = tmpy_dash_0;
  y_dash_1 = tmpy_dash_1;
  y_dash_2 = tmpy_dash_2;
  y_dash_3 = tmpy_dash_3;
  a = tmpa;
#ifdef COMPET
  b = tmpb;
#endif
  connect = tmpconnect;
  alive = tmpalive;
  trophic_level = tmptrophic_level;
  age = tmpage;

  current_max_species = new_max_species;
}

//-----------------------
void Ecosystem05::Jumyou_out(ofstream & jumout) {
  for( int i=NUM_PLANT; i<species; i++) {
    if(alive[i]) {
      age[i] ++;
      continue;  //生きていたら出力しなくてよい
    }

    if(age[i] > 1) {   //０歳(侵入できなかった種)は出力しない
      jumout << age[i]-1 << endl; //年齢はage-1になる
      age[i] = 0;
    }
  }
}
