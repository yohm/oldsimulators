#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;
#include "classEcosystem05.cpp"
#include "classInvasion.cpp"
#include "mtrand64.cpp"

//#define BENCH

int main (int argc, char* argv[]) {
  if( argc != 10) {
    cerr << "Input arguments" << endl
	 << "program <ID> <m> <seed> <y_initial> <num_plant> <growth> <dt> <percentage> <t_end>" << endl;
    exit(1);
  }
  char *id;
  id = argv[1];
  int m = atoi(argv[2]);
  unsigned long long seed = atol(argv[3]);
  double y_initial = atof(argv[4]);
  int num_plant = atoi(argv[5]);
  double growth = atof(argv[6]);
  double dt = atof(argv[7]);
  double percentage = atof(argv[8]);
  int END = atoi(argv[9]);
  int OUTPUT_SPECIES = 100; //100種になったらa,yを出力

  char suvfilename[30];
  sprintf(suvfilename,"%s.suv",id);
  ofstream suvout(suvfilename);
  char afilename[15];
  char yfilename[15];
  char jumyofile[15];
  sprintf( jumyofile, "%s.jumyo",id);
  ofstream jumout( jumyofile);
  
  Invasion eco( m, y_initial, seed, num_plant, growth, dt, percentage);

#ifdef BENCH
  clock_t start = clock();
#endif
  cerr << "seed:"<<seed<<endl;
  for( ; ; ) {
    if( !( eco.Solve() )  ) {
      int survivor = eco.Survivor();
      suvout << eco.time << ' '<<survivor<<' '<<eco.Total_pop()<<endl;// ' '<<eco.Clustering()<<' '<<eco.Link()<<endl;
      eco.Jumyou_out(jumout);
      if( survivor == OUTPUT_SPECIES) {
	char afilename[15];
	char yfilename[15];
	sprintf( afilename, "%s.a%04d",id,survivor);
	sprintf( yfilename, "%s.y%04d",id,survivor);
	eco.Show_a(afilename);
	eco.Show_y(yfilename);
	OUTPUT_SPECIES *= 2;
	if(survivor == 3200) break;
      }
      eco.Invade();
      if(eco.time == END) break;
    }
  }

#ifdef BENCH
  clock_t end = clock();
  cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<endl;
#endif
  
  return 0;
}
