#include <iostream>
#include "classEcosystem05.hpp"
#include "mtrand64.hpp"
using namespace std;

class Invasion:public Ecosystem05 {
private:
  //Invasion() のサブルーチン
  int ID_new_commer() const; //new commer のIDを返す。
  int Num_inter(); //Interactionの数を返す。
  void Add_random_interaction(int newcommer); //ランダムな相互作用を与える。
  void Add_population(int newcommer); //population を newcommerに与える。
  int Maindish_animal(int i); //i番目の種が最も良く食べる動物種のIDを返す。なければ最も良く食べるplant種のIDを返す。
  int Maindish(int i); //i番目の種が最も良く食べる種のIDを返す。
  mtrand64 rnd; //
  int M;
  double Y_INITIAL;
  unsigned long long SEED;
public:
  Invasion(int m, double y_initial, unsigned long long seed,
	   int num_plant,double growth,double h,double percentage);
  //引数
  ~Invasion(); //
  void Invade(); //Invadeルーチン
  double Clustering();
  int Link();
  long time;
};


//----------------------------
Invasion::Invasion(int m, double y_initial, unsigned long long seed,
		   int num_plant,double growth,double h,double percentage)
 :Ecosystem05(num_plant, growth, h, percentage){
  M = m;  Y_INITIAL = y_initial;  SEED = seed;
  if( M<1 || Y_INITIAL<=0.0 || SEED < 0) {
    cerr << "Error! in classInvasion constructor."<<endl
	 << "M Y_INITIAL SEED" << endl << M<<' '<<Y_INITIAL<<' '<<SEED<<endl;
    exit(1);
  }
  rnd.init_genrand64(SEED);
  time = 0;
#ifdef COMPET
  for(int i=0; i<MAX_SPECIES; i++) {
    for(int j=0; j<MAX_SPECIES; j++) {
      b[i][j] = 0;
    }
  } //念のため
#endif
}


//----------------------------
Invasion::~Invasion() {
}


//----------------------------
void Invasion::Invade() {
  int new_commer = ID_new_commer();
  Add_random_interaction(new_commer);
  Add_population(new_commer);
  time++;
}


//----------------------------
int Invasion::ID_new_commer() const{
  int newcommer = species;
  for(int i=0; i<species; i++) {
    if(!alive[i]) newcommer = i;
  }
  return newcommer;
}

//----------------------------
int Invasion::Num_inter() {
  int temp = (int) (rnd.genrand64_bit(16)/65536.0 * M + 1);
  int suv = Ecosystem05::Survivor();
  if (temp > suv) temp = suv;
  return temp;
}

//----------------------------
void Invasion::Add_random_interaction(int newcommer) {
  int inter; //何番目の種と相互作用をするか。
  int num_inter = Num_inter(); //相互作用の数
  int kouho = Survivor(); //相互作用を持ちうる数

  for( ; num_inter > 0; num_inter--, kouho--) {
    inter = (int) (kouho* rnd.genrand64_bit(16)/65536.0 );

    for(int i=0; i<species; i++) {
      if(alive[i] && a[i][newcommer] == 0) {
	if(inter) inter--;
	else {
	  if( i < NUM_PLANT) a[i][newcommer] = -1 * ((long)rnd.genrand64_bit(16))/65536.0;
	  else a[i][newcommer] = 2 * ((long)rnd.genrand64_bit(16))/65536.0 -1;
	  a[newcommer][i] = -a[i][newcommer];
	  connect[newcommer][i] = true;
	  connect[i][newcommer] = true;
	  break; //interが0になったらbreak
	}
      }
    }

  }
}

//----------------------------
void Invasion::Add_population(int newcommer) {
  y[newcommer] = Y_INITIAL;
  alive[newcommer] = 1;
  if(newcommer == species) species++;
  if(species > current_max_species-1) Reallocate_memory();
#ifdef COD
  cod1->Set_coordinate( newcommer, Maindish(newcommer) );
#endif
}


//-----------------------------
int Invasion::Maindish_animal(int i) {
  //i番目の種のanimalの中で最もa_ijが大きい相手のIDを返す。
  //餌の中にanimmalがなければplantの中で最もa_ijが大きい相手のIDを返す。
  double max = 0;
  int temp = 0;
  for(int j =NUM_PLANT; j<species; j++) {
    if(!alive[j]) continue;
    if( max <a[i][j]) {
      max = a[i][j];
      temp = j;
    }
  }

  if(max > 0) return temp;

  for(int j=0; j<NUM_PLANT; j++) {
    if( max < a[i][j]) {
      max = a[i][j];
      temp = j;
    }
  }
  return temp;
}

//----------------
int Invasion::Maindish(int i) {
  double max = 0;
  int temp = 0;
  for(int j=0; j<species; j++) {
    if(!alive[j] ) continue;
    if( max < a[i][j] ) {
      max = a[i][j];
      temp = j;
    }
  }
  return temp;
}

//--------------------
double Invasion::Clustering() {
  double temp = 0;
  int s_count = 0;
  for (int i=NUM_PLANT; i<species; i++) {
    if(!alive[i]) continue;
    int count = 0;
    int triangle = 0;
    for( int j=i+1; j<species; j++) {
      if(!alive[j]) continue;
      if(connect[i][j]) {
	count++;
	for( int k=j+1; k<species; k++) {
	  if(!alive[k]) continue;
	  if(connect[i][k] && connect[j][k]) {
	    triangle++;
	    break;
	  }
	}
      }
    }
    if( count < 2) continue;
    temp += 2.0*triangle/( count*(count-1));
  }
  temp /= (double)Survivor();
  return temp;
}

//---------------------
int Invasion::Link() {
  int count = 0;
  for(int i=NUM_PLANT; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=i+1; j<species; j++) {
      if(!alive[j]) continue;
      if(connect[i][j]) count++;
    }
  }
  return count;
}
