#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>

//#define COD
//#define COMPET

#ifdef COD
#include "classCoordinate.hpp"
#endif

#ifndef ECO05
#define ECO05

using namespace std;

const int MAX_SPECIES = 500;

class Ecosystem05 {
protected:
  double *y,*y_0,*y_dash_0,*y_dash_1,*y_dash_2,*y_dash_3,**a;
#ifdef COMPET
  double **b;
#endif
  bool ** connect;
  bool* alive;
  int* trophic_level;
  int* age;
  int species;
  int current_max_species;
  int NUM_PLANT;
  void Extinct(int i); //i番目の種を絶滅させる
  void Extinct_plant(int i); //i番目の植物を絶滅させる
  void Reallocate_memory(); //memoryの確保。current_max_species<speciesになったら実行。
private:
  double GROWTH; //growth rate
  double H; 
  double PERCENTAGE;
  double EPSIRON;
  //コンストラクタのサブルーチン
  void Set_initial_state(); //植物にポピュレーションを与える
  //Solve() のサブルーチン------
  void Copy_y_dash(); //Subroutine of Solve. y_dashをコピーする
  double Y_dash(int i); //i番目の種の微分方程式の右辺を計算する。競争を入れる場合もOK
  void Calc_y_dash_0(); //Sub of Solve(). y_dash_0を計算する。
  int Calc_predictor(); //Sub of Solve(). predictorの計算。inv_ngを返す
  void Calc_y_dash_1(); //Sub of Solve(). y_dash_1を計算する。
  int Calc_corrector(); //Sub of Solve(). correctorの計算。inv_ngを返す。
  int Check_isolated_species(); //Sub of Solve(). 孤立した種がいないか確かめinv_ngを返す。
  //-------------------------
public:
  int num_plant() { return NUM_PLANT;}
  Ecosystem05(int num_plant,double growth,double h,double percentage); 
  //メモリの確保、イニシャライズ、植物にポピュレーションを与える。
  ~Ecosystem05(); //メモリの解放
  double Plant_pop(); //植物のyの合計を返す
  double Get_pop(int i) { return y[i];}//y[i]を返す
  double Total_pop(); //動物のyの合計を返す
  void Show_y(char *str);
  void Show_a(char *str); //すべてのaをファイルに出力。ファイル名:afilename
  void Show_ay(char *str); //a[i][j]*y[j]を出力
  int Solve(); //Predictor-correctorを1step進める。inv_ngを返す。
  int Survivor(); //生き残っている種数を返す
  void Show_coordinate(string str); //movie用の出力。座標とyをファイルに出力。ファイル名はstr
  void Show_niche(string str); //movie用の出力。植物との相互作用、動物との相互作用を出力。
  void Output_movie(int i); //movie用のファイルの出力(coord.dat, a.dat) iはファイル番号
#ifdef COD
  Coordinate* cod1; //coordinate class
#endif
  void Set_restart_state(char* cod_filename, char* a_filename,char* b_filename);
  //restart用の関数。ファイルからデータを読み込み状態をセットする
  void Jumyou_out(ofstream & jumout);
};

#endif
