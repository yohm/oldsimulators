#include <iostream>
#include "classEcosystem05.hpp"
#include "mtrand64.hpp"
using namespace std;

class InvasionRandomNet:public Ecosystem05 {
private:
  //InvasionRandomNet() のサブルーチン
  int ID_new_commer() const; //new commer のIDを返す。
  int Num_inter(); //Interactionの数を返す。
  void Add_random_interaction(int newcommer); //ランダムな相互作用を与える。
  void Add_population(int newcommer); //population を newcommerに与える。
  int Maindish_animal(int i); //i番目の種が最も良く食べる動物種のIDを返す。なければ最も良く食べるplant種のIDを返す。
  int Maindish(int i); //i番目の種が最も良く食べる種のIDを返す。
  mtrand64 rnd; //
  double PROB;
  double Y_INITIAL;
  unsigned long long SEED;
public:
  InvasionRandomNet(double prob, double y_initial, unsigned long long seed,
	   int num_plant,double growth,double h,double percentage);
  //引数
  ~InvasionRandomNet(); //
  void Invade(); //Invadeルーチン
  double Clustering();
  int Link();
  long time;
};


//----------------------------
InvasionRandomNet::InvasionRandomNet(double prob, double y_initial, unsigned long long seed,
		   int num_plant,double growth,double h,double percentage)
 :Ecosystem05(num_plant, growth, h, percentage){
  PROB = prob;  Y_INITIAL = y_initial;  SEED = seed;
  if( PROB<=0.0 || Y_INITIAL<=0.0 || SEED < 0) {
    cerr << "Error! in classInvasionRandomNet constructor."<<endl
	 << "PROB Y_INITIAL SEED" << endl <<PROB<<' '<<Y_INITIAL<<' '<<SEED<<endl;
    exit(1);
  }
  rnd.init_genrand64(SEED);
  time = 0;
#ifdef COMPET
  for(int i=0; i<MAX_SPECIES; i++) {
    for(int j=0; j<MAX_SPECIES; j++) {
      b[i][j] = 0;
    }
  } //念のため
#endif
}


//----------------------------
InvasionRandomNet::~InvasionRandomNet() {
}


//----------------------------
void InvasionRandomNet::Invade() {
  int new_commer = ID_new_commer();
  Add_random_interaction(new_commer);
  Add_population(new_commer);
  time++;
}


//----------------------------
int InvasionRandomNet::ID_new_commer() const{
  int newcommer = species;
  for(int i=0; i<species; i++) {
    if(!alive[i]) newcommer = i;
  }
  return newcommer;
}

//----------------------------
void InvasionRandomNet::Add_random_interaction(int newcommer) {
  const double factor = 1.0/65536.0;
  for( int i=0; i<species; i++) {
    if(alive[i]) {
      if(rnd.genrand64_bit(16) < PROB*65536.0) {
	if( i < NUM_PLANT) a[i][newcommer] = -1.0 * (long long)rnd.genrand64_bit(16) * factor;
	else a[i][newcommer] = ( (long long)rnd.genrand64_bit(16)<<1 ) * factor -1.0;
	a[newcommer][i] = -a[i][newcommer];
	connect[newcommer][i] = true;
	connect[i][newcommer] = true;
      }
    }
  }
}

//----------------------------
void InvasionRandomNet::Add_population(int newcommer) {
  y[newcommer] = Y_INITIAL;
  alive[newcommer] = 1;
  if(newcommer == species) species++;
  if(species > current_max_species-1) Reallocate_memory();
#ifdef COD
  cod1->Set_coordinate( newcommer, Maindish(newcommer) );
#endif
}


//-----------------------------
int InvasionRandomNet::Maindish_animal(int i) {
  //i番目の種のanimalの中で最もa_ijが大きい相手のIDを返す。
  //餌の中にanimmalがなければplantの中で最もa_ijが大きい相手のIDを返す。
  double max = 0;
  int temp = 0;
  for(int j =NUM_PLANT; j<species; j++) {
    if(!alive[j]) continue;
    if( max <a[i][j]) {
      max = a[i][j];
      temp = j;
    }
  }

  if(max > 0) return temp;

  for(int j=0; j<NUM_PLANT; j++) {
    if( max < a[i][j]) {
      max = a[i][j];
      temp = j;
    }
  }
  return temp;
}

//----------------
int InvasionRandomNet::Maindish(int i) {
  double max = 0;
  int temp = 0;
  for(int j=0; j<species; j++) {
    if(!alive[j] ) continue;
    if( max < a[i][j] ) {
      max = a[i][j];
      temp = j;
    }
  }
  return temp;
}

//--------------------
double InvasionRandomNet::Clustering() {
  double temp = 0;
  int s_count = 0;
  for (int i=NUM_PLANT; i<species; i++) {
    if(!alive[i]) continue;
    int count = 0;
    int triangle = 0;
    for( int j=i+1; j<species; j++) {
      if(!alive[j]) continue;
      if(connect[i][j]) {
	count++;
	for( int k=j+1; k<species; k++) {
	  if(!alive[k]) continue;
	  if(connect[i][k] && connect[j][k]) {
	    triangle++;
	    break;
	  }
	}
      }
    }
    if( count < 2) continue;
    temp += 2.0*triangle/( count*(count-1));
  }
  temp /= (double)Survivor();
  return temp;
}

//---------------------
int InvasionRandomNet::Link() {
  int count = 0;
  for(int i=NUM_PLANT; i<species; i++) {
    if(!alive[i]) continue;
    for( int j=i+1; j<species; j++) {
      if(!alive[j]) continue;
      if(connect[i][j]) count++;
    }
  }
  return count;
}

