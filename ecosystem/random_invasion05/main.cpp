#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;
#include "classEcosystem05.cpp"
#include "classInvasionRandomNet.cpp"
#include "mtrand64.cpp"

//#define BENCH
#ifdef BENCH
#include <ctime>
#endif


int main (int argc, char* argv[]) {
  if( argc != 10) {
    cerr << "Input arguments" << endl
	 << "program <ID> <prob> <seed> <y_initial> <num_plant> <growth> <dt> <percentage> <t_end>" << endl;
    exit(1);
  }
  char *id;
  id = argv[1];
  double prob = atof(argv[2]);
  unsigned long long seed = atol(argv[3]);
  double y_initial = atof(argv[4]);
  int num_plant = atoi(argv[5]);
  double growth = atof(argv[6]);
  double dt = atof(argv[7]);
  double percentage = atof(argv[8]);
  int END = atoi(argv[9]);
  int OUTPUT_SPECIES = 100; //100種になったらa,yを出力

  char suvfilename[30];
  sprintf(suvfilename,"%s.suv",id);
  ofstream suvout(suvfilename);
  
  InvasionRandomNet eco( prob, y_initial, seed, num_plant, growth, dt, percentage);
  char afilename[15];
  char yfilename[15];
  char jumyofile[15];
  sprintf( jumyofile, "%s.jumyo",id);
  ofstream jumout( jumyofile);

#ifdef BENCH
  clock_t start = clock();
#endif

  for( ; ; ) {
    if( !( eco.Solve() )  ) {
      int survivor = eco.Survivor();
      suvout << eco.time << ' '<<survivor<<' '<<eco.Total_pop()<<endl;// ' '<<eco.Clustering()<<' '<<eco.Link()<<endl;
      eco.Jumyou_out(jumout);

      if(eco.time == END) {
	sprintf( afilename, "%s.afinal",id);
	sprintf( yfilename, "%s.yfinal",id);
	eco.Show_a(afilename);
	eco.Show_y(yfilename);
	break;
      }

      if( survivor == OUTPUT_SPECIES) {
	sprintf( afilename, "%s.a%04d",id,survivor);
	sprintf( yfilename, "%s.y%04d",id,survivor);
	eco.Show_a(afilename);
	eco.Show_y(yfilename);
	OUTPUT_SPECIES *= 2;
      }

      eco.Invade();
    }
  }

#ifdef BENCH
  clock_t end = clock();
  cerr << "elapsed time: " << (double)(end-start)/(CLOCKS_PER_SEC) << endl;
#endif

  return 0;
}
