//#define DEBUG
//#define BASIC
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include "classTNIndBased.cpp"
#include "classTNIBMutation.cpp"
#include "mtrand64.cpp"


void output( classTNIBMutation& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, std::ofstream& genout, long time, std::string id);
void output_final( classTNIBMutation & eco, std::string id);

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 11 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <resource> <eta_prob> <mij_prob> <genome_length> <allel_bitlength> <mut_rate> <samp_interval> <t_end(sec)> <seed> <ID>" << std::endl
	      << std::endl;
    throw 1;
  }
  
  int fecundity = 2;
  double resource = boost::lexical_cast<double>( argv[1] );
  double b_max = 1.0;
  double eta_max = 1.0;
  double eta_prob = boost::lexical_cast<double>( argv[2] );
  double mij_max = 1.0;
  double mij_prob = boost::lexical_cast<double>( argv[3] );
  double mii_max = 1.0;
  int l_genome = boost::lexical_cast<int>( argv[4] ); // each genome has l_genome genes.
  int log2_allel = boost::lexical_cast<int>( argv[5] ); //each gene has a l_allel. Number of potential species is 2**(log2_allel*l_genome).
  double mutation_rate = boost::lexical_cast<double>( argv[6] );
  long sampling_interval = boost::lexical_cast<long>( argv[7] );
  long t_end = boost::lexical_cast<long>( argv[8] );
  unsigned long long seed = boost::lexical_cast<unsigned long long>( argv[9] );
  std::string id = argv[10];

  std::cerr << "Lists of given parameters are as follows:" << std::endl
	    << "fecundity:\t" << fecundity << std::endl
	    << "resource:\t" << resource << std::endl
	    << "b_max:\t" << b_max << std::endl
	    << "eta_max:\t" << eta_max << std::endl
	    << "eta_prob:\t" << eta_prob << std::endl
	    << "mij_max:\t" << mij_max << std::endl
	    << "mij_prob:\t" << mij_prob << std::endl
	    << "mii_max:\t" << mii_max << std::endl
	    << "l_genome:\t" << l_genome << std::endl
	    << "log2_allel:\t" << log2_allel << std::endl
	    << "mutation rate:\t" << mutation_rate << std::endl
	    << "t_end:\t" << t_end << std::endl
	    << "seed:\t" << seed << std::endl
	    << "id:\t" << id << std::endl;

  if( fecundity <= 1.0 || resource <= 0.0 || b_max <= 0.0 || eta_max <= 0.0 ||
      eta_prob <= 0.0 || eta_prob >= 1.0 || mij_max <= 0.0 || mij_prob <= 0.0 ||
      mij_prob >= 1.0 || mii_max <= 0.0 || l_genome <= 0 || t_end <= 0 || seed < 0) {
    std::cerr << "Given parameters are not valid for this model." << std::endl;
    throw 1;
  }

  //ofstreams
  std::string suvfilename = id + ".suv";
  std::ofstream suvout(suvfilename.c_str());

  std::string divfilename = id + ".diversity";
  std::ofstream divout(divfilename.c_str());

  std::string bmassfilename = id + ".biomass";
  std::ofstream bmassout(bmassfilename.c_str());

  std::string genomefilename = id + ".genome";
  std::ofstream genout(genomefilename.c_str());
  
  classTNIBMutation eco( fecundity, resource,
			 b_max, eta_max, eta_prob, mij_max, mij_prob, mii_max,
			 l_genome, log2_allel, mutation_rate, seed);

#ifdef BENCH
  clock_t start = clock();
#endif

  eco.Invade();

  //============== main loop ========================
  for( long t = 0; t < t_end; t++) {
    eco.UpdateMutate();
    if( (t%sampling_interval)==0 ) output( eco, suvout, divout, bmassout, genout, t, id);
  }
  //============== end of main loop =================
  output_final(eco,id);
#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<std::endl;
#endif
  return 0;
}



//--------------------    
void output( classTNIBMutation& eco, std::ofstream& suvout, std::ofstream& divout,
	     std::ofstream& bmassout, std::ofstream& genout, long time, std::string id) {
  static long times_output = 0;
  //======== output ===============
  int survivor = eco.numSurvivor();
  suvout << time << ' ' << survivor;
  for( int i=1; i<5; i++) suvout << ' ' << eco.numTrophSpecies(i);
  suvout << std::endl;
  
  divout << time << ' ' << eco.expSWindexAll() << ' '
	 << eco.expSWindexProducer() << ' ' << eco.expSWindexConsumer() << std::endl;
  
  bmassout << time << ' ' << eco.TotalBiomass();
  for( int i=1; i<5; i++) bmassout << ' ' << eco.TrophicBiomass(i);
  bmassout << std::endl;

  eco.GenomeOutput(genout,time);


  if( (time%1048576) == 0 ) {
    std::string temp = boost::io::str( boost::format("%04d") % static_cast<int>(time/1048576) );
    std::string afilename = id + ".a" + temp;
    std::string xfilename = id + ".x" + temp;
    eco.X_b_eta_output(xfilename.c_str());
    eco.Moutput(afilename.c_str());
  }

  times_output++;
}

//-----------------------
void output_final( classTNIBMutation & eco, std::string id){
  std::string afilename = id + ".afinal";
  std::string xfilename = id + ".xfinal";
  std::string lfilename = id + ".lspan";
  eco.X_b_eta_output(xfilename.c_str());
  eco.Moutput(afilename.c_str());
  eco.LspanOutput(lfilename.c_str());
}  
//----------------------end of the program-------------------------------
