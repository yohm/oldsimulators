#include "classTNIndBased.hpp"
#include "classTNIBMutation.hpp"

//#define DEBUG

//------------------------------------
classTNIBMutation::classTNIBMutation( int t_fecundity, double t_resource, 
				      double t_b_max, double t_eta_max, double t_eta_prob,
				      double t_mij_max, double t_mij_prob, double t_mii_max,
				      int t_l_genome, int t_log2_allel, double t_mutation_rate, unsigned long long t_seed)
  : classTNIndBased( t_fecundity, t_resource, t_seed) {
  b_max = t_b_max;
  eta_max = t_eta_max;
  eta_prob = t_eta_prob;
  mij_max = t_mij_max;
  mij_prob = t_mij_prob;
  mii_max = t_mii_max;
  l_genome = t_l_genome;
  l_allel = t_log2_allel;
  mutation_rate = t_mutation_rate;
  total_species = 1;
  for( int i=0; i<(l_genome*l_allel); i++) { total_species *= 2; }
  AllocateArrays();
  SetCoefficients();
}

//------------------------------------
void classTNIBMutation::AllocateArrays() {
  b_array = new double [total_species];
  eta_array = new double [total_species];
  prod_array = new bool [total_species];
  mij_x = new double [total_species];
  mij_y = new double [3*total_species];
  mii_array = new double [total_species];
  connect_x = new bool [total_species];
  connect_y = new bool [3*total_species];
}

//------------------------------------
void classTNIBMutation::SetCoefficients() {
  for( int i=0; i<total_species; i++) {
    b_array[i] = rnd.genrand64_real3() * b_max;
    if( rnd.genrand64_real3() < eta_prob ) {
      prod_array[i] = true;
      eta_array[i] = rnd.genrand64_real3() * eta_max;
    }
    else {
      prod_array[i] = false;
      eta_array[i] = 0;
    }
    mii_array[i] = - rnd.genrand64_real3() * mii_max;

    mij_x[i] = rnd.genrand64_real3() * mij_max;
    if( rnd.genrand64_real3() < sqrt(mij_prob) ) connect_x[i] = true;
    else connect_x[i] = false;
  }
  
  for( int i=0; i< 3*total_species; i++) {
    mij_y[i] = rnd.genrand64_real3() * mij_max;
    if( rnd.genrand64_real3() < sqrt(mij_prob) ) connect_y[i] = true;
    else connect_y[i] = false;
  }

#ifdef DEBUG
  for( int i=0; i<total_species; i++) {
    std::cerr << b_array[i] << ' ' << eta_array[i] << ' '<< connect_x[i] << ' ' << connect_y[i] << std::endl;
  }
  int connected = 0;
  for( int i=0; i<total_species; i++) {
    for( int j=0; j<i; j++) std::cerr << "0 ";
    std::cerr << mii_array[i] << ' ';
    for( int j=i+1; j<total_species; j++) {
      if( connect_x[(i^j)] && connect_y[(i^j)+2*(j+1)] ) {
	std::cerr << (mij_x[(i^j)]+mij_y[(i^j)+2*(j+1)]-1.0) << ' ';
	connected++;
      }
      else {
	std::cerr << 0 << ' ';
      }
    }
    std::cerr << std::endl;
  }
  std::cerr << "total connection : " << connected << std::endl;
#endif
}

//------------------------------
classTNIBMutation::~classTNIBMutation() {
  delete [] b_array;
  delete [] eta_array;
  delete [] prod_array;
  delete [] mij_x;
  delete [] mij_y;
  delete [] mii_array;
  delete [] connect_x;
  delete [] connect_y;
}

//------------------------------------
bool classTNIBMutation::UpdateMutate() {
  Update();
  return Mutate();
}

//------------------------------------
bool classTNIBMutation::Mutate() {
  if( x_tot <= 0 ) {
#ifdef DEBUG
    std::cerr << std::endl << std::endl << "Invasion started here." << std::endl << std::endl;
#endif
    Invade(); return false;} //there is no species
#ifdef DEBUG
  std::cerr << std::endl <<std::endl << "Mutation started here." << std::endl<< "Current configurations are:"<<std::endl;
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    std::cerr << id[i] << "\t" << x[i] << std::endl;
  }
#endif


#ifdef BASIC
  static const double prob_mutation_gene = mutation_rate / l_genome;
  for( int i=0; i<species; i++) { //loop for species
    if(!alive[i]) continue;
    for( int j=0; j<x[i]; j++) { //loop for individual who belongs to species i.
      long mutant_id = id[i];
      for( int k=0; k<l_genome; k++) { // loop for genome string
	if( rnd.genrand64_real3() < prob_mutation_gene) mutant_id = MutantID( mutant_id, k);
      }
      if(mutant_id != id[i]) { //j'th individual mutates to the species whose id is mutant_id.
	int mutant_number = SpeciesNumber(mutant_id); // if there is no species(mutants_id), mutant_number = -1.
	if( mutant_number < 0 ) {
	  mutant_number = ZeroPoint();
	  AddNewSpecies( mutant_number, mutant_id );
	}
	mutQueue.push(i);
	mutQueue.push(mutant_number);
      }
    }
  }
#endif

#ifndef BASIC
  static const double dev = 1.0 / log(1.0 - mutation_rate/l_genome);
  long gen = static_cast<long>( log(rnd.genrand64_real3())*dev );
  long mut_ind = gen / l_genome;
  long count = 0;
  for( int i=0; i<species; i++) {
    count += static_cast<long>(x[i]+0.5);
    while( mut_ind < count) { //species i is going to mutate
      long mutant_id = MutantID( id[i], (gen%l_genome) );
      gen +=  static_cast<long>( log(rnd.genrand64_real3())*dev ) + 1;
      while( mut_ind == (gen/l_genome) ) { //checking for multi-bit flip
	mutant_id = MutantID( mutant_id, (gen%l_genome) );
	gen += static_cast<long>( log(rnd.genrand64_real3())*dev ) + 1;
      }
      int mutant_number = SpeciesNumber( mutant_id); //searching for a mutant number
      if( mutant_number < 0 ) {
	mutant_number = ZeroPoint();
	AddNewSpecies( mutant_number, mutant_id );
      }
      mutQueue.push(i); mutQueue.push(mutant_number);
      mut_ind = gen / l_genome;
    }
  }  
#endif

  while( !mutQueue.empty() ) { //Set populations
    int p_species = mutQueue.front();
    mutQueue.pop();
    int m_species = mutQueue.front();
    mutQueue.pop();
    SetInitialPop( p_species, m_species);
#ifdef DEBUG
    std::cerr << "parent_id & mutant_id:\t" << id[p_species] << ' ' << id[m_species] << std::endl;
#endif
  }

  for(int i=0; i<species; i++) {
    if( x[i] < 1 ) Extinct(i);
  }
  CalcXtot();

  
  return true;
}

//------------------------------------
long classTNIBMutation::MutantID( long id, int flipping_genome) {
  if( l_allel == 1) return id^(1<<flipping_genome);
  unsigned long mask = 0;
  while( mask == 0) { mask = rnd.genrand64_bit(l_allel);}
  mask = mask<<(flipping_genome*l_allel);
  return id^mask;
}

//------------------------------------
int classTNIBMutation::ZeroPoint() {
  int temp = species;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) temp = i;
  }
  if( temp == max_species ) throw 10;

  if(temp == species) species++;

  if( species > current_max_species ) ReallocateMemory();
  
  return temp;
}

//------------------------------------
void classTNIBMutation::AddNewSpecies(int mutant_number, long mutant_id) {
  x[mutant_number] = 0;
  id[mutant_number] = mutant_id;
  alive[mutant_number] = true;
  age[mutant_number] = 0;
  b[mutant_number] = b_array[mutant_id];
  producer[mutant_number] = prod_array[mutant_id];
  eta[mutant_number] = eta_array[mutant_id];
  m[mutant_number][mutant_number] = mii_array[mutant_id];

  for( int i=0; i<species; i++) {
    if(!alive[i] || i==mutant_number) continue;
    long id1 = id[i];
    long id2 = mutant_id;
    if( id[i] > mutant_id ) { id2 = id[i]; id1 = mutant_id;} //id2 > id1
    long k = id1 ^ id2;
    if( connect_x[k] && connect_y[k+2*(id2+1)] ) {
      double temp = static_cast<double>(mij_x[k] + mij_y[k+2*(id2+1)] - 1.0);
      if( producer[mutant_number]==true && producer[i]==false ) m[mutant_number][i] = -fabs(temp);
      else if( producer[mutant_number]==false && producer[i]==true ) m[mutant_number][i] = fabs(temp);
      else if( mutant_id < id[i] ) { m[mutant_number][i] = temp;}
      else { m[mutant_number][i] = -temp;}
      m[i][mutant_number] = -m[mutant_number][i];
    }
  }
#ifdef DEBUG
  std::cerr << std::endl << "mutant_id :\t" << mutant_id << std::endl
	    << "mutant_number:\t" << mutant_number << std::endl
	    << "b :\t" << b[mutant_number] << std::endl
	    << "eta :\t" << eta[mutant_number] << std::endl;
  for( int i=0; i<species; i++) {
    if(!alive[i] ) continue;
    std::cerr << "id:\t"<<id[i]<<std::endl;
    for( int j=0; j<species; j++) {
      if(!alive[j]) continue;
      std::cerr << m[i][j] << ' ';
    }
    std::cerr << std::endl;
  }
#endif  
}

//------------------------------------
void classTNIBMutation::SetInitialPop(int parent_number, int mutant_number) {
  x[parent_number] -= 1;
  x[mutant_number] += 1;
}

//-------------------------------------
void classTNIBMutation::Invade() {
  long invader_id = static_cast<long>(rnd.genrand64_bit(l_genome*l_allel));
  int invader_number = SpeciesNumber(invader_id); // if there is no species(mutants_id), mutant_number = -1.
  if( invader_number < 0 ) {
    invader_number = ZeroPoint();
    AddNewSpecies( invader_number, invader_id );
  }
  x[invader_number] += 1;
  CalcXtot();
}
