#include <iostream>
#include <fstream>
#include <ctime>
#include "classEpsilonEco.cpp"
#include "classInvasionSystem.cpp"
#include "mtrand64.cpp"

//#define BENCH

int main (int argc, char* argv[]) {
  if( argc != 15) {
    std::cerr << "Input arguments" << std::endl
	      << "program <a_max> <m> <epsilon> <plant_scale> <x_initial> <num_plant> <growth> <dt> <invasion_interval> <output_interval> <t_end> <max_species> <seed> <ID>" << std::endl;
    exit(1);
  }
  double a_max = atof( argv[1] );
  int m = atoi(argv[2]);
  double epsilon = atof( argv[3] );
  double plant_scale = atof( argv[4] );
  double x_initial = atof( argv[5] );
  int num_plant = atoi( argv[6] );
  double growth_rate = atof( argv[7] );
  double dt = atof( argv[8] );
  double invasion_interval = atof( argv[9] );
  double output_interval = atof( argv[10] );
  long t_end = atol( argv[11] );
  int max_species = atoi( argv[12] );
  unsigned long long seed = atol( argv[13] );
  char* id = argv[14];
  
  char suvfilename[20];
  sprintf(suvfilename,"%s.suv",id);
  std::ofstream suvout(suvfilename);

  char divfilename[20];
  sprintf(divfilename,"%s.diversity",id);
  std::ofstream divout(divfilename);

  char jumyofilename[20];
  sprintf( jumyofilename, "%s.jumyo",id);

  char bmassfilename[20];
  sprintf(bmassfilename,"%s.biomass",id);
  std::ofstream bmassout(bmassfilename);

  char populationfilename[20];
  sprintf( populationfilename, "%s.pop",id);
  std::ofstream popout(populationfilename);
  
  classInvasionSystem eco( max_species, 0.01, dt, num_plant, growth_rate, //percentage is not used in this program.
			   epsilon, plant_scale, jumyofilename,x_initial, a_max, m, seed);

  int output_species = 125; //100種になったらa,yを出力

#ifdef BENCH
  clock_t start = clock();
#endif

  long inv = (long) (invasion_interval / dt);
  long output = (long)  (output_interval / dt);
  //============== main loop ========================
  long times_invasion = 0;
  for( ; times_invasion <t_end; times_invasion++) {
    //======== eco dynamics =========
    try{
      eco.AddInvador();
    } catch( int i) {
      std::cerr << "Number of species exceeds max_species" << std::endl;
      break;
    }

    for( long i=0; i< inv/output; i++) {
      eco.Xoutput(popout);
      eco.UpdateDynamicsTime(output);
    }
    
    //======== output ===============
    int survivor = eco.numSurvivor();
    suvout << times_invasion << ' ' << survivor;
    for( int i=1; i<7; i++) suvout << ' ' << eco.numTrophSpecies(i);
    suvout << std::endl;

    divout << times_invasion << ' ' << eco.expSWindexAnimals();
    for( int i=1; i<7; i++) divout << ' ' << eco.expSWindexAnimals(i);
    divout << std::endl;

    bmassout << times_invasion << ' ' << eco.TotalAnimalBiomass();
    for( int i=1; i<7; i++) bmassout << ' ' << eco.TotalAnimalBiomass(i);
    bmassout << std::endl;

    if( survivor == output_species ) {
	char afilename[15];
	char xfilename[15];
	sprintf( afilename, "%s.a%04d",id,survivor);
	sprintf( xfilename, "%s.x%04d",id,survivor);
	eco.Xoutput(xfilename);
	eco.Aoutput(afilename);
	output_species *= 2;
    }
  }
  //======== loop end ===============    
  char afilename[20];
  char xfilename[20];
  int survivor = eco.numSurvivor();
  sprintf( afilename, "%s.afinal",id,survivor);
  sprintf( xfilename, "%s.xfinal",id,survivor);
  eco.Xoutput(xfilename);
  eco.Aoutput(afilename);


#ifdef BENCH
  clock_t end = clock();
  cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC) <<endl;
#endif
  
  return 0;
}
