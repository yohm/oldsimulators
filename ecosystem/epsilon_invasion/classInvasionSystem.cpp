#include "classEpsilonEco.hpp"
#include "classInvasionSystem.hpp"

//------------------------------------
classInvasionSystem::classInvasionSystem(int t_max_species, double t_percentage, double t_dt,
					 int t_num_plant, double t_growth_rate,
					 double t_epsilon, double t_plant_scale, const char* jumyofilename,
					 double t_x_initial,
					 double t_a_max, int t_m, unsigned long long t_seed)
  : classEpsilonEco( t_max_species, t_percentage, t_dt, t_num_plant, t_growth_rate, t_epsilon, t_plant_scale, jumyofilename) {
  x_initial = t_x_initial;
  a_max = t_a_max; m = t_m; seed = t_seed;
  rnd.init_genrand64( seed );
}

//------------------------------------
bool classInvasionSystem::AddInvador() {
  int invador_id = ZeroPoint();
  SetRandomInteractionsMetabRate( invador_id );
  SetInitialPop( invador_id );
  IncrementAge();
}

//------------------------------------
int classInvasionSystem::ZeroPoint() {
  int temp = species;
  for(int i=0; i<species; i++) {
    if(!(alive[i])) temp = i;
  }
  if( temp == max_species ) throw 10;

  if(temp == species) species++;

  if( species > current_max_species ) ReallocateMemory();
  
  return temp;
}

//------------------------------------
void classInvasionSystem::SetRandomInteractionsMetabRate(int id) {
  b[id] = 1.0;
  int survivor = numSurvivor();
  int num_inter;
  num_inter = (int) (m * rnd.genrand64_real3() + 1);
  if(survivor < num_inter) num_inter = survivor;
  
  int inter_species;
  int kouho = survivor;
  for( ; num_inter>0; num_inter--, kouho--) {

    inter_species = (int) (kouho * rnd.genrand64_real3() ) ;

    for(int i=0; i<species; i++) {
      if(alive[i] && a[i][id] == 0) {
        if(inter_species > 0) inter_species--;
        else {
          if(i>=num_plant) a[i][id] = (2*rnd.genrand64_real3() - 1) * a_max;
          else  a[i][id] = (-1*rnd.genrand64_real3()) * a_max;

          a[id][i] = -a[i][id];
          break;
        }
      }
    }
  }
}

//------------------------------------
void classInvasionSystem::SetInitialPop(int id) {
  x[id] = x_initial;
  alive[id] = true;
  age[id] = 0;
}

//------------------------------------
void classInvasionSystem::IncrementAge() {
  for( int i=0; i<species; i++) {
    if(!alive[i]) continue;
    age[i]++;
  }
}
