#include <iostream>
#include <fstream>
#include <cmath>

#ifndef _C_EPSILON_H_
#define _C_EPSILON_H_

class classEpsilonEco {
public:
  classEpsilonEco(int t_max_species, double t_percentage, double dt,
		  int t_num_plant, double t_growth_rate,
		  double t_epsilon, double t_plant_scale, const char* jumyofilename);
  //population dynamics is \dot{x} = -x + \sum_j aij x_i^{1-\epsilon} x_j^{1-\epsilon}
  //plant : \dot{x} = growth_rate * x * ( plant_scale - x) + (interaction terms)
  ~classEpsilonEco();
  void UpdateDynamicsStable(); //update population dynamics until all x become stable
  void UpdateDynamicsTime( int i); //update population dynamics for i steps.
  int numSurvivor() const; //return number of animals
  double expSWindexAnimals() const; // return exponential Shannon-Wiener index for all animal species.
  int numTrophSpecies( int t_level); //return number of species belonging to trophic level t_level.
  double expSWindexAnimals(int t_level); // return exponential Shannon-Wiener index for animal species in trophic level t_level.
  void Xoutput( std::ofstream & t_fout); //output x[i] to the ofstream. "x[0] x[1] x[2] .... \n"
  void Xoutput( const char* filename); //output x[i] to file. "x[0]\nx[1]\nx[2]\n...."
  void Aoutput( const char* filename); //output a[i][j] to the file 'filename'.
  double TotalAnimalBiomass(); //return total biomass of all animal species.
  double TotalAnimalBiomass(int t_level); //return summation of x of animal species in trophic level t_level.
  bool Alive(int i) {return alive[i];}
protected:
  double *x_0,*x_dash_0,*x_dash_1,*x_dash_2,*x_dash_3,*x,**a, *b;
  //b denotes metablic rate. always positive
  int *trophic_level,*age;
  bool *alive;
  int num_plant;
  double plant_scale; //scale of plant ( scale of whole system )
  int species;
  double dt;
  double threshold; // When epsilon > 0, threshold should be zero.
  //In practice, you should set this value to very small value (10^-200) for the stability of numerical integration.
  void AllocateMemory(int size); //called in constructor.
  void ReleaseMemory(); //called in destructor and ReaalocateMemory.
  int inv_ng;
  int max_species,current_max_species;
  void ReallocateMemory();
  
  int Solve(); //return invasion_ng. invasion_ng == 0 means system becomes stable.
  double percentage; //used for judging if the system is stable or not.
  void Extinct(int i); //Extinction of species i.
  std::ofstream jumyo_out;
  
  double Interaction( int i, int j) const; //return the interaction between species i and species j.
  double epsilon; //interaction term is x_i^{1-\epsilon}x_j^{1-epsilon}
  
  int TrophicCount(); //Count trophic levels. This routine was called from numTrophSpecies.
  bool trophic_level_counted_flag; //When trophic level and sum_trophic have been counted, true. Else false.
  int sum_trophic[10]; //sum_trophic[i] means the number of species belonging to trophic_level i
  //  int zero_p() const;
  //  void Ofclose();
  bool Overmaxspecies();
};

#endif
