#include "classEpsilonEco.hpp"
#include "mtrand64.hpp"

#ifndef _C_INVSYS_H_
#define _C_INVSYS_H_

class classInvasionSystem : public classEpsilonEco {
public:
  classInvasionSystem( int t_max_species, double t_percentage, double t_dt,
		       int t_num_plant, double t_growth_rate,
		       double t_epsilon, double t_plant_scale, const char* jumyofilename,
		       double t_x_initial, double t_a_max, int t_m,
		       unsigned long long t_seed);
  ~classInvasionSystem() {};
  bool AddInvador(); //add one invador. Throw exception(10) when species exceeds max_species.
protected:
  double x_initial;
  double a_max;
  int m;
  unsigned long long seed;
  int ZeroPoint();
  void SetRandomInteractionsMetabRate(int zero_point); //
  void SetInitialPop( int zero_point );
  void IncrementAge();
  mtrand64 rnd;
};


#endif
