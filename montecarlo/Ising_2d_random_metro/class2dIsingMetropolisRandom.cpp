#include "class2dIsingMetropolisRandom.hpp"

//----------------------
class2dIsingMetropolisRandom::class2dIsingMetropolisRandom() {
  Lpow = L = N;
  spin = NULL; rnd = NULL;
  for( int i=0; i<3; i++) prob[i] = 0;
}
//----------------------
class2dIsingMetropolisRandom::~class2dIsingMetropolisRandom() {
  delete [] spin; delete rnd;
}
//----------------------
void class2dIsingMetropolisRandom::Initialize(int t_Lpower, double t_K,
					      unsigned long long sed) {
  Lpow = t_Lpower;
  Npow = Lpow * 2;
  L = 1 << Lpow;
  N = L*L;
  spin = new bool[N];
  K = t_K;
  seed = sed;
  rnd = new mtrand64(seed);
  for( long i=0; i<N; i++) spin[i] = true;
  unsigned long max = (1ULL << 32) -1 ; //2**32 -1
  for( int i=0; i<3; i++) prob[i] = exp(-i*4.0*K)*max;
}
//----------------------
void class2dIsingMetropolisRandom::Update() {
  unsigned long long mask = (1ULL << 32) -1;
  unsigned long long buffer = 0ULL;
  bool buffered = false;
  for( long i=0; i<N; i++) {
    long us = rnd->genrand64_bit( Npow);
    int delta_e = EnergyGain(us);
    //debug
    //    std::cerr << "delta_e:"<<delta_e << std::endl;
    //
    if( delta_e <= 0) {spin[us] = !(spin[us]);
      //     std::cerr << "negative" << std::endl;
    }
    else if( buffered ) {
      //debug
      //      std::cerr << "buffered : " << buffer << ' ' << prob[delta_e] << std::endl;
      //

      if( buffer < prob[delta_e] ) {
	spin[us] = !spin[us];// std::cerr<<"flipped"<<std::endl;
      }
      buffered = false;
    }
    else if( !buffered) {
      unsigned long long r = rnd->genrand64_int64();
      unsigned long temp = (r & mask);
      buffer = (r & ~mask) >> 32;
      buffered = true;
      //debug
      //      std::cerr << "r : " << r << std::endl;
      //      std::cerr << temp << ' ' << prob[delta_e] << std::endl;
      //
      if( temp < prob[delta_e] ) {spin[us] = !spin[us];}// std::cerr<<"flipped"<<std::endl;}
    }
  }
}
//-----------------------
inline int class2dIsingMetropolisRandom::EnergyGain(long spinid) {
  long next = spinid + 1;
  long previous = spinid -1;
  long spinBottom = spinid-L;
  long spinTop = spinid+L;
  if( spinid < L) {
    spinBottom += N;
  }
  else if( spinid > N-L-1) {
    spinTop -= N;
  }
  if( (spinid % L) == 0 ) {
    previous += L;
  }
  else if( (spinid % L) == L-1 ) {
    next -= L;
  }
  int deltaE = 2;
  if(spin[spinid]^spin[next]) deltaE--;
  if(spin[spinid]^spin[previous]) deltaE--;
  if(spin[spinid]^spin[spinTop]) deltaE--;
  if(spin[spinid]^spin[spinBottom]) deltaE--;
  //debug
  //  std::cerr << spin[spinid]<<' '<<spin[next]<<' '<<spin[previous]<<' '<<spin[spinTop]<<' '<<spin[spinBottom]<<std::endl;
  //  std::cerr << deltaE<<std::endl;
  return deltaE;
}
//-----------------------
double class2dIsingMetropolisRandom::OrderParameter() {
  int mag=0;
  for(int i=0; i<N; i++) {
    if(spin[i]) mag++;
    else mag--;
  }
  double temp = (double)mag/(double)N;
  return temp;
}
//-----------------------
double class2dIsingMetropolisRandom::Energy() {
  long ene = -N;//ground state
  for( int i=0; i<N; i++) {
    long next = i+1;
    long top = i+L;
    if( i > N-L-1) top -= N;
    if( (i%L) == L-1 ) next -= L;

    if( spin[i]^spin[next] ) ene++;
    if( spin[i]^spin[top] ) ene++;
  }
    
  return (double)ene/(double)N;
}

//-----------------------
void class2dIsingMetropolisRandom::OutputConfiguration(const char* filename) {
  std::ofstream fout( filename);
  for( int i=0; i<L; i++) {
    for( int j=0; j<L; j++) fout << spin[i*L+j];
    fout << std::endl;
  }
  fout.close();
}

