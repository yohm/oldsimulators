#include <iostream>
#include <ctime>
#include <fstream>
#include "class2dIsingMetropolisRandom.cpp"
#include "mtrand64.cpp"

//Ising model on square lattice.
//Random update dynamics with Metropolis type transiton probability.
//All spin-up configuration is used for the initial state.
//Exact value of Kc = 0.440686793509771. ( -0.5 * log( sqrt(2.0) - 1.0 ) )
//Bit length of random number is 32.

int main(int argc, char* argv[]) {
  
  if(argc!=6) {
    std::cerr << "Error! Invalid number of arguments." << std::endl;
    std::cerr << "<program> <power_of_L> <K> <tmax> <seed> <data_id>"<< std::endl;
    exit(1);
  }

  int lpow = atoi( argv[1] );
  double k = atof( argv[2] );
  long tmax = atol( argv[3]);
  unsigned long long sed = atol( argv[4]);
  
  char magfilename[50];
  sprintf( magfilename, "%s.NERo", argv[5]);
  std::ofstream magout( magfilename);
  magout.precision(10);
  char enefilename[50];
  sprintf( enefilename, "%s.NERe",argv[5]);
  std::ofstream eneout( enefilename);
  eneout.precision(10);
  
  class2dIsingMetropolisRandom sim;
  sim.Initialize( lpow, k, sed);

#ifdef BENCH
  clock_t start,end;
  start = clock();
#endif
  
  magout << 0 <<' '<< sim.OrderParameter() << std::endl;
  eneout << 0 <<' '<< sim.Energy() << std::endl;
  for( int t=1; t<tmax; t++) {
    sim.Update();
    magout << t <<' '<<sim.OrderParameter() << std::endl;
    eneout << t <<' '<<sim.Energy() << std::endl;
  }

  //  sim.OutputConfiguration("conf.dat");

#ifdef BENCH
  end = clock();
  std::cerr << (double)(end-start)/(CLOCKS_PER_SEC) << std::endl;
#endif

  return 0;
}
