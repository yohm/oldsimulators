#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"

//Ising model on square lattice.
//L = 2 ^ Lpower. Periodic boundary condition.
//Random update dynamics with Metropolis type transition probability.
class class2dIsingMetropolisRandom {
public:
  class2dIsingMetropolisRandom();
  ~class2dIsingMetropolisRandom();
  void Initialize(int Lpower, double K, unsigned long long sed);
  void Update(); //update ranodmly chosen N sites
  double OrderParameter(); //return magnetization
  double Energy();
  void OutputConfiguration(const char* filename);
  //  void BinaryDump( const char* filename );
private:
  short Lpow, Npow;
  long L,N;
  double K;
  bool * spin;
  mtrand64 *rnd;
  unsigned long long seed; //seed of random number;
  int EnergyGain( long spinid);
  unsigned long prob[3];//transition probabilities * (2**32).
};
