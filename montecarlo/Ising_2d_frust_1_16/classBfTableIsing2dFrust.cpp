#include "classBfTableIsing2dFrust.hpp"
using namespace std;

//---------------------------------
classBfTableIsing2dFrust::classBfTableIsing2dFrust() {
  tableSizePower = 0;
  tableSize = totalTableSize = 0;
  bfSeed = 0;
  inverseTemperature = 0;
  table = NULL;
}
//----------------------------------
classBfTableIsing2dFrust::~classBfTableIsing2dFrust() {
  delete [] table;
}
//----------------------------------
void classBfTableIsing2dFrust::Initialize(int pow, double K, unsigned long long bf_seed) {
  if( tableSizePower == pow && inverseTemperature == K && bfSeed == bf_seed) {return;}

  if( pow != tableSizePower) { //領域のとりなおし
    tableSizePower = pow;
    tableSize = 1 << tableSizePower;
    totalTableSize = tableSize * 2;
    delete [] table;
    table = new long long[ totalTableSize];
  }

  for( long i=0; i<totalTableSize; i++) table[i] = 0;

  inverseTemperature = K;
  bfSeed = bf_seed;
  MakeRandomTable( inverseTemperature, bfSeed);
}
//---------------------------------------------------------
void classBfTableIsing2dFrust::MakeRandomTable(double K, unsigned long long seed) {
  SetBit( K);
  ShuffleTable( seed);
}
//----------------------------------------------------------
void classBfTableIsing2dFrust::SetBit(double K) {
  long Num[3]; //Num[i]:{exp(-K*i)}*tableSize;
  Num[2] = (long) (exp(-8.0*K) * tableSize);
  Num[1] = (long) (exp(-4.0*K) * tableSize);
  Num[0] = tableSize;

  for( long j=0; j<Num[2]; j++) { //j番目の要素
    table[2*j] = -1;
    table[2*j+1] = 0; //set 2
  }
  for( long j=Num[2]; j<Num[1]; j++) {
    table[2*j] = 0;
    table[2*j+1] = -1; //set 1
  }
  for( long j=Num[1]; j<Num[0]; j++) {
    table[2*j] = 0;
    table[2*j+1] = 0; //set 0
  }
}
//----------------------------------------------------------
void classBfTableIsing2dFrust::ShuffleTable(unsigned long long seed) {
  mtrand64 rnd( seed);

  for( int i=0; i<64; i++) {
    for(int j=0; j<tableSize; j++) {
      long random = rnd.genrand64_bit(tableSizePower);
      //0 ~ tableSize-1 :random number
      Exchange( i, 2*j, 2*random);
      Exchange( i, 2*j+1, 2*random+1);
    }
  }
}
//----------------------------------------------------------
void classBfTableIsing2dFrust::Exchange( int i, int j, int k) {
  long long mask = 1LL;
  mask <<= i; //i-bit目のマスク
  long long temp1 = table[j] & mask; //table[j] の i-bit目
  long long temp2 = table[k] & mask; //table[k] の i-bit
  
  table[j] = table[j] & (~mask); //i-bit目以外は変更しない
  table[k] = table[k] & (~mask); //i-bit目は0にする

  table[j] = table[j] | temp2; //jにkのi-bitを入れる
  table[k] = table[k] | temp1; //kにjのi-bitを入れる
}
//---------------------------------------------------------
#ifdef DEBUG
void classBfTableIsing2dFrust::TableOutputBit() {
  for( int j=0; j<totalTableSize; j++) {
    long long mask = 1LL;
    for( int i=63; i>=0; i--) {
      mask = (1LL << i);
      if( (table[j] & mask)==0 ) cout << 0;
      else cout << 1;
    }
    cout << endl;
  }
}
//--------------------------------------------------------
void classBfTableIsing2dFrust::TableOutputNumber() {
  for( int j=0; j<tableSize; j++) {
    long long mask = 1LL;
    for( int i=63; i>=0; i--) {
      mask = (1LL << i);
      short temp = 0;
      if( table[3*j] & mask) temp += 4;
      if( table[3*j+1] & mask) temp += 2;
      if( table[3*j+2] & mask) temp += 1;
      cout << temp;
    }
    cout << endl;
  }
}
#endif
