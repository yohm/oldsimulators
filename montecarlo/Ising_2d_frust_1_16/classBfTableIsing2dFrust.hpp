#include <iostream>
#include <cmath>
#include "mtrand64.hpp"
using namespace std;

#ifndef _BFTABLE_ISING2DFRUST_H_
#define _BFTABLE_ISING2DFRUST_H_

#define DEBUG

class classBfTableIsing2dFrust {
  //64 sample 同じ温度についてしか計算できない
  //tableにはそれぞれのテーブルの要素を交互に並べてく。
  //IX4,IX2,IX1の順番で並べる。
public:
  classBfTableIsing2dFrust();
  ~classBfTableIsing2dFrust();
  void Initialize( int pow, double K, unsigned long long seed);
  //Initialize and allocate memory.If pow,K,seed are unchanged, this routine don't remake table.
  int TableSizePower() {return tableSizePower;}
  long long GetTableElement( long i) { return table[i];}
#ifdef DEBUG
  void TableOutputBit();//Output table bit
  void TableOutputNumber();
#endif //DEBUG
protected:
  long long * table;
  int tableSizePower;//tableSize = 2^(tableSizePower)
  long tableSize;
  long totalTableSize;//totalTableSize = tableSize*3
  unsigned long long bfSeed;
  double inverseTemperature;
  void MakeRandomTable(double K, unsigned long long seed);//sub of "initialize"
  void SetBit(double K);
  void ShuffleTable(unsigned long long seed);
  void Exchange(int i,int j,int k);//i-bit目のtable[j]とtable[random]の値を入れ替える
};

#endif
