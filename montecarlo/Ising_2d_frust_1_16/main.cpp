#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include "mtrand64.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTableIsing2dFrust.cpp"
#include "classIsing2dFrustMulti.cpp"

using namespace std;

#define BENCH

//input the arguments as follows
// program L k Tmax seed bitlength data_id
// ./a.out 300 0.5483 1024 23419721 20 hoge

int main(int argc, char* argv[]) {
  if(argc!=7) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <L> <k> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }

  //  clock_t start,end;
  long l=atoi( argv[1]);
  double k = atof( argv[2]);
  long tmax = atoi( argv[3]);
  unsigned long long sed = atol( argv[4]);
  int bitwt = atoi( argv[5]);
  unsigned long long bf_sed = sed + 12345LL;
  char str[20];
  strcpy( str, argv[6]);
  strcat( str, ".NERo");

  classIsing2dFrustMulti sim;
  sim.Initialize( l, l, k, sed, bitwt, bf_sed);

  ofstream fout(str);
  fout.precision(10);

#ifdef BENCH
  clock_t start = clock();
#endif

  
  for( int t=0; t<=tmax; t++) {
    fout << t << " " << sim.OrderParameter() <<endl;
    sim.Update();
  }
  
#ifdef BENCH
  clock_t end = clock();
  cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC)<<endl;
#endif
  
  fout.close();
  return 0;
}
