#include "classIsing2dFrustMulti.hpp"

//------------------------
classIsing2dFrustMulti::classIsing2dFrustMulti() {
  //適当な初期値を設定
  Lx=1;Ly=0; K=0.1;
  numAllocation = 0;
  spinA = spinB = spinC = spinD = NULL;
  rnd = NULL;
  seedOfRandomNumber = 0;
  bitWidth=0;
}

//------------------------
classIsing2dFrustMulti::~classIsing2dFrustMulti() {
  delete [] spinA;
  delete [] spinB;
  delete [] spinC;
  delete [] spinD;
  delete [] rnd;
}

//------------------------
void classIsing2dFrustMulti::Initialize(long lx, long ly, double k, unsigned long long sed,
					int bitwt, unsigned long long bfsed) {
  MemoryAllocation(lx,ly,k,sed);
  TableAllocation(bitwt, bfsed);
  InitialConfiguration();
}

//------------------------
void classIsing2dFrustMulti::MemoryAllocation(long lx, long ly, double k, unsigned long long sed) {
  long newAllocation = lx*(ly+2); //2列分コピー領域
  if( newAllocation != numAllocation) {
    delete [] spinA; delete [] spinB; delete [] spinC; delete [] spinD;
    spinA = new long long [newAllocation];
    spinB = new long long [newAllocation];
    spinC = new long long [newAllocation];
    spinD = new long long [newAllocation];
  }
  Lx = lx; Ly = ly; LxLy = Lx*Ly; numAllocation = newAllocation;
  delete rnd;
  rnd = new mtrand64(sed);
  seedOfRandomNumber = sed;
  bwmsc.Bitwise_multispincoding_Initialization(LxLy);
  K = k;
}
//-----------------------------
void classIsing2dFrustMulti::TableAllocation(int bitwt, unsigned long long bfsed) {
  table.Initialize(bitwt, K, bfsed);
  bitWidth = bitwt;
}
//-----------------------------
void classIsing2dFrustMulti::InitialConfiguration() {
  //set all up configuration
  for( long i=0; i<numAllocation; i++) {
    spinA[i] = spinB[i] = spinC[i] = spinD[i] = -1ULL;
  }
}
//-----------------------------
void classIsing2dFrustMulti::Update() {
  //update spinA
  UpdateSubAC( &spinA[Lx], &spinB[Lx-1], &spinC[0]); //us , left spin, bottom spin
  //update spinD
  UpdateSubBD( &spinD[Lx], &spinC[Lx], &spinB[2*Lx], & spinB[Lx]);//us, left spin,ferro spin, antiferro spin
  //copy spin
  for( long i=0; i<Lx; i++) {
    spinD[i] = spinD[i+LxLy];
    spinA[i+Lx*(Ly+1)] = spinA[i+Lx];
  }
  //update spinB
  UpdateSubBD( &spinB[Lx], &spinA[Lx], &spinD[0], &spinD[Lx] );
  //update spinC
  UpdateSubAC( &spinC[Lx], &spinD[Lx-1], &spinA[Lx]);
  //copy spin
  for( long i=0; i<Lx; i++) {
    spinB[i] = spinB[i+LxLy];
    spinB[i+Lx*(Ly+1)] = spinB[i+Lx];
    spinC[i] = spinC[i+LxLy];
    spinC[i+Lx*(Ly+1)] = spinC[i+Lx];
  }
}
//-----------------------------
void classIsing2dFrustMulti::UpdateSubAC(long long*us, long long*left, long long*bottom) {
  long long * right = left + 1;
  long long * top = bottom + Lx;
  for( long i=0; i<LxLy; i++) {
    Flip(*us,*left,*bottom,*right,*top);
    us++;    left++;    bottom++; right++; top++;
  }
}
//-----------------------------
void classIsing2dFrustMulti::UpdateSubBD(long long*us, long long*left, long long*fe, long long* af) {
  long long* right = left+1;
  long long temp = 0;
  for( long i=0; i<LxLy; i++) {
    temp = *af;
    if( i%2) {Flip(*us,*right,*left,*fe,~temp);}
    else {Flip(*us,*right,*left,*fe,temp);}
    us++;    left++;  right++; fe++; af++;
  }
}
//-----------------------------
inline void classIsing2dFrustMulti::Flip(long long&us,const long long&s1, const long long&s2,
					 const long long&s3, const long long&s4) {
  long long K1,K2,K3,K4,J1,J2,IX1T,IX2T,K1T,ID,S4;
  long ran = rnd->genrand64_bit(bitWidth);
  IX2T = table.GetTableElement(ran*2) ;
  IX1T = table.GetTableElement(ran*2+1) ;
  K1=s1^s2;
  K2=s1&s2;
  J2=K1^s3;
  K3=K1&s3;
  J1=K2|K3;
  J1=us^J1;
  J2=us^J2;
  S4=us^s4;
  K2=S4^IX1T;
  K1T=S4&IX1T;
  K1=K1T|IX2T;
  ID=J1|K1;
  K4=J2&K2;
  ID=ID|K4;
  us^=ID;
}
//-----------------------------
double classIsing2dFrustMulti::OrderParameter() {
  long imagA[64],imagB[64],imagC[64],imagD[64];
  bwmsc.NIbitcount(spinA, LxLy, imagA);
  bwmsc.NIbitcount(spinB, LxLy, imagB);
  bwmsc.NIbitcount(spinC, LxLy, imagC);
  bwmsc.NIbitcount(spinD, LxLy, imagD);
  double total = 0.0;
  for( int i=0; i<64; i++) {
    total += (double)(imagA[i]+imagB[i]+imagC[i]+imagD[i])/(2*LxLy)-1.0;
  }
  return total/64.0;
}
