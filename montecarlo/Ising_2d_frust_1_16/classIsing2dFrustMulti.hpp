//Ising model on square lattice.
//Anti-ferromagnetic bond is regularly disposed.
#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"
#include "classBfTableIsing2dFrust.hpp"
#include "vcpp2gcc.h"
#include "Bitwise_multispincoding.h"
using namespace std;

#ifndef ISING2D_FRUST_MULTI_H_
#define ISING2D_FRUST_MULTI_H_

class classIsing2dFrustMulti {
public:
  classIsing2dFrustMulti();
  ~classIsing2dFrustMulti();
  void Update();
  double OrderParameter();
  double Energy();
  void Initialize(long lx, long ly, double specK, unsigned long long sed, int bitwt, unsigned long long bf_sed);
  void Initialize(long lx, long ly, double specK, unsigned long long sed, int bitwt){
    Initialize( lx, ly, specK, sed, bitwt, sed+12345);}
  void InitialConfiguration();//set all 0 config
private:
  long Lx,Ly,LxLy,numAllocation;
  double K;
  unsigned long long seedOfRandomNumber, seedOfBfTable;
  long long * spinA, *spinB,*spinC,*spinD;
  mtrand64* rnd;
  int bitWidth;
  classBfTableIsing2dFrust table;
  Bitwise_multispincoding bwmsc;
  void MemoryAllocation(long lx, long ly, double k, unsigned long long sed);
  void TableAllocation(int bitwt, unsigned long long bfsed);
  void UpdateSubAC(long long*us, long long*left, long long*bottom);//us, left, bottom
  void UpdateSubBD(long long*us, long long*left, long long*fer, long long*af);//us, left, ferro, anti-ferro
  void Flip(long long&us,const long long&s1, const long long&s2, const long long&s3, const long long&s4);
};

#endif
