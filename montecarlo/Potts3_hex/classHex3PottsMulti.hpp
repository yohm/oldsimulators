#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"
#include "classBfTableHex3Potts.hpp"
#include "Bitwise_multispincoding.h"
using namespace std;

#ifndef POTTS3HEXMULTI_H_
#define POTTS3HEXMULTI_H_

class classHex3PottsMulti {
public:
  classHex3PottsMulti();
  ~classHex3PottsMulti();
  void Update();
  double AveragedOrderParameter();
  double Energy(){return 0;}
  void Initialize(long lx, long ly, double specK, unsigned long long sed,
		  int bit_width, unsigned long long bf_seed);
  void Initialize(long lx, long ly, double specK, unsigned long long sed,
		  int bit_width) {
    Initialize(lx,ly,specK,sed,bit_width,sed+12345);
  }
  //Set parameters and allocate memory and set all-2 configuration
  void InitialConfiguration();
  void ConfigurationOutput(int i);//output configuration of i-sample
private:
  long Lx,Ly,LxLy,numAllocation;
  double K;//inverse temperature
  unsigned long long seedOfRandomNumber, seedOfBfTable;
  int randomBitWidth;
  long long *spin1,*spin2; //state = spin1*1+spin2*2 (0,1,2)
  mtrand64* rnd;
  classBfTableHex3Potts bfTab;
  Bitwise_multispincoding bwmsc;
  void MemoryAllocation(long lx, long ly, unsigned long long sed);
  inline void UpdateSubOdd( const long & i);
  inline void UpdateSubEven( const long & i);
  inline void CopySub();
  void Plus(const long long&a,const long long&b,const long long&c,long long&d,long long&e);
  void Plus(const long long&a,const long long&b,const long long&c,long long&d);
};
#endif
