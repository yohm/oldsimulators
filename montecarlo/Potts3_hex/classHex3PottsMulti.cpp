#include "classHex3PottsMulti.hpp"
using namespace std;

//-------------------
classHex3PottsMulti::classHex3PottsMulti() {
  //適当な初期値を設定
  Lx=1;Ly=0; K=0.1;
  numAllocation = 0;
  spin1 = spin2 = NULL;
  rnd = NULL;
  seedOfRandomNumber = 0;
}

//------------------------
classHex3PottsMulti::~classHex3PottsMulti() {
  delete [] spin1;
  delete [] spin2;
  delete [] rnd;
}

//------------------------
void classHex3PottsMulti::Initialize(long lx, long ly, double k,
				    unsigned long long sed,
				    int bit_width,
				    unsigned long long bf_seed) {
  MemoryAllocation(lx,ly,sed);
  bfTab.Initialize(bit_width,k,bf_seed);
  seedOfBfTable = bf_seed; randomBitWidth = bit_width; K = k;
  InitialConfiguration();
}
//------------------------
void classHex3PottsMulti::MemoryAllocation(long lx, long ly,
					   unsigned long long sed) {
  if( (lx%2) == 0) {
    cerr << "Error! in classHex3PottsMulti."<<endl<<"lx must be odd."<<endl;
    exit(1);
  }
  if( (ly%2) == 1) {
    cerr << "Error! in classHex3PottsMulti."<<endl<<"ly must be even."<<endl;
    exit(1);
  }
  long newAllocation = lx*(ly+2);
  if( newAllocation != numAllocation) {
    delete [] spin1;
    delete [] spin2;
    spin1 = new long long [newAllocation];
    spin2 = new long long [newAllocation];
  }
  Lx = lx; Ly = ly; LxLy = Lx*Ly; numAllocation = newAllocation;

  delete rnd;
  rnd = new mtrand64(sed);
  seedOfRandomNumber = sed;
  bwmsc.Bitwise_multispincoding_Initialization(LxLy);
}
//-----------------------------
void classHex3PottsMulti::InitialConfiguration() {
  //set all 2 configuration
  for( long i=0; i<numAllocation; i++) {
    spin1[i] = 0LL;
    spin2[i] = -1LL;
  }
}
//----------------------------
void classHex3PottsMulti::Update() {
  //update odd spin
  for( long i=Lx; i<Lx*(Ly+1); i+=2) {
    UpdateSubOdd(i);
  }
  //copy usukawa
  CopySub();
  //update even spin
  for( long i=Lx+1; i<Lx*(Ly+1); i+=2) {
    UpdateSubEven(i);
  }
  //copy usukawa
  CopySub();
}
//---------------------------
inline void classHex3PottsMulti::UpdateSubOdd( const long& i) {
  //select new state
  long long r = rnd->genrand64_int64();
  long long us1 = ~spin1[i] & r;
  long long us2 = (~spin2[i]) & (~r);
  //calc IE
  long long b1 = (spin1[i-1] ^ spin1[i]) | (spin2[i-1] ^ spin2[i]);//そろっていなかったらtrue
  long long b2 = (spin1[i+Lx] ^ spin1[i]) | (spin2[i+Lx] ^ spin2[i]);
  long long b3 = (spin1[i-Lx] ^ spin1[i]) | (spin2[i-Lx] ^ spin2[i]);
  long long c1 = (~spin1[i-1] ^ us1) & (~spin2[i-1] ^ us2);
  long long c2 = (~spin1[i+Lx] ^ us1) & (~spin2[i+Lx] ^ us2);
  long long c3 = (~spin1[i-Lx] ^ us1) & (~spin2[i-Lx] ^ us2);
  //calc IX;
  long random = rnd->genrand64_bit(randomBitWidth);
  long long ix2 = bfTab.GetTableElement(2*random);
  long long ix1 = bfTab.GetTableElement(2*random+1);
  long long d2,d1,e2,e1,f2,f1,g2,g1,h2;
  //IE+IX(r)
  Plus(ix1,b1,b2,d2,d1);
  Plus(b3,c1,c2,e2,e1);
  Plus(d1,e1,c3,f2,f1);
  Plus(ix2,d2,e2,g2,g1);
  Plus(g1,f2,f1,h2);
  long long ID = g2 | h2;
  //update to new state
  spin1[i] = (ID & us1) | (~ID & spin1[i]);
  spin2[i] = (ID & us2) | (~ID & spin2[i]);
}
//--------------------------
inline void classHex3PottsMulti::UpdateSubEven( const long& i) {
  //select new state
  long long r = rnd->genrand64_int64();
  long long us1 = ~spin1[i] & r;
  long long us2 = (~spin2[i]) & (~r);
  //calc IE
  long long b1 = (spin1[i+1] ^ spin1[i]) | (spin2[i+1] ^ spin2[i]);//そろっていなかったらtrue
  long long b2 = (spin1[i+Lx] ^ spin1[i]) | (spin2[i+Lx] ^ spin2[i]);
  long long b3 = (spin1[i-Lx] ^ spin1[i]) | (spin2[i-Lx] ^ spin2[i]);
  long long c1 = (~spin1[i+1] ^ us1) & (~spin2[i+1] ^ us2);
  long long c2 = (~spin1[i+Lx] ^ us1) & (~spin2[i+Lx] ^ us2);
  long long c3 = (~spin1[i-Lx] ^ us1) & (~spin2[i-Lx] ^ us2);
  //calc IX;
  long random = rnd->genrand64_bit(randomBitWidth);
  long long ix2 = bfTab.GetTableElement(2*random);
  long long ix1 = bfTab.GetTableElement(2*random+1);
  long long d2,d1,e2,e1,f2,f1,g2,g1,h2;
  //IE+IX(r)
  Plus(ix1,b1,b2,d2,d1);
  Plus(b3,c1,c2,e2,e1);
  Plus(d1,e1,c3,f2,f1);
  Plus(ix2,d2,e2,g2,g1);
  Plus(g1,f2,f1,h2);
  long long ID = g2 | h2;
  //update to new state
  spin1[i] = (ID & us1) | (~ID & spin1[i]);
  spin2[i] = (ID & us2) | (~ID & spin2[i]);
}
//------------------------------
inline void classHex3PottsMulti::CopySub() {
  for( long i=0; i<Lx; i++) {
    spin1[i] = spin1[i+Lx*Ly];
    spin2[i] = spin2[i+Lx*Ly];
    spin1[i+Lx*(Ly+1)] = spin1[i+Lx];
    spin2[i+Lx*(Ly+1)] = spin2[i+Lx];
  }
}
//------------------------------
inline void classHex3PottsMulti::Plus(const long long& a,const long long& b,const long long& c,
				       long long &d, long long& e){
  //<d,e> = a+b+c
  e = a ^ b;
  d = (a&b) | (e&c); // second order
  e ^= c; // e+c; first order
}
inline void classHex3PottsMulti::Plus(const long long& a, const long long& b, const long long& c,
				       long long & d) {
  //<d,*> = a+b+c;
  d = (a&b) | ( (b^a)&c); //second order
}
//------------------------------
double classHex3PottsMulti::AveragedOrderParameter() {
  long imag[64];
  bwmsc.NIbitcount( spin2,LxLy, imag);
  double total = 0;
  for( int i=0; i<64; i++) {
    total += (double)imag[i]/(double)(LxLy)*1.5-0.5;
  }
  return total/64.0;
}
//------------------------------
void classHex3PottsMulti::ConfigurationOutput(int i) {
  if( i<0 || i>63) {
    cerr << "Error! in classHex3PottsMulti::ConfigurationOut()" << endl;
    exit(1);
  }
  unsigned long long mask = 1 << i;
  for( long j=0; j<Ly; j++) {
    for( long i=0; i<Lx; i++) {
      short temp = 0;
      if( spin1[j*Lx+i] & mask) temp +=1;
      if( spin2[j*Lx+i] & mask) temp +=2;
      cout << temp;
    }
    cout << endl;
  }
}
