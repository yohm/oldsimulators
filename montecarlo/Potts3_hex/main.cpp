#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include "mtrand64.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTableHex3Potts.cpp"
#include "classHex3PottsMulti.cpp"

using namespace std;

//input the arguments as follows
// program Lx Ly k Tmax seed bitlength data_id
// ./a.out 299 300 1.005 128 23419721 16 00002
int main(int argc, char* argv[]) {
  if(argc!=8) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <Lx> <Ly> <k> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }

  //  clock_t start,end;
  long T = atoi( argv[4]);
  long lx=atoi( argv[1]);
  long ly=atoi( argv[2]);
  double k = atof( argv[3]);
  unsigned long long sed = atol( argv[5]);
  int bitwt = atoi( argv[6]);
  unsigned long long bf_sed = sed + 72137ULL;
  char str[20];
  strcpy( str,argv[7]);
  strcat( str, ".NERo");
  
  classHex3PottsMulti ptm;
  ptm.Initialize(lx,ly,k,sed,bitwt,bf_sed);

  ofstream fout(str);

  clock_t start = clock();
  for( int t=0; t<T; t++) {
    fout << t << " " << ptm.AveragedOrderParameter() <<endl;
    ptm.Update();
  }
  clock_t end = clock();
  cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC)<<endl;
  fout.close();
  return 0;
}
