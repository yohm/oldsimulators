#include "fccIsingMPI.hpp"

//----------------------------------------------
fccIsingMPI::fccIsingMPI() {
  int argc = 0;
  MPI_Init( &argc, NULL);
  MPI_Comm_size( MPI_COMM_WORLD, &num_pe);
  MPI_Comm_rank( MPI_COMM_WORLD, &my_rank);

  //適当な初期値を設定
  Lx = Ly = Lz = numAllocation = 0;
  K = 0.1;
  //  spinA = spinB = spinC = spinD = NULL;
  //  random = NULL;
  tableWidthPow = 0;
  seedOfRandomNumber = 0;
}

//----------------------------------------------
fccIsingMPI::~fccIsingMPI() {
  spinA.clear();
  spinB.clear();
  spinC.clear();
  spinD.clear();
  random.clear();
  MPI_Finalize();

}

//-----------------------------------------------
void fccIsingMPI::Initialize(long total_Lx, long total_Ly, long total_Lz, 
			     double spec_K, unsigned long long spec_seed,
			     int bit_width, unsigned long long bf_seed,
			     std::string t_id ) {
  if( (total_Lz % num_pe) != 0) {
    std::cerr << "Lz must be a multiple of num_pe" << std::endl
	      << "number of processor elements is " << num_pe << std::endl;
    throw 10;
  }
  SpinMemoryAllocation( total_Lx, total_Ly, total_Lz/num_pe);
  TableMemoryAllocation( spec_K , bit_width, bf_seed+my_rank);
  rnd[0].rndini( spec_seed+my_rank , bit_width);
  for( int i=1; i<16; i++) rnd[i].rndini( rnd[0].rndI() , bit_width);
  seedOfRandomNumber = spec_seed + my_rank;
  //  AllUpConfiguration();
  id = t_id;
}
//-----------------------------------------------
void fccIsingMPI::SpinMemoryAllocation(long lx,long ly, long lz)
{
  numAllocation = lx*ly*lz + lx*ly + lx ;
  spinA.assign( numAllocation, -1);
  spinB.assign( numAllocation, -1);
  spinC.assign( numAllocation, -1);
  spinD.assign( numAllocation, -1); //all up configuration
  Lx = lx; Ly = ly; Lz = lz; LxLyLz = Lx*Ly*Lz;
  random.assign( LxLyLz, 0);
  if( (LxLyLz % 16) != 0) {
    std::cerr << "Error! in fccIsingMPI::SpinMemoryAllocation()" << std::endl
	      << "LxLyLz must be a multiple of 16." << std::endl;
    throw 99;
  }
  for( int i=0; i<16; i++) bwmsc[i].Bitwise_multispincoding_Initialization(LxLyLz/16);

}
//------------------------------------------------
void fccIsingMPI::TableMemoryAllocation( double specK, int bit_width, unsigned long long bfseed) {
  K = specK;  seedOfBfTable = bfseed;  tableWidthPow = bit_width;
  bfTable.Initialize( tableWidthPow, K, seedOfBfTable + my_rank);
}

//------------------------------------------------------
void fccIsingMPI::AllUpConfiguration()
{
  //set all up configuration
  for( long i=0; i < numAllocation; i++) {
    spinA[i] = -1;
    spinB[i] = -1;
    spinC[i] = -1;
    spinD[i] = -1;
  }
}

//------------------------------------------------
void fccIsingMPI::Run( long tmax )
{
  std::ofstream fout;
  if( my_rank == 0) {
    std::string filename = id + ".NERo";
    fout.open( filename.c_str());
    fout.precision(12);
  }
  for( long t=0; t<tmax; t++) {
    double m = AveragedOrderParameter();
    if(my_rank==0) {
      fout << t << " " << m << std::endl;
    }
    Update();
  }
  fout.close();
}

//------------------------------------------------
void fccIsingMPI::Update()
{
  int64_t *us,*nn_xy,*nn_yz,*nn_zx;
  //Update of SpinA
  us = & spinA[Lx*(Ly+1)-1];
  nn_xy = & spinB[Lx*Ly-1];
  nn_yz = & spinC[0];
  nn_zx = & spinD[Lx-1];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinA
  //  for( long i=0; i<Lx*(Ly+1)-1; i++) spinA[i] = spinA[i+Lx*Ly*Lz];
  MPI_Request req[4];
  MPI_Status stat[4];
  int top_neighbor = (my_rank+1)%num_pe;
  int bottom_neighbor = (my_rank-1+num_pe)%num_pe;
  //  std::cerr << "sending message from " << my_rank << " to " << top_neighbor << std::endl;
  MPI_Isend( &spinA[Lx*Ly*Lz] , (Lx*(Ly+1)-1) , MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[0]);
  //  std::cerr << "receiving message from " << (my_rank-1+num_pe)%num_pe << " to " << my_rank << std::endl;
  MPI_Irecv( &spinA[0], Lx*(Ly+1)-1, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[1]);
  //  for( long i=Lx*(Ly+1)+LxLyLz-1; i<numAllocation; i++) spinA[i] = spinA[i-LxLyLz];
  long length_buf = numAllocation - ( Lx*(Ly+1)+LxLyLz-1);
  MPI_Isend( &spinA[Lx*(Ly+1)-1], length_buf, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[2]);
  MPI_Irecv( &spinA[Lx*(Ly+1)+LxLyLz-1], length_buf, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[3]);
  MPI_Waitall( 4, req, stat);
  
  //Update of SpinB
  us = & spinB[Lx*Ly];
  nn_xy = & spinA[Lx*Ly-1];
  nn_yz = & spinD[0];
  nn_zx = & spinC[0];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinB
  //  for( long i=0; i<Lx*Ly; i++) spinB[i] = spinB[i+LxLyLz];
  MPI_Isend( &spinB[LxLyLz], Lx*Ly, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[0]);
  MPI_Irecv( &spinB[0], Lx*Ly, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[1]);
  //  for( long i=Lx*Ly+LxLyLz; i< numAllocation; i++) spinB[i] = spinB[i-LxLyLz];
  length_buf = numAllocation - (Lx*Ly+LxLyLz);
  MPI_Isend( &spinB[Lx*Ly], length_buf, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[2]);
  MPI_Irecv( &spinB[Lx*Ly+LxLyLz], length_buf, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[3]);
  MPI_Waitall( 4, req, stat);

  //update of SpinC
  us = & spinC[1];
  nn_xy = & spinD[0];
  nn_yz = & spinA[0];
  nn_zx = & spinB[0];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinC
  //  for( long i=0; i<1; i++) spinC[i] = spinC[i+LxLyLz];
  MPI_Isend( &spinC[LxLyLz], 1, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[0]);
  MPI_Irecv( &spinC[0], 1, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[1]);
  //  for( long i=1+LxLyLz; i< numAllocation; i++) spinC[i] = spinC[i-LxLyLz];
  length_buf = numAllocation - (1+LxLyLz);
  MPI_Isend( &spinC[1], length_buf, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[2]);
  MPI_Irecv( &spinC[1+LxLyLz], length_buf, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[3]);
  MPI_Waitall( 4, req, stat);
  

  //update of SpinD
  us = & spinD[Lx];
  nn_xy = & spinC[0];
  nn_yz = & spinB[0];
  nn_zx = & spinA[Lx-1];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);
  
  //Copy of SpinD
  //for( long i=0; i<Lx; i++) spinD[i] = spinD[i+LxLyLz];
  MPI_Isend( &spinD[LxLyLz], Lx, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[0]);
  MPI_Irecv( &spinD[0], Lx, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[1]);
  //  for( long i=Lx+LxLyLz; i< numAllocation; i++) spinD[i] = spinD[i-LxLyLz];
  length_buf = numAllocation - (Lx+LxLyLz);
  MPI_Isend( &spinD[Lx], length_buf, MPI_LONG_LONG,
	     bottom_neighbor, 0, MPI_COMM_WORLD, &req[2]);
  MPI_Irecv( &spinD[Lx+LxLyLz], length_buf, MPI_LONG_LONG,
	     top_neighbor, 0, MPI_COMM_WORLD, &req[3]);
  MPI_Waitall( 4, req, stat);
  
}

//-----------------------------------------------
inline void fccIsingMPI::PrepareRandomNumbers() {
  static const long loc_spins = LxLyLz/16;
  int which_rnd[16];
  for( int i=0; i<16; i++) which_rnd[i] = -1;
  for( int i=0; i<16; i++) {
    int r = static_cast<int>( ( rnd[0].rndD() ) * (16-i) );
    for( int j=0; j<16; j++) {
      if( which_rnd[j] == -1) r--;
      if( r < 0 ) { which_rnd[j] = i; break;}
      if( j==15 ) throw 99;
    }
  }

  //  std::cerr << "In PrepareRandomNumbers()." <<std::endl;
  //  for( int i=0; i<16; i++) { std::cerr << i << ' ' << which_rnd[i] << std::endl;}

  /*poption parallel */
  for( int pe = 0; pe < 16; pe++) {
    int j = which_rnd[pe];
    rnd[j].rndI_array( &random[loc_spins*pe], loc_spins);
  }
}		       

//-----------------------------------------------
inline void fccIsingMPI::UpdateSub(int64_t* us, int64_t* nn_xy, int64_t* nn_yz, int64_t* nn_zx) {
  int64_t IX1, IX2, IX3;
  int64_t *nn_xy1 = nn_xy + Lx;
  int64_t *nn_yz1 = nn_yz + Lx, *nn_yz2 = nn_yz + Lx*Ly, *nn_yz3 = nn_yz + Lx*(Ly+1);
  int64_t *nn_zx1 = nn_zx + Lx*Ly;
  int64_t f1,f2,f3,f4,f5,f6,f7,f8;
  int64_t h1, h2, i1, j1, j2, k1, k2, l1, l2, m1, m2, n1, ID;

  /*poption parallel */
  for( long i=0; i<LxLyLz; i++) {
    bfTable.GetTableElements( IX1, IX2, IX3, random[i]*3);

    Plus( *nn_xy, *nn_xy1, *nn_yz, f1, f2);
    Plus( *(++nn_xy), *(++nn_xy1), *nn_yz1, f3, f4);
    Plus( *nn_zx, *nn_zx1, *nn_yz2, f5, f6);
    Plus( *(++nn_zx), *(++nn_zx1), *nn_yz3, f7, f8);
    nn_yz++; nn_yz1++; nn_yz2++; nn_yz3++;

    f1 ^= *us;
    f2 ^= *us;
    f3 ^= *us;
    f4 ^= *us;
    f5 ^= *us;
    f6 ^= *us;
    f7 ^= *us;
    f8 ^= *us;

    Plus( IX3, f2, f4, h1, h2);
    Plus( f6, f8, h2, i1);
    Plus( h1, f1, f3, j1, j2);
    Plus( f5, f7, i1, k1, k2);
    Plus( IX2, j2, k2, l1, l2);
    Plus( IX1, j1, k1, m1, m2);
    Plus( m2, l1, l2, n1);
    ID = m1 | n1;
    
    *us ^= ID;
    us++;
    
  }
}
//---------------------------------------------------
inline void fccIsingMPI::Plus(int64_t a,int64_t b, int64_t c, int64_t & d, int64_t & e){
  //<d,e> = a+b+c
  d = a & b; //<d,e> = a+b
  e = a ^ b;
  a = e & c; // e+c;
  d = d | a; // second order
  e = e ^ c; // e+c; first order
}
//--------------------------------------------------
inline void fccIsingMPI::Plus(int64_t a,int64_t b, int64_t c, int64_t & d){
  //<d,*> = a+b+c;
  d = a & b; // < d,b> = a+b;
  b = b ^ a;
  a = b & c; //a+c
  d = d | a; //second order
}
//-----------------------------------------------
double fccIsingMPI::AveragedOrderParameter()
{
  long loc = LxLyLz/16;
  long magA[16][64], magB[16][64], magC[16][64], magD[16][64];
  /*poption parallel */
  for( int pe = 0; pe<16; pe++) {
    bwmsc[pe].NIbitcount( &spinA[loc*pe], loc, &(magA[pe][0]) );
    bwmsc[pe].NIbitcount( &spinB[loc*pe], loc, &(magB[pe][0]) );
    bwmsc[pe].NIbitcount( &spinC[loc*pe], loc, &(magC[pe][0]) );
    bwmsc[pe].NIbitcount( &spinD[loc*pe], loc, &(magD[pe][0]) );
  }
  
  int64_t mag = 0;
  for( int i=0; i<64; i++) {
    for( int pe=0; pe<16; pe++) mag += magA[pe][i] + magB[pe][i] + magC[pe][i] + magD[pe][i];
  }

  int64_t sum_all = 0;
  MPI_Reduce( &mag, &sum_all, 1, MPI_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

  if(my_rank == 0) {
    double center = static_cast<double>( 128*LxLyLz*num_pe);
    return (static_cast<double>(sum_all - 128*LxLyLz*num_pe) / center);
  }
  return 0.0;
}
//----------------------------------------------
void fccIsingMPI::ConfigurationOut(int i) {
  if( i<0 || i>63) {
    cerr << "Error! in fccIsingMPI::ConfigurationOut()" << endl;
    exit(1);
  }
  int64_t mask = 1 << i;
  
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinA[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinB[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinC[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinD[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
}
