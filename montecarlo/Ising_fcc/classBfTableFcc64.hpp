
#include "mtrand64.hpp"
#include <cmath>
#include <boost/cstdint.hpp>
using namespace std;

#ifndef _BFTABLE_FCC_H_
#define _BFTABLE_FCC_H_

#define DEBUG

class classBfTableFcc {
  //BfTable class 64sample同じ温度についてしか計算できない
  //tableにはそれぞれのテーブルの要素をを交互に並べていく
  //IX1,IX2,IX3...の順番で並べる。３つのテーブルが必要
  //それぞれのテーブルは独立ではない。シャッフルする時は3個のビットをまとめて混ぜる
  // 引数には逆温度Kを与える
public:
  classBfTableFcc();
  ~classBfTableFcc();
  void Initialize( int pow, double K, unsigned long long seed);
  //Initialize and allocate memory. If pow,K,seed are unchanged ,this routine don't make table.
  int TableSizePower() { return tableSizePower;}
  inline int64_t GetTableElement(long i) { return table[i];}
  inline void GetTableElements(int64_t &IX1,int64_t &IX2, int64_t & IX3, long i) {
    IX1 = table[i]; IX2 = table[i+1]; IX3 = table[i+2]; }
  inline void GetTableElements6(int64_t &IX1,int64_t &IX2, int64_t & IX3,
				int64_t &IX1_2, int64_t &IX2_2, int64_t & IX3_2, long i) {
    IX1 = table[i]; IX2 = table[i+1]; IX3 = table[i+2];
    IX1_2 = table[i+3]; IX2_2 = table[i+4]; IX3_2 = table[i+5]; }
#ifdef DEBUG
  void TableOutputBit(); //tableのビットを出力する
  void TableOutputNumber(); //Table_num番目のテーブルのビットだけを出力する
  //Table_num <= numTable-1
#endif //DEBUG

private:
  int64_t * table; //0,1を入れるテーブル。リストを必要な数だけ連続して取る  
  int tableSizePower; //tableSize = 2^(tableSizePower)
  long tableSize; 
  long totalTableSize; // = tableSize * 3
  unsigned long long bfSeed; //bit seed for random shuffling
  double inverseTemperature; 
  void MakeRandomTable(double K, unsigned long long seed); //テーブルに0,1を入れる "sub of Initialize"
  void SetBit(double K);
  void ShuffleTable(unsigned long long seed); //table1, table2...をシャッフルする。そのとき3bitまとめて混ぜる。引数は乱数の種 "sub of MakeRandomTable()"
  void ShuffleTableParallel(unsigned long long seed); //table1, table2...をシャッフルする。そのとき3bitまとめて混ぜる。引数は乱数の種 "sub of MakeRandomTable()"
  void Exchange( long i,long j, long k); //i-bit目のtable[j]とtable[random]の値を入れ替える "sub of ShuffleOnce()"
  inline void SwapBit( char mask, char& j , char & k);//maskのビットがたっているところのjとkのビットを入れ替える。
  void SwapBits( char mask, char* array, long i, long j);//swap bits between array[3*i ~ 3*i+2] and array[3*j ~ 3*j+2]
};

#endif //_BFTABLE_FCC_H_
