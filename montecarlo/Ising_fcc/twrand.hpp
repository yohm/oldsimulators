#include <iostream>
#include <cstdio>
#include <cmath>

#ifndef _RANDOM_HPP_
#define _RANDOM_HPP_

class tw250e103{
public:
  tw250e103(void);
  tw250e103(long,int);
  ~tw250e103();
  void rndini(long,int);
  long rndI(void);
  double rndD(void);
  void rndI_array(long *,int);
private:
  long seedseq[250];
  int im103[250],ip1[250];
  int current_position;
  int bitwidth;
  double normalization_factor;
};

class rand2{
public:
  rand2(long);
  ~rand2();
  void rndini(long);
  long rndI(void);
  double rndD(void);
private:
  long rndseed;
};

#endif
