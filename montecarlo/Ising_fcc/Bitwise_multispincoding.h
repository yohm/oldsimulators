#ifndef BITWISEMULTI
#define BITWISEMULTI


#include "vcpp2gcc.h"
class Bitwise_multispincoding
{
public:
	Bitwise_multispincoding(void);
	virtual ~Bitwise_multispincoding(void);
	void Bitwise_multispincoding_Initialization( long );
	void bitcount(__int64*,long,long*);
	void NIbitcount(__int64*,long,long*);
protected:
	long NA,NB,NC,N2,N3,N4;
	__int64 *I2S[2],*I4S[4],*I8S[8],*I32S;
private:
	long Nprev;
};

#endif
