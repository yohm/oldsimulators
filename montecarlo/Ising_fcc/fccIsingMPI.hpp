#include "mpi.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/cstdint.hpp>
//#include "mtrand64.hpp"
#include "twrand.hpp"
#include "Bitwise_multispincoding.h"
#include "classBfTableFcc64.hpp"


#ifndef CLASS_FCC_ISING_MPI_H_
#define CLASS_FCC_ISING_MPI_H_

class fccIsingMPI {
public:
  fccIsingMPI();
  ~fccIsingMPI();
  void Run( long tmax);
  void Update();
  double AveragedOrderParameter();
  //doulbe Energy();
  void Initialize( long lx, long ly, long lz, double specK, unsigned long long sed,
		   int bit_width, unsigned long long bf_seed, std::string id);
  //Set parameters and allocate memory and set all-up configuration
  void AllUpConfiguration();//set all up configuration
private:
  void SpinMemoryAllocation(long lx, long ly, long lz);
  //allocation of spin. If sizes are unchanged, this don't reallocate memory.
  void TableMemoryAllocation(double spec_K, int bit_width, unsigned long long bf_seed);
  //allocation of bf table. If K,bit_width,bf_seed are unchanged, this don't reallocate.
  long Lx, Ly, Lz, LxLyLz, numAllocation; //system size
  int tableWidthPow; //Width of bf table is 2^(tableWidthPow);
  double K;//inverse temperature
  unsigned long long seedOfRandomNumber, seedOfBfTable;//seeds of random number
  //  mtrand64 rnd;//random number generator
  std::string id;
  tw250e103 rnd[16];
  Bitwise_multispincoding bwmsc[16]; //bit counting class
  classBfTableFcc bfTable; // Boltzman factor table
  //  long * random; //random[LxLyLz]; random[i] is used in UpdateSub
  std::vector<long> random;
  std::vector<int64_t> spinA, spinB, spinC, spinD;
  //  int64_t * spinA, * spinB, *spinC, *spinD;//spin
  void UpdateSub( int64_t* us, int64_t* nnxy, int64_t *nnyz, int64_t* nnzx);
  void PrepareRandomNumbers();
  //sub routine of "Update()". Arguments are pointer of update spin, nearest-neighbor spin
  //which has youngest number in xy,yz,zx plane.
  void Plus(int64_t a, int64_t b, int64_t c, int64_t &d, int64_t &e);
  void Plus(int64_t a, int64_t b, int64_t c, int64_t &d);
  void ConfigurationOut(int i);//Ouput configuratin of i-bit's sample
  //-------- mpi ------------
  int my_rank;
  int num_pe;
};
  
#endif
