#include <iostream>
#include "classFccIsing.hpp"
#include "classBfTableFcc64.hpp"

//----------------------------------------------
classFccIsing::classFccIsing() {
  //適当な初期値を設定
  Lx = Ly = Lz = numAllocation = 0;
  K = 0.1;
  spinA = spinB = spinC = spinD = NULL;
  random = NULL;
  tableWidthPow = 0;
  seedOfRandomNumber = 0;
}

//----------------------------------------------
classFccIsing::~classFccIsing() {
  delete [] spinA;
  delete [] spinB;
  delete [] spinC;
  delete [] spinD;
  delete [] random;
}

//-----------------------------------------------
void classFccIsing::Initialize(long spec_Lx, long spec_Ly, long spec_Lz, 
					double spec_K, unsigned long long spec_seed,
					int bit_width, unsigned long long bf_seed)
{
  SpinMemoryAllocation( spec_Lx, spec_Ly, spec_Lz);
  TableMemoryAllocation( spec_K , bit_width, bf_seed);
  rnd.init_genrand64( spec_seed);
  seedOfRandomNumber = spec_seed;
  AllUpConfiguration();
}
//-----------------------------------------------
void classFccIsing::SpinMemoryAllocation(long lx,long ly, long lz)
{
  long newNumAllocation = lx*ly*lz + lx*ly + lx ;
  if( newNumAllocation != numAllocation) {
    delete [] spinA;
    delete [] spinB;
    delete [] spinC;
    delete [] spinD;
    spinA = new long long [newNumAllocation];
    spinB = new long long [newNumAllocation];
    spinC = new long long [newNumAllocation];
    spinD = new long long [newNumAllocation];
    numAllocation = newNumAllocation;
  }
  Lx = lx; Ly = ly; Lz = lz; LxLyLz = Lx*Ly*Lz;
  static long prevLxLyLz = 0;
  if( LxLyLz != prevLxLyLz) {
    delete random;
    random = new long[LxLyLz];
    prevLxLyLz = LxLyLz;
    bwmsc.Bitwise_multispincoding_Initialization(LxLyLz);
  }

}
//------------------------------------------------
void classFccIsing::TableMemoryAllocation( double specK, int bit_width, unsigned long long bfseed) {
  K = specK;  seedOfBfTable = bfseed;  tableWidthPow = bit_width;
  bfTable.Initialize( tableWidthPow, K, seedOfBfTable);

}

//------------------------------------------------------
void classFccIsing::AllUpConfiguration()
{
  //set all up configuration
  for( long i=0; i < numAllocation; i++) {
    spinA[i] = -1;
    spinB[i] = -1;
    spinC[i] = -1;
    spinD[i] = -1;
  }
}

//------------------------------------------------
void classFccIsing::Update()
{
  long long *us,*nn_xy,*nn_yz,*nn_zx;
  //Update of SpinA
  us = & spinA[Lx*(Ly+1)-1];
  nn_xy = & spinB[Lx*Ly-1];
  nn_yz = & spinC[0];
  nn_zx = & spinD[Lx-1];
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinA
  for( long i=0; i<Lx*(Ly+1)-1; i++) spinA[i] = spinA[i+Lx*Ly*Lz];
  for( long i=Lx*(Ly+1)+LxLyLz-1; i<numAllocation; i++) spinA[i] = spinA[i-LxLyLz];
  
  //Update of SpinB
  us = & spinB[Lx*Ly];
  nn_xy = & spinA[Lx*Ly-1];
  nn_yz = & spinD[0];
  nn_zx = & spinC[0];
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinB
  for( long i=0; i<Lx*Ly; i++) spinB[i] = spinB[i+LxLyLz];
  for( long i=Lx*Ly+LxLyLz; i< numAllocation; i++) spinB[i] = spinB[i-LxLyLz];

  //Update of SpinC
  us = & spinC[1];
  nn_xy = & spinD[0];
  nn_yz = & spinA[0];
  nn_zx = & spinB[0];
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinC
  for( long i=0; i<1; i++) spinC[i] = spinC[i+LxLyLz];
  for( long i=1+LxLyLz; i< numAllocation; i++) spinC[i] = spinC[i-LxLyLz];

  //Update of SpinD
  us = & spinD[Lx];
  nn_xy = & spinC[0];
  nn_yz = & spinB[0];
  nn_zx = & spinA[Lx-1];
  UpdateSub( us, nn_xy, nn_yz, nn_zx);
  
  //Copy of SpinD
  for( long i=0; i<Lx; i++) spinD[i] = spinD[i+LxLyLz];
  for( long i=Lx+LxLyLz; i< numAllocation; i++) spinD[i] = spinD[i-LxLyLz];
  
}

//-----------------------------------------------
inline void classFccIsing::UpdateSub(long long* us, long long* nn_xy, long long* nn_yz, long long* nn_zx) {
  long long IX1, IX2, IX3;
  long long *nn_xy1 = nn_xy + Lx;
  long long *nn_yz1 = nn_yz + Lx, *nn_yz2 = nn_yz + Lx*Ly, *nn_yz3 = nn_yz + Lx*(Ly+1);
  long long *nn_zx1 = nn_zx + Lx*Ly;


  for( long i=0; i<LxLyLz; i++) {
    long r = rnd.genrand64_bit( tableWidthPow) * 3;
    long long IX1 = bfTable.GetTableElement( r);
    long long IX2 = bfTable.GetTableElement( r+1);
    long long IX3 = bfTable.GetTableElement( r+2);
    long long f1,f2,f3,f4,f5,f6,f7,f8;
    Plus( *nn_xy, *nn_xy1, *nn_yz, f1, f2);
    Plus( *(++nn_xy), *(++nn_xy1), *nn_yz1, f3, f4);
    Plus( *nn_zx, *nn_zx1, *nn_yz2, f5, f6);
    Plus( *(++nn_zx), *(++nn_zx1), *nn_yz3, f7, f8);
    nn_yz++; nn_yz1++; nn_yz2++; nn_yz3++;

    f1 ^= *us;
    f2 ^= *us;
    f3 ^= *us;
    f4 ^= *us;
    f5 ^= *us;
    f6 ^= *us;
    f7 ^= *us;
    f8 ^= *us;

    long long h1, h2;
    Plus( IX3, f2, f4, h1, h2);
    long long i1;
    Plus( f6, f8, h2, i1);
    long long j1, j2;
    Plus( h1, f1, f3, j1, j2);
    long long k1, k2;
    Plus( f5, f7, i1, k1, k2);
    long long l1, l2;
    Plus( IX2, j2, k2, l1, l2);
    long long m1, m2;
    Plus( IX1, j1, k1, m1, m2);
    long long n1;
    Plus( m2, l1, l2, n1);
    long long ID = m1 | n1;
    
    *us ^= ID;
    us++;
    
  }
}
//---------------------------------------------------
inline void classFccIsing::Plus(long long a,long long b, long long c, long long & d, long long & e){
  //<d,e> = a+b+c
  d = a & b; //<d,e> = a+b
  e = a ^ b;
  a = e & c; // e+c;
  d = d | a; // second order
  e = e ^ c; // e+c; first order
}
//--------------------------------------------------
inline void classFccIsing::Plus(long long a,long long b, long long c, long long & d){
  //<d,*> = a+b+c;
  d = a & b; // < d,b> = a+b;
  b = b ^ a;
  a = b & c; //a+c
  d = d | a; //second order
}
//-----------------------------------------------
double classFccIsing::AveragedOrderParameter()
{
  long magA[64], magB[64], magC[64], magD[64];
  bwmsc.NIbitcount( spinA, LxLyLz, magA);
  bwmsc.NIbitcount( spinB, LxLyLz, magB);
  bwmsc.NIbitcount( spinC, LxLyLz, magC);
  bwmsc.NIbitcount( spinD, LxLyLz, magD);
  double total = 0;
  for( int i=0; i<64; i++) {
    long mag = magA[i] + magB[i] + magC[i] + magD[i];
    total += (double)( mag - 2*LxLyLz)/ (double)(2*LxLyLz);
  }
  return total/64.0;
}
//----------------------------------------------
void classFccIsing::ConfigurationOut(int i) {
  if( i<0 || i>63) {
    cerr << "Error! in classFccIsing::ConfigurationOut()" << endl;
    exit(1);
  }
  unsigned long long mask = 1 << i;
  
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinA[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinB[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinC[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinD[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
}
