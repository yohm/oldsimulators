#include "twrand.hpp"


tw250e103::tw250e103(void){
  rndini(105,31);
}

tw250e103::tw250e103(long seed,int iwidth){
  rndini(seed,iwidth);
}

tw250e103::~tw250e103(){
}

void tw250e103::rndini(long seed,int iwidth){
  char initnum_rnd=1;
  rand2 rndA(seed);
  //  int i;
  long *dim_random;
  if((iwidth<1)||(iwidth>31)){
    printf("in tw250e103::rndini, specified bitwidth, %d, is invalid. \n",iwidth);
    exit(1);
  };
  bitwidth=iwidth;
  normalization_factor=1.0/pow(2.0,bitwidth);
  for(int j=0;j<250;j++){
    seedseq[j]=(rndA.rndI()>>23)&1;
    for(int i=1;i<bitwidth;i++)seedseq[j]=(seedseq[j]<<1)|((rndA.rndI()>>23)&1);
    im103[j]=j-103;
    if(im103[j]<0)im103[j]=im103[j]+250;
    ip1[j]=j+1;
    if(ip1[j]==250)ip1[j]=0;
  };
  current_position=0;

  dim_random=new long[10000];
  if(dim_random == NULL){
    printf("tw250e103 has failed to allocate memory to dim_random for size 10000. \n");
    exit(1);
  }
  for(int i=0;i<initnum_rnd;i++)rndI_array(dim_random,10000);

  delete[] dim_random;
};

long tw250e103::rndI(void){
  long i;
  i=seedseq[current_position]^seedseq[im103[current_position]];
  seedseq[current_position]=i;
  current_position=ip1[current_position];
  return(i);
};

void tw250e103::rndI_array(long *Iarray,int array_size){
  const int array_size_threshold=350;
  // array_size_threshold should be larger than 250. 
  long *Iap1,*Iap2;
  if(array_size < array_size_threshold){
    for(int i=0;i<array_size;i++) Iarray[i]=rndI();
  }
  else{
    for(int i=0;i<250;i++)Iarray[i]=rndI();

    Iap1=&Iarray[-250];
    Iap2=&Iarray[-103];
    for(int i=250;i<array_size;i++)Iarray[i]=Iap1[i]^Iap2[i];
    for(int i=0;i<250;i++)seedseq[i]=Iarray[i+array_size-250];
    current_position=0;
  }
};

double tw250e103::rndD(void){
  double a;
  a=rndI()*normalization_factor;
  return(a);
};

//------------------------------------------------------------
//------------------------------------------------------------
rand2::rand2(long seed){
  rndini(seed);
};

rand2::~rand2(){
};

void rand2::rndini(long seed){
  rndseed=seed;
};

long rand2::rndI(void){
  rndseed=rndseed*48828125;
  if(rndseed<0)rndseed=(rndseed+2147483647)+1;
  return(rndseed);
};

double rand2::rndD(void){
  double a;
  rndseed=rndseed*48828125;
  if(rndseed<0)rndseed=(rndseed+2147483647)+1;
  a=rndseed/2147483648.0;
  return(a);
};



