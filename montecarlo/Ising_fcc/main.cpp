#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include "mtrand64.cpp"
#include "twrand.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTableFcc64.cpp"
//#include "classFccIsing.cpp"
#include "classFccIsing2.cpp"
//#include "classFccIsing3.cpp"
//#include "classBfTableFcc128.cpp"

using namespace std;

//#define BENCH

//input the arguments as follows
// program Lx Ly k Tmax seed bitlength data_id
// ./a.out 299 300 1.005 128 23419721 16 00002
int main(int argc, char* argv[]) {
  if(argc!=7) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <L> <k> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }

  //  clock_t start,end;
  long tmax = atoi( argv[3]);
  long l=atoi( argv[1]);
  double k = atof( argv[2]);
  unsigned long long sed = atol( argv[4]);
  int bitwt = atoi( argv[5]);
  unsigned long long bf_sed = sed + 72137ULL;
  char str[20];
  strcpy( str,argv[6]);
  strcat( str, ".NERo");

#ifdef BENCH
  clock_t start = clock();
#endif

  classFccIsing sim;
  sim.Initialize( l, l, l, k, sed, bitwt, bf_sed);

#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time for initialization:"<<(double)(end-start)/(CLOCKS_PER_SEC)<< std::endl;
#endif

  ofstream fout(str);
  fout.precision(10);

#ifdef BENCH
  start = clock();
#endif
  
  for( int t=0; t<tmax; t++) {
    fout << t << " " << sim.AveragedOrderParameter() <<endl;
    sim.Update();
  }
  
#ifdef BENCH
  end = clock();
  double time = (double)(end-start)/(CLOCKS_PER_SEC);
  std::cerr << "time for update:"<< time << std::endl;
  unsigned long long ll = static_cast<unsigned long long>(l);
  unsigned long long spins = ll * ll * ll * 4 * 64.0 * tmax;
  std::cerr << static_cast<double>(spins) / time / 1000000.0 << "M updates / sec / core" << std::endl;
#endif
  fout.close();
  return 0;
}
