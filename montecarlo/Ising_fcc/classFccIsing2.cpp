#include <iostream>
#include "classFccIsing2.hpp"
#include "classBfTableFcc64.hpp"

//----------------------------------------------
classFccIsing::classFccIsing() {
  //適当な初期値を設定
  Lx = Ly = Lz = numAllocation = 0;
  K = 0.1;
  spinA = spinB = spinC = spinD = NULL;
  random = NULL;
  tableWidthPow = 0;
  seedOfRandomNumber = 0;
}

//----------------------------------------------
classFccIsing::~classFccIsing() {
  delete [] spinA;
  delete [] spinB;
  delete [] spinC;
  delete [] spinD;
  delete [] random;
}

//-----------------------------------------------
void classFccIsing::Initialize(long spec_Lx, long spec_Ly, long spec_Lz, 
					double spec_K, unsigned long long spec_seed,
					int bit_width, unsigned long long bf_seed)
{
  SpinMemoryAllocation( spec_Lx, spec_Ly, spec_Lz);
  TableMemoryAllocation( spec_K , bit_width, bf_seed);
  rnd[0].rndini( spec_seed, bit_width);
  for( int i=1; i<16; i++) rnd[i].rndini( rnd[0].rndI() , bit_width);
  seedOfRandomNumber = spec_seed;
  AllUpConfiguration();
}
//-----------------------------------------------
void classFccIsing::SpinMemoryAllocation(long lx,long ly, long lz)
{
  long newNumAllocation = lx*ly*lz + lx*ly + lx ;
  if( newNumAllocation != numAllocation) {
    delete [] spinA;
    delete [] spinB;
    delete [] spinC;
    delete [] spinD;
    spinA = new int64_t [newNumAllocation];
    spinB = new int64_t [newNumAllocation];
    spinC = new int64_t [newNumAllocation];
    spinD = new int64_t [newNumAllocation];
    numAllocation = newNumAllocation;
  }
  Lx = lx; Ly = ly; Lz = lz; LxLyLz = Lx*Ly*Lz;
  static long prevLxLyLz = 0;
  if( LxLyLz != prevLxLyLz) {
    delete random;
    random = new long[LxLyLz];
    prevLxLyLz = LxLyLz;
    for( int i=0; i<16; i++) bwmsc[i].Bitwise_multispincoding_Initialization(LxLyLz/16);
  }

}
//------------------------------------------------
void classFccIsing::TableMemoryAllocation( double specK, int bit_width, unsigned long long bfseed) {
  K = specK;  seedOfBfTable = bfseed;  tableWidthPow = bit_width;
  bfTable.Initialize( tableWidthPow, K, seedOfBfTable);

}

//------------------------------------------------------
void classFccIsing::AllUpConfiguration()
{
  //set all up configuration
  for( long i=0; i < numAllocation; i++) {
    spinA[i] = -1;
    spinB[i] = -1;
    spinC[i] = -1;
    spinD[i] = -1;
  }
}

//------------------------------------------------
void classFccIsing::Update()
{
  int64_t *us,*nn_xy,*nn_yz,*nn_zx;
  //Update of SpinA
  us = & spinA[Lx*(Ly+1)-1];
  nn_xy = & spinB[Lx*Ly-1];
  nn_yz = & spinC[0];
  nn_zx = & spinD[Lx-1];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinA
  for( long i=0; i<Lx*(Ly+1)-1; i++) spinA[i] = spinA[i+Lx*Ly*Lz];
  for( long i=Lx*(Ly+1)+LxLyLz-1; i<numAllocation; i++) spinA[i] = spinA[i-LxLyLz];
  
  //Update of SpinB
  us = & spinB[Lx*Ly];
  nn_xy = & spinA[Lx*Ly-1];
  nn_yz = & spinD[0];
  nn_zx = & spinC[0];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinB
  for( long i=0; i<Lx*Ly; i++) spinB[i] = spinB[i+LxLyLz];
  for( long i=Lx*Ly+LxLyLz; i< numAllocation; i++) spinB[i] = spinB[i-LxLyLz];

  //Update of SpinC
  us = & spinC[1];
  nn_xy = & spinD[0];
  nn_yz = & spinA[0];
  nn_zx = & spinB[0];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);

  //Copy of SpinC
  for( long i=0; i<1; i++) spinC[i] = spinC[i+LxLyLz];
  for( long i=1+LxLyLz; i< numAllocation; i++) spinC[i] = spinC[i-LxLyLz];

  //Update of SpinD
  us = & spinD[Lx];
  nn_xy = & spinC[0];
  nn_yz = & spinB[0];
  nn_zx = & spinA[Lx-1];
  PrepareRandomNumbers();
  UpdateSub( us, nn_xy, nn_yz, nn_zx);
  
  //Copy of SpinD
  for( long i=0; i<Lx; i++) spinD[i] = spinD[i+LxLyLz];
  for( long i=Lx+LxLyLz; i< numAllocation; i++) spinD[i] = spinD[i-LxLyLz];
  
}

//-----------------------------------------------
inline void classFccIsing::PrepareRandomNumbers() {
  static const long loc_spins = LxLyLz/16;
  int which_rnd[16];
  for( int i=0; i<16; i++) which_rnd[i] = -1;
  for( int i=0; i<16; i++) {
    int r = static_cast<int>( ( rnd[0].rndD() ) * (16-i) );
    for( int j=0; j<16; j++) {
      if( which_rnd[j] == -1) r--;
      if( r < 0 ) { which_rnd[j] = i; break;}
      if( j==15 ) throw 99;
    }
  }

  //  std::cerr << "In PrepareRandomNumbers()." <<std::endl;
  //  for( int i=0; i<16; i++) { std::cerr << i << ' ' << which_rnd[i] << std::endl;}

  /*poption parallel */
  for( int pe = 0; pe < 16; pe++) {
    int j = which_rnd[pe];
    rnd[j].rndI_array( &random[loc_spins*pe], loc_spins);
  }
}		       

//-----------------------------------------------
inline void classFccIsing::UpdateSub(int64_t* us, int64_t* nn_xy, int64_t* nn_yz, int64_t* nn_zx) {
  int64_t IX1, IX2, IX3;
  int64_t *nn_xy1 = nn_xy + Lx;
  int64_t *nn_yz1 = nn_yz + Lx, *nn_yz2 = nn_yz + Lx*Ly, *nn_yz3 = nn_yz + Lx*(Ly+1);
  int64_t *nn_zx1 = nn_zx + Lx*Ly;
  int64_t f1,f2,f3,f4,f5,f6,f7,f8;
  int64_t h1, h2, i1, j1, j2, k1, k2, l1, l2, m1, m2, n1, ID;

  /*poption parallel */
  for( long i=0; i<LxLyLz; i++) {
    bfTable.GetTableElements( IX1, IX2, IX3, random[i]*3);

    Plus( *nn_xy, *nn_xy1, *nn_yz, f1, f2);
    Plus( *(++nn_xy), *(++nn_xy1), *nn_yz1, f3, f4);
    Plus( *nn_zx, *nn_zx1, *nn_yz2, f5, f6);
    Plus( *(++nn_zx), *(++nn_zx1), *nn_yz3, f7, f8);
    nn_yz++; nn_yz1++; nn_yz2++; nn_yz3++;

    f1 ^= *us;
    f2 ^= *us;
    f3 ^= *us;
    f4 ^= *us;
    f5 ^= *us;
    f6 ^= *us;
    f7 ^= *us;
    f8 ^= *us;

    Plus( IX3, f2, f4, h1, h2);
    Plus( f6, f8, h2, i1);
    Plus( h1, f1, f3, j1, j2);
    Plus( f5, f7, i1, k1, k2);
    Plus( IX2, j2, k2, l1, l2);
    Plus( IX1, j1, k1, m1, m2);
    Plus( m2, l1, l2, n1);
    ID = m1 | n1;
    
    *us ^= ID;
    us++;
    
  }
}
//---------------------------------------------------
inline void classFccIsing::Plus(int64_t a,int64_t b, int64_t c, int64_t & d, int64_t & e){
  //<d,e> = a+b+c
  d = a & b; //<d,e> = a+b
  e = a ^ b;
  a = e & c; // e+c;
  d = d | a; // second order
  e = e ^ c; // e+c; first order
}
//--------------------------------------------------
inline void classFccIsing::Plus(int64_t a,int64_t b, int64_t c, int64_t & d){
  //<d,*> = a+b+c;
  d = a & b; // < d,b> = a+b;
  b = b ^ a;
  a = b & c; //a+c
  d = d | a; //second order
}
//-----------------------------------------------
double classFccIsing::AveragedOrderParameter()
{
  long loc = LxLyLz/16;
  long magA[16][64], magB[16][64], magC[16][64], magD[16][64];
  /*poption parallel */
  for( int pe = 0; pe<16; pe++) {
    bwmsc[pe].NIbitcount( &spinA[loc*pe], loc, &(magA[pe][0]) );
    bwmsc[pe].NIbitcount( &spinB[loc*pe], loc, &(magB[pe][0]) );
    bwmsc[pe].NIbitcount( &spinC[loc*pe], loc, &(magC[pe][0]) );
    bwmsc[pe].NIbitcount( &spinD[loc*pe], loc, &(magD[pe][0]) );
  }
  
  long mag = 0;
  for( int i=0; i<64; i++) {
    for( int pe=0; pe<16; pe++) mag += magA[pe][i] + magB[pe][i] + magC[pe][i] + magD[pe][i];
  }
  double center = static_cast<double>( 128*LxLyLz);
  return (static_cast<double>(mag - 128*LxLyLz) / center);
}
//----------------------------------------------
void classFccIsing::ConfigurationOut(int i) {
  if( i<0 || i>63) {
    cerr << "Error! in classFccIsing::ConfigurationOut()" << endl;
    exit(1);
  }
  uint64_t mask = 1 << i;
  
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinA[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinB[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinC[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
  for( long i=0; i<LxLyLz; i++) {
    if( mask & spinD[i] ) cout << 1;
    else cout << 0;
  }
  cout << endl;
}
