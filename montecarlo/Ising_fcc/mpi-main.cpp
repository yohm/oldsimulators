#include "mpi.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include "mtrand64.cpp"
#include "twrand.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTableFcc64.cpp"
#include "fccIsingMPI.cpp"
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <string>

//#define BENCH

//input the arguments as follows
// program Lx Ly k Tmax seed bitlength data_id
// ./a.out 299 300 1.005 128 23419721 16 00002
int main(int argc, char* argv[]) {
  if(argc!=7) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <L> <k> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }


  
  //  clock_t start,end;
  int32_t l = boost::lexical_cast<int32_t>( argv[1]);
  double k = boost::lexical_cast<double>( argv[2]);
  int32_t tmax = boost::lexical_cast<int32_t>( argv[3]);
  uint64_t sed = boost::lexical_cast<uint64_t>( argv[4]);
  int32_t bitwt = boost::lexical_cast<int32_t>( argv[5]);
  uint64_t bf_sed = sed + 72137ULL;
  std::string id = argv[6];

#ifdef BENCH
  clock_t start = clock();
#endif

  fccIsingMPI sim;
  sim.Initialize( l, l, l, k, sed, bitwt, bf_sed, id);

#ifdef BENCH
  clock_t end = clock();
  std::cerr << "time for initialization:"<<(double)(end-start)/(CLOCKS_PER_SEC)<< std::endl;
#endif

#ifdef BENCH
  start = clock();
#endif

  sim.Run(tmax);
    
#ifdef BENCH
  end = clock();
  double time = (double)(end-start)/(CLOCKS_PER_SEC);
  std::cerr << "time for update:"<< time << std::endl;
  unsigned long long ll = static_cast<unsigned long long>(l);
  unsigned long long spins = ll * ll * ll * 4 * 64.0 * tmax;
  std::cerr << static_cast<double>(spins) / time / 1000000.0 << "M updates / sec" << std::endl;
#endif
  return 0;
}
