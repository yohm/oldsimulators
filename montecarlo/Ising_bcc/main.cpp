#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include "mtrand64.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTableBcc64.cpp"
#include "classBccIsing.cpp"

using namespace std;

//#define BENCH

//input the arguments as follows
// program Lx Ly k Tmax seed bitlength data_id
// ./a.out 299 300 1.005 128 23419721 16 00002
int main(int argc, char* argv[]) {
  if(argc!=7) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <L> <k> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }

  //  clock_t start,end;
  long tmax = atoi( argv[3]);
  long l=atoi( argv[1]);
  double k = atof( argv[2]);
  unsigned long long sed = atol( argv[4]);
  int bitwt = atoi( argv[5]);
  unsigned long long bf_sed = sed + 72137ULL;
  char str[20];
  strcpy( str,argv[6]);
  strcat( str, ".NERo");

  classBccIsing sim;
  sim.Initialize( l, l, l, k, sed, bitwt, bf_sed);

  ofstream fout(str);

#ifdef BENCH
  clock_t start = clock();
#endif
  
  for( int t=0; t<tmax; t++) {
    fout << t << " " << sim.AveragedOrderParameter() <<endl;
    sim.Update();
  }
#ifdef BENCH
  end = clock();
  cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC)<<endl;
#endif
  fout.close();
  return 0;
}
