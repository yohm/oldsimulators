#include <iostream>
#include "classBfTableBcc64.hpp"
using namespace std;

//---------------------------------------------------------
classBfTableBcc::classBfTableBcc() {
  tableSizePower = 0;
  tableSize = 0;
  totalTableSize = 0;
  bfSeed = 0;
  inverseTemperature = 0;
  table = NULL;
}

//---------------------------------------------------------
classBfTableBcc::~classBfTableBcc() {
  delete [] table;
}
//--------------------------------------------------------
void classBfTableBcc::Initialize( int pow, double K, unsigned long long bf_seed) {
  if( tableSizePower==pow && inverseTemperature==K && bfSeed==bf_seed) { return;}

  if( pow > tableSizePower) {
    tableSizePower = pow;
    tableSize = 1 << tableSizePower;
    totalTableSize = tableSize * 3;
    delete [] table;
    table = new long64[ totalTableSize];
  }
  else {
    tableSizePower = pow;
    tableSize = 1 << tableSizePower;
    totalTableSize = tableSize * 3;
  }
  
  for( long i=0; i < totalTableSize; i++) table[i] = 0; //initialize

  inverseTemperature = K;
  bfSeed = bf_seed;

  MakeRandomTable( inverseTemperature, bfSeed);
}

//--------------private member-----------------------------
//---------------------------------------------------------
void classBfTableBcc::MakeRandomTable(double K, unsigned long long seed) {
  SetBit( K);
  ShuffleTable( seed);
}
//----------------------------------------------------------
void classBfTableBcc::SetBit(double K) {
  long Num[5]; //Num[i]:{exp(-4K*i)}*tableSize;
  for( int i=0; i<5; i++) Num[i] =  (long) (exp(-4*K*i) * tableSize); //Numの計算

  for( long j=0; j<Num[4]; j++) { //j番目の要素
    table[3*j] = -1;
    table[3*j+1] = 0;
    table[3*j+2] = 0; //Set 4
  }
  for( long j=Num[4]; j<Num[3]; j++) {
    table[3*j] = 0;
    table[3*j+1] = -1;
    table[3*j+2] = -1; //Set 3
  }
  for( long j=Num[3]; j<Num[2]; j++) {
    table[3*j] = 0;
    table[3*j+1] = -1;
    table[3*j+2] = 0; //Set 2
  }
  for( long j=Num[2]; j<Num[1]; j++) {
    table[3*j] = 0;
    table[3*j+1] = 0;
    table[3*j+2] = -1; //Set 1
  }
  for( long j=Num[1]; j<Num[0]; j++) {
    table[3*j] = 0;
    table[3*j+1] = 0;
    table[3*j+2] = 0; //Set 0;
  }
}
//----------------------------------------------------------
void classBfTableBcc::ShuffleTable(unsigned long long seed) {
  mtrand64 rnd( seed);

  for( int i=0; i<64; i++) {
    for(int j=0; j<tableSize; j++) {
      long random = rnd.genrand64_bit(tableSizePower);
      Exchange( i, 3*j, 3*random);
      Exchange( i, 3*j+1, 3*random+1);
      Exchange( i, 3*j+2, 3*random+2);
    }
  }
}

//----------------------------------------------------------
void classBfTableBcc::Exchange( int i, int j, int k) {
  long64 mask = 1LL;
  mask <<= i; //i-bit目のマスク
  long64 temp1 = table[j] & mask; //table[j] の i-bit目
  long64 temp2 = table[k] & mask; //table[k] の i-bit
  
  table[j] = table[j] & (~mask); //i-bit目以外は変更しない
  table[k] = table[k] & (~mask); //i-bit目は0にする

  table[j] = table[j] | temp2; //jにkのi-bitを入れる
  table[k] = table[k] | temp1; //kにjのi-bitを入れる
}
//---------------------------------------------------------
#ifdef DEBUG
void classBfTableBcc::TableOutputBit() {
  for( int j=0; j<totalTableSize; j++) {
    long64 mask = 1LL;
    for( int i=63; i>=0; i--) {
      mask = (1LL << i);
      if( (table[j] & mask)==0 ) cout << 0;
      else cout << 1;
    }
    cout << endl;
  }
}
//--------------------------------------------------------
void classBfTableBcc::TableOutputNumber() {
  for( int j=0; j<tableSize; j++) {
    long64 mask = 1LL;
    for( int i=63; i>=0; i--) {
      mask = (1LL << i);
      short temp = 0;
      if( table[3*j] & mask) temp += 4;
      if( table[3*j+1] & mask) temp += 2;
      if( table[3*j+2] & mask) temp += 1;
      cout << temp;
    }
    cout << endl;
  }
}
#endif
