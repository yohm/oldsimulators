#ifndef _BFTABLE_BCC_H_
#define _BFTABLE_BCC_H_

#include <cmath>
#include "mtrand64.hpp"
using namespace std;

#define DEBUG

typedef long long long64;   //long long を 64bit と仮定

class classBfTableBcc {
  //BfTable class 64sample同じ温度についてしか計算できない
  //tableにはそれぞれのテーブルの要素をを交互に並べていく
  //IX1,IX2,IX3...の順番で並べる。３つのテーブルが必要
  //それぞれのテーブルは独立ではない。シャッフルする時はnumTable個のビットをまとめて混ぜる
  // 引数には逆温度Kを与える
public:
  classBfTableBcc();
  ~classBfTableBcc();
  void Initialize( int pow, double K, unsigned long long seed);
  //Initialize and allocate memory. If pow,K,seed are unchanged ,this routine don't make table.
  int TableSizePower() { return tableSizePower;}
  long64 GetTableElement(long i) { return table[i];}

#ifdef DEBUG
  void TableOutputBit(); //tableのビットを出力する
  void TableOutputNumber(); //Table_num番目のテーブルのビットだけを出力する
  //Table_num <= numTable-1
#endif //DEBUG

private:
  long64 * table; //0,1を入れるテーブル。リストを必要な数だけ連続して取る  
  int tableSizePower; //tableSize = 2^(tableSizePower)
  long tableSize; //テーブルサイズが 2^(tableSizePower) になる。
  long totalTableSize; // = tableSize * numTable 
  unsigned long long bfSeed; //bit seed for random shuffling
  double inverseTemperature; 
  void MakeRandomTable(double K, unsigned long long seed); //テーブルに0,1を入れる "sub of Initialize"
  void SetBit(double K);
  void ShuffleTable(unsigned long long seed); //table1, table2...をシャッフルする。そのとき3bitまとめて混ぜる。引数は乱数の種 "sub of MakeRandomTable()"
  void Exchange( int i, int j, int k); //i-bit目のtable[j]とtable[random]の値を入れ替える "sub of ShuffleOnce()"

};

#endif //_BFTABLE_H_
