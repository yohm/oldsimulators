//#include "StdAfx.h"
#include "classBccIsing.hpp"
using namespace std;
//-------------------------------------------------------------
classBccIsing::classBccIsing(){
  Lx = Ly = Lz = LxLyLz = numUsukawa = numAllocation = LxLy = LxxLy = 0;
  tableWidthPow = 0;
  K = 0;

  spinA = spinB = NULL;
  work = NULL;
  random = NULL;
}
//-------------------------------------------------------------
classBccIsing::~classBccIsing() {
  delete [] spinA;
  delete [] spinB;
}
//-------------------------------------------------------------
void classBccIsing::Initialize(long lx,long ly,long lz,double inverseTemperature,
					 unsigned long long seed,
					 int table_width_pow,
					 unsigned long long bf_seed){
  Lx = lx;  Ly = ly;  Lz = lz;  LxLyLz = Lx*Ly*Lz;  numUsukawa = Lx * (Ly+1) + 1;
  LxLy = Lx*Ly;  LxxLy = Lx*(Ly+1);
  numAllocation = LxLyLz + numUsukawa;
  static long realAllocatedSize = 0;
  if( numAllocation > realAllocatedSize ) {
    ReallocateSpin();
    realAllocatedSize = numAllocation;
  }
  static long prevLxLyLz = 0;
  if( LxLyLz > prevLxLyLz ) {
    delete random;
    delete work;
    random = new long[LxLyLz];
    work = new long long[LxLyLz];
    prevLxLyLz = LxLyLz;
  }

  bwmsc.Bitwise_multispincoding_Initialization(LxLyLz);

  tableWidthPow = table_width_pow;
  K = inverseTemperature;
  bfTable.Initialize( tableWidthPow, K, bf_seed);

  rnd.init_genrand64( seed);

  AllUpConfiguration();
}


//-------------------------------------------------------------
void classBccIsing::ReallocateSpin(){
  delete [] spinA; delete [] spinB;
  spinA = new long long [numAllocation];
  spinB = new long long [numAllocation];
}
//-------------------------------------------------------------
void classBccIsing::AllUpConfiguration() {
  for( int i=0; i<numAllocation; i++) {
    spinA[i] = -1;
    spinB[i] = -1;
  }
}
//-------------------------------------------------------------
double classBccIsing::AveragedOrderParameter() {
  long magA[64] ,magB[64];
  bwmsc.NIbitcount( spinA, LxLyLz, magA);
  bwmsc.NIbitcount( spinB, LxLyLz, magB);
  double total = 0;
  for( int i=0; i<64; i++) {
    long mag = magA[i] + magB[i];
    total += (double)(mag - LxLyLz) / (double)LxLyLz;
  }
  return total/64.0;
}
//-------------------------------------------------------------
double classBccIsing::AveragedEnergy() {
  long ene[64];
  CountEnergy( ene);
  double temp = 0;
  for( int i=0; i<64; i++) temp += ene[i];
  return temp/64.0;
}
//-------------------------------------------------------------
void classBccIsing::CountEnergy( long* ene) {
  long enetemp[64];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i];
  bwmsc.NIbitcount( work,LxLyLz, ene);
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+1];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+Lx];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+Lx+1];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+LxLy];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+LxLy+1];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+LxxLy];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  for( long i=0; i<LxLyLz; i++) work[i] = spinB[i] ^ spinA[i+LxxLy+1];
  bwmsc.NIbitcount( work,LxLyLz, enetemp);
  for(int i=0; i<64; i++) ene[i] += enetemp[i];
  
  for( int i=0; i<64; i++) ene[i] = (ene[i]*2) - (8*LxLyLz);
}
//-------------------------------------------------------------
void classBccIsing::Update(){
  //Update of SpinA
  long long * updateSpin = spinA+numUsukawa;
  long long * nn1 = spinB;
  UpdateSub( updateSpin, nn1);

  //Copy of SpinA
  updateSpin = spinA; //to
  nn1 = updateSpin + LxLyLz; //from
  CopySub( nn1, updateSpin);

  //Update of SpinB
  updateSpin = spinB;
  nn1 = spinA;
  UpdateSub( updateSpin,nn1);

  //Copy of SpinB
  updateSpin = spinB + LxLyLz; //to
  nn1 = spinB; //from
  CopySub(nn1, updateSpin);
}

void classBccIsing::UpdateSub( long long *ud, long long *nn1) {
  long long * nn2 = nn1+Lx, *nn3 = nn1+LxLy, *nn4 = nn1+LxxLy;

  //  rnd.genrand64_bit( random, LxLyLz);
  
  for( long i=0; i<LxLyLz; i++) {
    long r = rnd.genrand64_bit( tableWidthPow) * 3;
    long long IX1 = bfTable.GetTableElement(r);
    long long IX2 = bfTable.GetTableElement(r+1);
    long long IX3 = bfTable.GetTableElement(r+2);

    long long e1 = *ud ^ *nn1;
    long long e2 = *ud ^ *(++nn1);
    long long e3 = *ud ^ *nn2;
    long long e4 = *ud ^ *(++nn2);
    long long e5 = *ud ^ *nn3;
    long long e6 = *ud ^ *(++nn3);
    long long e7 = *ud ^ *nn4;
    long long e8 = *ud ^ *(++nn4);

    long long f1, f2, f3, f4, f5, f6;
    Plus(e1,e2,e3,f1,f2); //<f1,f2> = e1+e2+e3;
    Plus(e4,e5,e6,f3,f4);
    Plus(e7,e8,IX3,f5,f6);

    long long g1;
    Plus(f2,f4,f6, g1);
    long long h1,h2;
    Plus(IX2,f1,f3, h1,h2);
    long long i1;
    Plus(h2,f5,g1, i1);

    long long ID;
    ID = IX1 | h1;
    ID = ID | i1;

    *ud = *ud ^ ID;
    ud++;
  }
}
//---------------------------------------------------
void classBccIsing::Plus(long long a,long long b, long long c, long long & d, long long & e){
  //<d,e> = a+b+c
  d = a & b; //<d,e> = a+b
  e = a ^ b;
  a = e & c; // e+c;
  d = d | a; // second order
  e = e ^ c; // e+c; first order
}
//--------------------------------------------------
void classBccIsing::Plus(long long a,long long b, long long c, long long & d){
  //<d,*> = a+b+c;
  d = a & b; // < d,b> = a+b;
  b = b ^ a;
  a = b & c; //a+c
  d = d | a; //second order
}
//--------------------------------------------------
void classBccIsing::CopySub(long long* cfrom,long long* cto){
  //Copy (numUsukawa) times.
  for( int i=0; i<numUsukawa; i++) {
    *cto = *cfrom;//cfrom$B$+$i(Bcto$B$X$N%3%T!<(B
    cto++; cfrom++;
  }
}
