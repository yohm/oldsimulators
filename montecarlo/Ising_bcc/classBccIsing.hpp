#ifndef _BCC_ISING_H_
#define _BCC_ISING_H_

#include <iostream>
#include <cmath>
using namespace std;
#include "classBfTableBcc64.hpp"
#include "mtrand64.hpp"
#include "Bitwise_multispincoding.h"

class classBccIsing {
public:
  classBccIsing();
  ~classBccIsing();
  void Initialize(long lx,long ly,long lz,double inverseTemp,unsigned long long seed, int table_width_pow,unsigned long long bf_seed);
  //Set parameters and allocate memory and set all-up configuration
  void AllUpConfiguration(); //Set all spins up state.  
  double AveragedOrderParameter(); //averaged over 64 sample.
  double AveragedEnergy(); //averaged over 64 sample
  void Update();
  
private:
  long Lx,Ly,Lz;
  double K; //Inverse Temperature
  long long *spinA, *spinB;
  long long * work;//work[LxLyLz]; work[i] is used for counting energy
  long * random; //random[LxLyLz]; random[i] is used in UpdateSub
  Bitwise_multispincoding bwmsc; 
  mtrand64 rnd; //random number generator
  classBfTableBcc bfTable; //Boltzman factor table

  long LxLyLz, numAllocation, numUsukawa; //=Lx*Ly*Lz,LxLyLz+numUsukawa,Lx(Ly+1)+1
  long LxLy, LxxLy; //=Lx*Ly , Lx(Ly+1)
  int tableWidthPow; //Width of bf table is 2^(tableWidthPow);

  void ReallocateSpin();
  void UpdateSub(long long *us, long long *nn1); //sub of "Update()"
  void Plus(long long a, long long b,long long c,long long & d, long long & e); //sub of UpdateSub; <d,e> = a+b+c;
  void Plus(long long a, long long b,long long c,long long & d); //sub of UpdateSub; <d,*> = a+b+c;
  void CopySub(long long* cfrom,long long* cto);  //copy of sublattice
  void CountEnergy( long * ene); //count energy of each sample. sub of "Averaged Energy"

};



#endif
