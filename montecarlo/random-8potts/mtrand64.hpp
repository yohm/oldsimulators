#include <cmath>
#ifndef MTRAND64
#define MTRAND64

/* 
   A C-program for MT19937-64 (2004/9/29 version).
   Coded by Takuji Nishimura and Makoto Matsumoto.

   This is a 64-bit version of Mersenne Twister pseudorandom number
   generator.

   Before using, initialize the state by using init_genrand64(seed)  
   or init_by_array64(init_key, key_length).

   Copyright (C) 2004, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   3. The names of its contributors may not be used to endorse or promote 
   products derived from this software without specific prior written 
   permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   References:
   T. Nishimura, ``Tables of 64-bit Mersenne Twisters''
   ACM Transactions on Modeling and 
   Computer Simulation 10. (2000) 348--357.
   M. Matsumoto and T. Nishimura,
   ``Mersenne Twister: a 623-dimensionally equidistributed
   uniform pseudorandom number generator''
   ACM Transactions on Modeling and 
   Computer Simulation 8. (Jan. 1998) 3--30.

   Any feedback is very welcome.
   http://www.math.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove spaces)
*/

#define MATRIX_A 0xB5026F5AA96619E9ULL
#define UM 0xFFFFFFFF80000000ULL /* Most significant 33 bits */
#define LM 0x7FFFFFFFULL /* Least significant 31 bits */
#define NN 312
#define MM 156

class mtrand64 {
private:
	
  /* The array for the state vector */
  unsigned long long mt[NN]; 
  /* mti==NN+1 means mt[NN] is not initialized */
  int mti;
  unsigned long long mask[64]; //mask[i] = 00001111...11  (i-bit 1)
  void init_mask();

  unsigned long long stock;
  short int stockedBitLength;

  unsigned long stocked_32;
  bool flag_stocked_32;

  static const double PI;
  static const double PI2;
  void set_integers_into_stack(int* stack, const int size);//subroutine of load_16bit_integer().
public:
		
  /* initializes mt[NN] with a seed */
  void init_genrand64(unsigned long long seed);

  /* initialize by an array with array-length */
  /* init_key is the array for initializing keys */
  /* key_length is its length */
  void init_by_array64(unsigned long long init_key[], 
		       unsigned long long key_length);
  
  /* constructor. initialize with a seed */
  mtrand64(unsigned long long seed) {
    init_genrand64( seed);
    init_mask();
  }
	
  /* constructor. initialize by an array with array-length */
  mtrand64(unsigned long long init_key[], unsigned long long key_length) {
    init_by_array64( init_key, key_length);
    init_mask();
  }
	
  /* default constructor. */
  mtrand64() { init_genrand64(0); init_mask();}

  /* generates a random number on [0, 2^64-1]-interval */
  unsigned long long genrand64_int64(void);


  /* generates a random number on [0, 2^63-1]-interval */
  long long genrand64_int63(void);

  /* generates a random number on [0, 2^32-1]-interval */
  unsigned long genrand64_int32(void);
	
  /* generates a random n-bit */
  unsigned long long genrand64_bit( int n);

  /* generates a random number on [0,1]-real-interval */
  double genrand64_real1(void);

  /* generates a random number on [0,1)-real-interval */
  double genrand64_real2(void);

  /* generates a random number on (0,1)-real-interval */
  double genrand64_real3(void);

  // generates a random number on normal distribution (sigma=1.0 mean=0.0)
  double genrand64_nd();

  // generates two random number on normal distribution (sigma=1.0 mean=0.0 32bit)
  void genrand64_nd32(double& r1, double& r2);

  // load 16 bit integer from stack (0 ~ 65535)
  int load_16bit_integer();

  // generates one random number on exponential distribution (mean=sigma=1.0)
  // f(x) = exp(-x),  F(x) = 1 - exp(-x)
  double genrand64_exp();
};

#endif
