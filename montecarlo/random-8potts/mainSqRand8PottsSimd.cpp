#include "mpi.h"
#include <iostream>
#include <ctime>
#include <boost/lexical_cast.hpp>
#include <boost/cstdint.hpp>
#include <omp.h>
#include "classSqRand8PottsSimd.cpp"
#include "classBfTableSqRandQPottsSimd.cpp"
#include "mtrand64.cpp"
#include "RandomLib/Random.cpp"

#define BENCH

//input the arguments as follows
// program Lx Ly q p r k bitlength t-max bond-seed update-seed data_id
// ./sq-rand-8potts-bin.out 299 300 8 0.5 10.0 1.005 16 128 666777 888999 16 00002

int main (int argc, char* argv[]) {

  //check the validity of given parameters
  if( argc != 10 ) {
    std::cerr << "Input arguments" << std::endl
	      << "program <1:Lx> <2:Ly> <3:p> <4:r> <5:K> <6:bit_length> "
	      << "<7:t_max> <8:bond_seed> <9:update_seed>"
	      << std::endl << std::endl;
    throw 1;
  }

  long lx = boost::lexical_cast<long>(argv[1]);
  long ly = boost::lexical_cast<long>(argv[2]);
  double p = boost::lexical_cast<double>(argv[3]);
  int r = boost::lexical_cast<int>(argv[4]);
  double k = boost::lexical_cast<double>(argv[5]);
  int bit_length = boost::lexical_cast<int>(argv[6]);
  long t_max = boost::lexical_cast<long>(argv[7]);
  unsigned long long bond_seed = boost::lexical_cast<unsigned long long>(argv[8]);
  unsigned long long update_seed = boost::lexical_cast<unsigned long long>(argv[9]);
  
  std::cerr << "Specified parameters are the following:" << std::endl
	    << "lx:\t" << lx << std::endl
	    << "ly:\t" << ly << std::endl
	    << "p:\t" << p << std::endl
	    << "r:\t" << r << std::endl
	    << "k:\t" << k << std::endl
	    << "bit_length:\t" << bit_length << std::endl
	    << "t_max:\t" << t_max << std::endl
	    << "bond_seed:\t" << bond_seed << std::endl
	    << "update_seed:\t" << update_seed << std::endl;
  
  if( lx <= 0 || ly <= 0 || p < 0.0 || p > 1.0 || r < 1.0 || k <= 0.0 || bit_length < 0 || t_max < 0 ) {
    std::cerr << "Given parameters are not valid for this simulator." << std::endl;
    throw 1;
  }

  classSqRand8PottsSimd sim;
  sim.Initialize(lx,ly,p,r,k,bond_seed,update_seed, bit_length);
  //sim.SetDisorderedState();
  //  std::string filename = id + ".NERo";
  //  std::ofstream fout( filename );
  
#ifdef BENCH
  //  clock_t start = clock();
  double start = omp_get_wtime();
#endif
  //  sim.BondConfigurationOutput();
  //  sim.ConfigurationOutput();
  //  double magnetization[16];
  //int32_t energy[16];

  sim.Run_mag( t_max);
  //  for( int t=1; t<=t_max; t++) {
  //    sim.Update();
    //    sim.OrderParameter0( magnetization );
    //    for( int i=0; i<16; i++) std::cout << magnetization[i] << ' '; std::cout << std::endl;
    //    for( int i=0; i<16; i++) std::cout << sim.OrderParameter0(i) << ' '; std::cout << std::endl;
    //    sim.OrderParameter0( magnetization , 0);
    
//     sim.Energy( energy );
//     for( int i=0; i<16; i++) std::cout << energy[i] << ' '; std::cout << std::endl;
//     for( int i=0; i<16; i++) std::cout << sim.Energy(i) <<' '; std::cout<< std::endl;
    //    sim.OrderParameter2( magnetization );
    //    for( int i=0; i<16; i++) std::cout << magnetization[i] << ' '; std::cout << std::endl;
    //    std::cout << sim.OrderParameter0() << std::endl;
    // 	      << sim.OrderParameter1() << ' '
    // 	      << sim.OrderParameter2() << ' '
    // 	      << sim.Energy() << std::endl;
    //    sim.ConfigurationOutput();
  //}
  
#ifdef BENCH
  double end = omp_get_wtime();
  std::cerr << "time:"<<(end-start)<< std::endl;
  //  clock_t end = clock();
  //  std::cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC)<< std::endl;
#endif

  //  RandomLib::Random rnd_gen;
  //  for( int i=0; i<100; i++) { std::cerr << rnd_gen.Integer<16>() << std::endl; }

  return 0;
}
