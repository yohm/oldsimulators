#include "classSqRand8PottsSimd.hpp"

//-------------------
classSqRand8PottsSimd::classSqRand8PottsSimd() {
  //適当な初期値を設定
  Lx=1; Ly=0; k=0.1;
  numAllocation = 0;
  seedOfUpdate = 0;
}

//------------------------
classSqRand8PottsSimd::~classSqRand8PottsSimd() {
}

//------------------------
void classSqRand8PottsSimd::Initialize(long t_Lx, long t_Ly, double t_p, int t_bondRatio , double t_k, 
				   uint64_t t_seedOfBond,
				   uint64_t t_seedOfUpdate, 
				   uint16_t t_tableBitWidth ) {
  if( (t_Lx%2==0) || (t_Ly%2==1) || t_p < 0.0 || t_p > 1.0 || t_k<=0.0 ) throw 100;
  Lx = t_Lx; Ly = t_Ly; p = t_p; bondRatio = t_bondRatio; k = t_k;
  seedOfBond = t_seedOfBond; seedOfUpdate = t_seedOfUpdate;
  tableBitWidth = t_tableBitWidth;
  LxLy = Lx * Ly; numAllocation = Lx * (Ly + 2);
  
  MemoryAllocation();

#pragma omp parallel
  {
#pragma omp master
    { n_threads = omp_get_num_threads(); }
  }
  if( (LxLy/2) % n_threads ) {
    std::cerr << "Error! LxLy/2 must be a multiple of the number of OMP threads." << ' '
	      << n_threads << std::endl;
    throw 200;
  }
  
  rnd.resize(n_threads,RandomLib::Random(0));
  for( int i=0; i<n_threads; i++) {
    unsigned long long temp_seed[2] = {seedOfUpdate, i};
    rnd[i].Reseed( temp_seed, temp_seed+2 );
  }
  
  table.Initialize( tableBitWidth , k, bondRatio, seedOfUpdate+12345ULL);
  table_start_address = table.startAddress();
  SetOrderedState();
}
//------------------------
void classSqRand8PottsSimd::MemoryAllocation() {
  spin.resize( numAllocation);
  xBond.resize( numAllocation);
  yBond.resize( numAllocation);

  SetOrderedState();
  
  mtrand64 b_rand(seedOfBond);
  long temp = bondRatio;
  if( bondRatio > 8 ) temp = 8;
  else if( bondRatio < -8 ) temp = -8;
  for( int i=Lx; i<Lx+LxLy; i++) {
    for( int j=0; j<16; j++) {
      xBond[i].byte[j] = ( b_rand.genrand64_real1() < p ) ? temp : 1;
      yBond[i].byte[j] = ( b_rand.genrand64_real1() < p ) ? temp : 1;
    }
  }
  for( int i=0; i<Lx; i++) {
    xBond[i] = xBond[i+LxLy];
    yBond[i] = yBond[i+LxLy];
  }
  for( int i=Lx+LxLy; i<2*Lx+LxLy; i++) {
    xBond[i] = xBond[i-LxLy];
    yBond[i] = yBond[i-LxLy];
  }

  mask = 0UL;
  for( int i=0; i<tableBitWidth; i++) mask = (mask << 1) + 1;

  next_state.resize( LxLy/2);
  for( int i=0; i<LxLy/2; i++) next_state[i].vec = _mm_xor_si128( next_state[i].vec, next_state[i].vec);
  randoms.assign(LxLy/2,0);

}
//-----------------------------
void classSqRand8PottsSimd::SetOrderedState() {
  //set all 0 configuration
  for( long i=0; i<numAllocation; i++) spin[i].vec = _mm_xor_si128( spin[i].vec, spin[i].vec);
}
//-----------------------------
void classSqRand8PottsSimd::SetDisorderedState() {
  //set random configuration
  const int spt = LxLy/n_threads;
  //  const __m128i mask07 = { 0x0707070707070707ULL, 0x0707070707070707ULL};
#pragma omp parallel for schedule(static)
  for( int th = 0; th<n_threads; th++) {
    const long offset = Lx+spt*th;
    for( long i=0; i<spt; i++) {
      //spin[offset+i].vec = rnd[th].Integer<128>() & mask07;
      uint64_t temp_r = rnd[th].Integer<uint64_t>();
      spin[offset+i].integer64[0] = temp_r & 0x0707070707070707ULL;
      spin[offset+i].integer64[1] = ( temp_r & 0x7070707070707070ULL ) >> 4;
    }
    CopySub();
  }
}
//----------------------------
void classSqRand8PottsSimd::Run_mag( long t_max) {
  std::ofstream fout("mag0.NERo");
  fout.precision(12);
  for( int t=0; t<t_max; t++) {
    Update();
    double mag[16]; double avg = 0.0;
    OrderParameter0( mag );
    for( int i=0; i<16; i++) avg += mag[i];
    fout << t << ' ' << avg/16.0 << std::endl;
  }
}

//----------------------------
void classSqRand8PottsSimd::Update() {
#pragma omp parallel
  {
    //update odd spin
    PrepareRandomNumbers();
    UpdateSub(Lx);
    
    #pragma omp master
    { CopySub(); }  //copy usukawa
    
    //update even spin
    PrepareRandomNumbers();
    UpdateSub(Lx+1);
    
    #pragma omp master
    { CopySub(); }  //copy usukawa
  }
}

//---------------------------
void classSqRand8PottsSimd::PrepareRandomNumbers() {
  const int size_for_a_thread = LxLy/2/n_threads;
#pragma omp for schedule(static)
  for( int th=0; th<n_threads; th++) {
    for( int i=0; i<size_for_a_thread; i++) {
      uint64_t r_next = rnd[th].Integer<uint64_t>();
      next_state[i+size_for_a_thread*th].integer64[0] = r_next & 0x0707070707070707ULL;
      next_state[i+size_for_a_thread*th].integer64[1] = (r_next & 0x7070707070707070ULL) >> 4;
      randoms[i+size_for_a_thread*th] = rnd[th].Integer<uint32_t>() & mask;
    }
  }
}

//---------------------------
void classSqRand8PottsSimd::UpdateSub( const long& a) {
  const int size_for_a_thread = LxLy/2/n_threads;
  const __m128i zero = {0ULL,0ULL};
#pragma omp for schedule(static)
  for( int th=0; th<n_threads; th++) {
    vInt8 * update_spin = &(spin[a + size_for_a_thread * 2 * th]);
    vInt8 * left_bond = &(xBond[a-1 + size_for_a_thread * 2 * th]);
    vInt8 * bottom_bond = &(yBond[a-Lx + size_for_a_thread * 2 * th]);
    vInt8 * p_next = &(next_state[ size_for_a_thread * th]);
    uint32_t * p_random = &(randoms[ size_for_a_thread * th]);
    vInt8 * p_ir = table_start_address;
    vInt8 de, comp;
    for( int i=0; i<size_for_a_thread; i++) {
      p_ir = table_start_address + (*p_random);
      _mm_prefetch( p_ir, _MM_HINT_T1);
      de.vec = zero;//_mm_xor_si128( de.vec, de.vec);

      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( update_spin->vec, (update_spin-1)->vec ) , left_bond->vec);
      de.vec = _mm_add_epi8( de.vec, comp.vec);
      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( p_next->vec, (update_spin-1)->vec ) , left_bond->vec);
      de.vec = _mm_sub_epi8( de.vec, comp.vec);

      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( update_spin->vec, (update_spin+1)->vec ) , (left_bond+1)->vec);
      de.vec = _mm_add_epi8( de.vec, comp.vec);
      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( p_next->vec, (update_spin+1)->vec ) , (left_bond+1)->vec);
      de.vec = _mm_sub_epi8( de.vec, comp.vec);

      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( update_spin->vec, (update_spin-Lx)->vec ) , bottom_bond->vec);
      de.vec = _mm_add_epi8( de.vec, comp.vec);
      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( p_next->vec, (update_spin-Lx)->vec ) , bottom_bond->vec);
      de.vec = _mm_sub_epi8( de.vec, comp.vec);

      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( update_spin->vec, (update_spin+Lx)->vec ) , (bottom_bond+Lx)->vec);
      de.vec = _mm_add_epi8( de.vec, comp.vec);
      comp.vec = _mm_and_si128( _mm_cmpeq_epi8( p_next->vec, (update_spin+Lx)->vec ) , (bottom_bond+Lx)->vec);
      de.vec = _mm_sub_epi8( de.vec, comp.vec);

      
//       de += ( *update_spin == *(update_spin-1) ? *left_bond : 0 );
//       de -= ( next == *(update_spin-1) ? *left_bond : 0);
//       de += ( *update_spin == *(update_spin+1) ? *(left_bond+1) : 0);
//       de -= ( next == *(update_spin+1) ? *(left_bond+1) : 0);
//       de += ( *update_spin == *(update_spin-Lx) ? *(bottom_bond) : 0);
//       de -= ( next == *(update_spin-Lx) ? *(bottom_bond) : 0);
//       de += ( *update_spin == *(update_spin+Lx) ? *(bottom_bond+Lx) : 0);
//       de -= ( next == *(update_spin+Lx) ? *(bottom_bond+Lx) : 0);
      
      //     de += ( spin[i] == spin[i-1] ? xBond[i-1] : 0 );
      //     de -= ( next == spin[i-1] ? xBond[i-1] : 0);
      //     de += ( spin[i] == spin[i+1] ? xBond[i] : 0);
      //     de -= ( next == spin[i+1] ? xBond[i] : 0);
      //     de += ( spin[i] == spin[i-Lx] ? yBond[i-Lx] : 0);
      //     de -= ( next == spin[i-Lx] ? yBond[i-Lx] : 0);
      //     de += ( spin[i] == spin[i+Lx] ? yBond[i] : 0);
      //     de -= ( next == spin[i+Lx] ? yBond[i] : 0);
      
      //uint8_t ir = getIR();
      //      ir = *p_ir;
      //      vInt8 ir = table.GetTableElement( *p_random );
      //      uint8_t ir = table.GetTableElement( *p_random );
      
      //uint8_t id = static_cast<uint8_t>( ir>=de );
      //  ir + 1 > de
      comp.vec = _mm_cmpgt_epi8( p_ir->vec, de.vec);
      p_next->vec = _mm_and_si128( p_next->vec, comp.vec);
      comp.vec = _mm_andnot_si128( comp.vec, update_spin->vec);
      update_spin->vec = _mm_or_si128( p_next->vec, comp.vec);
      //      *update_spin = (ir-de)>=0 ? next : (*update_spin);
      //if( de <= 0 || rnd.genrand64_real1() < exp(-k*de) ) spin[i] = next;
      
      update_spin+=2; left_bond+=2; bottom_bond+=2; p_next++; p_random++;
    }
  }
}
//------------------------------
inline void classSqRand8PottsSimd::CopySub() {
  for( long i=0; i<Lx; i++) {
    spin[i] = spin[i+Lx*Ly];
    spin[i+Lx*(Ly+1)] = spin[i+Lx];
  }
}
//------------------------------
double classSqRand8PottsSimd::OrderParameter0(int j) {
  const int size_per_thread = LxLy/n_threads;
  std::vector<long> mag( n_threads , 0);
#pragma omp parallel for schedule(static)
  for( int th=0; th<n_threads; th++) {
    for( long i=0; i<size_per_thread; i++) mag[th] += spin[i + size_per_thread * th].byte[j] ? 0 : 1;
  }
  
  unsigned long sum = 0;
  for( int i = 0; i<n_threads; i++) sum += mag[i];
  double ratio = static_cast<double>(sum)/static_cast<double>(LxLy);
  return (8.0*ratio-1.0)/(8.0-1.0);
}

//------------------------------
void classSqRand8PottsSimd::OrderParameter0(double * result_array) {
  const __m128i zero = {0x0000000000000000ULL,0x0000000000000000ULL};
  const __m128i one = {0x0101010101010101ULL,0x0101010101010101ULL};
  const int spt = LxLy/n_threads;
  std::vector< std::vector<int32_t> > mags( n_threads, std::vector<int32_t>(16, 0) );
#pragma omp parallel for schedule(static)
  for( int th=0; th<n_threads; th++) {
    //------ count by 8 bit integer ---------
    __m128i count = zero; //set zero
    const long offset = spt * th;
    std::queue<__m128i> counter8;
    for( long i=0; i<spt; i++) {
      count = _mm_add_epi8( count, _mm_and_si128( _mm_cmpeq_epi8( spin[i+offset].vec, zero), one) );
      if( i % 255 == 254 ) {
	counter8.push( count );
	count = zero;
      }
    }
    counter8.push( count );
    SimdCounting( counter8, &(mags[th][0]) );
  }

  //------ summation over all threads --------
  for( int th=1; th<n_threads; th++) {
    for( int i=0; i<16; i++) mags[0][i] += mags[th][i];
  }
  for( int i=0; i<16; i++) {
    result_array[i] = (8.0*static_cast<double>(mags[0][i])/static_cast<double>(LxLy)-1.0)/7.0;
  }
}  
//     //------ count by 16 bit integer -----------
//     int size = 0;
//     const __m128i mask8 = { 0x00FF00FF00FF00FFULL, 0x00FF00FF00FF00FFULL};
//     __m128i count0 = zero, count1 = zero;
//     while( !counter8[th].empty() ) {
//       count0 = _mm_add_epi16( count0, _mm_and_si128( counter8[th].front(), mask8 ) );
//       count1 = _mm_add_epi16( count1, _mm_and_si128( _mm_srli_si128(counter8[th].front(),1), mask8 ) );
//       counter8[th].pop();
//       if( size == 255 ) {
// 	counter16_0[th].push( count0 );
// 	counter16_1[th].push( count1 );
// 	count0 = zero; count1 = zero;  size = 0;
//       }
//       size++;
//     }
    
//     counter16_0[th].push( count0 );
//     counter16_1[th].push( count1 );
//     //------ count by 32 bit integer -----------
//     const __m128i mask16 = { 0x0000FFFF0000FFFFULL, 0x0000FFFF0000FFFFULL };
//     while( !counter16_0[th].empty() || !counter16_1[th].empty() ) {
//       counter32_0[th] = _mm_add_epi32( counter32_0[th], _mm_and_si128( counter16_0[th].front(), mask16) );
//       counter32_1[th] = _mm_add_epi32( counter32_1[th], _mm_and_si128( counter16_1[th].front(), mask16) );
//       counter32_2[th] = _mm_add_epi32( counter32_2[th], _mm_and_si128( _mm_srli_si128(counter16_0[th].front(),2) , mask16) );
//       counter32_3[th] = _mm_add_epi32( counter32_3[th], _mm_and_si128( _mm_srli_si128(counter16_1[th].front(),2) , mask16) );
//       counter16_0[th].pop();
//       counter16_1[th].pop();
//     }
//   }

//   //----- summation for all threads --------------
//   for( int th = 1; th<n_threads; th++) {
//     counter32_0[0] = _mm_add_epi32( counter32_0[0], counter32_0[th]);
//     counter32_1[0] = _mm_add_epi32( counter32_1[0], counter32_1[th]);
//     counter32_2[0] = _mm_add_epi32( counter32_2[0], counter32_2[th]);
//     counter32_3[0] = _mm_add_epi32( counter32_3[0], counter32_3[th]);
//   }

//   for( int i=0; i<4; i++) {
//     vInt8 temp;
//     temp.vec = counter32_0[0];
//     mag_array[0 + 4*i] = (8.0*(double)(temp.integer32[i])/LxLy-1.0)/7.0;
//     temp.vec = counter32_1[0];
//     mag_array[1 + 4*i] = (8.0*(double)(temp.integer32[i])/LxLy-1.0)/7.0;
//     temp.vec = counter32_2[0];
//     mag_array[2 + 4*i] = (8.0*(double)(temp.integer32[i])/LxLy-1.0)/7.0;
//     temp.vec = counter32_3[0];
//     mag_array[3 + 4*i] = (8.0*(double)(temp.integer32[i])/LxLy-1.0)/7.0;
//   }
    //--------------------------------------------
  //   unsigned long sum = 0;
  //   for( int i = 0; i<n_threads; i++) sum += mag[i][0];
  //   double ratio = static_cast<double>(sum)/static_cast<double>(LxLy);
  //   return (8.0*ratio-1.0)/(8.0-1.0);
//}

//------------------------------
void classSqRand8PottsSimd::CountQSpin(int32_t * result_array, uint8_t state_q) {
  const __m128i zero = {0x0000000000000000ULL,0x0000000000000000ULL};
  const __m128i one = {0x0101010101010101ULL,0x0101010101010101ULL};
  vInt8 q_s;
  for( int i=0; i<16; i++) q_s.byte[i] = state_q;
  const int spt = LxLy/n_threads;
  std::vector< std::vector<int32_t> > mags( n_threads, std::vector<int32_t>(16,0) );
#pragma omp parallel for schedule(static)
  for( int th=0; th<n_threads; th++) {
    //------ count by 8 bit integer ---------
    __m128i count = zero; //set zero
    const long offset = spt * th;
    std::queue<__m128i> counter8;
    for( long i=0; i<spt; i++) {
      count = _mm_add_epi8( count, _mm_and_si128( _mm_cmpeq_epi8( spin[i+offset].vec, q_s.vec), one) );
      if( i % 255 == 254 ) {
	counter8.push( count );
	count = zero; //set zero
      }
    }
    counter8.push( count );
    SimdCounting( counter8, &(mags[th][0]) );
  }

  //---- summation over all threads -----------
  for( int th=1; th<n_threads; th++) {
    for( int i=0; i<16; i++) mags[0][i] += mags[th][i];
  }
  for( int i=0; i<16; i++) result_array[i] = mags[0][i];
}

//------------------------------
void classSqRand8PottsSimd::OrderParameter1(double * mag_array) {
  int32_t max[16];
  CountQSpin( max, 0);
  for( int i=1; i<8; i++) {
    int32_t temp[16];
    CountQSpin( temp, i);
    for( int j=0; j<16; j++) max[j] = max[j] > temp[j] ? max[j] : temp[j];
  }
  
  for( int i=0; i<16; i++) mag_array[i] = (8.0*max[i]/static_cast<double>(LxLy)-1.0)/7.0;
}
//------------------------------
void classSqRand8PottsSimd::OrderParameter2(double *mag_array) {
  double mag_dense[8][16];
  for( int i=0; i<8; i++) {
    int32_t temp[16];
    CountQSpin( temp, i);
    for( int j=0; j<16; j++) mag_dense[i][j] = static_cast<double>(temp[j])/static_cast<double>(LxLy);
  }

  for( int sample=0; sample<16; sample++) {
    double spin_direction[7];
    double base = 1.0;
    double rest = 1.0;
    for( int direc=0; direc < 8-1; direc++) {
      spin_direction[direc] = base * mag_dense[direc][sample];
      rest -= mag_dense[direc][sample];
      double temp = 8.0-1.0-direc;
      double sub = base / temp;
      spin_direction[direc] -= rest * sub;
      base *= sqrt( 1.0 - (1.0/(temp*temp)) );
    }
    double order = 0.0;
    for( int i=0; i<8-1; i++) order += spin_direction[i] * spin_direction[i];
    mag_array[sample] = sqrt(order);
  }
}
  
// //   double spin_direction[7];
// //   spin_direction[0] = mag_dense[0]*8.0/7.0 - 1.0/7.0;
// //   static const double d1 = sqrt(48.0/49.0);
// //   double temp = mag_dense[0] + mag_dense[1];
// //   spin_direction[1] = mag_dense[1]*d1 + (1.0-temp) * (-d1 / 6.0);
// //   static const double d2 = sqrt(20.0/21.0);
// //   temp += mag_dense[2];
// //   spin_direction[2] = mag_dense[2]*d2 + (1.0-temp) * (-d2 / 5.0);
// //   static const double d3 = sqrt(32.0/35.0);
// //   temp += mag_dense[3];
// //   spin_direction[3] = mag_dense[3]*d3 + (1.0-temp) * (-d3 / 4.0);
// //   static const double d4 = sqrt(6.0/7.0);
// //   temp += mag_dense[4];
// //   spin_direction[4] = mag_dense[4]*d4 + (1.0-temp) * (-d4 / 3.0);
// //   static const double d5 = sqrt(16.0/21.0);
// //   temp += mag_dense[5];
// //   spin_direction[5] = mag_dense[5]*d5 + (1.0-temp) * (-d5 / 2.0);
// //   static const double d6 = sqrt(5.0/7.0);
// //   spin_direction[6] = mag_dense[6]*d6 - mag_dense[7]*d6;

// //   double order = 0.0;
// //   for( int i=0; i<7; i++) order += spin_direction[i] * spin_direction[i];
// //   return sqrt(order);

// }
//------------------------------
void classSqRand8PottsSimd::Energy(int32_t * ene_array) {
  const __m128i zero = {0x0000000000000000ULL,0x0000000000000000ULL};
  const __m128i one = {0x0101010101010101ULL,0x0101010101010101ULL};
  const long spt = LxLy/n_threads;
  std::vector< std::vector<int32_t> > enes( n_threads, std::vector<int32_t>(16, 0) );

#pragma omp parallel for schedule(static)
  for( int th=0; th<n_threads; th++) {
    //------ count by 8 bit integer ---------
    __m128i count_n = zero; //set zero
    __m128i count_r = zero; //set zero
    std::queue<__m128i> counter8_nbond;
    std::queue<__m128i> counter8_rbond;
    const long offset = spt * th;
    for( long i=0; i<spt; i++) {
      __m128i same_state_x = _mm_and_si128( _mm_cmpeq_epi8( spin[i+offset].vec, spin[i+offset+1].vec ), one);
      __m128i n_or_r_x = _mm_cmpeq_epi8( xBond[i+offset].vec, one );
      count_n = _mm_add_epi8( count_n, _mm_and_si128(n_or_r_x,same_state_x) );
      count_r = _mm_add_epi8( count_r, _mm_andnot_si128(n_or_r_x,same_state_x) );
      __m128i same_state_y = _mm_and_si128( _mm_cmpeq_epi8( spin[i+offset].vec, spin[i+offset+Lx].vec ), one);
      __m128i n_or_r_y = _mm_cmpeq_epi8( yBond[i+offset].vec, one );
      count_n = _mm_add_epi8( count_n, _mm_and_si128(n_or_r_y,same_state_y) );
      count_r = _mm_add_epi8( count_r, _mm_andnot_si128(n_or_r_y,same_state_y) );
      if( i % 127 == 126 ) {
	counter8_nbond.push( count_n );
	counter8_rbond.push( count_r );
	count_n = zero; count_r = zero;
      }
    }
    counter8_nbond.push( count_n );
    counter8_rbond.push( count_r );

    int32_t num_bonds[16];
    SimdCounting( counter8_nbond, num_bonds );
    for( int i=0; i<16; i++) enes[th][i] = -num_bonds[i];
    SimdCounting( counter8_rbond, num_bonds );
    for( int i=0; i<16; i++) enes[th][i] -= num_bonds[i]*bondRatio;
  }

  //--- take sum over threads --------
  for( int i=0; i<16; i++) ene_array[i] = enes[0][i];
  for( int th=1; th<n_threads; th++) {
    for( int i=0; i<16; i++) ene_array[i] += enes[th][i];
  }
}
//--------------------------------------------
void classSqRand8PottsSimd::SimdCounting( std::queue<__m128i> & counter8,
					  int32_t * result_array ) {
  std::queue<__m128i> counter16_0; // contain 0,2,4,6,8,10,12,14'th data
  std::queue<__m128i> counter16_1; // contain 1,3,5,7,9,11,13,15'th data
  CountBy16bitIntegers( counter8, counter16_0, counter16_1);
  vInt8 counter32_0, counter32_1, counter32_2, counter32_3;
  // contain 0,4,8,12'th data, 1,5,9,13'th data, 2,6,10,14'th data, 3,7,11,15'th data
  CountBy32bitIntegers( counter16_0, counter16_1, counter32_0.vec, counter32_1.vec, counter32_2.vec, counter32_3.vec);
  for( int i=0; i<4; i++) {
    result_array[0+4*i] = counter32_0.integer32[i];
    result_array[1+4*i] = counter32_1.integer32[i];
    result_array[2+4*i] = counter32_2.integer32[i];
    result_array[3+4*i] = counter32_3.integer32[i];
  }
}

//--------------------------------------------
void classSqRand8PottsSimd::CountBy16bitIntegers( std::queue<__m128i>& counter8,
						  std::queue<__m128i>& counter16_0,
						  std::queue<__m128i>& counter16_1 ) {
  int size = 0;
  const __m128i mask8 = { 0x00FF00FF00FF00FFULL, 0x00FF00FF00FF00FFULL};
  const __m128i zero = { 0ULL, 0ULL};
  __m128i count0 = zero, count1 = zero;
  while( !counter8.empty() ) {
    count0 = _mm_add_epi16( count0, _mm_and_si128( counter8.front(), mask8 ) );
    count1 = _mm_add_epi16( count1, _mm_and_si128( _mm_srli_si128(counter8.front(),1), mask8 ) );
    counter8.pop();
    if( size == 255 ) {
      counter16_0.push( count0 );
      counter16_1.push( count1 );
      count0 = zero; count1 = zero;  size = 0;
    }
    size++;
  }
  counter16_0.push( count0 );
  counter16_1.push( count1 );
}
//-----------------------------------------------
void classSqRand8PottsSimd::CountBy32bitIntegers( std::queue<__m128i>& counter16_0,
						  std::queue<__m128i>& counter16_1,
						  __m128i& counter32_0,
						  __m128i& counter32_1,
						  __m128i& counter32_2,
						  __m128i& counter32_3) {
  const __m128i zero = {0ULL, 0ULL};
  counter32_0 = counter32_1 = counter32_2 = counter32_3 = zero;
  const __m128i mask16 = { 0x0000FFFF0000FFFFULL, 0x0000FFFF0000FFFFULL };
  while( !counter16_0.empty() || !counter16_1.empty() ) {
    counter32_0 = _mm_add_epi32( counter32_0, _mm_and_si128( counter16_0.front(), mask16) );
    counter32_1 = _mm_add_epi32( counter32_1, _mm_and_si128( counter16_1.front(), mask16) );
    counter32_2 = _mm_add_epi32( counter32_2, _mm_and_si128( _mm_srli_si128(counter16_0.front(),2) , mask16) );
    counter32_3 = _mm_add_epi32( counter32_3, _mm_and_si128( _mm_srli_si128(counter16_1.front(),2) , mask16) );
    counter16_0.pop();
    counter16_1.pop();
  }
}
//--------------------------------------------
long classSqRand8PottsSimd::Energy(int sample) {
  int spt = LxLy/n_threads;
  std::vector<long> enes(n_threads,0);
#pragma omp parallel for schedule(static)
  for( int th=0; th<n_threads; th++) {
    const long offset = spt*th;
    if( bondRatio <= 8 && bondRatio >= -8 ) {
      for( long i=0; i<spt; i++) {
	enes[th] -= spin[i+offset].byte[sample]==spin[i+offset+1].byte[sample] ? xBond[i+offset].byte[sample] : 0;
	enes[th] -= spin[i+offset].byte[sample]==spin[i+offset+Lx].byte[sample] ? yBond[i+offset].byte[sample] : 0;
      }
    }
    else {
      for( long i=0; i<spt; i++) {
	enes[th] -= spin[i+offset].byte[sample]==spin[i+offset+1].byte[sample] ? (xBond[i+offset].byte[sample]==1 ? 1 : bondRatio ) : 0;
	enes[th] -= spin[i+offset].byte[sample]==spin[i+offset+Lx].byte[sample] ? (yBond[i+offset].byte[sample]==1 ? 1 : bondRatio ) : 0;
      }
    }
  }

  long total_energy = 0;
  for( int th=0; th<n_threads; th++) total_energy += enes[th];
  return total_energy;
}
//------------------------------
void classSqRand8PottsSimd::ConfigurationOutput(int sample) {
  for( long j=1; j<Ly+1; j++) {
    for( long i=0; i<Lx; i++) {
      std::cout << static_cast<int16_t>(spin[i+Lx*j].byte[sample]);
    }
    std::cout << std::endl;
  }
}

//------------------------------
void classSqRand8PottsSimd::BondConfigurationOutput(int sample) {
  for( long j=0; j<Ly; j++) {
    for( long i=0; i<Lx; i++) {
      if( xBond[i+Lx*j].byte[sample] ) {
	if( yBond[i+Lx*j].byte[sample] ) std::cout << '+';
	else std::cout << '-';
      }
      else {
	if( yBond[i+Lx*j].byte[sample] ) std::cout << '|';
	else std::cout << ' ';
      }
    }
    std::cout << std::endl;
  }
}
