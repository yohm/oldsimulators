#include "classBfTableSqRandQPottsSimd.hpp"

//---------------------------------
classBfTableSqRandQPottsSimd::classBfTableSqRandQPottsSimd() {
  tableSizePower = 0;
  tableSize = 0;
  bfSeed = 0;
  k = 0;
}
//----------------------------------
classBfTableSqRandQPottsSimd::~classBfTableSqRandQPottsSimd() {
  table.clear();
}
//----------------------------------
void classBfTableSqRandQPottsSimd::Initialize(int pow, double K, int t_bondRatio, unsigned long long bf_seed) {
  tableSizePower = pow;
  tableSize = 1 << tableSizePower;
  table.resize( tableSize );
	       
  k = K;
  bfSeed = bf_seed;
  bondRatio = t_bondRatio;
  
  SetNumber();
  ShuffleTable();
}
//----------------------------------------------------------
void classBfTableSqRandQPottsSimd::SetNumber() {

  std::vector<int32_t> numbers(32,0);
  if( bondRatio <= 8 ) {
    //    numbers.assign(4*bondRatio,0); //r=8
    for( int i=0; i<4*bondRatio; i++) numbers[i] = static_cast<uint32_t>( exp(-(i+1)*k) * tableSize );
  } else {
    //    numbers.assign(32,0);
    for( int i=0; i<4; i++) numbers[i] = static_cast<uint32_t>( exp(-(i+1)*k) * tableSize );
    for( int i=4; i<12; i++) numbers[i] = static_cast<uint32_t>( exp(-(bondRatio+i-7)*k) * tableSize );
    for( int i=12; i<20; i++) numbers[i] = static_cast<uint32_t>( exp(-(2*bondRatio+i-15)*k) * tableSize );
    for( int i=20; i<28; i++) numbers[i] = static_cast<uint32_t>( exp(-(3*bondRatio+i-23)*k) * tableSize );
    for( int i=28; i<32; i++) numbers[i] = static_cast<uint32_t>( exp(-(4*bondRatio+i-31)*k) * tableSize );
  }

  for( int32_t i=0; i<numbers[31]; i++) {
    for( int j=0; j<16; j++) table[i].byte[j] = 32 + 1;
  }
  for( int32_t j=31; j>0; j--) { //Set j
    for( long i=numbers[j]; i<numbers[j-1]; i++) {
      for( int k=0; k<16; k++) table[i].byte[k] = j + 1;
    }
  }
  for( int32_t i=numbers[0]; i<tableSize; i++) {
    for( int j=0; j<16; j++) table[i].byte[j] = 0 + 1; //念のため
  }
}
//----------------------------------------------------------
void classBfTableSqRandQPottsSimd::ShuffleTable() {
#pragma omp parallel
  {
    int th = omp_get_thread_num();
    unsigned long long int seeds[2] = {bfSeed,th};
    mtrand64 rnd( seeds, 2);
#pragma omp for schedule(static)
    for( int b = 0; b<16; b++) {
      for( long i=0; i<tableSize; i++) {
	uint32_t random = rnd.genrand64_bit(tableSizePower);
	int8_t temp = table[i].byte[b];
	table[i].byte[b] = table[random].byte[b];
	table[random].byte[b] = temp;
      }
    }
  }
}

//   for( int i=0; i<64; i++) {
//     for(int j=0; j<tableSize; j++) {
//       long random = rnd.genrand64_bit(tableSizePower);
//       //0 ~ tableSize-1 :random number
//       Exchange( i, 3*j, 3*random);
//       Exchange( i, 3*j+1, 3*random+1);
//       Exchange( i, 3*j+2, 3*random+2);
//     }
//   }
//}
//----------------------------------------------------------
// void classBfTableSqRandQPottsSimd::Exchange( int i, int j, int k) {
//   long long mask = 1LL;
//   mask <<= i; //i-bit目のマスク
//   long long temp1 = table[j] & mask; //table[j] の i-bit目
//   long long temp2 = table[k] & mask; //table[k] の i-bit
  
//   table[j] = table[j] & (~mask); //i-bit目以外は変更しない
//   table[k] = table[k] & (~mask); //i-bit目は0にする

//   table[j] = table[j] | temp2; //jにkのi-bitを入れる
//   table[k] = table[k] | temp1; //kにjのi-bitを入れる
// }
//---------------------------------------------------------
#ifdef DEBUG
// void classBfTableSqRandQPottsSimd::TableOutputBit() {
//   for( int j=0; j<totalTableSize; j++) {
//     long long mask = 1LL;
//     for( int i=63; i>=0; i--) {
//       mask = (1LL << i);
//       if( (table[j] & mask)==0 ) cout << 0;
//       else cout << 1;
//     }
//     cout << endl;
//   }
// }
//--------------------------------------------------------
void classBfTableSqRandQPottsSimd::TableOutputNumber(int i) {
  for( long i = 0; i<tableSize; i++) {
    for( int b=0; b<16 ; b++) {
	std::cerr << static_cast<int32_t>(table[i].byte[b]) << ' ';
	 }
    std::cerr << std::endl;
  }
}
//   for( int j=0; j<tableSize; j++) {
//     long long mask = 1LL;
//     for( int i=63; i>=0; i--) {
//       mask = (1LL << i);
//       short temp = 0;
//       if( table[3*j] & mask) temp += 4;
//       if( table[3*j+1] & mask) temp += 2;
//       if( table[3*j+2] & mask) temp += 1;
//       cout << temp;
//     }
//     cout << endl;
//   }
// }
#endif
