#include <iostream>
#include <cmath>
#include <vector>
#include <boost/cstdint.hpp>
#include <omp.h>
#include "sse.hpp"
#include "mtrand64.hpp"

#ifndef _BFTABLE_SQ_RANDOM_QPOTTS_SIMD_H_
#define _BFTABLE_SQ_RANDOM_QPOTTS_SIMD_H_

#define DEBUG

class classBfTableSqRandQPottsSimd {
  //16 sample must have the same temperature
  //tableにはそれぞれのテーブルの要素を交互に並べてく。
public:
  classBfTableSqRandQPottsSimd();
  ~classBfTableSqRandQPottsSimd();
  void Initialize( int pow, double K, int bondRatio, uint64_t seed);
  //Initialize and allocate memory.If pow,K,seed are unchanged, this routine don't remake table.
  int TableSizePower() {return tableSizePower;}
  inline vInt8 * startAddress() { return &(table[0]); }
  inline vInt8 GetTableElement( uint32_t i ) { return table[i]; }
  std::vector<vInt8> table;
#ifdef DEBUG
  //  void TableOutputBit();//Output table bit
  void TableOutputNumber(int i);
#endif //DEBUG
protected:
  int bondRatio;
  uint32_t tableSizePower;//tableSize = 2^(tableSizePower)
  int32_t tableSize;
  uint64_t bfSeed;
  double k;
  void SetNumber();
  void ShuffleTable();
  //  void Exchange(int i,int j,int k);//i-bit目のtable[j]とtable[random]の値を入れ替える
};

#endif
