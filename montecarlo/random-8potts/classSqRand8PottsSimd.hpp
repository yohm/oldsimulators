#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <string>
#include <queue>
#include <boost/cstdint.hpp>
#include <boost/random.hpp>
#include <boost/integer/integer_mask.hpp>
#include <omp.h>
#include "sse.hpp"
#include "classBfTableSqRandQPottsSimd.hpp"
//#include "mtrand64.hpp"
#include "RandomLib/Random.hpp"

#ifndef SQ_RAND_8_POTTS_SIMD_H_
#define SQ_RAND_8_POTTS_SIMD_H_


class classSqRand8PottsSimd {
public:
  classSqRand8PottsSimd();
  ~classSqRand8PottsSimd();
  void Run_mag(long tmax);// iterate { update and output of averaged_mag0. }
  void Update();
  void OrderParameter0( double * mag_array); // fraction of spins in state "0"
  double OrderParameter0( int j); // fraction of 0 spins in j'th sample
  void OrderParameter1(double *mag_array); // fraction of spins which takes the maximally occupied state
  void OrderParameter2(double *mag_array); // each spin state is assigned to q symmetric vectors in the q-1 dimensional hypertetrahedron
  void Energy(int32_t * ene_array); // energy in j'th sample
  long Energy(int j); // energy in j'th sample
  void Initialize(long lx, long ly, double p, int bondRatio, double k,
		  uint64_t bond_seed,  uint64_t update_seed, uint16_t t_tableBitWidth );
  void SetOrderedState(); //all 0 state
  void SetDisorderedState(); //random configuration

  void Serialize( std::string binfilename); //output binary configuration to the file.
  void ConfigurationOutput( int i);//output configuration in text format. (for debug)
  void BondConfigurationOutput( int i); //output bond configuration
private:
  long Lx,Ly,LxLy,numAllocation;
  double k;//inverse temperature
  int8_t bondRatio;
  double p; //probability of the strong bonds, strength of the strong bonds
  unsigned long long seedOfBond, seedOfUpdate;
  uint16_t tableBitWidth;
  std::vector< vInt8 > spin; //spin states
  std::vector< vInt8 > xBond, yBond; //bond=rJ:true, bond=J:false
  std::vector< vInt8 > next_state; //randomly chosen next state
  std::vector< uint32_t > randoms; //table of random numbers
  uint32_t mask;
  classBfTableSqRandQPottsSimd table;
  vInt8 * table_start_address;
  std::vector<RandomLib::Random> rnd;
  //  mtrand64 * rnd;
  int n_threads; //number of OpenMP threads
  //  std::vector< std::vector<uint32_t> > mag; //two-dim array which stores magnetization
  void MemoryAllocation();
  void PrepareRandomNumbers();
  inline void UpdateSub( const long & i);
  //  inline uint8_t getIR();
  inline void CopySub();
  void CountQSpin( int32_t * result_array, uint8_t state_q);
  // number of spins in state "state_q", size of result_array must be 16.
  void SimdCounting( std::queue<__m128i>& counter8, int32_t* result_array);
  //second argument must be a pointer of array whose size is 16.
  void CountBy16bitIntegers( std::queue<__m128i>& counter8,
			     std::queue<__m128i>& counter16_0,
			     std::queue<__m128i>& counter16_1);
  void CountBy32bitIntegers( std::queue<__m128i>& counter16_0,
			     std::queue<__m128i>& counter16_1,
			     __m128i& counter32_0,
			     __m128i& counter32_1,
			     __m128i& counter32_2,
			     __m128i& counter32_3);
};
#endif
