#include <boost/cstdint.hpp>
#include <xmmintrin.h> //SSE
#include <emmintrin.h> //SSE2
// #include <pmmintrin.h> //SSE3

#ifndef _SSE_HPP_
#define _SSE_HPP_

// vector of byte
union vInt8 {
  __m128i vec;
  uint8_t byte[16];
  uint16_t integer16[8];
  uint32_t integer32[4];
  uint64_t integer64[2];
};

#endif
