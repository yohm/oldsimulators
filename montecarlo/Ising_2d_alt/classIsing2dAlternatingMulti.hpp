#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"
#include "classBfTableIsing2dAlt.hpp"
#include "vcpp2gcc.h"
#include "Bitwise_multispincoding.h"
using namespace std;

#ifndef ISING2D_ALT_MULTI_H_
#define ISING2D_ALT_MULTI_H_

class classIsing2dAlternatingMulti{
public:
  classIsing2dAlternatingMulti();
  ~classIsing2dAlternatingMulti();
  void Update();
  double OrderParameter();
  double Energy();
  void Initialize(long lx, long ly, double specK, double jprime, unsigned long long sed,
		  int ran_width,unsigned long long bf_seed);
  void Initialize(long lx, long ly, double specK, double jprime, unsigned long long sed,
		  int ran_width) {
    Initialize( lx, ly, specK, jprime, sed, ran_width, sed+12345ULL);
  }
  void InitialConfiguration();//set all 0 config
  void ConfigurationOutput();
private:
  long Lx,Ly,LxLy,numAllocation;
  double K,KprimeOverK;
  unsigned long long seedOfRandomNumber,seedOfBfTable;
  int randomBitWidth;
  long long * spinA, *spinB,*spinC,*spinD;
  mtrand64* rnd;
  double prob[9];
  classBfTableIsing2dAlt bfTab;
  Bitwise_multispincoding bwmsc;
  long long IX4,IX2,IX1;
  void Plus(const long long& a,const long long& b,const long long& c, long long &d, long long& e);
  void Plus(const long long& a,const long long& b,const long long& c, long long &d);
  void MemoryAllocation(long lx, long ly, double k, double jprime, unsigned long long sed,
			int ran_width, unsigned long long bf_seed);
  void UpdateSub(long long*us, long long*s1, long long*s2, long long*w1, long long*w2);
};

#endif
