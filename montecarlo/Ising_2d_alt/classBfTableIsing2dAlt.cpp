#include "classBfTableIsing2dAlt.hpp"
using namespace std;

//---------------------------------
classBfTableIsing2dAlt::classBfTableIsing2dAlt() {
  tableSizePower = 0;
  tableSize = totalTableSize = 0;
  bfSeed = 0;
  inverseTemperature = 0;
  table = NULL;
}
//----------------------------------
classBfTableIsing2dAlt::~classBfTableIsing2dAlt() {
  delete [] table;
}
//----------------------------------
void classBfTableIsing2dAlt::Initialize(int pow, double K, double jprime, unsigned long long bf_seed) {
  if( tableSizePower == pow && inverseTemperature == K && jPrime == jprime && bfSeed == bf_seed) {return;}

  if( pow != tableSizePower) { //領域のとりなおし
    tableSizePower = pow;
    tableSize = 1 << tableSizePower;
    totalTableSize = tableSize * 3;
    delete [] table;
    table = new long long[ totalTableSize];
  }

  for( long i=0; i<totalTableSize; i++) table[i] = 0;

  if( jprime >= 0.5) {
    cerr << "Error! in classBfTableIsing2dAlt. jPrime must be larger than 0.5."<<endl;
    exit(1);
  }
  inverseTemperature = K;
  jPrime = jprime;
  bfSeed = bf_seed;
  MakeRandomTable( inverseTemperature, jPrime, bfSeed);
}
//---------------------------------------------------------
void classBfTableIsing2dAlt::MakeRandomTable(double K, double jprime, unsigned long long seed) {
  SetBit( K, jprime);
  ShuffleTable( seed);
}
//----------------------------------------------------------
void classBfTableIsing2dAlt::SetBit(double K,double jprime) {
  long Num[5]; //Num[i]:{exp(-K*i)}*tableSize;
  Num[4] = (long) (exp(-4.0*(1+jprime)*K) * tableSize);
  Num[3] = (long) (exp(-4.0*K) * tableSize);
  Num[2] = (long) (exp(-4.0*(1-jprime)*K) * tableSize);
  Num[1] = (long) (exp(-4.0*jprime*K) * tableSize);
  Num[0] = tableSize;

  for( long j=0; j<Num[4]; j++) { //j番目の要素
    table[3*j] = -1;
    table[3*j+1] = 0;
    table[3*j+2] = 0; //Set 4
  }
  for( long j=Num[4]; j<Num[3]; j++) {
    table[3*j] = 0;
    table[3*j+1] = -1;
    table[3*j+2] = -1; //Set 3
  }
  for( long j=Num[3]; j<Num[2]; j++) {
    table[3*j] = 0;
    table[3*j+1] = -1;
    table[3*j+2] = 0; //Set 2
  }
  for( long j=Num[2]; j<Num[1]; j++) {
    table[3*j] = 0;
    table[3*j+1] = 0;
    table[3*j+2] = -1; //Set 1
  }
  for( long j=Num[1]; j<Num[0]; j++) {
    table[3*j] = 0;
    table[3*j+1] = 0;
    table[3*j+2] = 0; //Set 0;
  }
}
//----------------------------------------------------------
void classBfTableIsing2dAlt::ShuffleTable(unsigned long long seed) {
  mtrand64 rnd( seed);

  for( int i=0; i<64; i++) {
    for(int j=0; j<tableSize; j++) {
      long random = rnd.genrand64_bit(tableSizePower);
      //0 ~ tableSize-1 :random number
      Exchange( i, 3*j, 3*random);
      Exchange( i, 3*j+1, 3*random+1);
      Exchange( i, 3*j+2, 3*random+2);
    }
  }
}
//----------------------------------------------------------
void classBfTableIsing2dAlt::Exchange( int i, int j, int k) {
  long long mask = 1LL;
  mask <<= i; //i-bit目のマスク
  long long temp1 = table[j] & mask; //table[j] の i-bit目
  long long temp2 = table[k] & mask; //table[k] の i-bit
  
  table[j] = table[j] & (~mask); //i-bit目以外は変更しない
  table[k] = table[k] & (~mask); //i-bit目は0にする

  table[j] = table[j] | temp2; //jにkのi-bitを入れる
  table[k] = table[k] | temp1; //kにjのi-bitを入れる
}
//---------------------------------------------------------
#ifdef DEBUG
void classBfTableIsing2dAlt::TableOutputBit() {
  for( int j=0; j<totalTableSize; j++) {
    long long mask = 1LL;
    for( int i=63; i>=0; i--) {
      mask = (1LL << i);
      if( (table[j] & mask)==0 ) cout << 0;
      else cout << 1;
    }
    cout << endl;
  }
}
//--------------------------------------------------------
void classBfTableIsing2dAlt::TableOutputNumber() {
  for( int j=0; j<tableSize; j++) {
    long long mask = 1LL;
    for( int i=63; i>=0; i--) {
      mask = (1LL << i);
      short temp = 0;
      if( table[3*j] & mask) temp += 4;
      if( table[3*j+1] & mask) temp += 2;
      if( table[3*j+2] & mask) temp += 1;
      cout << temp;
    }
    cout << endl;
  }
}
#endif
