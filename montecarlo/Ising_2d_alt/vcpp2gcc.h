#ifndef VCPP2GCC2_H
#define VCPP2GCC2_H

#define _time64 time64
#define __int64 int64type
#define __time64_t time64type
#define _getcwd getcwd
#define _mkdir mkdir
#define _chdir chdir
#define _strdate strdate
#define _tmain main //main関数の記述の変更

#include <unistd.h> //posix関数を使うための記述
#include <time.h>
#include <stdlib.h>
#include <string.h>

//Linux 環境で動作させるための変更を記述する
typedef signed long long __int64;
typedef time_t __time64_t;
typedef char _TCHAR;

extern __time64_t _time64(__time64_t *timer);
extern int _mkdir( const char* dirname);
extern char* _strdate(char* datestr);

#endif
