#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include "mtrand64.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTableIsing2dAlt.cpp"
#include "classIsing2dAlternatingMulti.cpp"

using namespace std;

#define BENCH

//input the arguments as follows
// program L k Tmax jprime seed bitlength data_id
// ./a.out 300 1.005 0.4 128 23419721 16 00002
int main(int argc, char* argv[]) {
  if(argc!=8) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <L> <k> <jprime> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }

  //  clock_t start,end;
  long l=atoi( argv[1]);
  double k = atof( argv[2]);
  double jprime = atof( argv[3]);
  long tmax = atoi( argv[4]);
  unsigned long long sed = atol( argv[5]);
  int bitwt = atoi( argv[6]);
  unsigned long long bf_sed = sed + 12345ULL;
  char str[20];
  strcpy( str,argv[7]);
  strcat( str, ".NERo");

  classIsing2dAlternatingMulti sim;
  sim.Initialize( l, l, k, jprime, sed, bitwt, bf_sed);

  ofstream fout(str);

#ifdef BENCH
  clock_t start = clock();
#endif
  
  for( int t=0; t<tmax; t++) {
    fout << t << " " << sim.OrderParameter() <<endl;
    sim.Update();
  }
  
#ifdef BENCH
  clock_t end = clock();
  cerr << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC)<<endl;
#endif
  
  fout.close();
  return 0;
}
