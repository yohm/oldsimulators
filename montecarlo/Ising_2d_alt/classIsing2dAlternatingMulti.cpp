#include "classIsing2dAlternatingMulti.hpp"

//------------------------
classIsing2dAlternatingMulti::classIsing2dAlternatingMulti() {
  //適当な初期値を設定
  Lx=1;Ly=0; K=0.1; KprimeOverK = 0.1;
  numAllocation = 0;
  spinA = spinB = spinC = spinD = NULL;
  rnd = NULL;
  seedOfRandomNumber = seedOfBfTable = 0;
  randomBitWidth = 0;
}

//------------------------
classIsing2dAlternatingMulti::~classIsing2dAlternatingMulti() {
  delete [] spinA;
  delete [] spinB;
  delete [] spinC;
  delete [] spinD;
  delete [] rnd;
}

//------------------------
void classIsing2dAlternatingMulti::Initialize(long lx, long ly, double k,double jprime,
						unsigned long long sed, int ran_width,
						unsigned long long bf_seed) {
  MemoryAllocation(lx,ly,k,jprime,sed,ran_width,bf_seed);
  InitialConfiguration();
}

//------------------------
void classIsing2dAlternatingMulti::MemoryAllocation(long lx, long ly, double k,double kprime,
						      unsigned long long sed, int ran_width,
						      unsigned long long bf_seed) {
  long newAllocation = lx*(ly+2); //2列分コピー領域
  if( newAllocation != numAllocation) {
    delete [] spinA; delete [] spinB; delete [] spinC; delete [] spinD;
    spinA = new long long [newAllocation];
    spinB = new long long [newAllocation];
    spinC = new long long [newAllocation];
    spinD = new long long [newAllocation];
  }
  Lx = lx; Ly = ly; LxLy = Lx*Ly; numAllocation = newAllocation;
  delete rnd;
  rnd = new mtrand64(sed);
  seedOfRandomNumber = sed;
  randomBitWidth = ran_width;
  seedOfBfTable = bf_seed;
  if( kprime > 0.5) {
    cerr << "Error! in classIsing2dAlternatingMulti."<<endl
	 << "kprime must be smaller than 0.5." << endl;
    exit(1);
  }
  K = k;
  KprimeOverK = kprime;
  bfTab.Initialize(randomBitWidth,K,KprimeOverK,seedOfBfTable);
  bwmsc.Bitwise_multispincoding_Initialization(LxLy);
}
//   prob[0] = exp(-K*(4.0+4.0*KprimeOverK));
//   prob[1] = exp(-K*4.0);
//   prob[2] = exp(-K*(4.0-4.0*KprimeOverK));
//   prob[3] = exp(-K*KprimeOverK*4);
//   for( int i=4; i<9; i++) prob[i] = 1.0;

//-----------------------------
void classIsing2dAlternatingMulti::InitialConfiguration() {
  //set all up configuration
  for( long i=0; i<numAllocation; i++) {
    spinA[i] = spinB[i] = spinC[i] = spinD[i] = -1ULL;
  }
}
//-----------------------------
void classIsing2dAlternatingMulti::Update() {
  //update spinA
  UpdateSub( &spinA[Lx], &spinB[Lx], &spinC[Lx], &spinB[Lx-1], &spinC[0] );
  //update spinD
  UpdateSub( &spinD[Lx], &spinB[Lx], &spinC[Lx], &spinB[2*Lx], &spinC[Lx+1] );
  //copy spin
  for( long i=0; i<Lx; i++) {
    spinA[i] = spinA[i+LxLy];
    spinD[i] = spinD[i+LxLy];
    spinA[i+Lx*(Ly+1)] = spinA[i+Lx];
    spinD[i+Lx*(Ly+1)] = spinA[i+Lx];
  }
  //update spinB
  UpdateSub( &spinB[Lx], &spinA[Lx], &spinD[Lx], &spinA[Lx+1], &spinD[0] );
  //update spinC
  UpdateSub( &spinC[Lx], &spinA[Lx], &spinD[Lx], &spinA[2*Lx], &spinD[Lx-1] );
  //copy spin
  for( long i=0; i<Lx; i++) {
    spinB[i] = spinB[i+LxLy];
    spinB[i+Lx*(Ly+1)] = spinB[i+Lx];
    spinC[i] = spinC[i+LxLy];
    spinC[i+Lx*(Ly+1)] = spinC[i+Lx];
  }
}
//-----------------------------
void classIsing2dAlternatingMulti::UpdateSub(long long*us, long long*s1, long long*s2, long long*w1, long long*w2) {
  for( long i=0; i<LxLy; i++) {
    long ran = rnd->genrand64_bit(randomBitWidth);
    IX4 = bfTab.GetTableElement(3*ran);
    IX2 = bfTab.GetTableElement(3*ran+1);
    IX1 = bfTab.GetTableElement(3*ran+2);

    long long a1,a2;
    a1 = a2 = *us^*s1;
    long long b1,b2;
    b1 = b2 = *us^*s2;
    long long c1 = *us^*w1;
    long long d1 = *us^*w2;
    long long e2,e1;
    Plus(IX1,a1,b1,e2,e1);
    long long f2;
    Plus(e1,c1,d1,f2);
    long long g4,g2;
    Plus(IX2,a2,b2,g4,g2);
    long long h4;
    Plus(g2,e2,f2,h4);
    long long ID = IX4 | g4 | h4;
    *us = ID ^ *us;

    us++;    s1++;    s2++;    w1++;   w2++;
  }
}

//------------------------------
inline void classIsing2dAlternatingMulti::Plus(const long long& a,const long long& b,const long long& c,
					       long long &d, long long& e){
  //<d,e> = a+b+c
  e = a ^ b;
  d = (a&b) | (e&c); // second order
  e ^= c; // e+c; first order
}
inline void classIsing2dAlternatingMulti::Plus(const long long& a, const long long& b, const long long& c,
					       long long & d) {
  //<d,*> = a+b+c;
  d = (a&b) | ( (b^a)&c); //second order
}

//-----------------------------
double classIsing2dAlternatingMulti::OrderParameter() {
  long imagA[64],imagB[64],imagC[64],imagD[64];
  bwmsc.NIbitcount( spinA,LxLy, imagA);
  bwmsc.NIbitcount( spinB,LxLy, imagB);
  bwmsc.NIbitcount( spinC,LxLy, imagC);
  bwmsc.NIbitcount( spinD,LxLy, imagD);
  double total = 0;
  for( int i=0; i<64; i++) {
    total +=(double)(imagA[i]+imagB[i]+imagC[i]+imagD[i])/(2*LxLy)-1.0;
  }
  return total/64.0;
}
//-----------------------------
void classIsing2dAlternatingMulti::ConfigurationOutput() {
  for( long j=1; j<Ly+1; j++) {
    for( long i=0; i<Lx; i++) {
      cout << spinA[j*Lx+i];
    }
    cout << endl;
  }
}
