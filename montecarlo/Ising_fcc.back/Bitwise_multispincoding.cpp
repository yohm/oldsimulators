#include "Bitwise_multispincoding.h"
#include <stdio.h>
Bitwise_multispincoding::Bitwise_multispincoding(void)
{
	long i;
	for( i = 0; i<2; i++) I2S[i] = NULL;
	for( i = 0; i<4; i++) I4S[i] = NULL;
	for( i = 0; i<8; i++) I8S[i] = NULL;
	I32S = NULL;
	Nprev = 0;
}

Bitwise_multispincoding::~Bitwise_multispincoding(void)
{
	long i;
	delete[] I32S;
	for( i = 0; i<2; i++) delete[] I2S[i];
	for( i = 0; i<4; i++) delete[] I4S[i];
	for( i = 0; i<8; i++) delete[] I8S[i];
//	cout <<"ending in Bitwise_multispincoding::~Bitwise_multispincoding"<<endl;
}

void Bitwise_multispincoding::Bitwise_multispincoding_Initialization( long num){
	long i;

	if( num < Nprev ) return;

	delete[] I32S;
	for( i = 0; i<2; i++) delete[] I2S[i];
	for( i = 0; i<4; i++) delete[] I4S[i];
	for( i = 0; i<8; i++) delete[] I8S[i];

	NA=num/3+6;
    NB=NA/5+18;
    NC=NB/17+1;
    N2=NA*2+1;
    N3=N2+NB*4;
    N4=N3+NC*8;
	for(i=0;i<2;i++){
		I2S[i]=new __int64[N2];
		if(I2S[i] == NULL){
			printf("Isingvp2D has failed to allocate memory to spin. \n");
			exit(1);
		}
	}
	for(i=0;i<4;i++){
		I4S[i]=new __int64[N3];
		if(I4S[i] == NULL){
			printf("Isingvp2D has failed to allocate memory to spin. \n");
			exit(1);
		}
	}
	for(i=0;i<8;i++){
		I8S[i]=new __int64[N4];
		if(I8S[i] == NULL){
			printf("Isingvp2D has failed to allocate memory to spin. \n");
			exit(1);
		}
	}
	I32S=new __int64[64];
	Nprev = num;
}

void Bitwise_multispincoding::bitcount(__int64* array,long array_size,long* count){
	long i;
	char j;
	__int64 k;
	for(j=0;j<64;j++)count[j]=0;
	for(i=0;i<array_size;i++){
		k=array[i];
		for(j=0;j<64;j++){
			count[j]=count[j]+long(k&1LL);
			k=k>>1LL;
		}
	}
}

void Bitwise_multispincoding::NIbitcount(__int64* IDIM,long NNUM,long* ICNT)
{
	long I,IJ,ILOOP,L,J,J1,J2,J3,J4;
	long L1long,L2long,L3long,L4long,L5long,L6long,L7long,L8long;
	__int64 K1,K2,K3,K4,K5,K6,K7,K8,ISUM1,ISUM2,L1,L2,L3;
	ILOOP=NNUM/3;
	for(I=0;I<ILOOP;I++){
		IJ=I*3;
        K1=IDIM[IJ];
        K2=IDIM[IJ+1];
        K3=IDIM[IJ+2];
        ISUM1=K1&(6148914691236517205LL);
        ISUM1=ISUM1+(K2&(6148914691236517205LL));
        I2S[0][I]=ISUM1+(K3&(6148914691236517205LL));
        L1=K1>>1;
        L2=K2>>1;
        L3=K3>>1;
        ISUM2=L1&(6148914691236517205LL);
        ISUM2=ISUM2+(L2&(6148914691236517205LL));
        I2S[1][I]=ISUM2+(L3&(6148914691236517205LL));
	}
    IJ=ILOOP*3;
    K1=0;
    K2=0;
    K3=0;
    if( IJ   <NNUM)K1=IDIM[IJ];
    if((IJ+1)<NNUM)K2=IDIM[IJ+1];
    if((IJ+2)<NNUM)K3=IDIM[IJ+2];
    ISUM1=(K1&(6148914691236517205LL));
    ISUM1=ISUM1+(K2&(6148914691236517205LL));
    I2S[0][ILOOP]=ISUM1+(K3&(6148914691236517205LL));
    L1=K1>>1;
    L2=K2>>1;
    L3=K3>>1;
    ISUM2=(L1&(6148914691236517205LL));
    ISUM2=ISUM2+(L2&(6148914691236517205LL));
    I2S[1][ILOOP]=ISUM2+(L3&(6148914691236517205LL));

    I2S[0][ILOOP+1]=0;
    I2S[0][ILOOP+2]=0;
    I2S[0][ILOOP+3]=0;
    I2S[0][ILOOP+4]=0;
    I2S[0][ILOOP+5]=0;
    I2S[1][ILOOP+1]=0;
    I2S[1][ILOOP+2]=0;
    I2S[1][ILOOP+3]=0;
    I2S[1][ILOOP+4]=0;
    I2S[1][ILOOP+5]=0;

    ILOOP=ILOOP/5+1;
//*VDIR LOOPCNT=1200000
	for(L=0;L<2;L++){
        L1long=2*L;
        L2long=2*L+1;
//*VDIR LOOPCNT=1200000
		for(I=0;I<ILOOP;I++){
          IJ=I*5;
          J1=IJ+1;
          J2=IJ+2;
          J3=IJ+3;
          J4=IJ+4;
          K1=I2S[L][IJ];
          K2=I2S[L][J1];
          K3=I2S[L][J2];
          K4=I2S[L][J3];
          K5=I2S[L][J4];
          I4S[L1long][I]=(K1&(3689348814741910323LL));
          I4S[L1long][I]=I4S[L1long][I]+(K2&(3689348814741910323LL));
          I4S[L1long][I]=I4S[L1long][I]+(K3&(3689348814741910323LL));
          I4S[L1long][I]=I4S[L1long][I]+(K4&(3689348814741910323LL));
          I4S[L1long][I]=I4S[L1long][I]+(K5&(3689348814741910323LL));
          K1=K1>>2;
          K2=K2>>2;
          K3=K3>>2;
          K4=K4>>2;
          K5=K5>>2;
          I4S[L2long][I]=(K1&(3689348814741910323LL));
          I4S[L2long][I]=I4S[L2long][I]+(K2&(3689348814741910323LL));
          I4S[L2long][I]=I4S[L2long][I]+(K3&(3689348814741910323LL));
          I4S[L2long][I]=I4S[L2long][I]+(K4&(3689348814741910323LL));
          I4S[L2long][I]=I4S[L2long][I]+(K5&(3689348814741910323LL));
		}
	}

	for(I=0;I<4;I++){
		for(J=0;J<17;J++){
           I4S[I][ILOOP+J]=0;
		}
	}

    ILOOP=ILOOP/17+1;
	for(I=0;I<8;I++){
//*VDIR LOOPCNT=1200000
		for(J=0;J<ILOOP;J++){
	        I8S[I][J]=0;
		}
	}

	for(L=0;L<4;L++){
        L1long=2*L;
        L2long=2*L+1;
		for(J=0;J<17;J++){
//*VDIR LOOPCNT=1200000
			for(I=0;I<ILOOP;I++){
	            IJ=I*17+J;
	            K1=I4S[L][IJ];
	            I8S[L1long][I]=I8S[L1long][I]+(K1&(1085102592571150095LL));
	            K2=K1>>4;
	            I8S[L2long][I]=I8S[L2long][I]+(K2&(1085102592571150095LL));
			}
		}
	}
	for(L=0;L<8;L++){
        L1long=8*L;
        L2long=8*L+1;
        L3long=8*L+2;
		L4long=8*L+3;
        L5long=8*L+4;
        L6long=8*L+5;
        L7long=8*L+6;
        L8long=8*L+7;
        I32S[L1long]=0;
        I32S[L2long]=0;
        I32S[L3long]=0;
        I32S[L4long]=0;
        I32S[L5long]=0;
        I32S[L6long]=0;
        I32S[L7long]=0;
        I32S[L8long]=0;
//*VDIR LOOPCNT=1200000
		for(I=0;I<ILOOP;I++){
          K1=I8S[L][I];
		  I32S[L1long]=I32S[L1long]+(K1&255);
          K2=K1>>8;
          I32S[L2long]=I32S[L2long]+(K2&255);
          K3=K1>>16;
          I32S[L3long]=I32S[L3long]+(K3&255);
          K4=K1>>24;
          I32S[L4long]=I32S[L4long]+(K4&255);
          K5=K1>>32;
          I32S[L5long]=I32S[L5long]+(K5&255);
          K6=K1>>40;
          I32S[L6long]=I32S[L6long]+(K6&255);
          K7=K1>>48;
          I32S[L7long]=I32S[L7long]+(K7&255);
          K8=K1>>56;
          I32S[L8long]=I32S[L8long]+(K8&255);
		}

	}

      ICNT[64-64]=I32S[ 1-1];
      ICNT[64-56]=I32S[ 2-1];
      ICNT[64-48]=I32S[ 3-1];
      ICNT[64-40]=I32S[ 4-1];
      ICNT[64-32]=I32S[ 5-1];
      ICNT[64-24]=I32S[ 6-1];
      ICNT[64-16]=I32S[ 7-1];
      ICNT[64- 8]=I32S[ 8-1];
      ICNT[64-60]=I32S[ 9-1];
      ICNT[64-52]=I32S[10-1];
      ICNT[64-44]=I32S[11-1];
      ICNT[64-36]=I32S[12-1];
      ICNT[64-28]=I32S[13-1];
      ICNT[64-20]=I32S[14-1];
      ICNT[64-12]=I32S[15-1];
      ICNT[64- 4]=I32S[16-1];
      ICNT[64-62]=I32S[17-1];
      ICNT[64-54]=I32S[18-1];
      ICNT[64-46]=I32S[19-1];
      ICNT[64-38]=I32S[20-1];
      ICNT[64-30]=I32S[21-1];
      ICNT[64-22]=I32S[22-1];
      ICNT[64-14]=I32S[23-1];
      ICNT[64- 6]=I32S[24-1];
      ICNT[64-58]=I32S[25-1];
      ICNT[64-50]=I32S[26-1];
      ICNT[64-42]=I32S[27-1];
      ICNT[64-34]=I32S[28-1];
      ICNT[64-26]=I32S[29-1];
      ICNT[64-18]=I32S[30-1];
      ICNT[64-10]=I32S[31-1];
      ICNT[64- 2]=I32S[32-1];
      ICNT[64-63]=I32S[33-1];
      ICNT[64-55]=I32S[34-1];
      ICNT[64-47]=I32S[35-1];
      ICNT[64-39]=I32S[36-1];
      ICNT[64-31]=I32S[37-1];
      ICNT[64-23]=I32S[38-1];
      ICNT[64-15]=I32S[39-1];
      ICNT[64- 7]=I32S[40-1];
      ICNT[64-59]=I32S[41-1];
      ICNT[64-51]=I32S[42-1];
      ICNT[64-43]=I32S[43-1];
      ICNT[64-35]=I32S[44-1];
      ICNT[64-27]=I32S[45-1];
      ICNT[64-19]=I32S[46-1];
      ICNT[64-11]=I32S[47-1];
      ICNT[64- 3]=I32S[48-1];
      ICNT[64-61]=I32S[49-1];
      ICNT[64-53]=I32S[50-1];
      ICNT[64-45]=I32S[51-1];
      ICNT[64-37]=I32S[52-1];
      ICNT[64-29]=I32S[53-1];
      ICNT[64-21]=I32S[54-1];
      ICNT[64-13]=I32S[55-1];
      ICNT[64- 5]=I32S[56-1];
      ICNT[64-57]=I32S[57-1];
      ICNT[64-49]=I32S[58-1];
      ICNT[64-41]=I32S[59-1];
      ICNT[64-33]=I32S[60-1];
      ICNT[64-25]=I32S[61-1];
      ICNT[64-17]=I32S[62-1];
      ICNT[64- 9]=I32S[63-1];
      ICNT[64- 1]=I32S[64-1];
}

