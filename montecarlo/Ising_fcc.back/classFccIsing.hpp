#include <iostream>
#include <fstream>
#include "mtrand64.hpp"
#include "Bitwise_multispincoding.h"
#include "classBfTableFcc64.hpp"

#ifndef CLASS_FCC_ISING_H_
#define CLASS_FCC_ISING_H_

using namespace std;

class classFccIsing {
public:
  classFccIsing();
  ~classFccIsing();
  void Update();
  double AveragedOrderParameter();
  //doulbe Energy();
  void Initialize( long lx, long ly, long lz, double specK, unsigned long long sed,
		     int bit_width, unsigned long long bf_seed);
  //Set parameters and allocate memory and set all-up configuration
  void AllUpConfiguration();//set all up configuration
private:
  void SpinMemoryAllocation(long lx, long ly, long lz);
  //allocation of spin. If sizes are unchanged, this don't reallocate memory.
  void TableMemoryAllocation(double spec_K, int bit_width, unsigned long long bf_seed);
  //allocation of bf table. If K,bit_width,bf_seed are unchanged, this don't reallocate.
  long Lx, Ly, Lz, LxLyLz, numAllocation; //system size
  int tableWidthPow; //Width of bf table is 2^(tableWidthPow);
  double K;//inverse temperature
  unsigned long long seedOfRandomNumber, seedOfBfTable;//seeds of random number
  mtrand64 rnd;//random number generator
  Bitwise_multispincoding bwmsc; //bit counting class
  classBfTableFcc bfTable; // Boltzman factor table
  long * random; //random[LxLyLz]; random[i] is used in UpdateSub
  long long * spinA, * spinB, *spinC, *spinD;//spin
  void UpdateSub( long long* us, long long* nnxy, long long *nnyz, long long* nnzx);
  //sub routine of "Update()". Arguments are pointer of update spin, nearest-neighbor spin
  //which has youngest number in xy,yz,zx plane.
  void Plus(long long a, long long b, long long c, long long &d, long long &e);
  void Plus(long long a, long long b, long long c, long long &d);
  void ConfigurationOut(int i);//Ouput configuratin of i-bit's sample
};
  
#endif
