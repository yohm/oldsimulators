#include "classTri3PottsMulti.hpp"
using namespace std;

//-------------------
classTri3PottsMulti::classTri3PottsMulti() {
  //適当な初期値を設定
  Lx=1;Ly=0; K=0.1;
  numAllocation = 0;
  spinA1 = spinA2 = spinB1 = spinB2 = NULL;
  rnd = NULL;
  seedOfRandomNumber = 0;
}

//------------------------
classTri3PottsMulti::~classTri3PottsMulti() {
  delete [] spinA1;
  delete [] spinA2;
  delete [] spinB1;
  delete [] spinB2;
  delete [] rnd;
}

//------------------------
void classTri3PottsMulti::Initialize(long lx, long ly, double k,
				     unsigned long long sed,
				     int bit_width,
				     unsigned long long bf_seed) {
  MemoryAllocation(lx,ly,sed);
  bfTab.Initialize(bit_width,k,bf_seed);
  seedOfBfTable = bf_seed; randomBitWidth = bit_width; K = k;
  InitialConfiguration();

}

//------------------------
void classTri3PottsMulti::MemoryAllocation(long lx, long ly,
					   unsigned long long sed) {
  if( (lx%2) == 1) {
    cerr << "Error! in classTri3PottsMulti."<<endl<<"lx must be even."<<endl;
    exit(1);
  }
  if( (ly%2) == 1) {
    cerr << "Error! in classTri3PottsMulti."<<endl<<"ly must be even."<<endl;
    exit(1);
  }
  long newAllocation = lx*(ly/2+1)+1;
  if( newAllocation != numAllocation) {
    delete [] spinA1;
    delete [] spinA2;
    spinA1 = new long long [newAllocation];
    spinA2 = new long long [newAllocation];
    delete [] spinB1;
    delete [] spinB2;
    spinB1 = new long long [newAllocation];
    spinB2 = new long long [newAllocation];
  }
  Lx = lx; Ly = ly/2; LxLy = Lx*Ly; numAllocation = newAllocation;

  delete rnd;
  rnd = new mtrand64(sed);
  seedOfRandomNumber = sed;
  bwmsc.Bitwise_multispincoding_Initialization(LxLy);
}
//-----------------------------
void classTri3PottsMulti::InitialConfiguration() {
  //set all 2 configuration
  for( long i=0; i<numAllocation; i++) {
    spinA1[i] = 0LL;
    spinA2[i] = -1LL;
    spinB1[i] = 0LL;
    spinB2[i] = -1LL;
  }
}
//----------------------------
void classTri3PottsMulti::Update() {
  //update odd spinA
  for( long i=Lx+1; i<Lx*(Ly+1); i+=2) {
    UpdateSubA(i);
  }
  //copy spin
  CopySubA();
  //update even spinA
  for( long i=Lx; i<Lx*(Ly+1); i+=2) {
    UpdateSubA(i);
  }
  //copy spin
  CopySubA();
  //update odd spinB
  for( long i=1; i<Lx*Ly+1; i+=2) {
    UpdateSubB(i);
  }
  //copy spin
  CopySubB();
  //update even spinB
  for( long i=2; i<Lx*Ly+1; i+=2) {
    UpdateSubB(i);
  }
  //copy spin
  CopySubB();
}
//---------------------------
inline void classTri3PottsMulti::UpdateSubA( const long& i) {
  //select new state
  long long r = rnd->genrand64_int64();
  long long us1 = ~spinA1[i] & r;
  long long us2 = (~spinA2[i]) & (~r);
  //calc IE
  long long b1 = (spinA1[i+1] ^ spinA1[i]) | (spinA2[i+1] ^ spinA2[i]);//そろっていなかったらtrue
  long long b2 = (spinA1[i-1] ^ spinA1[i]) | (spinA2[i-1] ^ spinA2[i]);
  long long b3 = (spinB1[i] ^ spinA1[i]) | (spinB2[i] ^ spinA2[i]);
  long long b4 = (spinB1[i-Lx+1] ^ spinA1[i]) | (spinB2[i-Lx+1] ^ spinA2[i]);
  long long b5 = (spinB1[i+1] ^ spinA1[i]) | (spinB2[i+1] ^ spinA2[i]);
  long long b6 = (spinB1[i-Lx+2] ^ spinA1[i]) | (spinB2[i-Lx+2] ^ spinA2[i]);
  long long c1 = (~spinA1[i+1] ^ us1) & (~spinA2[i+1] ^ us2);
  long long c2 = (~spinA1[i-1] ^ us1) & (~spinA2[i-1] ^ us2);
  long long c3 = (~spinB1[i] ^ us1) & (~spinB2[i] ^ us2);
  long long c4 = (~spinB1[i-Lx+1] ^ us1) & (~spinB2[i-Lx+1] ^ us2);
  long long c5 = (~spinB1[i+1] ^ us1) & (~spinB2[i+1] ^ us2);
  long long c6 = (~spinB1[i-Lx+2] ^ us1) & (~spinB2[i-Lx+2] ^ us2);
  //calc IX;
  long random = rnd->genrand64_bit(randomBitWidth);
  long long ix4 = bfTab.GetTableElement(3*random);
  long long ix2 = bfTab.GetTableElement(3*random+1);
  long long ix1 = bfTab.GetTableElement(3*random+2);
  //IE+IX(r)
  long long ID = CalcID( ix4,ix2,ix1,b1,b2,b3,b4,b5,b6,c1,c2,c3,c4,c5,c6);
  //update to new state
  spinA1[i] = (ID & us1) | (~ID & spinA1[i]);
  spinA2[i] = (ID & us2) | (~ID & spinA2[i]);
}
//---------------------------
inline void classTri3PottsMulti::UpdateSubB( const long& i) {
  //select new state
  long long r = rnd->genrand64_int64();
  long long us1 = ~spinB1[i] & r;
  long long us2 = (~spinB2[i]) & (~r);
  //calc IE
  long long b1 = (spinB1[i+1] ^ spinB1[i]) | (spinB2[i+1] ^ spinB2[i]);//そろっていなかったらtrue
  long long b2 = (spinB1[i-1] ^ spinB1[i]) | (spinB2[i-1] ^ spinB2[i]);
  long long b3 = (spinA1[i-1] ^ spinB1[i]) | (spinA2[i-1] ^ spinB2[i]);
  long long b4 = (spinA1[i+Lx-1] ^ spinB1[i]) | (spinA2[i+Lx-1] ^ spinB2[i]);
  long long b5 = (spinA1[i] ^ spinB1[i]) | (spinA2[i] ^ spinB2[i]);
  long long b6 = (spinA1[i+Lx-2] ^ spinB1[i]) | (spinA2[i+Lx-2] ^ spinB2[i]);
  long long c1 = (~spinB1[i+1] ^ us1) & (~spinB2[i+1] ^ us2);
  long long c2 = (~spinB1[i-1] ^ us1) & (~spinB2[i-1] ^ us2);
  long long c3 = (~spinA1[i-1] ^ us1) & (~spinA2[i-1] ^ us2);
  long long c4 = (~spinA1[i+Lx-1] ^ us1) & (~spinA2[i+Lx-1] ^ us2);
  long long c5 = (~spinA1[i] ^ us1) & (~spinA2[i] ^ us2);
  long long c6 = (~spinA1[i+Lx-2] ^ us1) & (~spinA2[i+Lx-2] ^ us2);
  //calc IX;
  long random = rnd->genrand64_bit(randomBitWidth);
  long long ix4 = bfTab.GetTableElement(3*random);
  long long ix2 = bfTab.GetTableElement(3*random+1);
  long long ix1 = bfTab.GetTableElement(3*random+2);
  //IE+IX(r)
  long long ID = CalcID( ix4,ix2,ix1,b1,b2,b3,b4,b5,b6,c1,c2,c3,c4,c5,c6);
  //update to new state
  spinB1[i] = (ID & us1) | (~ID & spinB1[i]);
  spinB2[i] = (ID & us2) | (~ID & spinB2[i]);
}
//------------------------------
inline long long classTri3PottsMulti::CalcID(const long long& ix4,const long long& ix2,const long long& ix1,
					     const long long& b1,const long long& b2,const long long& b3, const long long& b4, const long long& b5, const long long& b6,
					     const long long& c1,const long long& c2,const long long& c3, const long long& c4, const long long& c5, const long long& c6)
{
  long long d2,d1,e2,e1,f2,f1,g2,g1,h2,h1,i2,j4,j2,k4,k2,l4,l2;
  Plus(b1,b2,b3,d2,d1);
  Plus(b4,b5,b6,e2,e1);
  Plus(c1,c2,c3,f2,f1);
  Plus(c4,c5,c6,g2,g1);
  Plus(ix1,d1,e1,h2,h1);
  Plus(f1,g1,h1,i2);

  Plus(ix2,d2,e2,j4,j2);
  Plus(f2,g2,h2,k4,k2);
  Plus(i2,j2,k2,l4,l2);
  
  long long m2,m1,n2;
  Plus(ix4,j4,k4,m2,m1);
  Plus(l4,l2,m1,n2);
  long long id = m2 | n2;
  return id;
}
//------------------------------
inline void classTri3PottsMulti::CopySubA() {
  for( long i=0; i<Lx; i++) {
    spinA1[i] = spinA1[i+LxLy];
    spinA2[i] = spinA2[i+LxLy];
  }
  spinA1[Lx*(Ly+1)] = spinA1[Lx];
  spinA2[Lx*(Ly+1)] = spinA2[Lx];
}
//------------------------------
inline void classTri3PottsMulti::CopySubB() {
  for( long i=1; i<Lx+1; i++) {
    spinB1[i+LxLy] = spinB1[i];
    spinB2[i+LxLy] = spinB2[i];
  }
  spinB1[0] = spinB1[Lx*Ly];
  spinB2[0] = spinB2[Lx*Ly];
}
//------------------------------
inline void classTri3PottsMulti::Plus(const long long& a,const long long& b,const long long& c,
				       long long &d, long long& e){
  //<d,e> = a+b+c
  e = a ^ b;
  d = (a&b) | (e&c); // second order
  e = e ^ c; // e+c; first order
}
inline void classTri3PottsMulti::Plus(const long long& a, const long long& b, const long long& c,
				       long long & d) {
  //<d,*> = a+b+c;
  d = (a&b) | ( (b^a)&c); //second order
}
//------------------------------
double classTri3PottsMulti::AveragedOrderParameter() {
  long imagA[64];
  long imagB[64];
  bwmsc.NIbitcount( spinA2, LxLy, imagA);
  bwmsc.NIbitcount( spinB2, LxLy, imagB);
  double total = 0;
  for(int i=0; i<64; i++) {
    total += (double)(imagA[i]+imagB[i])/(double)(2*LxLy)*1.5-0.5;
  }
  return total/64.0;
}
//------------------------------
void classTri3PottsMulti::ConfigurationOutput(int i) {
  if( i<0 || i>63) {
    cerr << "Error! in classTri3PottsMulti::ConfigurationOut()" << endl;
    exit(1);
  }
  unsigned long long mask = 1 << i;
  for( long j=0; j<Ly; j++) {
    for( long i=0; i<Lx; i++) {
      short temp = 0;
      if( spinA1[j*Lx+i] & mask) temp += 1;
      if( spinA2[j*Lx+i] & mask) temp += 2;
      cout << temp;
    }
    cout << endl;
    for( long i=0; i<Lx; i++) {
      short temp = 0;
      if( spinA1[j*Lx+i] & mask) temp += 1;
      if( spinA2[j*Lx+i] & mask) temp += 2;
      cout << temp;
    }
    cout << endl;
  }
}
