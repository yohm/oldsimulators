#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"
#include "classBfTableTri3Potts.hpp"
#include "Bitwise_multispincoding.h"
using namespace std;

#ifndef POTTS3TRIMULTI_H_
#define POTTS3TRIMULTI_H_

class classTri3PottsMulti {
public:
  classTri3PottsMulti();
  ~classTri3PottsMulti();
  void Update();
  double AveragedOrderParameter();
  double Energy() { return 0;}
  void Initialize(long lx, long ly, double specK, unsigned long long sed, int bit_wd, unsigned long long bf_sed);
  void Initialize(long lx, long ly, double specK,
		  unsigned long long sed, int bit_wd) {
    Initialize(lx,ly,specK,sed,bit_wd,sed+12345);
  }
  void InitialConfiguration();//set all 2 configuration
  void ConfigurationOutput(int i);
private:
  long Lx,Ly,LxLy,numAllocation;
  double K;
  unsigned long long seedOfRandomNumber,seedOfBfTable;
  int randomBitWidth;
  long long *spinA1,*spinA2, *spinB1,*spinB2; //state = spin1*1+spin2*2 (0,1,2)
  mtrand64* rnd;
  classBfTableTri3Potts bfTab;
  Bitwise_multispincoding bwmsc;
  void MemoryAllocation(long lx, long ly, unsigned long long sed);
  inline void UpdateSubA( const long & i);
  inline void CopySubA();
  inline void UpdateSubB( const long & i);
  inline void CopySubB();
  inline long long CalcID(const long long & ix4,const long long & ix2,const long long & ix1,
			  const long long & b1,const long long & b2,const long long & b3, const long long & b4, const long long & b5, const long long & b6,
			  const long long & c1,const long long & c2,const long long & c3, const long long & c4, const long long & c5, const long long & c6);
  void Plus(const long long&a,const long long&b,const long long&c,long long&d,long long&e);
  void Plus(const long long&a,const long long&b,const long long&c,long long&d);
};
#endif
