#include "class2dIsingDemonRandom.hpp"
using namespace std;
//----------------------
class2dIsingDemonRandom::class2dIsingDemonRandom() {
  Lpow = L = N = demon = 0;
  spin = NULL; rnd = NULL;
}
//----------------------
class2dIsingDemonRandom::~class2dIsingDemonRandom() {
  delete [] spin; delete rnd;
}
//----------------------
void class2dIsingDemonRandom::Initialize(int Lpower, double EOverEc,
					 unsigned long long sed) {
  Lpow = Lpower;
  Npow = Lpow * 2;
  L = 1 << Lpow;
  N = L*L;
  spin = new bool[N];
  seed = sed;
  rnd = new mtrand64(seed);
  for( long i=0; i<N; i++) spin[i] = true;
  E_Ec = EOverEc;
  Etotal = (long) ((2.0-sqrt(2.0))*N*E_Ec/4.0) ;//unit energy is 4J
  demon = Etotal;
  for( int i=0; i<20; i++) ene_dist[i] = 0;
  count_flag = false;
}
//----------------------
void class2dIsingDemonRandom::Update() {
  for( long i=0; i<N; i++) {
    long us = rnd->genrand64_bit( Npow);
    long after = demon + EnergyGain(us);
    if( after >=0) {
      spin[us] = !(spin[us]);
      demon = after;
    }
    if( count_flag ) { ene_dist[demon]++;}
  }
}
//-----------------------
inline long class2dIsingDemonRandom::EnergyGain(long spinid) {
  long next = spinid + 1;
  long previous = spinid -1;
  long spinBottom = spinid-L;
  long spinTop = spinid+L;
  if( spinid < L) {
    spinBottom += N;
  }
  else if( spinid > N-L-1) {
    spinTop -= N;
  }
  if( (spinid % L) == 0 ) {
    previous += L;
  }
  else if( (spinid % L) == L-1 ) {
    next -= L;
  }
  long deltaE = -2;
  if(spin[spinid]^spin[next]) deltaE++;
  if(spin[spinid]^spin[previous]) deltaE++;
  if(spin[spinid]^spin[spinTop]) deltaE++;
  if(spin[spinid]^spin[spinBottom]) deltaE++;
  return deltaE;
}
//-----------------------
// inline long class2dIsingDemonRandom::EnergyGain(long spinid) {
//   long Next = spinid + 1;
//   long Previous = spinid -1;
//   long spinBottom = spinid-L;
//   long spinTop = spinid+L;
//   if( spinid < L) {
//     spinBottom += N;
//     if( spinid == 0) Previous = N-1;
//   }
//   else if( spinid > N-L-1) {
//     spinTop -= N;
//     if( spinid == N-1) Next = 0;
//   }
//   long deltaE = -2;
//   if(spin[spinid]^spin[Next]) deltaE++;
//   if(spin[spinid]^spin[Previous]) deltaE++;
//   if(spin[spinid]^spin[spinTop]) deltaE++;
//   if(spin[spinid]^spin[spinBottom]) deltaE++;
//   return deltaE;
// }
//-----------------------
double class2dIsingDemonRandom::OrderParameter() {
  int mag=0;
  for(int i=0; i<N; i++) {
    if(spin[i]) mag++;
    else mag--;
  }
  double temp = (double)mag/(double)N;
  return temp;
}
//-----------------------
double class2dIsingDemonRandom::Energy() {
  return -N/2+(Etotal - demon);
}

//-----------------------
void class2dIsingDemonRandom::OutputConfiguration(const char* filename) {
  std::ofstream fout( filename);
  for( int i=0; i<L; i++) {
    for( int j=0; j<L; j++) fout << spin[i*L+j];
    fout << std::endl;
  }
  fout.close();
}

//-----------------------
void class2dIsingDemonRandom::EneDistCountStart() {
  for( int i=0; i<20; i++) ene_dist[i] = 0;
  count_flag = true;
}

//-----------------------
void class2dIsingDemonRandom::EneDistCountOutput(const char* filename) {
  if( count_flag ) {
    std::ofstream fout(filename);
    for( int i=0; i<20; i++) fout << i << ' ' << ene_dist[i] << std::endl;
    fout.close();
    count_flag = false;
  }
}
