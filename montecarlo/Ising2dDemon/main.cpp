#include <iostream>
#include <ctime>
#include <fstream>
#include "class2dIsingDemonRandom.cpp"
#include "mtrand64.cpp"

//#define BENCH

int main(int argc, char* argv[] ) {
  if(argc!=6) {
    std::cerr << "Error! Invalid number of arguments." << std::endl;
    std::cerr << "<program> <power_of_L> <E/Ec> <tmax> <seed> <data_id>"<< std::endl;
    exit(1);
  }

  int lpow = atoi( argv[1] );
  double e_ec = atof( argv[2] );
  long tmax = atol( argv[3]);
  unsigned long long sed = atol( argv[4]);
  char str[50];
  strcpy( str,argv[5]);
  strcat( str, ".NERo");
  std::ofstream magout( str);
  magout.precision(10);

  class2dIsingDemonRandom sim;
  sim.Initialize( lpow, e_ec, sed);

#ifdef BENCH
  clock_t start,end;
  start = clock();
#endif
  
  magout << 0 << ' ' << sim.OrderParameter() << std::endl;
  for( int t=1; t<tmax; t++) {
    sim.Update();
    magout << t <<' '<<sim.OrderParameter() << std::endl;
  }
  //  sim.EneDistCountStart(); sim.Update(); sim.EneDistCountOutput("dist.dat");
  //  sim.OutputConfiguration("conf.dat");
#ifdef BENCH
  end = clock();
  cerr << "time: "<<(double)(end-start)/(CLOCKS_PER_SEC) << endl;
#endif

  return 0;
}
