#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"

//using namespace std;
//coupling constant is J/4. demon take 0,1,2,3,4,...

class class2dIsingDemonRandom {
public:
  class2dIsingDemonRandom();
  ~class2dIsingDemonRandom();
  void Initialize(int Lpower, double EOverEc,unsigned long long sed);
  void Update(); //update ranodmly chosen N sites
  double OrderParameter();
  double Energy();
  void OutputConfiguration(const char* filename);
  void EneDistCountStart();
  void EneDistCountOutput(const char* filename);
private:
  short Lpow, Npow;
  long L,N;
  double E_Ec;
  long Etotal;
  bool * spin;
  long demon;
  mtrand64 *rnd;
  unsigned long long seed; //seed of random number;
  long EnergyGain( long spinid);
  unsigned long ene_dist[20]; //distribution of demon energy
  bool count_flag; //if count_flag = true; count energy distribution when updating
};
