#include <iostream>
#include <fstream>
#include <cmath>
#include "mtrand64.hpp"
#include "classBfTable2d3Potts.hpp"
#include "Bitwise_multispincoding.h"
using namespace std;

#ifndef POTTS32DMULTI_H_
#define POTTS32DMULTI_H_

class class2d3PottsMulti {
public:
  class2d3PottsMulti();
  ~class2d3PottsMulti();
  void Update();
  double AveragedOrderParameter();
  double Energy(){return 0;}
  void Initialize(long lx, long ly, double specK, unsigned long long sed,
		  int bit_width, unsigned long long bf_seed);
  //Set parameters and allocate memory and set all-2 configuration
  void ConfigurationOutput(int i);//i-bit目のConfiguration
  void InitialConfiguration();//set all 2 configuration
private:
  long Lx,Ly,LxLy,numAllocation;
  double K;//inverse temperature
  unsigned long long seedOfRandomNumber, seedOfBfTable;
  int randomBitWidth;
  long long *spin1,*spin2; //state = spin1*1+spin2*2 (0,1,2)
  mtrand64* rnd;
  classBfTable2d3Potts bfTab;
  Bitwise_multispincoding bwmsc;
  void MemoryAllocation(long lx, long ly, unsigned long long sed);
  inline void UpdateSub( const long & i);
  inline void CopySub();
  void Plus(const long long&a,const long long&b,const long long&c,long long&d,long long&e);
  void Plus(const long long&a,const long long&b,const long long&c,long long&d);
};
#endif
