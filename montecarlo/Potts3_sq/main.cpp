#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>
//#include "class2d3PottsSimple.cpp"
//#include "class2d3PottsSimple2.cpp"
//#include "class2d3PottsSimple3.cpp"
#include "mtrand64.cpp"
#include "Bitwise_multispincoding.cpp"
#include "classBfTable2d3Potts.cpp"
#include "class2d3PottsMulti.cpp"

using namespace std;

#define BENCH

//input the arguments as follows
// program Lx Ly k Tmax seed bitlength data_id
// ./a.out 299 300 1.005 128 23419721 16 00002
int main(int argc, char* argv[]) {
  if(argc!=8) {
    cerr << "Error! Wrong number of arguments." << endl;
    cerr << "<program> <Lx> <Ly> <k> <tmax> <seed> <bit_length> <data_id>"<<endl;
    exit(1);
  }


  long T = atoi( argv[4]);
  long lx=atoi( argv[1]);
  long ly=atoi( argv[2]);
  double k = atof( argv[3]);
  unsigned long long sed = atol( argv[5]);
  int bitwt = atoi( argv[6]);
  unsigned long long bf_sed = sed + 72137ULL;
  char str[20];
  strcpy( str,argv[7]);
  strcat( str, ".NERo");
  
  class2d3PottsMulti ptm;
  ptm.Initialize(lx,ly,k,sed,bitwt,bf_sed);

  ofstream fout(str);
  fout.precision(10);

#ifdef BENCH
  clock_t start = clock();
#endif
  for( int t=0; t<T; t++) {
    fout << t << " " << ptm.AveragedOrderParameter() <<endl;
    ptm.Update();
  }
#ifdef BENCH
  clock_t end = clock();
  cout << "time:"<<(double)(end-start)/(CLOCKS_PER_SEC)<<endl;
#endif
  fout.close();
  return 0;
}
