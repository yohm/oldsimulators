# created by Yohsuke Murase


##
## select compiler
##

CXX = c++
#CXX = g++-4.2
#CXX = g++ -m64
#CXX = icpc
#CXX = xlc++
#CXX = sCC

CC = cc
#CC = gcc -m64
#CC = icc
#CC = xlc

##
## select MPI compiler
##

MPICXX = mpicxx
#MPICXX = mpiCC
#MPICXX = mpicxx.gcc

##
## compile option
##

OPT = -O3 -Wall
#OPT = -O3 -ip -ipo -fno-alias
#OPT = -O3 -fopenmp -finline-functions -Wall
#OPT = -O5
#OPT = -Os -nolimit -noscope -loglist -I/opt/STLport-4.6.2/stlport -L/opt/STLport-4.6.2/lib64 -lstlport_sCC -lm
#OPT = -Os -noparallel -nolimit -noscope -I/opt/STLport-4.6.2/stlport -L/opt/STLport-4.6.2/lib64 -lstlport_sCC -lm
#OPT =  -Os -nolimit -noscope -loglist -I/batch/h02008/boost_1_34_1 -I/opt/STLport-4.6.2/stlport -L/opt/STLport-4.6.2/lib64 -lstlport_sCC -lm

##
## MPI compile option
##

#MPIOPT = $(OPT)
MPIOPT = -cxx=g++-4.2 $(OPT)
#MPIOPT = -O3 -Wall
#MPIOPT = -Os -nolimit -noscope -loglist -I/opt/STLport-4.6.2/stlport -L/opt/STLport-4.6.2/lib64 -lstlport_sCC -lm
#MPIOPT = -Os -noparallel -nolimit -noscope -I/opt/STLport-4.6.2/stlport -L/opt/STLport-4.6.2/lib64 -lstlport_sCC -lm

all : mc_ising_bcc mc_ising_fcc mc_3potts_hex \
mc_3potts_sq mc_3potts_tri mc_ising_2d_alt \
mc_ising_2d_frust eco_inv05 eco_inv05_maxspecies \
eco_random_inv05 mc_ising_2d_demon mc_ising_2d_random_metro \
eco_si_invasion eco_epsilon_invasion eco_tangled_nature \
eco_tn_poisson eco_tn-ind-inv eco_tn-ind-mut eco_tn-map-inv \
eco_tn-map-mut eco_tn-noisedmap-mut eco_tn-a-noisedmap-mut \
eco_tn-b-pop-inv eco_tn-noisedmap-inv eco_tn-a-noisedmap-inv \
mc_random_8potts
	
mc_ising_bcc : 
	cd montecarlo/Ising_bcc/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_ising_fcc :
	cd montecarlo/Ising_fcc/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_3potts_hex :
	cd montecarlo/Potts3_hex/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_3potts_sq :
	cd montecarlo/Potts3_sq/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_3potts_tri :
	cd montecarlo/Potts3_tri/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_ising_2d_alt :
	cd montecarlo/Ising_2d_alt/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_ising_2d_frust :
	cd montecarlo/Ising_2d_frust_1_16/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_inv05 :
	cd ecosystem/invasion05/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_inv05_maxspecies :
	cd ecosystem/invasion05_maxspecies/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_random_inv05 :
	cd ecosystem/random_invasion05/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_ising_2d_demon :
	cd montecarlo/Ising2dDemon/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_ising_2d_random_metro :
	cd montecarlo/Ising_2d_random_metro/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_si_invasion :
	cd ecosystem/SImodel-invasion/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_epsilon_invasion :
	cd ecosystem/epsilon_invasion/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tangled_nature :
	cd ecosystem/tangled-nature-model/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn_poisson :
	cd ecosystem/tangled-nature-poisson/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-ind-inv :
	cd ecosystem/tn-ind-inv/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-ind-mut :
	cd ecosystem/tn-ind-mut/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-map-inv :
	cd ecosystem/tn-map-inv/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-map-mut :
	cd ecosystem/tn-map-mut/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-noisedmap-mut :
	cd ecosystem/tn-noisedmap-mut/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-a-noisedmap-mut :
	cd ecosystem/tn-a-noisedmap-mut/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-b-pop-inv : 
	cd ecosystem/tn-pop-inv/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-noisedmap-inv :
	cd ecosystem/tn-noisedmap-inv/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
eco_tn-a-noisedmap-inv :
	cd ecosystem/tn-a-noisedmap-inv/; make CXX=$(CXX) CC=$(CC) OPT=$(OPT)
mc_random_8potts :
	cd montecarlo/random-8potts/; make

clean :
	cd montecarlo/Ising_bcc/; make clean
	cd montecarlo/Ising_fcc/; make clean
	cd montecarlo/Potts3_hex/; make clean
	cd montecarlo/Potts3_sq/; make clean
	cd montecarlo/Potts3_tri/; make clean
	cd montecarlo/Ising_2d_alt/; make clean
	cd montecarlo/Ising_2d_frust_1_16/; make clean
	cd ecosystem/invasion05/; make clean
	cd ecosystem/invasion05_maxspecies/; make clean
	cd ecosystem/random_invasion05/; make clean
	cd montecarlo/Ising2dDemon/; make clean
	cd montecarlo/Ising_2d_random_metro/; make clean
	cd ecosystem/SImodel-invasion/; make clean
	cd ecosystem/epsilon_invasion/; make clean
	cd ecosystem/tangled-nature-model/; make clean
	cd ecosystem/tangled-nature-poisson/; make clean
	cd ecosystem/tn-ind-inv/; make clean
	cd ecosystem/tn-ind-mut/; make clean
	cd ecosystem/tn-map-inv/; make clean
	cd ecosystem/tn-map-mut/; make clean
	cd ecosystem/tn-noisedmap-mut/; make clean
	cd ecosystem/tn-a-noisedmap-mut/; make clean
	cd ecosystem/tn-pop-inv/; make clean
	cd ecosystem/tn-noisedmap-inv/; make clean
	cd ecosystem/tn-a-noisedmap-inv/; make clean
	cd montecarlo/random-8potts/; make clean
