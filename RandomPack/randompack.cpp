#ifdef OMP
#include <omp.h>
#endif

#include "randompack.hpp"
#include "mt19937ar.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <math.h>
#include <sstream>
#include <iomanip>
#include <string>
#define PI 3.1415926535897931
#define SQ3 1.7320508075688772
#define SQ2 1.4142135623731

using namespace std;

/**************** Constructer & Deconstructer  ********************/
randompack::randompack(int aN, double aLx, double aLy, double aLz,  int seed, double growth_rate){

#ifdef OMP
#pragma omp parallel
  {
    Thread = omp_get_num_threads(); //Thread number;
  }
	cout<<"Number of threads is "<<Thread<<endl;
  Thread_Array = new double[Thread];
  Thread_Array2 = new int[Thread*2];
#endif


  //set parameters
  total_time = 0;
  current_time=0;
  N = aN;
  Lx = aLx;
  Ly = aLy;
  Lz = aLz;
  hLx=Lx*0.5;
  hLy=Ly*0.5;
  hLz=Lz*0.5;
  dia = 2.0;
  idia = 1.0/dia;
  dia2 = dia*dia;
  grate=growth_rate;
  threshold = pow(10.0,-12);
  tc = 0;

  //grid information
  LxSQ3 = Lx*SQ3;
  LySQ3 = Ly*SQ3;
  LzSQ3 = Lz*SQ3;
  ngx = static_cast<int>(LxSQ3*idia) + 1;
  ngy = static_cast<int>(LySQ3*idia) + 1;
  ngz = static_cast<int>(LzSQ3*idia) + 1;
  Lgx = Lx/ngx;
  Lgy = Ly/ngy;
  Lgz = Lz/ngz;
  /*   
       if(Lgx<(Lgmin =(Lgy>Lgz?Lgz:Lgy))){
       Lgmin=Lgx;// minimum grid length
       }
  */
  Lgmin = Lgx;
  CLfree = Lx*Ly*Lz/(SQ2*N*PI);
  Lfree = CLfree/dia2;

  mask = static_cast<int>(Lfree/Lgmin + dia/Lgmin);

  //particles information
  p = new particle[N];
  nb = new neighbor[N];
  gr = new gridn[N];
  gc = new int[ngx*ngy*ngz];

  //initialize grid cell
  for(int i=0;i<ngx*ngy*ngz;i++){
    gc[i]=-1;
  }

  init_genrand(seed);  //initialize Mersenne Twister
  position_init();
  velocity_init();

  // sort
  int target,grid,num=0;
  for(int i=0;i<ngx*ngy*ngz;i++){
    target = gc[i];
    if(target ==-1)continue;
    SWAPP(p[target],p[num]);
    grid =gr[num].nx+ngx*gr[num].ny+ngx*ngy*gr[num].nz;
    SWAPG(gr[target],gr[num]);

    gc[i] = num;
    gc[grid]= target;

    num++;
  }


  //set grid
  getNeighborList();
}

randompack::randompack(string DATA_FILE,int seed, int init,double grate, double rad){

#ifdef OMP
#pragma omp parallel
  {
    Thread = omp_get_num_threads(); //Thread number;
  }
	cout<<"Number of threads is "<<Thread<<endl;
  Thread_Array = new double[Thread];
  Thread_Array2 = new int[Thread*2];
#endif


	getHeader(DATA_FILE,init,grate,rad);
  threshold = pow(10.0,-12);
  tc = 0;

  //particles information
  p = new particle[N];
  nb = new neighbor[N];
  gr = new gridn[N];
  gc = new int[ngx*ngy*ngz];

  //initialize grid cell
  for(int i=0;i<ngx*ngy*ngz;i++){
    gc[i]=-1;
  }

  init_genrand(seed);  //initialize Mersenne Twister


}


randompack::~randompack(){
  delete [] p;
  delete [] nb;
  delete [] gr;
  delete [] gc;
}

/**************** Initialization   ********************/

/**
 *  Initialize randompack positions
 */
void randompack::position_init(){
  for(int i=0; i<N; i++){
    do{
      p[i].x = Lx* genrand_real2();
      p[i].y = Ly* genrand_real2();
      p[i].z = Lz* genrand_real2();
      gr[i].nx =static_cast<int>( p[i].x/Lgx)%ngx;
      gr[i].ny =static_cast<int>( p[i].y/Lgy)%ngy;
      gr[i].nz =static_cast<int>( p[i].z/Lgz)%ngz;
    }while(reject(i));
    gc[gr[i].nx+ngx*gr[i].ny+ngx*ngy*gr[i].nz] = i;// registory particle number in grid
  }
}


/**
 *  Initialize randompack velocities
 */
void randompack::velocity_init(){
  //double rand1,rand2;
  double v, vmax=0;
  for(int i=0; i<N; i++){
    //Boltzmann distribution
    p[i].vx = boltzmann() ;
    p[i].vy = boltzmann() ;
    p[i].vz = boltzmann();
    v = sqrt(pow(p[i].vx,2)+pow(p[i].vy,2)+pow(p[i].vz,2));
    if(v>vmax)vmax=v;
  }
  tcmin = Lgmin/vmax - dia/(2.0*vmax); //grid refresh time  ( shortest collision time with outside particle of grid mask)
}


/**
 *  Check whether a particle position reject or accept
 *  @parameter i    particle number
 *  @return  particle reject-> false ,  accept -> true
 */
bool randompack::reject(int target){
  int mx,my,mz,pair;
  double dx,dy,dz,dis=0;
  for(int k=-2;k<=2;k++){
    mz = (gr[target].nz + k+ ngz)%ngz;
    for(int j=-2;j<=2;j++){
      my = (gr[target].ny + j+ ngy)%ngy;
      for(int i=-2;i<=2;i++){
	mx = (gr[target].nx + i+ ngx)%ngx;
	pair = gc[mx + my*ngx + mz*ngx*ngy];
	if(pair==-1)continue;
	dx = p[target].x - p[pair].x; if(fabs(dx)>hLx){dx>0?dx-=Lx:dx+=Lx;}
	dy = p[target].y - p[pair].y; if(fabs(dy)>hLy){dy>0?dy-=Ly:dy+=Ly;}
	dz = p[target].z - p[pair].z; if(fabs(dz)>hLz){dz>0?dz-=Lz:dz+=Lz;}
	dis = pow(dx,2) + pow(dy,2) + pow(dz,2);
	if(dis <dia2 - threshold) return true; //reject
      }
    }
  }
  return false; // not reject
}


/**************** Main function   ********************/

/**
 *  Set ahead to next time
 *  @parameter blow_enable   blow up particle-> true,  not -> false
 */
void randompack::stepTime(){
  double dia_old;
  int target1=0,target2=0;
  /**   1. Translate   **/
  getCollisionTime(target1,target2);
  tc=tc*0.5; //half of collision time

  if(tc>threshold){  //threshold
    setNextPosition(tc);
    current_time+=tc;
    total_time+=tc;
  }
  /**   2. Expand  **/
  dia_old = dia;  // keep old diameter
  while(!Expand(target1,target2)){
    //Exception
    cout<<"Overlap"<<endl;
    dia = dia_old;
    idia = 1.0/dia;
    dia2 = dia*dia;
    tc=tc*0.5;
    setNextPosition(-tc);  // back to the little past
    current_time-=tc;
    total_time-=tc;
  }
  //Set new velocities if connected particles should collition
  setCollisionVelocity(target1,target2);

  /**   other.  Refresh grid **/
  if(current_time>tcmin){
    resetGrid();
    getNeighborList();
    current_time=0;
  }

  /**   3. Collision **/
  getCollisionTime(target1,target2);

  if(tc>threshold){
    setNextPosition(tc);
    current_time+=tc;
    total_time+=tc;
  }
  //Set new velocities
  setNextVelocity(target1,target2);
}


/**************** Calculate Collision  ********************/
/**
 *  Calculate collision time
 */
inline void randompack::getCollisionTime(int &target1, int &target2){
#pragma omp parallel 
  {
    double tmin=1000,t;// relative to time
    double dx,dy,dz,dvx,dvy,dvz,b,dr2,dv2;
    double root,alpha,alpha2;
    int pair,t1=0,t2=0;
    int Thread_num;
#pragma omp for schedule(static)
    for(int i=0;i<N;i++){
#ifdef OMP
      Thread_num = omp_get_thread_num();
#endif
      for(int j=0;j<nb[i].nnl;j++){
	dx = p[i].x; dy = p[i].y; dz = p[i].z; dvx = p[i].vx; dvy = p[i].vy; dvz = p[i].vz;
	pair=nb[i].nl[j];            
	dx -= p[pair].x; dy -= p[pair].y; dz -= p[pair].z; dvx -= p[pair].vx; dvy -= p[pair].vy; dvz -= p[pair].vz;
	if(fabs(dx)>hLx){dx>0?dx-=Lx:dx+=Lx;}
	if(fabs(dy)>hLy){dy>0?dy-=Ly:dy+=Ly;}
	if(fabs(dz)>hLz){dz>0?dz-=Lz:dz+=Lz;}
	dr2 =dx*dx + dy*dy + dz*dz;
	b = dx*dvx + dy*dvy + dz*dvz;
	dv2 = dvx*dvx + dvy*dvy + dvz*dvz;

	if(b>0)continue;
	root = (b*b- dv2*dr2) + dv2*dia2 ;

	if(root < 0 )continue;
	alpha = -b/dv2;
	alpha2 = alpha + sqrt(root)/dv2;
	t= 2.0*alpha - alpha2;

	if(t>tmin)continue;
	tmin=t;
	t1 = i;
	t2 = pair;

      }
#ifdef OMP
      Thread_Array[Thread_num]=tmin;
      Thread_Array2[(Thread_num<<1)] = t1;
      Thread_Array2[(Thread_num<<1)+1]=t2;
#endif    	
    }
#ifdef OMP	
  }
  tc = Thread_Array[0];
  target1 = Thread_Array2[0];
  target2 = Thread_Array2[1];
  for(int i=1;i<Thread;i++){
    if(Thread_Array[i] < tc){
      tc = Thread_Array[i];
      target1 = Thread_Array2[(i<<1)];
      target2 = Thread_Array2[(i<<1)+1];
    }
  }
#else

  //cout<<tc<<endl;

  target1 =t1;
  target2 = t2;
  tc = tmin;
}
#endif
}


/**
 *  New velocities after collision
 *  @parameter i   collision target 1
 *  @parameter j   collision target 2
 */
inline void randompack::setNextVelocity(int target1, int target2){
  double dx,dy,dz,dvx,dvy,dvz,c,cx,cy,cz,dr2;
  dx = p[target2].x; dy = p[target2].y; dz = p[target2].z; dvx = p[target2].vx; dvy = p[target2].vy; dvz = p[target2].vz;
  dx -= p[target1].x; dy -= p[target1].y; dz -= p[target1].z; dvx -= p[target1].vx; dvy -= p[target1].vy; dvz -= p[target1].vz;
  if(fabs(dx)>hLx){dx>0?dx-=Lx:dx+=Lx;}
  if(fabs(dy)>hLy){dy>0?dy-=Ly:dy+=Ly;}
  if(fabs(dz)>hLz){dz>0?dz-=Lz:dz+=Lz;}
  dr2 = dx*dx + dy*dy + dz*dz;

  if(dr2< dia2 - threshold){
    //cout<<"Overlap"<<endl;
    tc=tc*0.5;
    setNextPosition(-tc);  // back to the little past
    current_time-=tc;
    total_time-=tc;
  }

  c = (dx*dvx + dy*dvy + dz*dvz)/dr2;
  cx = c*dx; cy = c*dy; cz = c*dz;
  p[target1].vx += cx;
  p[target1].vy += cy;
  p[target1].vz += cz;
  p[target2].vx -= cx;
  p[target2].vy -= cy;
  p[target2].vz -= cz;
}

/**
 *  New velocities after expansion
 *  @parameter i   collision target 1
 *  @parameter j   collision target 2
 */
void randompack::setCollisionVelocity(int target1, int target2){
  double dx,dy,dz,dvx,dvy,dvz,c,cx,cy,cz,dr2,dv2,b;
  dx = p[target2].x; dy = p[target2].y; dz = p[target2].z; dvx = p[target2].vx; dvy = p[target2].vy; dvz = p[target2].vz;
  dx -= p[target1].x; dy -= p[target1].y; dz -= p[target1].z; dvx -= p[target1].vx; dvy -= p[target1].vy; dvz -= p[target1].vz;
  if(fabs(dx)>hLx){dx>0?dx-=Lx:dx+=Lx;}
  if(fabs(dy)>hLy){dy>0?dy-=Ly:dy+=Ly;}
  if(fabs(dz)>hLz){dz>0?dz-=Lz:dz+=Lz;}
  dr2 = dx*dx + dy*dy + dz*dz;

  if(dr2< dia2 - threshold){
    //cout<<"Overlap"<<endl;
    tc=tc*0.5;
    setNextPosition(-tc);  // back to the little past
    current_time-=tc;
    total_time-=tc;
  }

  dv2 = dvx*dvx + dvy*dvy + dvz*dvz;
  b = dx*dvx + dy*dvy + dz*dvz;
  if(b>0 || (b*b + dv2*dia2< dv2*dr2 ))return;
  c = b/dr2;
  cx = c*dx; cy = c*dy; cz = c*dz;
  p[target1].vx += cx;
  p[target1].vy += cy;
  p[target1].vz += cz;
  p[target2].vx -= cx;
  p[target2].vy -= cy;
  p[target2].vz -= cz;
}




/**
 *  Set next positions for randompack
 */
void randompack::setNextPosition(double time){
#pragma omp parallel
  {
#pragma omp for schedule(static)
    for(int i=0;i<N;i+=2){
      p[i].x += p[i].vx*time;
      p[i].y += p[i].vy*time;
      p[i].z += p[i].vz*time;
      p[i+1].x += p[i+1].vx*time;
      p[i+1].y += p[i+1].vy*time;
      p[i+1].z += p[i+1].vz*time;
      if(p[i].x>Lx){p[i].x-=Lx;}else if(p[i].x<0){p[i].x+=Lx;}
      if(p[i].y>Ly){p[i].y-=Ly;}else if(p[i].y<0){p[i].y+=Ly;}
      if(p[i].z>Lz){p[i].z-=Lz;}else if(p[i].z<0){p[i].z+=Lz;}
      if(p[i+1].x>Lx){p[i+1].x-=Lx;}else if(p[i+1].x<0){p[i+1].x+=Lx;}
      if(p[i+1].y>Ly){p[i+1].y-=Ly;}else if(p[i+1].y<0){p[i+1].y+=Ly;}
      if(p[i+1].z>Lz){p[i+1].z-=Lz;}else if(p[i+1].z<0){p[i+1].z+=Lz;}
    }
  }
}


/**
 *  Reset Grid
 */
void randompack::resetGrid(){
  double vmax=0,v;
  int Thread_num;
  int ngxy=ngx*ngy; 

  //reset grid data
#pragma omp  parallel for schedule(static)
  for(int i=0;i<N;i++){
    gc[gr[i].nx+ngx*gr[i].ny+ngxy*gr[i].nz]=-1;// initialize grid cell
  }

  //create new grid cell
  ngx = static_cast<int>(LxSQ3*idia) + 1;
  ngy = static_cast<int>(LySQ3*idia) + 1;
  ngz = static_cast<int>(LzSQ3*idia) + 1;
  ngxy = ngx*ngy;
  Lgx = Lx/ngx;
  Lgy = Ly/ngy;
  Lgz = Lz/ngz;
  /*    if(Lgx<(Lgmin =(Lgy>Lgz?Lgz:Lgy))){
	Lgmin=Lgx;// minimum grid length
	}
  */
  Lgmin = Lgx;

  //set new grid information
#pragma omp parallel for schedule(static) private(v,vmax,Thread_num)
  for(int i=0; i<N; i++){

#ifdef OMP
    Thread_num = omp_get_thread_num();
#endif
    gr[i].nx =static_cast<int>( p[i].x/Lgx)%ngx;
    gr[i].ny =static_cast<int>( p[i].y/Lgy)%ngy;
    gr[i].nz =static_cast<int>( p[i].z/Lgz)%ngz;
    gc[gr[i].nx+ngx*gr[i].ny+ngxy*gr[i].nz] = i;//registory randompack into grid
    v =sqrt(p[i].vx*p[i].vx + p[i].vy*p[i].vy + p[i].vz*p[i].vz);

    if(v >vmax ){
      vmax = v;
#ifdef OMP
      Thread_Array[Thread_num] = vmax;
#endif
    }
  }

#ifdef OMP
  vmax = Thread_Array[0];
  for(int i=1;i<Thread;i++){
    if(Thread_Array[i] > vmax)
      vmax = Thread_Array[i];
  }
#endif
  tcmin = (Lgmin - dia*0.5)/vmax;
  Lfree = CLfree/dia2;
  mask = static_cast<int>(Lfree+dia/Lgmin);

}


/**
 *  Expand spheres
 */
bool randompack::Expand(int &target1,int &target2){
#pragma omp parallel
  {
    particle target, pair;
    neighbor tnl;
    double drc=1000,dr;//relative to radius
    double dx,dy,dz;
    int t1=0,t2=0,neighbors,pair_num;
    int Thread_num;
#pragma omp for schedule(static)
    for(int i=0;i<N;i++){
#ifdef OMP
      Thread_num = omp_get_thread_num();
#endif
      target = p[i];
      tnl = nb[i];
      neighbors = tnl.nnl;
      for(int j=0;j<neighbors;j++){
	dx = target.x; dy = target.y; dz = target.z;
	pair_num = tnl.nl[j];
	pair = p[pair_num];
	dx -= pair.x; dy -= pair.y; dz -= pair.z;
	if(fabs(dx)>hLx){dx>0?dx-=Lx:dx+=Lx;}
	if(fabs(dy)>hLy){dy>0?dy-=Ly:dy+=Ly;}
	if(fabs(dz)>hLz){dz>0?dz-=Lz:dz+=Lz;}
	dr = dx*dx+dy*dy+dz*dz;

	if(dr<drc){
	  drc =  dr;//search minimum radius which is possible to blow up
	  t1 = i;
	  t2 = pair_num;
	}
      }
#ifdef OMP
      Thread_Array[Thread_num] = drc;
      Thread_Array2[(Thread_num<<1)] = t1;
      Thread_Array2[(Thread_num<<1)+1]=t2;
#endif
    }

#ifdef OMP	
  }
  double drc = Thread_Array[0];
  target1 = Thread_Array2[0];
  target2 = Thread_Array2[1];
  for(int i=1;i<Thread;i++){
    if(Thread_Array[i] < drc){
      drc = Thread_Array[i];
      target1 = Thread_Array2[(i<<1)];
      target2 = Thread_Array2[(i<<1)+1];
    }
  }
  dia = sqrt(drc)*grate + dia - grate*dia;
#else
  dia = sqrt(drc)*grate + dia - grate*dia;
  target1 = t1;
  target2 = t2;
}
#endif
idia = 1.0/dia;
dia2 = dia*dia;
return true;
}

/**
 * Get Neighbor List
 */
void randompack::getNeighborList(){
  int mx,my,mz,num,pair,ngxy=ngx*ngy;
#pragma omp parallel for schedule(static) private(mx,my,mz,num,pair)
  for(int m=0;m<N;m++){
    num=0;
    for(int k=-mask;k<=mask;k++){
      mz = (gr[m].nz + k+ ngz)%ngz;
      for(int j=-mask;j<=mask;j++){
	my = (gr[m].ny + j+ ngy)%ngy;
	for(int i=-mask;i<=mask;i++){
	  mx = (gr[m].nx + i+ ngx)%ngx;
	  if(i!=0 || j!=0 || k!=0){
	    pair = gc[mx + my*ngx + mz*ngxy];
	    if(pair!=-1)nb[m].nl[num++] =pair;
	  }
	}
      }
    }
    nb[m].nnl =num;
  }
}



/**
 *  Create rondom number under the Boltzmann distribution
 *  @parameter m   average
 *  @parameter sigma  dispersion
 *  @return  rundom number
 */
double randompack::boltzmann(){
  double x, y,r1,r2;
  x=1.0-genrand_real2();
  y=1.0-genrand_real2();
  r1=sqrt(-2.0*log(x))*sin(2.0*PI*y);
  r2=sqrt(-2.0*log(x))*cos(2.0*PI*y);
  return r1;
}


/**************** File I/O stream **********************/

string randompack::getFileName(string  dir,int number){
  ostringstream oss;
  string name;

  //oss <<dir<<"/RP"<<id<<"/RP"<<id<<setw( 5 ) << setfill( '0' ) << number <<".lcd";// create file name
  oss <<"RP"<<dir<<"."<<setw( 5 ) << setfill( '0' ) << number <<".lcd";// create file name
  name =(string ) oss.str().c_str();

  return name;
}


/**
 *  Get header information
 *  @parameter dir random_packing data file direcory
 *  @parameter id ID number
 *  @parameter start_no start file number
 *  @parameter end_no end file number
 */
void randompack::getHeader(string rp_file,int init, double  agrate, double arad){
    ifstream input_file,exists;

    string dummy,str_Lx,str_Ly,str_Lz,str_N,str_r,str_time,str_grate;
    string filename;

		filename = rp_file;
    cout << "Initialize using "<<filename<<endl;
    input_file.open(filename.c_str(),ios::in);
    exists.close();
    
    //Set system parameters
    input_file >> dummy>>dummy>>dummy>>str_Lx>>str_Ly>>str_Lz>> dummy;
    input_file >> str_N>>str_r>> str_time>>dummy>>str_grate;
    Lx =  atof(str_Lx.substr(str_Lx.find("=")+1,str_Lx.find(",")-str_Lx.find("=")-1).c_str());
    Ly =  atof(str_Ly.substr(str_Ly.find("=")+1,str_Ly.find(",")-str_Ly.find("=")-1).c_str());
    Lz =  atof(str_Lz.substr(str_Lz.find("=")+1,str_Lz.find(",")-str_Lz.find("=")-1).c_str());
    N =  atoi(str_N.substr(str_N.find("=")+1,str_N.find(",")-str_N.find("=")-1).c_str());
    dia = (atof(str_r.substr(str_r.find("=")+1,str_r.find(",")-str_r.find("=")-1).c_str()))*2.0;
    idia = 1.0/dia;
    dia2 = dia*dia;
    total_time = atof(str_time.substr(str_time.find("=")+1,str_time.find(",")-str_time.find("=")-1).c_str());
    grate = atof(str_grate.substr(str_grate.find("=")+1,str_grate.find(",")-str_grate.find("=")-1).c_str());
    input_file.close();

		if(init==1){
			total_time=0;
			if(agrate>=0){
			 grate = agrate;
			}
			if(arad>0){
			 dia = arad*2.0;
  		 idia = 1.0/dia;
  		 dia2 = dia*dia;
			}
		}
	
		 //grid information
	  LxSQ3 = Lx*SQ3;
	  LySQ3 = Ly*SQ3;
	  LzSQ3 = Lz*SQ3;
	  ngx = static_cast<int>(LxSQ3*idia) + 1;
	  ngy = static_cast<int>(LySQ3*idia) + 1;
	  ngz = static_cast<int>(LzSQ3*idia) + 1;
	  Lgx = Lx/ngx;
	  Lgy = Ly/ngy;
	  Lgz = Lz/ngz;
	  hLx = Lx/2.0;
    hLy = Ly/2.0;
    hLz = Lz/2.0;

	  Lgmin = Lgx;
	  CLfree = Lx*Ly*Lz/(SQ2*N*PI);
	  Lfree = CLfree/dia2;

	  mask = static_cast<int>(Lfree/Lgmin + dia/Lgmin);
    
}


/**
  * Get Random Packing File Name
 *  @parameter dir random_packing data file direcory
 *  @parameter id ID number
 *  @parameter number file number
  */
void randompack::getParticleInfo(string rp_file,int init){
    FILE *input_file;
	string filename;


		//filename = getHTFileName(dir, id,number);
    filename = rp_file;
    char dummy[400];
    char sx[20],sy[20],sz[20],svx[20],svy[20],svz[20];

    memset(gc,-1,sizeof(int)*ngx*ngy*ngz);

    input_file = fopen(filename.c_str(),"r");// open file

    if( input_file == NULL ){
        //if file does not exists, return message
        cout << "Error: cannot open "<<filename<<endl;
        return;
    }

    //skip header information
    fgets(dummy,400,input_file);
    fgets(dummy,400,input_file);

    double vmax=0,v;
    double a1,a2,c1,c2;

    for(int i=0;i<N;i++){
        fscanf(input_file,"%s %s %s %s %s %s %s %s",dummy,dummy,sx,sy,sz,svx,svy,svz);
        p[i].x=atof(sx);
        p[i].y=atof(sy);
        p[i].z=atof(sz);
        p[i].vx=atof(svx);
        p[i].vy=atof(svy);
        p[i].vz=atof(svz);

        if(init==1){
            //temperature profile
            a1=1.0-genrand_real2();
            a2=1.0-genrand_real2();
            c1 = sqrt(-2.0*log(a1));
            c2 = 2.0*PI*a2;
            p[i].vx = c1*sin(c2);
            p[i].vy = c1*cos(c2);
            a1=1.0-genrand_real2();
            a2=1.0-genrand_real2();
            c1 = sqrt(-2.0*log(a1));
            c2 = 2.0*PI*a2;
            p[i].vz = c1*sin(c2);
        }

        v = p[i].vx*p[i].vx+p[i].vy*p[i].vy+p[i].vz*p[i].vz;
        if(v>vmax)vmax=v;

        gr[i].nx =static_cast<int>( p[i].x/Lgx)%ngx;
        gr[i].ny =static_cast<int>( p[i].y/Lgy)%ngy;
        gr[i].nz =static_cast<int>( p[i].z/Lgz)%ngz;
        gc[gr[i].nx+ngx*gr[i].ny+ngx*ngy*gr[i].nz] = i;// registory particle number in grid
    }

    fclose(input_file);

    int target,grid,num=0;

    for(int i=0;i<ngx*ngy*ngz;i++){
        target = gc[i];
        if(target ==-1)continue;
        SWAPP(p[target],p[num]);
        grid =gr[num].nx+ngx*gr[num].ny+ngx*ngy*gr[num].nz;
        SWAPG(gr[target],gr[num]);

        gc[i] = num;
        gc[grid]= target;

        num++;
    }

    getNeighborList();

}


/**
 *  Initialize particle positions & velocity from file
 */
/*void randompack::read_file( string dir, int id, int number, int v_init,int seed){
  ifstream input_file;
  FILE *input_file2;

  string dummy,str_r,str_time;
  string filename = getFileName(dir, id,number);

  char dummy2[400];
  char sx[20],sy[20],sz[20],svx[20],svy[20],svz[20];

  /////Read Header//////////
  input_file.open(filename.c_str(),ios::in);// open file

  if( !input_file ){
    //if file does not exists, return message
    cout << "Error: cannot open "<<filename<<endl;
    exit(0);
  }

  //Set system parameters
  input_file >> dummy>>dummy>>dummy>>dummy>>dummy>>dummy>> dummy;
  input_file >> dummy>>str_r>> str_time>> dummy>>dummy;
  dia = (atof(str_r.substr(str_r.find("=")+1,str_r.find(",")-str_r.find("=")-1).c_str()))*2.0;
  idia = 1.0/dia;
  dia2 = dia*dia;
  total_time = atof(str_time.substr(str_time.find("=")+1,str_time.find(",")-str_time.find("=")-1).c_str());
  input_file.close();

  //initialize grid cell
  for(int i=0;i<ngx*ngy*ngz;i++){
    gc[i]=-1;
  }

  //set grid
  ngx = static_cast<int>(LxSQ3*idia) + 1;
  ngy = static_cast<int>(LySQ3*idia) + 1;
  ngz = static_cast<int>(LzSQ3*idia) + 1;
  Lgx = Lx/ngx;
  Lgy = Ly/ngy;
  Lgz = Lz/ngz;
  if(Lgx<(Lgmin =(Lgy>Lgz?Lgz:Lgy))){
    Lgmin=Lgx;// minimum grid length
  }

  ////Read Particles/////////
  input_file2 = fopen(filename.c_str(),"r");// open file

  //skip header information
  fgets(dummy2,400,input_file2);
  fgets(dummy2,400,input_file2);

  for(int i=0;i<N;i++){
    fscanf(input_file2,"%s %s %s %s %s %s %s %s",dummy2,dummy2,sx,sy,sz,svx,svy,svz);
    p[i].x=atof(sx);
    p[i].y=atof(sy);
    p[i].z=atof(sz);
    gr[i].nx =static_cast<int>( p[i].x/Lgx)%ngx;
    gr[i].ny =static_cast<int>( p[i].y/Lgy)%ngy;
    gr[i].nz =static_cast<int>( p[i].z/Lgz)%ngz;
    p[i].vx=atof(svx);
    p[i].vy=atof(svy);
    p[i].vz=atof(svz);
    gc[gr[i].nx+ngx*gr[i].ny+ngx*ngy*gr[i].nz] = i;// registory particle number in grid
    //cout<<dummy2<<"  "<<p[i].x<<"  "<<p[i].y<<"  "<<p[i].z<<"  "<<p[i].vx<<"  "<<p[i].vy<<"  "<<p[i].vz<<endl;
  }

  fclose(input_file2);

  // velocity initialize if v_ini ==1
  if(v_init==1){
    init_genrand(seed);  //initialize Mersenne Twister
    velocity_init();
  }

  // sort
  int target,grid,num=0;
  for(int i=0;i<ngx*ngy*ngz;i++){
    target = gc[i];
    if(target ==-1)continue;
    SWAPP(p[target],p[num]);
    grid =gr[num].nx+ngx*gr[num].ny+ngx*ngy*gr[num].nz;
    SWAPG(gr[target],gr[num]);

    gc[i] = num;
    gc[grid]= target;

    num++;
  }

  //set grid
  getNeighborList();

}

*/


/**
 *  Output results
 */
void randompack::outConfig(string  dir, int number, int time){
  ofstream out_file;

  //if initial condition was loaded from file, initial state does not save
  string filename = getFileName(dir,number+time);
  cout<<"Result File "<<filename<<" is written."<<endl;
  out_file.open(filename.c_str());// open file

  out_file <<"#box_sx=0, box_sy=0, box_sz=0";
  out_file <<", box_ex="<< setprecision(15)<< fixed<<Lx;
  out_file <<", box_ey="<< setprecision(15)<< fixed<<Ly;
  out_file <<", box_ez="<< setprecision(15)<< fixed<<Lz;
  out_file <<", box_wt=0.01";
  out_file <<",r0="<< setprecision(15)<<fixed<<dia*5.0/7.0;
  out_file <<",r1="<< setprecision(15)<< fixed<<dia*5.0/7.0<<endl;

  out_file <<"#particles="<<N;
  out_file <<", r="<< setprecision(15)<< fixed<<dia/2.0;
  out_file <<", simulation_time="<< setprecision(15)<< fixed<<total_time;
  out_file <<", packing_rate="<< setprecision(15)<< fixed<<((3.141592/6.0*pow(dia,3)*N)/(Lx*Ly*Lz))/0.5236;
  out_file <<", growth_rate="<< setprecision(15)<< fixed<<grate<<endl;

  for(int i=0;i<N;i++){
    //set data to file
    out_file<< setw(5)<< right<< i;
    out_file<< setw(5)<< right<< 0;
    out_file<< setw(20)<< setprecision(15)<< right<< fixed<< p[i].x;
    out_file<< setw(20)<< setprecision(15)<< right<< fixed<< p[i].y;
    out_file<< setw(20)<< setprecision(15)<< right<< fixed<< p[i].z;
    out_file<< setw(20)<< setprecision(15)<< right<< fixed<< p[i].vx;
    out_file<< setw(20)<< setprecision(15)<< right<< fixed<< p[i].vy;
    out_file<< setw(20)<< setprecision(15)<< right<< fixed<< p[i].vz;
    out_file<<endl;
  }
  out_file.close();

}

