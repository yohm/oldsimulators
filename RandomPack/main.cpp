/*
  Compile Options

  -DBound    : Define "Bound".
                random_zbound.cpp file is needed as a compile argument  
  -DOMP      : Define "OMP". number of threads sets as env OMP_NUM_THREADS.
                enable parallelization.
             
 */

#include <iostream>

//#define Bound

#if defined Bound
    #include "randompack_zbound.hpp"
#else
    #include "randompack.hpp"
#endif

using namespace std;

int main(int argc, char* argv[])
{
    /**  Check inputs **/
    if(argc!=11) {
		cerr << "Error! Wrong number of arguments. You should input following two types;" << endl;
		cerr << "<type> <DATA_DIR> <N> <Lx> <Ly> <Lz> <seed> <growth_rate> <global_time> <loop_time>" << endl;
    cerr << "<type> <DATA_DIR> <DATA_FILE> <number> <seed> <global_time> <loop_time> <initialize?> <groth_rate> <radius>" << endl;	
		exit(1);
	}

	/**  Set parameters **/
	int type = atoi(argv[1]);
	string DATA_DIR,DATA_FILE;
	int number=0,N,seed,global_time,loop_time,init;
	double Lx,Ly,Lz,growth_rate,rad;
	if(type==1){
		DATA_DIR= argv[2];
		N = atoi ( argv[3]);  //particle total number
		Lx = atof( argv[4]);  //size of x direction
		Ly = atof ( argv[5]);  //size of y direction
		Lz = atof ( argv[6]); //size of z direction
		seed = atoi ( argv[7]);  //random seed
		growth_rate = atof ( argv[8]);  //random seed
		global_time = atoi( argv[9]); //simulation global time ( file number)
	  loop_time = atoi ( argv[10]);  //simulation loop time( before write file)
	}else if(type==2){
 	 	DATA_DIR= argv[2];
		DATA_FILE= argv[3];
		number=atoi(argv[4]);
		seed = atoi(argv[5]);
		global_time = atoi(argv[6]);
		loop_time = atoi( argv[7]);
		init = atoi(argv[8]);
		growth_rate = atof(argv[9]);
		rad = atof(argv[10]);
		if(init==1)number=0;
	}
	
    /**  Initialize **/
#if defined Bound
    randompack_zbound rp(N, Lx, Ly, Lz, seed,growth_rate);
#else
	randompack *rp;
	if(type==1){
		rp = new randompack(N, Lx, Ly, Lz, seed,growth_rate);
		rp->outConfig(DATA_DIR,number,0);
	}
	if(type==2){
		rp = new randompack(DATA_FILE,seed,init,growth_rate,rad);
		rp->getParticleInfo(DATA_FILE,init);
		if(init==1)rp->outConfig(DATA_DIR,number,0);
	}
#endif
    for(int time=1; time<global_time;time++){
        for(int j=0;j<loop_time && time < global_time-1 ;j++){
            rp->stepTime();
        }
        rp->outConfig(DATA_DIR,number,time);
    }

	return 0;
}
