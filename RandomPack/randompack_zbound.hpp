#include <iostream>
#ifndef _ini_randompack_zbound_
#define _ini_randompack_zbound_


#define SWAPP(a,b) tempp=(a);(a)=(b);(b)=tempp
#define SWAPG(a,b) tempg=(a);(a)=(b);(b)=tempg

using namespace std;

class randompack_zbound{

public:
  //parameters
  int N;      //particle total number
  double rad,dia,dia2,idia; //diameter
  double grate;//growth rate
  double Lx,Ly,Lz,hLx,hLy,hLz,LxSQ3,LySQ3,LzSQ3; //system size
  int ngx,ngy,ngz;//grid cell number
  double Lgx,Lgy,Lgz,Lgmin,Lfree,CLfree;//grid cell size
  double tc,tcmin,current_time,total_time;//collision time

#ifdef OMP
  //MPI�p
  int Thread;
  double *Thread_Array;
  int  *Thread_Array2;
#endif

  struct particle{
    double x,y,z;
    double vx,vy,vz;
  } *p,tempp;

  struct neighbor{
    int nnl;
    int nl[100];
  } *nb;

  struct gridn{
    int nx,ny,nz;
  } *gr, tempg;

  //particle information
  int *gc; //grid cell
  int mask; //grid mask

  /**************** Constructer & Deconstructer ********************/
  randompack_zbound(int, double,double,double, int, double);
  ~randompack_zbound();
  /**************** Main function   ********************/
  void stepTime();//Set ahead to next time
  /*************** File I/O stream *******************/
  string getFileName(string ,int,int); //get filen name
  void read_file(string ,int,int,int,int); //read particle information from file
  void result_file(string ,int,int,int); //write result file

private:
  double threshold, threshold_time;
  /**************** Calculate Collision  ********************/
  void getCollisionTime(int & , int &);// Calculate collision time
  void setNextVelocity(int, int);//New velocities after collision
  void setCollisionVelocity(int,int);//New velocities after expand
  void setNextPosition(double);// Set next positions for randompack
  void resetGrid();//Reset Grid
  void Expand(int &,int &);//expand spheres

  /**************** Initialization   ********************/
  void position_init();  //initialize coordinator
  void velocity_init() ; //initialize velocity
  bool reject(int);//particle reject or accept in the grid(second int ) for particle(first int)
  void getNeighborList(); //get neighbor list
  double boltzmann(); //Create rondom number under the Boltzmann distribution

};

#endif
